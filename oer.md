---
geometry: "top=25mm, bottom=25mm, left=25mm, right=25mm"
urlcolor: blue
linkcolor: blue
---

# InnovationPlus presented in Twillo

This document describes the results of the "Innovation Plus" project conducated by the [Software Engineering Group](https://uol.de/en/se) of the University of Oldenburg, Germany, in 2020 and 2021.
This project is supported by [Niedersächsisches Ministerium für Wissenschaft und Kultur (Ministry for Science and Culture of Lower Saxony)](https://www.mwk.niedersachsen.de) under grant number 27 - 73724 / 16 - 4 (2019).
This document is used to represent InnovationPlus in [Twillo](https://www.twillo.de) (formerly known as OER), an initiative to share resources and materials for academic teaching.

## About InnovationPlus

To support the self-study of students in computing science, this project InnovationPlus aims to develop a platform which provides feedback for modeling and programming tasks automatically. 
Independently from manual feedback given by lecturers and tutors, the platform allows to submit solutions for modeling and programming tasks by students
and generates hints based on registered teacher solutions regarding errors, missing content or tips for improvement. 
This helps students to learn with additional tasks and to get feedback for their solutions on-the-fly.
The lecturers can benefit from generated statistics regarding the analyzed solutions of students.


## Resources

* Website: [https://uol.de/se?innoplus](https://uol.de/se?innoplus) (mainly in german)
* User Guides: Lecture 'Programming, Data Structures, and Algorithms' [https://uol.de/se?pdf-innovationplus-pda](https://uol.de/se?pdf-innovationplus-pda), Lecture 'Object-oriented Modeling and Programming' [https://uol.de/se?pdf-innovationplus-omp](https://uol.de/se?pdf-innovationplus-omp), Lecture 'Software Engineering 1' [https://uol.de/se?pdf-innovationplus-st1](https://uol.de/se?pdf-innovationplus-st1) (in german)
* Slide set for Introduction of InnovationPlus and the tooling: [https://gitlab.uni-oldenburg.de/se/projects/innoplus/innoplus-prototype/-/tree/master/slides](https://gitlab.uni-oldenburg.de/se/projects/innoplus/innoplus-prototype/-/tree/master/slides) (in german)
* Questionnaire to improve InnovationPlus: [https://umfragen.uni-oldenburg.de/index.php?r=survey/index&sid=376876](https://umfragen.uni-oldenburg.de/index.php?r=survey/index&sid=376876) (in german)
* Sourcecode: [https://gitlab.uni-oldenburg.de/se/projects/innoplus/innoplus-prototype](https://gitlab.uni-oldenburg.de/se/projects/innoplus/innoplus-prototype)
* UpdateSite: [http://sehome.informatik.uni-oldenburg.de/software/InnoPlusUpdateSite](http://sehome.informatik.uni-oldenburg.de/software/InnoPlusUpdateSite) (ask for credentials) 


## Contact

For more information, questions, ideas or found bugs, please write a mail to
[innoplus-reports@se.uol.de](mailto:innoplus-reports@se.uol.de).


## Licence of this Document

![(graphic of CC BY 4.0)](https://i.creativecommons.org/l/by/4.0/88x31.png "CC BY 4.0")
This document by [Johannes Meier](https://uol.de/se?meier), Paul Holt and Florian Brandt is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) (Creative Commons Attribution 4.0 International License).

Please note, that other resources developed in InnovationPlus (linked by this document) might use other licences.
