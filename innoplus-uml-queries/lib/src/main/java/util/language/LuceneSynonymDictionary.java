package util.language;

import org.apache.lucene.analysis.core.FlattenGraphFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.synonym.SynonymGraphFilter;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.text.ParseException;
import java.util.Objects;

/** Abstract {@link Dictionary} that wraps Lucene and uses some other knowledge base **/
public abstract class LuceneSynonymDictionary implements Dictionary {

	/** Internal list of synonyms **/
	private final SynonymMap map;

	/** Constructs a new instance by parsing the specified file **/
	public LuceneSynonymDictionary(Reader path) throws IOException, ParseException {
		this.map = parse(path);
	}

	protected abstract SynonymMap parse(Reader path) throws IOException, ParseException;

	protected final SynonymMap getInternalMap() {
		return this.map;
	}

	// Matches nameStudent to nameTeacher using the internal map
	@Override
	public boolean areEquivalent(String nameStudent, String nameTeacher) {
		// Try exact match to avoid the overhead below
		if (Objects.equals(nameStudent, nameTeacher))
			return true;

		// Only one null => missmatch
		if (nameTeacher == null || nameStudent == null)
			return false;

		// Actually query Lucene's data, potentually matching (multi-word) synonyms
		try (
			var tokens = new StandardTokenizer();
			var sgf = new SynonymGraphFilter(tokens, map, true);
			var fgf = new FlattenGraphFilter(sgf);
		) {
			tokens.setReader(new StringReader(nameTeacher));
			tokens.reset();

			var attr = fgf.addAttribute(CharTermAttribute.class);
			var found = false;

			while (!found && fgf.incrementToken()) {
				var syn = attr.toString();
				// System.out.print(" - ");
				// System.out.println(syn);
				found = syn.equalsIgnoreCase(nameStudent);
			}
			fgf.end();

			return found;
		} catch (IOException e) {
			throw new RuntimeException("Internal failure", e);
		}
	}
}
