package util.language;

import org.eclipse.uml2.uml.NamedElement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

/**
 * Utility class to match model elements using an internal dictionary.
 *
 * @see Dictionary
 **/
public final class NameMatcher {

	public static final NameMatcher instance;

	static {
		var cl = NameMatcher.class.getClassLoader();
		try (var is = cl.getResourceAsStream("openthesaurus.txt")) {
			var reader = new BufferedReader(new InputStreamReader(is));
			var dict = new OpenThesaurusDictionary(reader);
			instance = new NameMatcher(dict);
		} catch (IOException | ParseException e) {
			throw new RuntimeException("Failed to initialize NameMatcher instance");
		}
	}

	/** Dictionary for actual string matching **/
	private final Dictionary dict;

	/** Creates a new matcher that uses the supplied dictionary **/
	public NameMatcher(Dictionary dict) {
		super();
		this.dict = dict;
	}

	/**
	 * Checks if both element names are equivalent under the rules of the internal
	 * dictionary. Respects metadata associated with the supplied elements.
	 *
	 * @param teacherEl the expected element
	 * @param studentEl the actual element
	 * @return whether studentEl matches teacherEl according to the internal
	 *         {@link Dictionary dictionary}.
	 **/
	public boolean areEquivalent(NamedElement teacherEl, NamedElement studentEl) {
		var studentName = studentEl.getName();
		var teacherName = teacherEl.getName();

		// Consider two unnamed elements as equivalent
		if (studentName == null)
			return teacherName == null;

		if (teacherName == null)
			return false;

		return dict.areEquivalent(teacherName, studentName);
	}


	/** Returns the internal dictionary **/
	public Dictionary getDictionary() {
		return dict;
	}
}
