package util.language;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.synonym.WordnetSynonymParser;

import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;

/** {@link Dictionary} wrapper for Lucene's {@link WordnetSynonymParser} **/
public class LuceneWordnetDictionary extends LuceneSynonymDictionary {

	public LuceneWordnetDictionary(Reader reader) throws IOException, ParseException {
		super(reader);
	}

	/** Parses the file by forwarding to {@link WordnetSynonymParser} **/
	protected SynonymMap parse(Reader reader) throws IOException, ParseException {

		var analyzer = new StandardAnalyzer();
		var parser = new WordnetSynonymParser(true, true, analyzer);
		parser.parse(reader);
		return parser.build();
	}
}
