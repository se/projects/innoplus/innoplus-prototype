package util.language;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.util.CharsRef;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;

/** {@link Dictionary} wrapper for OpenThesaurus using Lucene **/
public class OpenThesaurusDictionary extends LuceneSynonymDictionary {

	public OpenThesaurusDictionary(Reader reader) throws IOException, ParseException {
		super(reader);
	}

	protected SynonymMap parse(Reader reader) throws IOException, ParseException {

		var analyzer = new StandardAnalyzer();
		var parser = new ThesaurusParser(analyzer);
		parser.parse(reader);
		return parser.build();
	}

	private static final class ThesaurusParser extends SynonymMap.Parser {

		public ThesaurusParser(Analyzer analyzer) {
			super(true, analyzer);
		}

		@Override
		public void parse(Reader in) throws IOException, ParseException {
			var br = in instanceof BufferedReader ? (BufferedReader) in : new BufferedReader(in);

			String line;

			// Skip comments
			while ((line = br.readLine()).charAt(0) == '#') {
			}

			do {
				var arr = line.toCharArray();
				var end = findEnd(line, 0);
				var word = parseEntry(line, arr, 0, end);
//				System.out.println(word);

				for (int start; (start = end + 1) < arr.length;) {

					end = findEnd(line, start);
					var synonym = parseEntry(line, arr, start, end);
//					System.out.println(" - " + synonym);
					this.add(word, synonym, true);
					this.add(synonym, word, true);
				}

			} while ((line = br.readLine()) != null);

		}

		private static CharsRef parseEntry(final String line, final char[] arr, final int startIdx, final int endIdx) {
			int start = startIdx;
			int end = endIdx;

			while (line.charAt(start) == '(') {
				start = line.indexOf(')', start) + 2;
			}

			while (line.charAt(end - 1) == ')') {
				end = line.lastIndexOf('(', end) - 1;
			}

			var len = end - start;

			if (len <= 0)
				return new CharsRef(line.substring(startIdx + 1, endIdx - 1).toLowerCase());

			for (int i = start; i < end; i++)
				arr[i] = Character.toLowerCase(arr[i]);

			return new CharsRef(arr, start, len);
		}

		private static int findEnd(String line, int cur) {
			int escape = 0;
			int len = line.length();

			while (cur < len) {
				switch (line.charAt(cur)) {
				case '(':
					escape++;
					break;

				case ')':
					escape--;
					break;
				case ';':
					if (escape == 0)
						return cur;
				}
				cur++;
			}

			return len;
		}
	}
}
