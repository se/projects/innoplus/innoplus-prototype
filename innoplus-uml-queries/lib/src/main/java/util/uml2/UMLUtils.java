package util.uml2;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.*;
import org.eclipse.uml2.uml.util.UMLUtil;
import util.language.Dictionary;
import util.language.NameMatcher;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UMLUtils {

    /** private dictionary instance to check if strings are equivalent. */
    private static Dictionary dictionary = NameMatcher.instance.getDictionary();

    public static Optional<Class> findClass(Model model, String name){
        var clazz = model.getPackagedElements()
                .stream()
                .filter(Class.class::isInstance)
                .filter(element -> dictionary.areEquivalent(element.getName(), name))
                .map(Class.class::cast)
                .findFirst();
        return clazz;
    }

    public static List<Association> findAssociations(Model model, Optional<Class> first, Optional<Class> second){
        if(first.isEmpty() || second.isEmpty())
            return Collections.emptyList();
        return model.getPackagedElements()
                .stream()
                .filter(Association.class::isInstance)
                .map(Association.class::cast)
                .filter(association -> hasAssociation(first, association) && hasAssociation(second, association))
                .collect(Collectors.toList());
    }

    private static boolean hasAssociation(Optional<Class> clazz, Association association){
        if (clazz.isEmpty())
            return false;
        return clazz.get().getAssociations()
                .stream()
                .anyMatch(asso -> asso.equals(association));
    }

    /**
     * Searches for a property for a given Class
     * @param clazz Optional of a {{@link Class}}
     * @param name The name of the Property
     * @return
     */
    public static Optional<Property> findProperty(Optional<Class> clazz, String name){
        if (clazz.isEmpty())
            return Optional.empty();

        var superClasses = clazz.get().getSuperClasses();
        if (!superClasses.isEmpty()){
            var prop = findProperty(Optional.of(superClasses.get(0)), name);
            if(prop.isPresent())
                return prop;
        }
        var ownedA = clazz.get().getOwnedAttributes();
        return clazz.get().getOwnedAttributes()
                .stream()
                .filter(prop -> dictionary.areEquivalent(prop.getName(), name))
                .findFirst();
    }

    public static Optional<Generalization> findGeneralization(Model model, Optional<Class> sup, Optional<Class> sub){
        if (sup.isEmpty() || sub.isEmpty())
            return Optional.empty();
        return model.getPackagedElements()
                .stream()
                .filter(Generalization.class::isInstance)
                .map(Generalization.class::cast)
                .filter(gen -> containsClass(gen.getSources(), sub.get()) && containsClass(gen.getTargets(), sup.get()))
                .findFirst();
    }

    private static boolean containsClass(EList<Element> elements, Class clazz){
        return elements
                .stream()
                .anyMatch(element -> element.equals(clazz));
    }

    public static Optional<Boolean> propertyIsString(Optional<Property> property) {
        if(property.isEmpty())
            return Optional.empty();
        return Optional.of(UMLUtil.isString(property.get().getType()));
    }

    public static Optional<Boolean> classHasSelfReference(Optional<Class> clazz) {
        if (clazz.isEmpty())
            return Optional.empty();

        return Optional.of(clazz.get().getAssociations()
                .stream()
                .anyMatch(asso -> asso.getMemberEnds()
                                .stream()
                                .allMatch(prop -> prop.getType().equals(clazz.get())))
        );
    }

    public static Optional<Boolean> classHasNoSelfReference(Optional<Class> clazz) {
        var result = classHasSelfReference(clazz);
        if (result.isEmpty())
            return result;
        return Optional.of(!result.get());
    }

    public static Optional<Boolean> isSubClass(Optional<Class> clazz, Optional<Class> superClass) {
        if (clazz.isEmpty() || superClass.isEmpty())
            return Optional.empty();
        var superClasses = clazz.get().getSuperClasses();
        if (superClasses.isEmpty())
            return Optional.of(false);
        if (superClasses.get(0).equals(superClass.get()))
            return Optional.of(true);
        return isSubClass(Optional.of(superClasses.get(0)), superClass);
    }

    public static Optional<Boolean> hasAggregation(Optional<Class> clazz, Optional<Class> aggregate) {
        if (clazz.isEmpty() || aggregate.isEmpty())
            return Optional.empty();
        return Optional.of(clazz.get().getAllAttributes()
                .stream()
                .filter(property -> property.getType().equals(aggregate.get()))
                .anyMatch(property -> property.upperBound() == -1)
        );
    }

    public static Optional<Boolean> hasNoProperty(Optional<Class> clazz, Optional<Class> prop) {
        if (clazz.isEmpty() || prop.isEmpty())
            return Optional.empty();
        return Optional.of(!hasAggregation(clazz, prop).get());
    }

    public static Optional<Boolean> propertyIsBoolean(Optional<Property> property) {
        if (property.isEmpty())
            return Optional.empty();
        return Optional.of(UMLUtil.isBoolean(property.get().getType()));
    }

    public static Optional<Boolean> hasCorrectLowerBound(Optional<Property> property, int lowerBound) {
        if (property.isEmpty())
            return Optional.empty();
        return Optional.of(property.get().lowerBound() == lowerBound);
    }


    public static Optional<Boolean> hasCorrectUpperBound(Optional<Property> property, int upperBound) {
        if (property.isEmpty())
            return Optional.empty();
        return Optional.of(property.get().upperBound() == upperBound);
    }

    public static Optional<Boolean> propertyIsComposite(Optional<Property> property) {
        if (property.isEmpty())
            return Optional.empty();
        return Optional.of(property.get().isComposite());
    }

    public static Optional<Boolean> propertyIsAggregate(Optional<Property> property) {
        if (property.isEmpty())
            return Optional.empty();
        return Optional.of(property.get().isComposite());
    }
}
