package util.evaluation;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class QueryResultChain {

    protected Optional<QueryResult> condition;
    protected ArrayList<QueryResult> results = new ArrayList<>();

    protected QueryResultChain(Optional<QueryResult> condition) {
        this.condition = condition;
    }

    public static QueryResultChain create() {
        return new QueryResultChain(Optional.empty());
    }

    public static QueryResultChain create(QueryResult condition) {
        return new QueryResultChain(Optional.ofNullable(condition));
    }

    public QueryResultChain add(QueryResult result) {
        results.add(result);
        return this;
    }

    public QueryResultChain add(QueryResultChain chain) {
        results.addAll(chain.getResults());
        return this;
    }

    public Optional<QueryResult> getCondition() {
        return condition;
    }

    public QueryResultChain setCondition(Optional<QueryResult> condition) {
        this.condition = condition;
        return this;
    }

    public List<QueryResult> getSuccessfulResults() {
        if (condition.isPresent() && condition.get().getType().equals(QueryResultType.ERROR)) {
            return Collections.emptyList();
        }
        final var res = new ArrayList<QueryResult>();
        condition.ifPresent(res::add);
        res.addAll(results.stream()
                .filter(r -> r.getType().equals(QueryResultType.SUCCESS))
                .collect(Collectors.toList()));
        return res;
    }

    public List<QueryResult> getErroneousResults() {
        final var res = new ArrayList<QueryResult>();
        condition.ifPresent(c -> {
            if (c.getType().equals(QueryResultType.ERROR)) {
                res.add(c);
            }
        });
        res.addAll(results.stream()
                .filter(r -> r.getType().equals(QueryResultType.ERROR))
                .collect(Collectors.toList()));
        return res;
    }

    public List<QueryResult> getResults() {
        final var res = new ArrayList<QueryResult>();
        if (condition.isPresent()) {
            res.add(condition.get());
            if (condition.get().getType() == QueryResultType.ERROR) {
                return res;
            }
        }
        res.addAll(results);
        return res;
    }
}
