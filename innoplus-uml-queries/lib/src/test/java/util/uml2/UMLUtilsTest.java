package util.uml2;

import de.unioldenburg.se.innoplus.message.EcoreIO;
import org.eclipse.uml2.uml.*;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.Objects;

import org.eclipse.uml2.uml.Class;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UMLUtilsTest {

    private Model getModel() throws Exception{

        var solution = Paths.get(getClass().getClassLoader().getResource("models/GUI.uml").toURI());

        var umlResource = getClass()
                .getClassLoader()
                .getResource("org.eclipse.uml2.uml.resources_5.5.0.v20210228-1829.jar")
                .toString();
        var eclipseUmlResource = org.eclipse.emf.common.util.URI.createURI("jar:" + umlResource + "!/");
        var solutionURI = org.eclipse.emf.common.util.URI.createFileURI(solution.toString());
        return EcoreIO.loadUmlModel(new BufferedInputStream(new FileInputStream(solution.toFile())));
    }

    @Test
    void test() throws Exception {
        final var model = getModel();
        final var list = UMLUtils.findClass(model, "List");
        final var entry = UMLUtils.findClass(model, "Entry");
        final var associations = UMLUtils.findAssociations(model, list, entry);
        associations.forEach(a -> {
           a.getMemberEnds().forEach(e -> {
               System.out.println(e.allOwnedElements());
           });
        });
        //System.out.println(associations.get(1).allFeatures().get(0).allOwnedElements());
        //model.getPackagedElements().stream().filter(Class.class::isInstance).filter(c -> c.getName().equals("List"))
                //.map(Element::allOwnedElements).forEach(System.out::println);
    }
}
