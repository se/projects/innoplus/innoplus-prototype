package util.evaluation;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;

import java.sql.Date;
import java.time.Instant;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class QueryResultChainTest {

    @Test
    void testChainWithoutCondition() {
        final var chain = createTestChain();
        assertEquals(2, chain.getSuccessfulResults().size());
        assertEquals(2, chain.getErroneousResults() .size());
        assertEquals(4, chain.getResults().size());
    }

    @Test
    void testChainWithSuccessfulCondition() {
        final var chain = QueryResultChain.create(createQueryResult("success", true));
        chain.add(createTestChain());
        assertEquals(3, chain.getSuccessfulResults().size());
        assertEquals(2, chain.getErroneousResults() .size());
        assertEquals(5, chain.getResults().size());
    }

    @Test
    void testChainWithSuccessfulAndFailureCondition() {
        final var chain = QueryResultChain.create(createQueryResult("success", true));
        chain.add(createTestChain());
        final var chain2 = QueryResultChain.create(createQueryResult("failure", false));
        chain2.add(createTestChain());
        chain.add(chain2);
        assertEquals(3, chain.getSuccessfulResults().size());
        assertEquals(3, chain.getErroneousResults() .size());
        assertEquals(5, chain.getResults().size());
    }

    @Test
    void testChainWithFailureCondition() {
        final var chain = QueryResultChain.create(createQueryResult("failure", false));
        chain.add(createTestChain());
        assertEquals(0, chain.getSuccessfulResults().size());
        assertEquals(3, chain.getErroneousResults() .size());
        assertEquals(1, chain.getResults().size());
    }

    private static QueryResultChain createTestChain() {
        final var chain = QueryResultChain.create();
        chain.add(createQueryResult("a first successfully result", true));
        chain.add(createQueryResult("a second successfully result", true));
        chain.add(createQueryResult("a first failure result", false));
        chain.add(createQueryResult("a second failure result", false));
        return chain;
    }

    private static QueryResult createQueryResult(String summary, boolean met){
        final var qs = DataFactory.eINSTANCE.createQueryResult();
        qs.setDate(Date.from(Instant.now()));
        qs.setType(met ? QueryResultType.SUCCESS : QueryResultType.ERROR);
        qs.setSummary(summary);
        return qs;
    }
}
