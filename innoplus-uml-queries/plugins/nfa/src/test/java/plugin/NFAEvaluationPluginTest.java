package plugin;

import de.unioldenburg.se.innoplus.message.EcoreIO;
import junit.framework.TestCase;
import org.eclipse.uml2.uml.Model;

import java.io.BufferedInputStream;
import java.util.Objects;

public class NFAEvaluationPluginTest extends TestCase {

    private Model getModel() throws Exception{
        return EcoreIO.loadUmlModel(new BufferedInputStream(Objects.requireNonNull(
                getClass().getClassLoader().getResourceAsStream("solutions/GUI.uml"))));
    }

    public void testEvaluate() throws Exception{
        var model = getModel();
        var plugin = new NFAEvaluationPlugin();
        var results = plugin.evaluate(model);
        results.forEach(System.out::println);
    }
}