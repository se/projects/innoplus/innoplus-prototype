package plugin;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.uml_api.UMLEvaluator;
import org.eclipse.uml2.uml.Model;
import org.pf4j.Extension;
import util.evaluation.QueryResultChain;
import util.uml2.UMLUtils;

import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Extension
public class NFAEvaluationPlugin implements UMLEvaluator {

    @Override
    public List<QueryResult> evaluate(Model model) {

        // 1
        final var stateClass = UMLUtils.findClass(model, "State");

        // 2
        final var stateOutRole = UMLUtils.findProperty(stateClass, "out");
        final var stateOutLowerBound = UMLUtils.hasCorrectLowerBound(stateOutRole, -1);
        final var stateOutUpperBound = UMLUtils.hasCorrectUpperBound(stateOutRole, -1);

        // 3
        final var stateInRole = UMLUtils.findProperty(stateClass, "in");
        final var stateInLowerBound = UMLUtils.hasCorrectLowerBound(stateOutRole, -1);
        final var stateInUpperBound = UMLUtils.hasCorrectUpperBound(stateOutRole, -1);

        // 4
        final var stateFinalAttribute = UMLUtils.findProperty(stateClass, "final");
        final var stateFinalAttributeIsBoolean = UMLUtils.propertyIsBoolean(stateFinalAttribute);

        // 5

        // variation own start state class
        final var startState = UMLUtils.findClass(model, "StartState");
        final var startStateExtendsState = UMLUtils.isSubClass(startState, stateClass);

        final var stateAutomateRole = UMLUtils.findProperty(startState.or(() -> stateClass), "start");

        // start state has exactly one nfa state has endless nfa's
        final var stateAutomateRoleBound = startState.isPresent() ? 1 : -1;
        final var stateAutomateLowerBound = UMLUtils.hasCorrectLowerBound(stateAutomateRole, stateAutomateRoleBound);
        final var stateAutomateUpperBound = UMLUtils.hasCorrectUpperBound(stateAutomateRole, stateAutomateRoleBound);

        // 6,9 transition inheritance
        final var transitionClass = UMLUtils.findClass(model, "Transition");

        // check that transition is abstract?

        final var stateTransitionAssociations = UMLUtils.findAssociations(model, stateClass, transitionClass);

        // 6
        final var epsylonTransitionClass = UMLUtils.findClass(model, "EpsylonTransition");

        final var epsylonTransitionExtendsFromTransitionClass = UMLUtils.isSubClass(epsylonTransitionClass, transitionClass);

        // 7
        final var epsylonTransitionSourceRole = UMLUtils.findProperty(epsylonTransitionClass, "source");
        final var epsylonTransitionSourceLowerBound = UMLUtils.hasCorrectLowerBound(epsylonTransitionSourceRole, 1);
        final var epsylonTransitionSourceUpperBound = UMLUtils.hasCorrectUpperBound(epsylonTransitionSourceRole, 1);

        // 8
        final var epsylonTransitionSinkRole = UMLUtils.findProperty(epsylonTransitionClass, "sink");
        final var epsylonTransitionSinkLowerBound = UMLUtils.hasCorrectLowerBound(epsylonTransitionSinkRole, 1);
        final var epsylonTransitionSinkUpperBound = UMLUtils.hasCorrectUpperBound(epsylonTransitionSinkRole, 1);

        // 9
        final var symbolTransitionClass = UMLUtils.findClass(model, "SymbolTransition");

        final var symbolTransitionClassExtendsFromTransitionClass = UMLUtils.isSubClass(symbolTransitionClass, transitionClass);

        // 10
        final var symbolTransitionSourceRole = UMLUtils.findProperty(symbolTransitionClass, "source");
        final var symbolTransitionSourceLowerBound = UMLUtils.hasCorrectLowerBound(symbolTransitionSourceRole, 1);
        final var symbolTransitionSourceUpperBound = UMLUtils.hasCorrectUpperBound(symbolTransitionSourceRole, 1);

        // 11
        final var symbolTransitionSinkRole = UMLUtils.findProperty(symbolTransitionClass, "sink");
        final var symbolTransitionSinkLowerBound = UMLUtils.hasCorrectLowerBound(symbolTransitionSinkRole, 1);
        final var symbolTransitionSinkUpperBound = UMLUtils.hasCorrectUpperBound(symbolTransitionSinkRole, 1);

        // 12
        final var symbolTransitionClassSymbolAttribute = UMLUtils.findProperty(symbolTransitionClass, "symbol");
        final var symbolAttributeIsString = UMLUtils.propertyIsString(symbolTransitionClassSymbolAttribute);

        // 13
        final var nfaClass = UMLUtils.findClass(model, "NFA");

        // 14
        final var nfaStartRole = UMLUtils.findProperty(nfaClass, "start");
        final var nfaStartLowerBound = UMLUtils.hasCorrectLowerBound(nfaStartRole, 1);
        final var nfaStartUpperBound = UMLUtils.hasCorrectUpperBound(nfaStartRole, 1);

        final var response = QueryResultChain.create()
                .add( // 1
                QueryResultChain.create(createQueryResult("Es existieren Zustände", startState.isPresent()))
                        .add(createQueryResult("Ein Zustand hat beliebig viele ausgehende Transitionen",
                                stateOutLowerBound.isPresent() && stateOutLowerBound.get() &&
                                        stateOutUpperBound.isPresent() && stateOutUpperBound.get()))
                        .add(createQueryResult("Ein Zustand hat beliebig viele eingehende Transitionen",
                                stateInLowerBound.isPresent() && stateInLowerBound.get() &&
                                stateInUpperBound.isPresent() && stateInUpperBound.get()))
                        .add(createQueryResult("Ein Zustand kann ein Endzustand sein",
                                stateFinalAttributeIsBoolean.isPresent() && stateFinalAttributeIsBoolean.get()))
                        .add(createQueryResult("Ein Zustand kann ein Startzustand sein",
                                ((startState.isPresent() && startStateExtendsState.isPresent() &&
                                        startStateExtendsState.get()) || startState.isEmpty()) &&
                                        stateAutomateRole.isPresent() && stateAutomateLowerBound.isPresent() &&
                                        stateAutomateLowerBound.get() && stateAutomateUpperBound.isPresent() &&
                                        stateAutomateUpperBound.get()
                                )))
                .add( // 6
                QueryResultChain.create(createQueryResult("Es existieren \u000F-Transitionen",
                        epsylonTransitionExtendsFromTransitionClass.isPresent() &&
                                epsylonTransitionExtendsFromTransitionClass.get()))
                        .add(createQueryResult("Eine \u000F-Transition hat genau einen Quellzustand",
                                epsylonTransitionSourceLowerBound.isPresent() &&
                                        epsylonTransitionSourceLowerBound.get() &&
                                        epsylonTransitionSourceUpperBound.isPresent() &&
                                        epsylonTransitionSourceUpperBound.get()))
                        .add(createQueryResult("Eine \u000F-Transition hat genau einen Zielzustand",
                                epsylonTransitionSinkLowerBound.isPresent() &&
                                        epsylonTransitionSinkLowerBound.get() &&
                                        epsylonTransitionSinkUpperBound.isPresent() &&
                                        epsylonTransitionSinkUpperBound.get())))
                .add( // 9
                QueryResultChain.create(createQueryResult("Es existieren Symbol-Transitionen",
                        symbolTransitionClassExtendsFromTransitionClass.isPresent() &&
                                symbolTransitionClassExtendsFromTransitionClass.get()))
                        .add(createQueryResult("Eine Symbol-Transition hat genau einen Quellzustand",
                                symbolTransitionSourceLowerBound.isPresent() &&
                                        symbolTransitionSourceLowerBound.get() &&
                                        symbolTransitionSourceUpperBound.isPresent() &&
                                        symbolTransitionSourceUpperBound.get()))
                        .add(createQueryResult("Eine Symbol-Transition hat genau einen Zielzustand",
                                symbolTransitionSinkLowerBound.isPresent() &&
                                        symbolTransitionSinkLowerBound.get() &&
                                        symbolTransitionSinkUpperBound.isPresent() &&
                                        symbolTransitionSinkUpperBound.get()))
                        .add(createQueryResult("Eine Symbol-Transition hat ein Symbol",
                                symbolAttributeIsString.isPresent() && symbolAttributeIsString.get())))
                .add( // 13
                QueryResultChain.create(createQueryResult("Es existieren Automaten", nfaClass.isPresent()))
                        .add(createQueryResult("Ein Automat hat genau einen Startzustand",
                                nfaStartLowerBound.isPresent() && nfaStartLowerBound.get() &&
                                        nfaStartUpperBound.isPresent() && nfaStartUpperBound.get())));

        return response.getResults();
    }

    private QueryResult createQueryResult(String summary, boolean met){
        var qs = DataFactory.eINSTANCE.createQueryResult();
        qs.setDate(Date.from(Instant.now()));
        qs.setType(met ? QueryResultType.SUCCESS : QueryResultType.ERROR);
        qs.setSummary(summary);
        return qs;
    }
}
