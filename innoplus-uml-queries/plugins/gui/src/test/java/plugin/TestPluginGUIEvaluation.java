package plugin;

import de.unioldenburg.se.innoplus.message.EcoreIO;
import junit.framework.TestCase;
import org.eclipse.uml2.uml.Model;
import plugin.GUIEvaluationPlugin;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.nio.file.Paths;

public class TestPluginGUIEvaluation extends TestCase {

    private Model getModel() throws Exception{

        var solution = Paths.get(getClass().getClassLoader().getResource("solutions/GUI.uml").toURI());

        var umlResource = getClass()
                .getClassLoader()
                .getResource("uml2-resources/org.eclipse.uml2.uml.resources_5.5.0.v20210228-1829.jar")
                .toString();
        var eclipseUmlResource = org.eclipse.emf.common.util.URI.createURI("jar:" + umlResource + "!/");
        var solutionURI = org.eclipse.emf.common.util.URI.createFileURI(solution.toString());
        return EcoreIO.loadUmlModel(new BufferedInputStream(new FileInputStream(solution.toFile())));
    }

    public void testEvaluate() throws Exception{

        var model = getModel();
        var plugin = new GUIEvaluationPlugin();
        var results = plugin.evaluate(model);
        results.forEach(System.out::println);
    }
}