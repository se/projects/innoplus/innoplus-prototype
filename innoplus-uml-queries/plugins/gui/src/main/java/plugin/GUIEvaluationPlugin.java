package plugin;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.uml_api.UMLEvaluator;
import org.eclipse.uml2.uml.Model;
import org.pf4j.Extension;
import util.uml2.UMLUtils;

import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Extension
public class GUIEvaluationPlugin implements UMLEvaluator {

    @Override
    public List<QueryResult> evaluate(Model model) {

        //1
        var frameClass = UMLUtils.findClass(model, "Frame");
        var frameHasAttributeTitle = UMLUtils.findProperty(frameClass, "title");
        var frameAttributeTitleIsString = UMLUtils.propertyIsString(frameHasAttributeTitle);
        var frameCanNotContainFrames = UMLUtils.classHasNoSelfReference(frameClass);

        //2
        var elementClass = UMLUtils.findClass(model, "Element");

        //3
        var panelClass = UMLUtils.findClass(model, "Panel");
        var panelIsElement = UMLUtils.isSubClass(panelClass, elementClass);
        var panelHasAggregation = UMLUtils.hasAggregation(panelClass, elementClass);
        var panelHasNoFrames = UMLUtils.hasNoProperty(panelClass, frameClass);

        //4
        var labelClass = UMLUtils.findClass(model, "Label");
        var labelIsElement = UMLUtils.isSubClass(labelClass, elementClass);
        var labelHasAttributeText = UMLUtils.findProperty(labelClass, "text");
        var labelTextIsString = UMLUtils.propertyIsString(labelHasAttributeText);

        //5
        var buttonClass = UMLUtils.findClass(model, "Button");
        var buttonIsElement = UMLUtils.isSubClass(buttonClass, elementClass);
        var buttonHasAttributeText = UMLUtils.findProperty(buttonClass, "text");
        var buttonTextIsString = UMLUtils.propertyIsString(buttonHasAttributeText);

        //6
        var checkBoxClass = UMLUtils.findClass(model, "CheckBox");
        var checkBoxIsElement = UMLUtils.isSubClass(checkBoxClass, elementClass);
        var checkBoxHasAttributeText = UMLUtils.findProperty(checkBoxClass, "text");
        var checkBoxTextIsString = UMLUtils.propertyIsString(checkBoxHasAttributeText);
        var checkBoxHasAttributeSelected = UMLUtils.findProperty(checkBoxClass, "selected");
        var checkBoxSelectedIsBoolean = UMLUtils.propertyIsBoolean(checkBoxHasAttributeSelected);

        //7 & 8
        var listClass = UMLUtils.findClass(model, "List");
        var listIsElement = UMLUtils.isSubClass(listClass, elementClass);
        var listHasNoText = UMLUtils.findProperty(listClass, "text").isEmpty() ? Optional.of(true) : Optional.of(false);

        var entryClass = UMLUtils.findClass(model, "Entry");
        var entryHasText = UMLUtils.findProperty(entryClass, "text");
        var entryTextIsString = UMLUtils.propertyIsString(entryHasText);

        var listHasEntry = UMLUtils.findProperty(listClass, "Entry");
        var listHasTwoElements = UMLUtils.hasCorrectLowerBound(listHasEntry, 2);
        var listIsComposite = UMLUtils.propertyIsComposite(listHasEntry);

        var listHasSelectedEntry = UMLUtils.findProperty(listClass, "selectedEntry");
        var listHasOneSelectedEntry = UMLUtils.hasCorrectUpperBound(listHasSelectedEntry, 1);


        var s1 = createQueryResult("Die Klasse Frame ist vorhanden", frameClass.isPresent());
        var s1_1 = createQueryResult("Ein Frame hat eine Eigenschaft Titel.", frameHasAttributeTitle.isPresent() && isSuccess(s1));
        var s1_2 = createQueryResult("Die Eigenschaft Titel kann nur Zeichenketten Werte annehmen.",
                frameAttributeTitleIsString.isPresent() && frameAttributeTitleIsString.get() && isSuccess(s1) && isSuccess(s1_1));
        var s1_3 = createQueryResult("Ein Frame kann keine Frames enthalten.",
                frameCanNotContainFrames.isPresent() && frameCanNotContainFrames.get() && isSuccess(s1));

        var s2 = createQueryResult("Es existieren Elemente", elementClass.isPresent());

        var s3 = createQueryResult("Es existiert eine Klasse Panel", panelClass.isPresent());
        var s3_1 = createQueryResult("Ein Panel ist ein Element", panelIsElement.isPresent() && panelIsElement.get() && isSuccess(s3));
        var s3_2 = createQueryResult("Ein Panel enthält beliebig viele Elemente",
                panelHasAggregation.isPresent() && panelHasAggregation.get() && isSuccess(s3));
        var s3_3 = createQueryResult("Ein Panel kann keine Frames enthalten",
                panelHasNoFrames.isPresent() && panelHasNoFrames.get() && isSuccess(s3));

        var s4 = createQueryResult("Es existiert eine Klasse Label", labelClass.isPresent());
        var s4_1 = createQueryResult("Ein Label ist ein Element.", labelIsElement.isPresent() && labelIsElement.get());
        var s4_2 = createQueryResult("Ein Label hat eine Eigenschaft Text", labelHasAttributeText.isPresent() && isSuccess(s4));
        var s4_2_1 = createQueryResult("Die Eigenschaft Text kann nur Zeichenketten Werte annehmen.",
                labelTextIsString.isPresent() && labelTextIsString.get());

        var s5 = createQueryResult("Es existiert eine Klasse Button.", buttonClass.isPresent());
        var s5_1 = createQueryResult("Ein Button ist ein Element.", buttonIsElement.isPresent() && buttonIsElement.get());
        var s5_1_1 = createQueryResult("Die Eigenschaft Text kann nur Zeichenketten Werte annehmen.",
                buttonTextIsString.isPresent() && buttonTextIsString.get());

        var s6 = createQueryResult("Es existiert eine Klasse CheckBox.", checkBoxClass.isPresent());
        var s6_1 = createQueryResult("Eine CheckBox ist ein Element", checkBoxIsElement.isPresent() && checkBoxIsElement.get());
        var s6_2 = createQueryResult("Eine CheckBox hat eine Eigenschaft Text", checkBoxHasAttributeText.isPresent());
        var s6_2_1 = createQueryResult("Die Eigenschaft Text kann nur Zeichenketten Werte annehmen.",
                checkBoxTextIsString.isPresent() && checkBoxTextIsString.get());
        var s6_3 = createQueryResult("Eine CheckBox hat eine Eigenschaft selected", checkBoxHasAttributeSelected.isPresent());
        var s6_4 = createQueryResult("Die Eigenschaft selected kann nur boolesche Werte annehmen",
                checkBoxSelectedIsBoolean.isPresent() && checkBoxSelectedIsBoolean.get());

        var s7 = createQueryResult("Es existieren Listen.", listClass.isPresent());
        var s7_1 = createQueryResult("Eine Liste ist ein Element.", listIsElement.isPresent() && listIsElement.get());
        var s7_2 = createQueryResult("Eine Liste enthält mindestens zwei Einträge.",
                listHasTwoElements.isPresent() && listHasTwoElements.get());
        var s7_3 = createQueryResult("Eine Liste hat genau einen selektierten Eintrag.",
                listHasOneSelectedEntry.isPresent() && listHasOneSelectedEntry.get());
        var s7_4 = createQueryResult("Eine Liste hat keine Eigenschaft Text.", listHasNoText.get());

        var s8 = createQueryResult("Es existieren Einträge.", entryClass.isPresent());
        var s8_1 = createQueryResult("Ein Eintrag hat einen Text.", entryHasText.isPresent());
        var s8_1_1 = createQueryResult("Der Text kann nur Zeichenketten Werte annehmen.",
                entryTextIsString.isPresent() && entryTextIsString.get());
        var s8_2 = createQueryResult("Ein Eintrag muss in einer Liste enthalten sein.",
                listIsComposite.isPresent() && listIsComposite.get());

        var results = new QueryResult[]{s1, s1_1, s1_2, s1_3, s2, s3, s3_1, s3_2, s3_3, s4, s4_1, s4_2, s4_2_1, s5,
        s5_1, s5_1_1, s6, s6_1, s6_2, s6_2_1, s6_3, s6_4, s7, s7_1, s7_2, s7_3, s7_4, s8, s8_1, s8_1_1, s8_2};
        return Arrays.stream(results).filter(s -> !isSuccess(s)).collect(Collectors.toList());
    }

    private QueryResult createQueryResult(String summary, boolean met){
        var qs = DataFactory.eINSTANCE.createQueryResult();
        qs.setDate(Date.from(Instant.now()));
        qs.setType(met ? QueryResultType.SUCCESS : QueryResultType.ERROR);
        qs.setSummary(summary);
        return qs;
    }

    private boolean isSuccess(QueryResult result){
        return result.getType().equals(QueryResultType.SUCCESS);
    }
}
