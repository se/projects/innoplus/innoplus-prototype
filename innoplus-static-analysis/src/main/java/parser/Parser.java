package parser;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.utils.SourceRoot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import parser.wrappers.ClassOrInterfaceDeclarationWrapper;
import parser.wrappers.EnumDeclarationWrapper;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Parser {

    private static final Logger logger = LogManager.getLogger(Parser.class);

    protected List<CompilationUnit> compilationUnits = new ArrayList<>();
    protected Map<String, ClassOrInterfaceDeclarationWrapper> classes = new HashMap<>();
    protected Map<String, ClassOrInterfaceDeclarationWrapper> interfaces = new HashMap<>();
    protected Map<String, EnumDeclarationWrapper> enums = new HashMap<>();

    public Parser(Path... javaSrcPath) {
        for (final var src : javaSrcPath) {
            final var sourceRoot = new SourceRoot(src);
            try {
                final var results = sourceRoot.tryToParse();
                // check results for parse errors and throw ParseError because very likely test will not run correctly
                for (final var result : results) {
                    if (!result.getProblems().isEmpty()) {
                        throw new ParseError(result);
                    }
                }
                // get all compilation units
                final var allCUs = results.stream()
                        .filter(r -> r.getResult().isPresent())
                        .map(r -> r.getResult().get())
                        .collect(Collectors.toList());
                compilationUnits.addAll(allCUs);
            } catch (IOException e) {
                logger.warn("IOException happened during parsing of javaSrcPath {}", src, e);
            }
        }
        // collect all classes, interfaces and enums
        compilationUnits.forEach(cu -> {
            cu.findAll(ClassOrInterfaceDeclaration.class).forEach(coid -> {
                final var name = coid.getNameAsString().toLowerCase();
                final var wrapper = new ClassOrInterfaceDeclarationWrapper(coid);
                if (coid.isInterface()) {
                    interfaces.put(name, wrapper);
                } else {
                    classes.put(name, wrapper);
                }
            });
            cu.findAll(EnumDeclaration.class).forEach(ed -> {
                final var name = ed.getNameAsString().toLowerCase();
                final var wrapper = new EnumDeclarationWrapper(ed);
                enums.put(name, wrapper);
            });
        });
    }

    public Set<ClassOrInterfaceDeclarationWrapper> getClassesAndInterfaces(String... classOrInterfaceName) {
        final var cai = new HashSet<ClassOrInterfaceDeclarationWrapper>();
        if (classOrInterfaceName.length > 0) {
            for (var coid : classOrInterfaceName) {
                coid = coid.toLowerCase();
                if (classes.containsKey(coid)) {
                    cai.add(classes.get(coid));
                }
                if (interfaces.containsKey(coid)) {
                    cai.add(interfaces.get(coid));
                }
            }
        } else {
            cai.addAll(classes.values());
            cai.addAll(interfaces.values());
        }
        return cai;
    }

    public Set<ClassOrInterfaceDeclarationWrapper> getClasses(String... classNames) {
        final var cai = new HashSet<ClassOrInterfaceDeclarationWrapper>();
        if (classNames.length > 0) {
            for (var className : classNames) {
                className = className.toLowerCase();
                if (classes.containsKey(className)) {
                    cai.add(classes.get(className));
                }
            }
        } else {
            cai.addAll(classes.values());
        }
        return cai;
    }

    public Optional<ClassOrInterfaceDeclarationWrapper> getClass(String className) {
        return Optional.ofNullable(classes.get(className.toLowerCase()));
    }

    public Set<ClassOrInterfaceDeclarationWrapper> getInterfaces(String... interfaceNames) {
        final var cai = new HashSet<ClassOrInterfaceDeclarationWrapper>();
        if (interfaceNames.length > 0) {
            for (var interfaceName : interfaceNames) {
                interfaceName = interfaceName.toLowerCase();
                if (interfaces.containsKey(interfaceName)) {
                    cai.add(interfaces.get(interfaceName));
                }
            }
        } else {
            cai.addAll(interfaces.values());
        }
        return cai;
    }

    public Optional<ClassOrInterfaceDeclarationWrapper> getInterface(String interfaceName) {
        return Optional.ofNullable(interfaces.get(interfaceName.toLowerCase()));
    }

    public Set<EnumDeclarationWrapper> getEnums(String... enumNames) {
        final var ed = new HashSet<EnumDeclarationWrapper>();
        if (enumNames.length > 0) {
            for (var enumName : enumNames) {
                enumName = enumName.toLowerCase();
                if (enums.containsKey(enumName)) {
                    ed.add(enums.get(enumName));
                }
            }
        } else {
            ed.addAll(enums.values());
        }
        return ed;
    }

    public Optional<EnumDeclarationWrapper> getEnum(String enumName) {
        return Optional.ofNullable(enums.get(enumName.toLowerCase()));
    }

    public List<CompilationUnit> getCompilationUnits() {
        return compilationUnits;
    }
}
