package parser.wrappers;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.nodeTypes.NodeWithSimpleName;
import com.github.javaparser.ast.nodeTypes.modifiers.NodeWithPublicModifier;

import java.util.*;
import java.util.stream.Collectors;

public class ClassOrInterfaceDeclarationWrapper {

    final ClassOrInterfaceDeclaration declaration;
    final Map<String, Set<MethodDeclarationWrapper>> methodDeclarations = new HashMap<>();

    public ClassOrInterfaceDeclarationWrapper(ClassOrInterfaceDeclaration declaration) {
        this.declaration = declaration;
        declaration.findAll(MethodDeclaration.class).forEach(md -> {
            methodDeclarations.computeIfAbsent(md.getNameAsString(), k -> new HashSet<>());
            methodDeclarations.computeIfPresent(md.getNameAsString(), (k, v) -> {
                v.add(new MethodDeclarationWrapper(md));
                return v; });
        });
    }

    public String getName() {
        return declaration.getNameAsString();
    }

    public boolean isAbstract() {
        return declaration.isAbstract();
    }

    public boolean hasTypeParameter(){
        return !declaration.getTypeParameters().isEmpty();
    }

    public boolean typeParameterExtends(ClassOrInterfaceDeclarationWrapper type){
        if (!hasTypeParameter())
            return false;

        for (var type_ : declaration.getTypeParameters()){
            for (var bound : type_.getTypeBound()){
                if (bound.getName().equals(type.getDeclaration().getName()))
                    return true;
            }
        }
        return false;
    }

    public ClassOrInterfaceDeclaration getDeclaration() {
        return declaration;
    }

    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof ClassOrInterfaceDeclarationWrapper)) {
            return false;
        }
        final var o = (ClassOrInterfaceDeclarationWrapper) other;
        // compare name
        final boolean nameEqual = declaration.getNameAsString().equals(o.declaration.getNameAsString());
        /*
        if(declaration.isInnerClass()) {
            if (!o.isInnerClass()) {
                return false;
            }
            // compare parents
            final boolean parentEqual = new ClassOrInterfaceDeclarationWrapper(declaration.getParentNode().get().findFirst(ClassOrInterfaceDeclaration.class).get())
                    .equals(new ClassOrInterfaceDeclarationWrapper(o.getParentNode().get().findFirst(ClassOrInterfaceDeclaration.class).get()));
            return nameEqual && parentEqual;
        }
         */
        return nameEqual;
    }

    public int hashCode() {
        /*
        if (declaration.isInnerClass()) {
            return Objects.hash(declaration.getFullyQualifiedName()); /*,
                    new ClassOrInterfaceDeclarationWrapper(declaration.getParentNode().get()
                            .findFirst(ClassOrInterfaceDeclaration.class).get()));

        } else {
            return Objects.hash(declaration.getName());
        }
        */
        return Objects.hash(declaration.getNameAsString());
    }

    public String toString() {
        return this.declaration.toString();
    }

    public Set<MethodDeclarationWrapper> getMethodDeclarations() {
        return methodDeclarations.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    public Set<MethodDeclarationWrapper> getMethodDeclaration(String name) {
        return methodDeclarations.getOrDefault(name, Collections.emptySet());
    }

    public boolean isInterfaceLike(ClassOrInterfaceDeclarationWrapper other) {
        if (!this.equals(other)) {
            return false;
        }
        return other.declaration.getFields().stream()
                .filter(NodeWithPublicModifier::isPublic)
                .collect(Collectors.toSet())
                .containsAll(this.declaration.getFields().stream()
                        .filter(NodeWithPublicModifier::isPublic)
                        .collect(Collectors.toSet()))
                // compare public methods (student must have all public methods from teacher)
                && other.declaration.findAll(MethodDeclaration.class).stream()
                .filter(NodeWithPublicModifier::isPublic)
                .map(MethodDeclarationWrapper::new)
                .collect(Collectors.toSet())
                .containsAll(this.declaration.findAll(MethodDeclaration.class).stream()
                        .filter(NodeWithPublicModifier::isPublic)
                        .map(MethodDeclarationWrapper::new)
                        .collect(Collectors.toSet()));
    }

    public boolean isExtensionLike(ClassOrInterfaceDeclaration other) {
        this.declaration.getExtendedTypes();
        return false;
    }

    public boolean isImplementationLike(ClassOrInterfaceDeclaration other) {
        this.declaration.getImplementedTypes();
        return false;
    }

    public Set<MethodDeclarationWrapper> getPublicMethodsIntersection(ClassOrInterfaceDeclarationWrapper other) {
        final var thisPublicMethods = this.declaration.findAll(MethodDeclaration.class).stream()
                .filter(NodeWithPublicModifier::isPublic)
                .map(MethodDeclarationWrapper::new)
                .collect(Collectors.toSet());
        return other.declaration.findAll(MethodDeclaration.class).stream()
                .filter(NodeWithPublicModifier::isPublic)
                .map(MethodDeclarationWrapper::new)
                .filter(thisPublicMethods::contains)
                .collect(Collectors.toSet());
    }

    public boolean hasAllPublicMethodsFrom(ClassOrInterfaceDeclarationWrapper other) {
        final var numPublicMethods = this.declaration.findAll(MethodDeclaration.class).stream()
                .filter(NodeWithPublicModifier::isPublic)
                .count();
        return getPublicMethodsIntersection(other).size() == numPublicMethods;
    }

    public Set<FieldDeclaration> getPublicAttributesIntersection(ClassOrInterfaceDeclarationWrapper other) {
        final var thisPublicFields = this.declaration.getFields().stream()
                .filter(NodeWithPublicModifier::isPublic)
                .collect(Collectors.toSet());
        return other.declaration.getFields().stream()
                .filter(NodeWithPublicModifier::isPublic)
                .filter(thisPublicFields::contains)
                .collect(Collectors.toSet());
    }

    public boolean hasAllPublicAttributesFrom(ClassOrInterfaceDeclarationWrapper other) {
        final var numPublicFields = this.declaration.getFields().stream()
                .filter(NodeWithPublicModifier::isPublic)
                .count();
        return getPublicAttributesIntersection(other).size() == numPublicFields;
    }

    public boolean extendz(String... classOrInterfaceNames) {
        final var names = Set.of(classOrInterfaceNames);
        return declaration.getExtendedTypes().stream()
                .map(NodeWithSimpleName::getNameAsString)
                .filter(names::contains)
                .count() == names.size();
    }

    public Set<String> getExtendedClassesOrInterfacesAsString() {
        return declaration.getExtendedTypes().stream()
                .map(NodeWithSimpleName::getNameAsString)
                .collect(Collectors.toSet());
    }

    public boolean implementz(String... interfaceNames) {
        final var names = Set.of(interfaceNames);
        return declaration.getImplementedTypes().stream()
                .map(NodeWithSimpleName::getNameAsString)
                .filter(names::contains)
                .count() == names.size();
    }

    public Set<String> getImplementedInterfacesAsString() {
        return declaration.getImplementedTypes().stream()
                .map(NodeWithSimpleName::getNameAsString)
                .collect(Collectors.toSet());
    }

    public boolean hasMethod(String name) {
        return !declaration.getMethodsByName(name).isEmpty();
    }
}
