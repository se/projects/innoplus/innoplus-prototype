package parser.wrappers;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.SimpleName;

import java.util.Objects;

public class MethodDeclarationWrapper {

    protected final MethodDeclaration declaration;

    public MethodDeclarationWrapper(MethodDeclaration declaration) {
        this.declaration = declaration;
    }

    public MethodDeclaration getDeclaration() {
        return declaration;
    }

    public String getName() {
        return declaration.getNameAsString();
    }

    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof MethodDeclarationWrapper)) {
            return false;
        }
        final var otherMethod = (MethodDeclarationWrapper) other;
        // compare name
        return this.declaration.getName().equals(otherMethod.declaration.getName())
                // compare return type
                && this.declaration.getType().equals(otherMethod.declaration.getType())
                // compare parameters
                && this.declaration.getParameters().equals(otherMethod.declaration.getParameters())
                // compare name of parent class or interface
                && this.declaration.getParentNode().get().findFirst(ClassOrInterfaceDeclaration.class).get().getName()
                .equals(otherMethod.declaration.getParentNode().get().findFirst(ClassOrInterfaceDeclaration.class).get().getName());
    }

    public int hashCode() {
        final var parent = this.declaration.getParentNode().get().findFirst(ClassOrInterfaceDeclaration.class);
        SimpleName parentName = null;
        if (parent.isPresent()) {
            parentName = parent.get().getName();
        }
        return Objects.hash(this.declaration.getName(), this.declaration.getType(), this.declaration.getParameters(), parentName);
    }

    public boolean isReplaceable(MethodDeclarationWrapper other) {
        return this.equals(other);
    }

    public String toString() {
        return this.declaration.toString();
    }

    /*
    public static boolean contains(Set<MethodDeclaration> a, MethodDeclaration b) {
        return a.stream().anyMatch(m -> equals(m, b));
    }

    /**
     * Contains a all elements from b
     * @param a a set
     * @param b a set
     * @return does a contain all elements from b
     *
    public static boolean containsAll(Set<MethodDeclaration> a, Set<MethodDeclaration> b) {
        return b.stream().allMatch(m -> contains(a, m));
    }
    */
}
