package parser.wrappers;

import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;

import java.util.Objects;

public class EnumDeclarationWrapper {

    protected final EnumDeclaration declaration;

    public EnumDeclarationWrapper(EnumDeclaration declaration) {
        this.declaration = declaration;
    }

    public boolean hasEntry(String entryName) {
        return declaration.getEntries().stream()
                .map(EnumConstantDeclaration::getNameAsString)
                .anyMatch(n -> n.equalsIgnoreCase(entryName));
    }

    public String getName() {
        return declaration.getNameAsString();
    }

    public EnumDeclaration getDeclaration() {
        return declaration;
    }

    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof EnumDeclarationWrapper)) {
            return false;
        }
        final var o = (EnumDeclarationWrapper) other;
        // compare name
        final boolean nameEqual = declaration.getNameAsString().equals(o.declaration.getNameAsString());
        return nameEqual;
    }

    public int hashCode() {
        return Objects.hash(declaration.getNameAsString());
    }

    public String toString() {
        return this.declaration.toString();
    }
}
