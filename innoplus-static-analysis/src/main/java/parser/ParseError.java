package parser;

import com.github.javaparser.ParseResult;
import com.github.javaparser.Position;
import com.github.javaparser.ast.CompilationUnit;

import java.nio.file.Path;

public class ParseError extends RuntimeException {

    protected ParseResult<CompilationUnit> result;

    public ParseError(ParseResult<CompilationUnit> result) {
        this.result = result;
    }

    public Path getFile() {
        return result.getResult().get().getStorage().get().getPath();
    }

    public Position getPosition() {
        return result.getProblem(0).getLocation().get().toRange().get().begin;
    }

    @Override
    public String getMessage() {
        return result.getProblem(0).getMessage();
    }

    public String getVerboseMessage() {
        return result.getProblem(0).getVerboseMessage();
    }

    public ParseResult<CompilationUnit> getResult() {
        return result;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + getFile() + " " + getVerboseMessage();
    }
}
