package testutils;

import parser.Parser;
import parser.wrappers.ClassOrInterfaceDeclarationWrapper;
import parser.wrappers.EnumDeclarationWrapper;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Assertions {

    public static Set<ClassOrInterfaceDeclarationWrapper> assertClassOrInterfaceExist(Parser parser,
                                                                                      String... classOrInterfaceNames) {
        for (final var classOrInterfaceName : classOrInterfaceNames) {
            assertFalse(parser.getClassesAndInterfaces(classOrInterfaceNames).isEmpty(),
                    "Class or Interface " + classOrInterfaceName + " must exist!");
        }
        return parser.getClassesAndInterfaces(classOrInterfaceNames);
    }

    public static ClassOrInterfaceDeclarationWrapper assertClassExist(Parser parser, String className) {
        assertTrue(parser.getClass(className).isPresent(),
                "Class " + className + " must exist!");
        return parser.getClass(className).get();
    }

    public static ClassOrInterfaceDeclarationWrapper assertInterfaceExist(Parser parser, String interfaceName) {
        assertTrue(parser.getInterface(interfaceName).isPresent(),
                "Interface " + interfaceName + " must exist!");
        return parser.getInterface(interfaceName).get();
    }

    public static EnumDeclarationWrapper assertEnumExist(Parser parser, String enumName) {
        assertTrue(parser.getEnum(enumName).isPresent(),
                "Enum " + enumName + " must exist!");
        return parser.getEnum(enumName).get();
    }

    public static void assertHasEntries(EnumDeclarationWrapper enumDeclarationWrapper, String... entryNames) {
        for (final var entryName : entryNames) {
            assertTrue(enumDeclarationWrapper.hasEntry(entryName),
                    enumDeclarationWrapper.getName() + " must have an Entry called " + entryName);
        }
    }

    public static void assertExtends(ClassOrInterfaceDeclarationWrapper classOrInterfaceDeclarationWrapper,
                                     String... classOrInterfaceNames) {
        for (final var classOrInterfaceName : classOrInterfaceNames) {
            assertTrue(classOrInterfaceDeclarationWrapper.extendz(classOrInterfaceName),
                    classOrInterfaceDeclarationWrapper.getName() + " must extend " + classOrInterfaceName);
        }
    }

    public static void assertImplements(ClassOrInterfaceDeclarationWrapper classOrInterfaceDeclarationWrapper,
                                        String... interfaceNames) {
        for (final var interfaceName : interfaceNames) {
            assertTrue(classOrInterfaceDeclarationWrapper.implementz(interfaceName),
                    classOrInterfaceDeclarationWrapper.getName() + " must implement " + interfaceName);
        }
    }

    public static void assertIsAbstract(ClassOrInterfaceDeclarationWrapper classOrInterfaceDeclarationWrapper) {
        assertTrue(classOrInterfaceDeclarationWrapper.isAbstract(),
                "Class " + classOrInterfaceDeclarationWrapper.getName() + " must be abstract");
    }

    public static void assertHasTypeParameter(ClassOrInterfaceDeclarationWrapper clazz,
                                              ClassOrInterfaceDeclarationWrapper parameter){
        assertTrue(clazz.hasTypeParameter(),
                clazz.getName() + " should have a type parameter");
        assertTrue(clazz.typeParameterExtends(parameter),
                clazz.getName() + "'s type parameter should extend " + parameter.getName());
    }

    public static void assertHasMethod(ClassOrInterfaceDeclarationWrapper clazz, String methodName){
        assertTrue(clazz.hasMethod(methodName),
                clazz.getName() + " should have method: " + methodName);
    }
}
