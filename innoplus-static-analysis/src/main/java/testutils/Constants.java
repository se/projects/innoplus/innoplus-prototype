package testutils;

import java.nio.file.Path;

public class Constants {
    public static final Path INPUT_PATH = Path.of(".","input").toAbsolutePath().normalize();
}
