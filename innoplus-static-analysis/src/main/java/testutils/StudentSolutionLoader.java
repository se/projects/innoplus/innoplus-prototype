package testutils;

import com.sun.jdi.InvalidTypeException;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;
import parser.Parser;

public class StudentSolutionLoader implements TestInstancePostProcessor {

    @Override
    public void postProcessTestInstance(Object test, ExtensionContext extensionContext) throws Exception {
        for (final var field : test.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(StudentCodeParser.class)) {
                if (field.getType().equals(Parser.class)) {
                    final var parser = new Parser(Constants.INPUT_PATH);
                    final var accessible = field.trySetAccessible();
                    if (!accessible) {
                        field.setAccessible(true);
                    }
                    field.set(test, parser);
                    field.setAccessible(accessible);
                } else {
                    throw new InvalidTypeException("Fields annotated with " + StudentCodeParser.class.getCanonicalName()
                            + " must be of type " + Parser.class.getCanonicalName());
                }
            }
        }
    }
}
