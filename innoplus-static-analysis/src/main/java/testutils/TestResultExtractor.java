package testutils;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.junit.platform.commons.annotation.Testable;
import org.opentest4j.AssertionFailedError;
import parser.ParseError;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

public class TestResultExtractor implements TestWatcher {

    private int testRun = 0;
    private int testMethods;

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        checkFirst(context);
        writeToXml(
                context.getTestClass().get().getSimpleName(),
                context.getTestMethod().get().getName(),
                "No Implementation for Tested Methods found",
                "",
                "::-:",
                "Disabled"
        );
        checkLast(context);
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        checkFirst(context);
        checkLast(context);
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        checkFirst(context);
        writeToXml(
                context.getTestClass().get().getSimpleName(),
                context.getTestMethod().get().getName(),
                cause.getClass().getCanonicalName(),
                cause.getMessage(),
                "::-:",
                "Exception"
        );
        checkLast(context);
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        checkFirst(context);
        if (cause instanceof AssertionFailedError){
            final var description = cause.getMessage().contains("==>") ?
                    cause.getMessage().substring(0, cause.getMessage().indexOf("==>")) :
                    cause.getMessage();
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    context.getDisplayName(),
                    description,
                    "::-:",
                    "AssertionError");
        } else if (cause instanceof ParseError) {
            final var error = (ParseError) cause;
            final var trace = Constants.INPUT_PATH
                    .relativize(error.getFile()).toString() + ":" + error.getPosition().line + ":-:";
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    context.getDisplayName(),
                    error.getMessage(),
                    trace,
                    "Exception");
        } else {
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    "Exception: " + cause.getClass(),
                    cause.getMessage(),
                    "::-:",
                    "Exception"
            );
        }
        checkLast(context);
    }

    // Util Methods
    private static void writeToXml(String filename, String testName, String summary, String description, String trace,
                                   String result) {
        try(final var writer = new BufferedWriter(new FileWriter("junit-reports/report.xml", true))) {
            writer.write("    <testcase>" + testName + "\n");
            writer.write("        <result>" + result + "</result>\n");
            writer.write("        <summary>" + summary +  "</summary>" + "\n");
            writer.write("        <description>" + description + "</description>" + "\n");
            writer.write("        <trace>" + trace + "</trace>" + "\n");
            writer.write("    </testcase>" + "\n");
        } catch (IOException ignored) {}
    }

    private void checkFirst(ExtensionContext context){
        if (testRun != 0) return;
        testMethods = (int) Arrays.stream(context.getTestClass().get().getDeclaredMethods())
                .filter(method -> Arrays.stream(method.getAnnotations())
                        .anyMatch(annotation -> annotation.annotationType().isAnnotationPresent(Testable.class)))
                .count();
        try(final var writer = new BufferedWriter(new FileWriter("junit-reports/report.xml"))) {
            writer.write("<report>\n");
        } catch (IOException ignored) {}
    }

    private void checkLast(ExtensionContext context){
        if (++testRun >= testMethods) {
            try (final var writer = new BufferedWriter(new FileWriter("junit-reports/report.xml", true))) {
                writer.write("</report>");
            } catch (IOException ignored) {}
        }
    }
}
