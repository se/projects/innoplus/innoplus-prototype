
public class GenericCutlery {

	public static void main(String[] args) {
		PastryFork fork = new PastryFork();
		SoupSpoon spoon = new SoupSpoon();
		TravelSpork<SoftFood> spork = new TravelSpork<>();

		Cake cake = () -> "Cake";
		Soup soup = () -> "Soup";
		Pudding pudding = () -> "Pudding";

		fork.process(cake);
		spoon.process(soup);
		spork.process(pudding);
		spork.process(cake);
		spork.process(soup);
	}

	public interface Food {
		String getName();
	}
	public interface SoftFood extends Food { }
	public interface SolidFood extends Food { }
	public interface LiquidFood extends Food { }
	public interface Cake extends SolidFood, SoftFood { }
	public interface Soup extends LiquidFood, SoftFood { }
	public interface Pudding extends SoftFood { }

	public interface Cutlery<T extends Food> {
		void process(T food);
	}
	public interface Fork<T extends SolidFood> extends Cutlery<T> { }
	public interface Spoon<T extends LiquidFood> extends Cutlery<T> { }
	public interface Spork<T extends SoftFood> extends Cutlery<T> { }

	public static class PastryFork implements Fork<Cake> {
		@Override
		public void process(Cake food) {
			System.out.println("Eating " + food.getName() + " with fork.");
		}
	}

	public static class SoupSpoon implements Spoon<Soup> {
		@Override
		public void process(Soup food) {
			System.out.println("Eating " + food.getName() + " with spoon.");
		}
	}

	public static class TravelSpork<T extends SoftFood> implements Spork<T> {
		@Override
		public void process(T food) {
			System.out.println("Eating " + food.getName() + " with spork.");
		}
	}
}
