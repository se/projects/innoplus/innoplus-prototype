/**
 */
package de.unioldenburg.se.innoplus.data.impl;

import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.SolutionCategory;
import de.unioldenburg.se.innoplus.data.TaskCategory;

import de.unioldenburg.se.innoplus.data.TeacherSolution;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solution Category</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl#getSolutions <em>Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl#getTaskCategories <em>Task Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolutionCategoryImpl extends MinimalEObjectImpl.Container implements SolutionCategory {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSolutions() <em>Solutions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolutions()
	 * @generated
	 * @ordered
	 */
	protected EList<TeacherSolution> solutions;

	/**
	 * The cached value of the '{@link #getTaskCategories() <em>Task Categories</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskCategories()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskCategory> taskCategories;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolutionCategoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.SOLUTION_CATEGORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SOLUTION_CATEGORY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TeacherSolution> getSolutions() {
		if (solutions == null) {
			solutions = new EObjectWithInverseResolvingEList<TeacherSolution>(TeacherSolution.class, this, DataPackage.SOLUTION_CATEGORY__SOLUTIONS, DataPackage.TEACHER_SOLUTION__CATEGORY);
		}
		return solutions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TaskCategory> getTaskCategories() {
		if (taskCategories == null) {
			taskCategories = new EObjectWithInverseResolvingEList.ManyInverse<TaskCategory>(TaskCategory.class, this, DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES, DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES);
		}
		return taskCategories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InnoplusDatabase getDatabase() {
		if (eContainerFeatureID() != DataPackage.SOLUTION_CATEGORY__DATABASE) return null;
		return (InnoplusDatabase)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatabase(InnoplusDatabase newDatabase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDatabase, DataPackage.SOLUTION_CATEGORY__DATABASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabase(InnoplusDatabase newDatabase) {
		if (newDatabase != eInternalContainer() || (eContainerFeatureID() != DataPackage.SOLUTION_CATEGORY__DATABASE && newDatabase != null)) {
			if (EcoreUtil.isAncestor(this, newDatabase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDatabase != null)
				msgs = ((InternalEObject)newDatabase).eInverseAdd(this, DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES, InnoplusDatabase.class, msgs);
			msgs = basicSetDatabase(newDatabase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.SOLUTION_CATEGORY__DATABASE, newDatabase, newDatabase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.SOLUTION_CATEGORY__SOLUTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSolutions()).basicAdd(otherEnd, msgs);
			case DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTaskCategories()).basicAdd(otherEnd, msgs);
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDatabase((InnoplusDatabase)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.SOLUTION_CATEGORY__SOLUTIONS:
				return ((InternalEList<?>)getSolutions()).basicRemove(otherEnd, msgs);
			case DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES:
				return ((InternalEList<?>)getTaskCategories()).basicRemove(otherEnd, msgs);
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				return basicSetDatabase(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				return eInternalContainer().eInverseRemove(this, DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES, InnoplusDatabase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.SOLUTION_CATEGORY__NAME:
				return getName();
			case DataPackage.SOLUTION_CATEGORY__SOLUTIONS:
				return getSolutions();
			case DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES:
				return getTaskCategories();
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				return getDatabase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.SOLUTION_CATEGORY__NAME:
				setName((String)newValue);
				return;
			case DataPackage.SOLUTION_CATEGORY__SOLUTIONS:
				getSolutions().clear();
				getSolutions().addAll((Collection<? extends TeacherSolution>)newValue);
				return;
			case DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES:
				getTaskCategories().clear();
				getTaskCategories().addAll((Collection<? extends TaskCategory>)newValue);
				return;
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				setDatabase((InnoplusDatabase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.SOLUTION_CATEGORY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DataPackage.SOLUTION_CATEGORY__SOLUTIONS:
				getSolutions().clear();
				return;
			case DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES:
				getTaskCategories().clear();
				return;
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				setDatabase((InnoplusDatabase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.SOLUTION_CATEGORY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DataPackage.SOLUTION_CATEGORY__SOLUTIONS:
				return solutions != null && !solutions.isEmpty();
			case DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES:
				return taskCategories != null && !taskCategories.isEmpty();
			case DataPackage.SOLUTION_CATEGORY__DATABASE:
				return getDatabase() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SolutionCategoryImpl
