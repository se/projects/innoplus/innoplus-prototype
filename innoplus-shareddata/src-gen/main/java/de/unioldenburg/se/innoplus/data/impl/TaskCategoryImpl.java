/**
 */
package de.unioldenburg.se.innoplus.data.impl;

import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.SolutionCategory;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.data.TaskCategory;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Category</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl#getTasks <em>Tasks</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl#getSolutionCategories <em>Solution Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskCategoryImpl extends MinimalEObjectImpl.Container implements TaskCategory {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> tasks;

	/**
	 * The cached value of the '{@link #getSolutionCategories() <em>Solution Categories</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolutionCategories()
	 * @generated
	 * @ordered
	 */
	protected EList<SolutionCategory> solutionCategories;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskCategoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.TASK_CATEGORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.TASK_CATEGORY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Task> getTasks() {
		if (tasks == null) {
			tasks = new EObjectWithInverseResolvingEList<Task>(Task.class, this, DataPackage.TASK_CATEGORY__TASKS, DataPackage.TASK__CATEGORY);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SolutionCategory> getSolutionCategories() {
		if (solutionCategories == null) {
			solutionCategories = new EObjectWithInverseResolvingEList.ManyInverse<SolutionCategory>(SolutionCategory.class, this, DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES, DataPackage.SOLUTION_CATEGORY__TASK_CATEGORIES);
		}
		return solutionCategories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InnoplusDatabase getDatabase() {
		if (eContainerFeatureID() != DataPackage.TASK_CATEGORY__DATABASE) return null;
		return (InnoplusDatabase)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatabase(InnoplusDatabase newDatabase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDatabase, DataPackage.TASK_CATEGORY__DATABASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabase(InnoplusDatabase newDatabase) {
		if (newDatabase != eInternalContainer() || (eContainerFeatureID() != DataPackage.TASK_CATEGORY__DATABASE && newDatabase != null)) {
			if (EcoreUtil.isAncestor(this, newDatabase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDatabase != null)
				msgs = ((InternalEObject)newDatabase).eInverseAdd(this, DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES, InnoplusDatabase.class, msgs);
			msgs = basicSetDatabase(newDatabase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.TASK_CATEGORY__DATABASE, newDatabase, newDatabase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.TASK_CATEGORY__TASKS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTasks()).basicAdd(otherEnd, msgs);
			case DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSolutionCategories()).basicAdd(otherEnd, msgs);
			case DataPackage.TASK_CATEGORY__DATABASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDatabase((InnoplusDatabase)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.TASK_CATEGORY__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
			case DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES:
				return ((InternalEList<?>)getSolutionCategories()).basicRemove(otherEnd, msgs);
			case DataPackage.TASK_CATEGORY__DATABASE:
				return basicSetDatabase(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DataPackage.TASK_CATEGORY__DATABASE:
				return eInternalContainer().eInverseRemove(this, DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES, InnoplusDatabase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.TASK_CATEGORY__NAME:
				return getName();
			case DataPackage.TASK_CATEGORY__TASKS:
				return getTasks();
			case DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES:
				return getSolutionCategories();
			case DataPackage.TASK_CATEGORY__DATABASE:
				return getDatabase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.TASK_CATEGORY__NAME:
				setName((String)newValue);
				return;
			case DataPackage.TASK_CATEGORY__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends Task>)newValue);
				return;
			case DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES:
				getSolutionCategories().clear();
				getSolutionCategories().addAll((Collection<? extends SolutionCategory>)newValue);
				return;
			case DataPackage.TASK_CATEGORY__DATABASE:
				setDatabase((InnoplusDatabase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.TASK_CATEGORY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DataPackage.TASK_CATEGORY__TASKS:
				getTasks().clear();
				return;
			case DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES:
				getSolutionCategories().clear();
				return;
			case DataPackage.TASK_CATEGORY__DATABASE:
				setDatabase((InnoplusDatabase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.TASK_CATEGORY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DataPackage.TASK_CATEGORY__TASKS:
				return tasks != null && !tasks.isEmpty();
			case DataPackage.TASK_CATEGORY__SOLUTION_CATEGORIES:
				return solutionCategories != null && !solutionCategories.isEmpty();
			case DataPackage.TASK_CATEGORY__DATABASE:
				return getDatabase() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //TaskCategoryImpl
