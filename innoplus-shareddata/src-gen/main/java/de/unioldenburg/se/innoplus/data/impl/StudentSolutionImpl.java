/**
 */
package de.unioldenburg.se.innoplus.data.impl;

import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.StudentSolution;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Student Solution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.StudentSolutionImpl#getAuthor <em>Author</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.StudentSolutionImpl#getQueryResults <em>Query Results</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudentSolutionImpl extends SolutionImpl implements StudentSolution {
	/**
	 * The cached value of the '{@link #getQueryResults() <em>Query Results</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueryResults()
	 * @generated
	 * @ordered
	 */
	protected EList<QueryResult> queryResults;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudentSolutionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.STUDENT_SOLUTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Person getAuthor() {
		if (eContainerFeatureID() != DataPackage.STUDENT_SOLUTION__AUTHOR) return null;
		return (Person)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAuthor(Person newAuthor, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAuthor, DataPackage.STUDENT_SOLUTION__AUTHOR, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAuthor(Person newAuthor) {
		if (newAuthor != eInternalContainer() || (eContainerFeatureID() != DataPackage.STUDENT_SOLUTION__AUTHOR && newAuthor != null)) {
			if (EcoreUtil.isAncestor(this, newAuthor))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAuthor != null)
				msgs = ((InternalEObject)newAuthor).eInverseAdd(this, DataPackage.PERSON__SUBMITTED_SOLUTIONS, Person.class, msgs);
			msgs = basicSetAuthor(newAuthor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.STUDENT_SOLUTION__AUTHOR, newAuthor, newAuthor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<QueryResult> getQueryResults() {
		if (queryResults == null) {
			queryResults = new EObjectContainmentWithInverseEList<QueryResult>(QueryResult.class, this, DataPackage.STUDENT_SOLUTION__QUERY_RESULTS, DataPackage.QUERY_RESULT__SOLUTION);
		}
		return queryResults;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAuthor((Person)otherEnd, msgs);
			case DataPackage.STUDENT_SOLUTION__QUERY_RESULTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getQueryResults()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				return basicSetAuthor(null, msgs);
			case DataPackage.STUDENT_SOLUTION__QUERY_RESULTS:
				return ((InternalEList<?>)getQueryResults()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				return eInternalContainer().eInverseRemove(this, DataPackage.PERSON__SUBMITTED_SOLUTIONS, Person.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				return getAuthor();
			case DataPackage.STUDENT_SOLUTION__QUERY_RESULTS:
				return getQueryResults();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				setAuthor((Person)newValue);
				return;
			case DataPackage.STUDENT_SOLUTION__QUERY_RESULTS:
				getQueryResults().clear();
				getQueryResults().addAll((Collection<? extends QueryResult>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				setAuthor((Person)null);
				return;
			case DataPackage.STUDENT_SOLUTION__QUERY_RESULTS:
				getQueryResults().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.STUDENT_SOLUTION__AUTHOR:
				return getAuthor() != null;
			case DataPackage.STUDENT_SOLUTION__QUERY_RESULTS:
				return queryResults != null && !queryResults.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //StudentSolutionImpl
