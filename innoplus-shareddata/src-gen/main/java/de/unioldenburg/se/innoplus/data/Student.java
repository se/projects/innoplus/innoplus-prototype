/**
 */
package de.unioldenburg.se.innoplus.data;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getStudent()
 * @model
 * @generated
 */
public interface Student extends Person {
} // Student
