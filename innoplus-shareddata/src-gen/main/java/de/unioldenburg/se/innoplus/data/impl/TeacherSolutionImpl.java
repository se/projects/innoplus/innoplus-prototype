/**
 */
package de.unioldenburg.se.innoplus.data.impl;

import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.SolutionCategory;
import de.unioldenburg.se.innoplus.data.TeacherSolution;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Teacher Solution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.TeacherSolutionImpl#getDatabase <em>Database</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.TeacherSolutionImpl#getCategory <em>Category</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TeacherSolutionImpl extends SolutionImpl implements TeacherSolution {
	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected SolutionCategory category;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TeacherSolutionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.TEACHER_SOLUTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InnoplusDatabase getDatabase() {
		if (eContainerFeatureID() != DataPackage.TEACHER_SOLUTION__DATABASE) return null;
		return (InnoplusDatabase)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatabase(InnoplusDatabase newDatabase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDatabase, DataPackage.TEACHER_SOLUTION__DATABASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabase(InnoplusDatabase newDatabase) {
		if (newDatabase != eInternalContainer() || (eContainerFeatureID() != DataPackage.TEACHER_SOLUTION__DATABASE && newDatabase != null)) {
			if (EcoreUtil.isAncestor(this, newDatabase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDatabase != null)
				msgs = ((InternalEObject)newDatabase).eInverseAdd(this, DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS, InnoplusDatabase.class, msgs);
			msgs = basicSetDatabase(newDatabase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.TEACHER_SOLUTION__DATABASE, newDatabase, newDatabase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SolutionCategory getCategory() {
		if (category != null && category.eIsProxy()) {
			InternalEObject oldCategory = (InternalEObject)category;
			category = (SolutionCategory)eResolveProxy(oldCategory);
			if (category != oldCategory) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DataPackage.TEACHER_SOLUTION__CATEGORY, oldCategory, category));
			}
		}
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionCategory basicGetCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCategory(SolutionCategory newCategory, NotificationChain msgs) {
		SolutionCategory oldCategory = category;
		category = newCategory;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DataPackage.TEACHER_SOLUTION__CATEGORY, oldCategory, newCategory);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCategory(SolutionCategory newCategory) {
		if (newCategory != category) {
			NotificationChain msgs = null;
			if (category != null)
				msgs = ((InternalEObject)category).eInverseRemove(this, DataPackage.SOLUTION_CATEGORY__SOLUTIONS, SolutionCategory.class, msgs);
			if (newCategory != null)
				msgs = ((InternalEObject)newCategory).eInverseAdd(this, DataPackage.SOLUTION_CATEGORY__SOLUTIONS, SolutionCategory.class, msgs);
			msgs = basicSetCategory(newCategory, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.TEACHER_SOLUTION__CATEGORY, newCategory, newCategory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDatabase((InnoplusDatabase)otherEnd, msgs);
			case DataPackage.TEACHER_SOLUTION__CATEGORY:
				if (category != null)
					msgs = ((InternalEObject)category).eInverseRemove(this, DataPackage.SOLUTION_CATEGORY__SOLUTIONS, SolutionCategory.class, msgs);
				return basicSetCategory((SolutionCategory)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				return basicSetDatabase(null, msgs);
			case DataPackage.TEACHER_SOLUTION__CATEGORY:
				return basicSetCategory(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				return eInternalContainer().eInverseRemove(this, DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS, InnoplusDatabase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				return getDatabase();
			case DataPackage.TEACHER_SOLUTION__CATEGORY:
				if (resolve) return getCategory();
				return basicGetCategory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				setDatabase((InnoplusDatabase)newValue);
				return;
			case DataPackage.TEACHER_SOLUTION__CATEGORY:
				setCategory((SolutionCategory)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				setDatabase((InnoplusDatabase)null);
				return;
			case DataPackage.TEACHER_SOLUTION__CATEGORY:
				setCategory((SolutionCategory)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.TEACHER_SOLUTION__DATABASE:
				return getDatabase() != null;
			case DataPackage.TEACHER_SOLUTION__CATEGORY:
				return category != null;
		}
		return super.eIsSet(featureID);
	}

} //TeacherSolutionImpl
