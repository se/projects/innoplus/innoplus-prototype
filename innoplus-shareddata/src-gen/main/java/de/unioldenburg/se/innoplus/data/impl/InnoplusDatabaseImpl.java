/**
 */
package de.unioldenburg.se.innoplus.data.impl;

import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.SolutionCategory;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.data.TaskCategory;

import de.unioldenburg.se.innoplus.data.TeacherSolution;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Innoplus Database</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getChildDatabases <em>Child Databases</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getParentDatabase <em>Parent Database</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getPersons <em>Persons</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getLectures <em>Lectures</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getTaskCategories <em>Task Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getSolutionCategories <em>Solution Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getSampleSolutions <em>Sample Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl#getTasks <em>Tasks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InnoplusDatabaseImpl extends MinimalEObjectImpl.Container implements InnoplusDatabase {
	/**
	 * The cached value of the '{@link #getChildDatabases() <em>Child Databases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildDatabases()
	 * @generated
	 * @ordered
	 */
	protected EList<InnoplusDatabase> childDatabases;

	/**
	 * The cached value of the '{@link #getPersons() <em>Persons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPersons()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> persons;

	/**
	 * The cached value of the '{@link #getLectures() <em>Lectures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectures()
	 * @generated
	 * @ordered
	 */
	protected EList<Lecture> lectures;

	/**
	 * The cached value of the '{@link #getTaskCategories() <em>Task Categories</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskCategories()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskCategory> taskCategories;

	/**
	 * The cached value of the '{@link #getSolutionCategories() <em>Solution Categories</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSolutionCategories()
	 * @generated
	 * @ordered
	 */
	protected EList<SolutionCategory> solutionCategories;

	/**
	 * The cached value of the '{@link #getSampleSolutions() <em>Sample Solutions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSampleSolutions()
	 * @generated
	 * @ordered
	 */
	protected EList<TeacherSolution> sampleSolutions;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> tasks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InnoplusDatabaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.INNOPLUS_DATABASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<InnoplusDatabase> getChildDatabases() {
		if (childDatabases == null) {
			childDatabases = new EObjectContainmentWithInverseEList<InnoplusDatabase>(InnoplusDatabase.class, this, DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES, DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE);
		}
		return childDatabases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InnoplusDatabase getParentDatabase() {
		if (eContainerFeatureID() != DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE) return null;
		return (InnoplusDatabase)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentDatabase(InnoplusDatabase newParentDatabase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentDatabase, DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setParentDatabase(InnoplusDatabase newParentDatabase) {
		if (newParentDatabase != eInternalContainer() || (eContainerFeatureID() != DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE && newParentDatabase != null)) {
			if (EcoreUtil.isAncestor(this, newParentDatabase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentDatabase != null)
				msgs = ((InternalEObject)newParentDatabase).eInverseAdd(this, DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES, InnoplusDatabase.class, msgs);
			msgs = basicSetParentDatabase(newParentDatabase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE, newParentDatabase, newParentDatabase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Person> getPersons() {
		if (persons == null) {
			persons = new EObjectContainmentWithInverseEList<Person>(Person.class, this, DataPackage.INNOPLUS_DATABASE__PERSONS, DataPackage.PERSON__DATABASE);
		}
		return persons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Lecture> getLectures() {
		if (lectures == null) {
			lectures = new EObjectContainmentWithInverseEList<Lecture>(Lecture.class, this, DataPackage.INNOPLUS_DATABASE__LECTURES, DataPackage.LECTURE__DATABASE);
		}
		return lectures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TaskCategory> getTaskCategories() {
		if (taskCategories == null) {
			taskCategories = new EObjectContainmentWithInverseEList<TaskCategory>(TaskCategory.class, this, DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES, DataPackage.TASK_CATEGORY__DATABASE);
		}
		return taskCategories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SolutionCategory> getSolutionCategories() {
		if (solutionCategories == null) {
			solutionCategories = new EObjectContainmentWithInverseEList<SolutionCategory>(SolutionCategory.class, this, DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES, DataPackage.SOLUTION_CATEGORY__DATABASE);
		}
		return solutionCategories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TeacherSolution> getSampleSolutions() {
		if (sampleSolutions == null) {
			sampleSolutions = new EObjectContainmentWithInverseEList<TeacherSolution>(TeacherSolution.class, this, DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS, DataPackage.TEACHER_SOLUTION__DATABASE);
		}
		return sampleSolutions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Task> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentWithInverseEList<Task>(Task.class, this, DataPackage.INNOPLUS_DATABASE__TASKS, DataPackage.TASK__DATABASE);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildDatabases()).basicAdd(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentDatabase((InnoplusDatabase)otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__PERSONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPersons()).basicAdd(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__LECTURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLectures()).basicAdd(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTaskCategories()).basicAdd(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSolutionCategories()).basicAdd(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSampleSolutions()).basicAdd(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__TASKS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTasks()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES:
				return ((InternalEList<?>)getChildDatabases()).basicRemove(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				return basicSetParentDatabase(null, msgs);
			case DataPackage.INNOPLUS_DATABASE__PERSONS:
				return ((InternalEList<?>)getPersons()).basicRemove(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__LECTURES:
				return ((InternalEList<?>)getLectures()).basicRemove(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES:
				return ((InternalEList<?>)getTaskCategories()).basicRemove(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES:
				return ((InternalEList<?>)getSolutionCategories()).basicRemove(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS:
				return ((InternalEList<?>)getSampleSolutions()).basicRemove(otherEnd, msgs);
			case DataPackage.INNOPLUS_DATABASE__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				return eInternalContainer().eInverseRemove(this, DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES, InnoplusDatabase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES:
				return getChildDatabases();
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				return getParentDatabase();
			case DataPackage.INNOPLUS_DATABASE__PERSONS:
				return getPersons();
			case DataPackage.INNOPLUS_DATABASE__LECTURES:
				return getLectures();
			case DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES:
				return getTaskCategories();
			case DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES:
				return getSolutionCategories();
			case DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS:
				return getSampleSolutions();
			case DataPackage.INNOPLUS_DATABASE__TASKS:
				return getTasks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES:
				getChildDatabases().clear();
				getChildDatabases().addAll((Collection<? extends InnoplusDatabase>)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				setParentDatabase((InnoplusDatabase)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__PERSONS:
				getPersons().clear();
				getPersons().addAll((Collection<? extends Person>)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__LECTURES:
				getLectures().clear();
				getLectures().addAll((Collection<? extends Lecture>)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES:
				getTaskCategories().clear();
				getTaskCategories().addAll((Collection<? extends TaskCategory>)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES:
				getSolutionCategories().clear();
				getSolutionCategories().addAll((Collection<? extends SolutionCategory>)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS:
				getSampleSolutions().clear();
				getSampleSolutions().addAll((Collection<? extends TeacherSolution>)newValue);
				return;
			case DataPackage.INNOPLUS_DATABASE__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends Task>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES:
				getChildDatabases().clear();
				return;
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				setParentDatabase((InnoplusDatabase)null);
				return;
			case DataPackage.INNOPLUS_DATABASE__PERSONS:
				getPersons().clear();
				return;
			case DataPackage.INNOPLUS_DATABASE__LECTURES:
				getLectures().clear();
				return;
			case DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES:
				getTaskCategories().clear();
				return;
			case DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES:
				getSolutionCategories().clear();
				return;
			case DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS:
				getSampleSolutions().clear();
				return;
			case DataPackage.INNOPLUS_DATABASE__TASKS:
				getTasks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.INNOPLUS_DATABASE__CHILD_DATABASES:
				return childDatabases != null && !childDatabases.isEmpty();
			case DataPackage.INNOPLUS_DATABASE__PARENT_DATABASE:
				return getParentDatabase() != null;
			case DataPackage.INNOPLUS_DATABASE__PERSONS:
				return persons != null && !persons.isEmpty();
			case DataPackage.INNOPLUS_DATABASE__LECTURES:
				return lectures != null && !lectures.isEmpty();
			case DataPackage.INNOPLUS_DATABASE__TASK_CATEGORIES:
				return taskCategories != null && !taskCategories.isEmpty();
			case DataPackage.INNOPLUS_DATABASE__SOLUTION_CATEGORIES:
				return solutionCategories != null && !solutionCategories.isEmpty();
			case DataPackage.INNOPLUS_DATABASE__SAMPLE_SOLUTIONS:
				return sampleSolutions != null && !sampleSolutions.isEmpty();
			case DataPackage.INNOPLUS_DATABASE__TASKS:
				return tasks != null && !tasks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InnoplusDatabaseImpl
