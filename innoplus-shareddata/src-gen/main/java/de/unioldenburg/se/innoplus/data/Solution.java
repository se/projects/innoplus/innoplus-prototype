/**
 */
package de.unioldenburg.se.innoplus.data;

import java.util.Date;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Solution#getTask <em>Task</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Solution#getContents <em>Contents</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Solution#getDate <em>Date</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolution()
 * @model abstract="true"
 * @generated
 */
public interface Solution extends EObject {
	/**
	 * Returns the value of the '<em><b>Task</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Task#getSolutions <em>Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task</em>' reference.
	 * @see #setTask(Task)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolution_Task()
	 * @see de.unioldenburg.se.innoplus.data.Task#getSolutions
	 * @model opposite="solutions" required="true"
	 * @generated
	 */
	Task getTask();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Solution#getTask <em>Task</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task</em>' reference.
	 * @see #getTask()
	 * @generated
	 */
	void setTask(Task value);

	/**
	 * Returns the value of the '<em><b>Contents</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getSolution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contents</em>' containment reference.
	 * @see #setContents(SolutionContent)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolution_Contents()
	 * @see de.unioldenburg.se.innoplus.data.SolutionContent#getSolution
	 * @model opposite="solution" containment="true" required="true"
	 * @generated
	 */
	SolutionContent getContents();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Solution#getContents <em>Contents</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contents</em>' containment reference.
	 * @see #getContents()
	 * @generated
	 */
	void setContents(SolutionContent value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolution_Date()
	 * @model required="true"
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Solution#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

} // Solution
