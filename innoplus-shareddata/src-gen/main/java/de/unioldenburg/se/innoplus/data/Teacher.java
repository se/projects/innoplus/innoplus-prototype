/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Teacher</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Teacher#getCreatedTasks <em>Created Tasks</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTeacher()
 * @model
 * @generated
 */
public interface Teacher extends Person {
	/**
	 * Returns the value of the '<em><b>Created Tasks</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Task}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Task#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Created Tasks</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTeacher_CreatedTasks()
	 * @see de.unioldenburg.se.innoplus.data.Task#getAuthor
	 * @model opposite="author"
	 * @generated
	 */
	EList<Task> getCreatedTasks();

} // Teacher
