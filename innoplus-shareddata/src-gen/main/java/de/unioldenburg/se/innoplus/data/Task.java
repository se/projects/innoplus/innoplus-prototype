/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getName <em>Name</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getDescription <em>Description</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getAuthor <em>Author</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getSolutions <em>Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getCategory <em>Category</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getUsedInLectures <em>Used In Lectures</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getDatabase <em>Database</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Task#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Task#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Task#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Author</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Teacher#getCreatedTasks <em>Created Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Author</em>' reference.
	 * @see #setAuthor(Teacher)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Author()
	 * @see de.unioldenburg.se.innoplus.data.Teacher#getCreatedTasks
	 * @model opposite="createdTasks" required="true"
	 * @generated
	 */
	Teacher getAuthor();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Task#getAuthor <em>Author</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' reference.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(Teacher value);

	/**
	 * Returns the value of the '<em><b>Solutions</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Solution}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Solution#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solutions</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Solutions()
	 * @see de.unioldenburg.se.innoplus.data.Solution#getTask
	 * @model opposite="task"
	 * @generated
	 */
	EList<Solution> getSolutions();

	/**
	 * Returns the value of the '<em><b>Category</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' reference.
	 * @see #setCategory(TaskCategory)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Category()
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getTasks
	 * @model opposite="tasks" required="true"
	 * @generated
	 */
	TaskCategory getCategory();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Task#getCategory <em>Category</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' reference.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(TaskCategory value);

	/**
	 * Returns the value of the '<em><b>Used In Lectures</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Lecture}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Lecture#getSelectedTasks <em>Selected Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used In Lectures</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_UsedInLectures()
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getSelectedTasks
	 * @model opposite="selectedTasks"
	 * @generated
	 */
	EList<Lecture> getUsedInLectures();

	/**
	 * Returns the value of the '<em><b>Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' container reference.
	 * @see #setDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Database()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTasks
	 * @model opposite="tasks" required="true" transient="false"
	 * @generated
	 */
	InnoplusDatabase getDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Task#getDatabase <em>Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' container reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(InnoplusDatabase value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTask_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Task#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

} // Task
