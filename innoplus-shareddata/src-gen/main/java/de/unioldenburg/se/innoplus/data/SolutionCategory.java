/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getName <em>Name</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getSolutions <em>Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getTaskCategories <em>Task Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionCategory()
 * @model
 * @generated
 */
public interface SolutionCategory extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionCategory_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Solutions</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.TeacherSolution}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solutions</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionCategory_Solutions()
	 * @see de.unioldenburg.se.innoplus.data.TeacherSolution#getCategory
	 * @model opposite="category"
	 * @generated
	 */
	EList<TeacherSolution> getSolutions();

	/**
	 * Returns the value of the '<em><b>Task Categories</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.TaskCategory}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getSolutionCategories <em>Solution Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Categories</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionCategory_TaskCategories()
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getSolutionCategories
	 * @model opposite="solutionCategories"
	 * @generated
	 */
	EList<TaskCategory> getTaskCategories();

	/**
	 * Returns the value of the '<em><b>Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSolutionCategories <em>Solution Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' container reference.
	 * @see #setDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionCategory_Database()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSolutionCategories
	 * @model opposite="solutionCategories" required="true" transient="false"
	 * @generated
	 */
	InnoplusDatabase getDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getDatabase <em>Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' container reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(InnoplusDatabase value);

} // SolutionCategory
