/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.TaskCategory#getName <em>Name</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.TaskCategory#getTasks <em>Tasks</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.TaskCategory#getSolutionCategories <em>Solution Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.TaskCategory#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTaskCategory()
 * @model
 * @generated
 */
public interface TaskCategory extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTaskCategory_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Task}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Task#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTaskCategory_Tasks()
	 * @see de.unioldenburg.se.innoplus.data.Task#getCategory
	 * @model opposite="category"
	 * @generated
	 */
	EList<Task> getTasks();

	/**
	 * Returns the value of the '<em><b>Solution Categories</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.SolutionCategory}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getTaskCategories <em>Task Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solution Categories</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTaskCategory_SolutionCategories()
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getTaskCategories
	 * @model opposite="taskCategories"
	 * @generated
	 */
	EList<SolutionCategory> getSolutionCategories();

	/**
	 * Returns the value of the '<em><b>Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTaskCategories <em>Task Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' container reference.
	 * @see #setDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTaskCategory_Database()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTaskCategories
	 * @model opposite="taskCategories" required="true" transient="false"
	 * @generated
	 */
	InnoplusDatabase getDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getDatabase <em>Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' container reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(InnoplusDatabase value);

} // TaskCategory
