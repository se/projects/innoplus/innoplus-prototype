/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lecture</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Lecture#getName <em>Name</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Lecture#getSelectedTasks <em>Selected Tasks</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Lecture#getParticipants <em>Participants</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Lecture#getDatabase <em>Database</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Lecture#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getLecture()
 * @model
 * @generated
 */
public interface Lecture extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getLecture_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Lecture#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Selected Tasks</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Task}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Task#getUsedInLectures <em>Used In Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Tasks</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getLecture_SelectedTasks()
	 * @see de.unioldenburg.se.innoplus.data.Task#getUsedInLectures
	 * @model opposite="usedInLectures"
	 * @generated
	 */
	EList<Task> getSelectedTasks();

	/**
	 * Returns the value of the '<em><b>Participants</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Person}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Person#getLectures <em>Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participants</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getLecture_Participants()
	 * @see de.unioldenburg.se.innoplus.data.Person#getLectures
	 * @model opposite="lectures"
	 * @generated
	 */
	EList<Person> getParticipants();

	/**
	 * Returns the value of the '<em><b>Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getLectures <em>Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' container reference.
	 * @see #setDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getLecture_Database()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getLectures
	 * @model opposite="lectures" required="true" transient="false"
	 * @generated
	 */
	InnoplusDatabase getDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Lecture#getDatabase <em>Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' container reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(InnoplusDatabase value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getLecture_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Lecture#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

} // Lecture
