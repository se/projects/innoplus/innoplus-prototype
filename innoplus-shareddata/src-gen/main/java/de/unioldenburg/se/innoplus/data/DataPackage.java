/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.unioldenburg.se.innoplus.data.DataFactory
 * @model kind="package"
 * @generated
 */
public interface DataPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "data";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://innoplus2/data";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "data";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DataPackage eINSTANCE = de.unioldenburg.se.innoplus.data.impl.DataPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl <em>Innoplus Database</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getInnoplusDatabase()
	 * @generated
	 */
	int INNOPLUS_DATABASE = 0;

	/**
	 * The feature id for the '<em><b>Child Databases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__CHILD_DATABASES = 0;

	/**
	 * The feature id for the '<em><b>Parent Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__PARENT_DATABASE = 1;

	/**
	 * The feature id for the '<em><b>Persons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__PERSONS = 2;

	/**
	 * The feature id for the '<em><b>Lectures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__LECTURES = 3;

	/**
	 * The feature id for the '<em><b>Task Categories</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__TASK_CATEGORIES = 4;

	/**
	 * The feature id for the '<em><b>Solution Categories</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__SOLUTION_CATEGORIES = 5;

	/**
	 * The feature id for the '<em><b>Sample Solutions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__SAMPLE_SOLUTIONS = 6;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE__TASKS = 7;

	/**
	 * The number of structural features of the '<em>Innoplus Database</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Innoplus Database</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INNOPLUS_DATABASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.TaskImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__AUTHOR = 2;

	/**
	 * The feature id for the '<em><b>Solutions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SOLUTIONS = 3;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__CATEGORY = 4;

	/**
	 * The feature id for the '<em><b>Used In Lectures</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__USED_IN_LECTURES = 5;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__DATABASE = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__ID = 7;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl <em>Task Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTaskCategory()
	 * @generated
	 */
	int TASK_CATEGORY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CATEGORY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CATEGORY__TASKS = 1;

	/**
	 * The feature id for the '<em><b>Solution Categories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CATEGORY__SOLUTION_CATEGORIES = 2;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CATEGORY__DATABASE = 3;

	/**
	 * The number of structural features of the '<em>Task Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CATEGORY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Task Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_CATEGORY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.SolutionImpl <em>Solution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.SolutionImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getSolution()
	 * @generated
	 */
	int SOLUTION = 3;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__TASK = 0;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__CONTENTS = 1;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__DATE = 2;

	/**
	 * The number of structural features of the '<em>Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl <em>Solution Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getSolutionCategory()
	 * @generated
	 */
	int SOLUTION_CATEGORY = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CATEGORY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Solutions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CATEGORY__SOLUTIONS = 1;

	/**
	 * The feature id for the '<em><b>Task Categories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CATEGORY__TASK_CATEGORIES = 2;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CATEGORY__DATABASE = 3;

	/**
	 * The number of structural features of the '<em>Solution Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CATEGORY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Solution Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CATEGORY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.PersonImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__ID = 0;

	/**
	 * The feature id for the '<em><b>Submitted Solutions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__SUBMITTED_SOLUTIONS = 1;

	/**
	 * The feature id for the '<em><b>Lectures</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__LECTURES = 2;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__DATABASE = 3;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.StudentImpl <em>Student</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.StudentImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getStudent()
	 * @generated
	 */
	int STUDENT = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__ID = PERSON__ID;

	/**
	 * The feature id for the '<em><b>Submitted Solutions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__SUBMITTED_SOLUTIONS = PERSON__SUBMITTED_SOLUTIONS;

	/**
	 * The feature id for the '<em><b>Lectures</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__LECTURES = PERSON__LECTURES;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__DATABASE = PERSON__DATABASE;

	/**
	 * The number of structural features of the '<em>Student</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_FEATURE_COUNT = PERSON_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Student</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.TeacherImpl <em>Teacher</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.TeacherImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTeacher()
	 * @generated
	 */
	int TEACHER = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER__ID = PERSON__ID;

	/**
	 * The feature id for the '<em><b>Submitted Solutions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER__SUBMITTED_SOLUTIONS = PERSON__SUBMITTED_SOLUTIONS;

	/**
	 * The feature id for the '<em><b>Lectures</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER__LECTURES = PERSON__LECTURES;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER__DATABASE = PERSON__DATABASE;

	/**
	 * The feature id for the '<em><b>Created Tasks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER__CREATED_TASKS = PERSON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Teacher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_FEATURE_COUNT = PERSON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Teacher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.TeacherSolutionImpl <em>Teacher Solution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.TeacherSolutionImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTeacherSolution()
	 * @generated
	 */
	int TEACHER_SOLUTION = 8;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION__TASK = SOLUTION__TASK;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION__CONTENTS = SOLUTION__CONTENTS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION__DATE = SOLUTION__DATE;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION__DATABASE = SOLUTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Category</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION__CATEGORY = SOLUTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Teacher Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION_FEATURE_COUNT = SOLUTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Teacher Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEACHER_SOLUTION_OPERATION_COUNT = SOLUTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.StudentSolutionImpl <em>Student Solution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.StudentSolutionImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getStudentSolution()
	 * @generated
	 */
	int STUDENT_SOLUTION = 9;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION__TASK = SOLUTION__TASK;

	/**
	 * The feature id for the '<em><b>Contents</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION__CONTENTS = SOLUTION__CONTENTS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION__DATE = SOLUTION__DATE;

	/**
	 * The feature id for the '<em><b>Author</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION__AUTHOR = SOLUTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Query Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION__QUERY_RESULTS = SOLUTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Student Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION_FEATURE_COUNT = SOLUTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Student Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_SOLUTION_OPERATION_COUNT = SOLUTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.SolutionContentImpl <em>Solution Content</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.SolutionContentImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getSolutionContent()
	 * @generated
	 */
	int SOLUTION_CONTENT = 10;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CONTENT__PATH = 0;

	/**
	 * The feature id for the '<em><b>Solution</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CONTENT__SOLUTION = 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CONTENT__CONTENT = 2;

	/**
	 * The number of structural features of the '<em>Solution Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CONTENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Solution Content</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_CONTENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.LectureImpl <em>Lecture</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.LectureImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getLecture()
	 * @generated
	 */
	int LECTURE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Selected Tasks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE__SELECTED_TASKS = 1;

	/**
	 * The feature id for the '<em><b>Participants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE__PARTICIPANTS = 2;

	/**
	 * The feature id for the '<em><b>Database</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE__DATABASE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE__ID = 4;

	/**
	 * The number of structural features of the '<em>Lecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Lecture</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.impl.QueryResultImpl <em>Query Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.impl.QueryResultImpl
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getQueryResult()
	 * @generated
	 */
	int QUERY_RESULT = 12;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT__DATE = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Solution</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT__SOLUTION = 2;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT__LOCATION = 3;

	/**
	 * The feature id for the '<em><b>Summary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT__SUMMARY = 4;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT__DESCRIPTION = 5;

	/**
	 * The number of structural features of the '<em>Query Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Query Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_RESULT_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link de.unioldenburg.se.innoplus.data.QueryResultType <em>Query Result Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.unioldenburg.se.innoplus.data.QueryResultType
	 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getQueryResultType()
	 * @generated
	 */
	int QUERY_RESULT_TYPE = 13;


	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase <em>Innoplus Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Innoplus Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase
	 * @generated
	 */
	EClass getInnoplusDatabase();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getChildDatabases <em>Child Databases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child Databases</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getChildDatabases()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_ChildDatabases();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getParentDatabase <em>Parent Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getParentDatabase()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_ParentDatabase();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Persons</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getPersons()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_Persons();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getLectures <em>Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lectures</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getLectures()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_Lectures();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTaskCategories <em>Task Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Task Categories</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTaskCategories()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_TaskCategories();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSolutionCategories <em>Solution Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Solution Categories</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSolutionCategories()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_SolutionCategories();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSampleSolutions <em>Sample Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sample Solutions</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSampleSolutions()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_SampleSolutions();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTasks()
	 * @see #getInnoplusDatabase()
	 * @generated
	 */
	EReference getInnoplusDatabase_Tasks();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Task#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getName()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Task#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getDescription()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Description();

	/**
	 * Returns the meta object for the reference '{@link de.unioldenburg.se.innoplus.data.Task#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Author</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getAuthor()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Author();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.Task#getSolutions <em>Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Solutions</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getSolutions()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Solutions();

	/**
	 * Returns the meta object for the reference '{@link de.unioldenburg.se.innoplus.data.Task#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Category</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getCategory()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Category();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.Task#getUsedInLectures <em>Used In Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Used In Lectures</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getUsedInLectures()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_UsedInLectures();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.Task#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getDatabase()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Database();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Task#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Task#getId()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Id();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.TaskCategory <em>Task Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Category</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory
	 * @generated
	 */
	EClass getTaskCategory();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getName()
	 * @see #getTaskCategory()
	 * @generated
	 */
	EAttribute getTaskCategory_Name();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tasks</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getTasks()
	 * @see #getTaskCategory()
	 * @generated
	 */
	EReference getTaskCategory_Tasks();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getSolutionCategories <em>Solution Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Solution Categories</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getSolutionCategories()
	 * @see #getTaskCategory()
	 * @generated
	 */
	EReference getTaskCategory_SolutionCategories();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getDatabase()
	 * @see #getTaskCategory()
	 * @generated
	 */
	EReference getTaskCategory_Database();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.Solution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Solution
	 * @generated
	 */
	EClass getSolution();

	/**
	 * Returns the meta object for the reference '{@link de.unioldenburg.se.innoplus.data.Solution#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Solution#getTask()
	 * @see #getSolution()
	 * @generated
	 */
	EReference getSolution_Task();

	/**
	 * Returns the meta object for the containment reference '{@link de.unioldenburg.se.innoplus.data.Solution#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Contents</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Solution#getContents()
	 * @see #getSolution()
	 * @generated
	 */
	EReference getSolution_Contents();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Solution#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Solution#getDate()
	 * @see #getSolution()
	 * @generated
	 */
	EAttribute getSolution_Date();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.SolutionCategory <em>Solution Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution Category</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory
	 * @generated
	 */
	EClass getSolutionCategory();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getName()
	 * @see #getSolutionCategory()
	 * @generated
	 */
	EAttribute getSolutionCategory_Name();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getSolutions <em>Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Solutions</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getSolutions()
	 * @see #getSolutionCategory()
	 * @generated
	 */
	EReference getSolutionCategory_Solutions();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getTaskCategories <em>Task Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Task Categories</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getTaskCategories()
	 * @see #getSolutionCategory()
	 * @generated
	 */
	EReference getSolutionCategory_TaskCategories();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getDatabase()
	 * @see #getSolutionCategory()
	 * @generated
	 */
	EReference getSolutionCategory_Database();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Person#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Person#getId()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Id();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.Person#getSubmittedSolutions <em>Submitted Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Submitted Solutions</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Person#getSubmittedSolutions()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_SubmittedSolutions();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.Person#getLectures <em>Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Lectures</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Person#getLectures()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_Lectures();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.Person#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Person#getDatabase()
	 * @see #getPerson()
	 * @generated
	 */
	EReference getPerson_Database();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.Student <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Student</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Student
	 * @generated
	 */
	EClass getStudent();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.Teacher <em>Teacher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Teacher</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Teacher
	 * @generated
	 */
	EClass getTeacher();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.Teacher#getCreatedTasks <em>Created Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Created Tasks</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Teacher#getCreatedTasks()
	 * @see #getTeacher()
	 * @generated
	 */
	EReference getTeacher_CreatedTasks();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.TeacherSolution <em>Teacher Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Teacher Solution</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TeacherSolution
	 * @generated
	 */
	EClass getTeacherSolution();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TeacherSolution#getDatabase()
	 * @see #getTeacherSolution()
	 * @generated
	 */
	EReference getTeacherSolution_Database();

	/**
	 * Returns the meta object for the reference '{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Category</em>'.
	 * @see de.unioldenburg.se.innoplus.data.TeacherSolution#getCategory()
	 * @see #getTeacherSolution()
	 * @generated
	 */
	EReference getTeacherSolution_Category();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.StudentSolution <em>Student Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Student Solution</em>'.
	 * @see de.unioldenburg.se.innoplus.data.StudentSolution
	 * @generated
	 */
	EClass getStudentSolution();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.StudentSolution#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Author</em>'.
	 * @see de.unioldenburg.se.innoplus.data.StudentSolution#getAuthor()
	 * @see #getStudentSolution()
	 * @generated
	 */
	EReference getStudentSolution_Author();

	/**
	 * Returns the meta object for the containment reference list '{@link de.unioldenburg.se.innoplus.data.StudentSolution#getQueryResults <em>Query Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Query Results</em>'.
	 * @see de.unioldenburg.se.innoplus.data.StudentSolution#getQueryResults()
	 * @see #getStudentSolution()
	 * @generated
	 */
	EReference getStudentSolution_QueryResults();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.SolutionContent <em>Solution Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution Content</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionContent
	 * @generated
	 */
	EClass getSolutionContent();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionContent#getPath()
	 * @see #getSolutionContent()
	 * @generated
	 */
	EAttribute getSolutionContent_Path();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getSolution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Solution</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionContent#getSolution()
	 * @see #getSolutionContent()
	 * @generated
	 */
	EReference getSolutionContent_Solution();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see de.unioldenburg.se.innoplus.data.SolutionContent#getContent()
	 * @see #getSolutionContent()
	 * @generated
	 */
	EAttribute getSolutionContent_Content();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.Lecture <em>Lecture</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lecture</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Lecture
	 * @generated
	 */
	EClass getLecture();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Lecture#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getName()
	 * @see #getLecture()
	 * @generated
	 */
	EAttribute getLecture_Name();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.Lecture#getSelectedTasks <em>Selected Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Selected Tasks</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getSelectedTasks()
	 * @see #getLecture()
	 * @generated
	 */
	EReference getLecture_SelectedTasks();

	/**
	 * Returns the meta object for the reference list '{@link de.unioldenburg.se.innoplus.data.Lecture#getParticipants <em>Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participants</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getParticipants()
	 * @see #getLecture()
	 * @generated
	 */
	EReference getLecture_Participants();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.Lecture#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Database</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getDatabase()
	 * @see #getLecture()
	 * @generated
	 */
	EReference getLecture_Database();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.Lecture#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getId()
	 * @see #getLecture()
	 * @generated
	 */
	EAttribute getLecture_Id();

	/**
	 * Returns the meta object for class '{@link de.unioldenburg.se.innoplus.data.QueryResult <em>Query Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Query Result</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult
	 * @generated
	 */
	EClass getQueryResult();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.QueryResult#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getDate()
	 * @see #getQueryResult()
	 * @generated
	 */
	EAttribute getQueryResult_Date();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.QueryResult#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getType()
	 * @see #getQueryResult()
	 * @generated
	 */
	EAttribute getQueryResult_Type();

	/**
	 * Returns the meta object for the container reference '{@link de.unioldenburg.se.innoplus.data.QueryResult#getSolution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Solution</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getSolution()
	 * @see #getQueryResult()
	 * @generated
	 */
	EReference getQueryResult_Solution();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.QueryResult#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getLocation()
	 * @see #getQueryResult()
	 * @generated
	 */
	EAttribute getQueryResult_Location();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.QueryResult#getSummary <em>Summary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Summary</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getSummary()
	 * @see #getQueryResult()
	 * @generated
	 */
	EAttribute getQueryResult_Summary();

	/**
	 * Returns the meta object for the attribute '{@link de.unioldenburg.se.innoplus.data.QueryResult#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getDescription()
	 * @see #getQueryResult()
	 * @generated
	 */
	EAttribute getQueryResult_Description();

	/**
	 * Returns the meta object for enum '{@link de.unioldenburg.se.innoplus.data.QueryResultType <em>Query Result Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Query Result Type</em>'.
	 * @see de.unioldenburg.se.innoplus.data.QueryResultType
	 * @generated
	 */
	EEnum getQueryResultType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DataFactory getDataFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl <em>Innoplus Database</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.InnoplusDatabaseImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getInnoplusDatabase()
		 * @generated
		 */
		EClass INNOPLUS_DATABASE = eINSTANCE.getInnoplusDatabase();

		/**
		 * The meta object literal for the '<em><b>Child Databases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__CHILD_DATABASES = eINSTANCE.getInnoplusDatabase_ChildDatabases();

		/**
		 * The meta object literal for the '<em><b>Parent Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__PARENT_DATABASE = eINSTANCE.getInnoplusDatabase_ParentDatabase();

		/**
		 * The meta object literal for the '<em><b>Persons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__PERSONS = eINSTANCE.getInnoplusDatabase_Persons();

		/**
		 * The meta object literal for the '<em><b>Lectures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__LECTURES = eINSTANCE.getInnoplusDatabase_Lectures();

		/**
		 * The meta object literal for the '<em><b>Task Categories</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__TASK_CATEGORIES = eINSTANCE.getInnoplusDatabase_TaskCategories();

		/**
		 * The meta object literal for the '<em><b>Solution Categories</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__SOLUTION_CATEGORIES = eINSTANCE.getInnoplusDatabase_SolutionCategories();

		/**
		 * The meta object literal for the '<em><b>Sample Solutions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__SAMPLE_SOLUTIONS = eINSTANCE.getInnoplusDatabase_SampleSolutions();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INNOPLUS_DATABASE__TASKS = eINSTANCE.getInnoplusDatabase_Tasks();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.TaskImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__NAME = eINSTANCE.getTask_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__DESCRIPTION = eINSTANCE.getTask_Description();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__AUTHOR = eINSTANCE.getTask_Author();

		/**
		 * The meta object literal for the '<em><b>Solutions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__SOLUTIONS = eINSTANCE.getTask_Solutions();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__CATEGORY = eINSTANCE.getTask_Category();

		/**
		 * The meta object literal for the '<em><b>Used In Lectures</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__USED_IN_LECTURES = eINSTANCE.getTask_UsedInLectures();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__DATABASE = eINSTANCE.getTask_Database();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__ID = eINSTANCE.getTask_Id();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl <em>Task Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.TaskCategoryImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTaskCategory()
		 * @generated
		 */
		EClass TASK_CATEGORY = eINSTANCE.getTaskCategory();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK_CATEGORY__NAME = eINSTANCE.getTaskCategory_Name();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_CATEGORY__TASKS = eINSTANCE.getTaskCategory_Tasks();

		/**
		 * The meta object literal for the '<em><b>Solution Categories</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_CATEGORY__SOLUTION_CATEGORIES = eINSTANCE.getTaskCategory_SolutionCategories();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_CATEGORY__DATABASE = eINSTANCE.getTaskCategory_Database();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.SolutionImpl <em>Solution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.SolutionImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getSolution()
		 * @generated
		 */
		EClass SOLUTION = eINSTANCE.getSolution();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION__TASK = eINSTANCE.getSolution_Task();

		/**
		 * The meta object literal for the '<em><b>Contents</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION__CONTENTS = eINSTANCE.getSolution_Contents();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLUTION__DATE = eINSTANCE.getSolution_Date();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl <em>Solution Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.SolutionCategoryImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getSolutionCategory()
		 * @generated
		 */
		EClass SOLUTION_CATEGORY = eINSTANCE.getSolutionCategory();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLUTION_CATEGORY__NAME = eINSTANCE.getSolutionCategory_Name();

		/**
		 * The meta object literal for the '<em><b>Solutions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_CATEGORY__SOLUTIONS = eINSTANCE.getSolutionCategory_Solutions();

		/**
		 * The meta object literal for the '<em><b>Task Categories</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_CATEGORY__TASK_CATEGORIES = eINSTANCE.getSolutionCategory_TaskCategories();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_CATEGORY__DATABASE = eINSTANCE.getSolutionCategory_Database();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.PersonImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__ID = eINSTANCE.getPerson_Id();

		/**
		 * The meta object literal for the '<em><b>Submitted Solutions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__SUBMITTED_SOLUTIONS = eINSTANCE.getPerson_SubmittedSolutions();

		/**
		 * The meta object literal for the '<em><b>Lectures</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__LECTURES = eINSTANCE.getPerson_Lectures();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON__DATABASE = eINSTANCE.getPerson_Database();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.StudentImpl <em>Student</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.StudentImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getStudent()
		 * @generated
		 */
		EClass STUDENT = eINSTANCE.getStudent();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.TeacherImpl <em>Teacher</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.TeacherImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTeacher()
		 * @generated
		 */
		EClass TEACHER = eINSTANCE.getTeacher();

		/**
		 * The meta object literal for the '<em><b>Created Tasks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEACHER__CREATED_TASKS = eINSTANCE.getTeacher_CreatedTasks();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.TeacherSolutionImpl <em>Teacher Solution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.TeacherSolutionImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getTeacherSolution()
		 * @generated
		 */
		EClass TEACHER_SOLUTION = eINSTANCE.getTeacherSolution();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEACHER_SOLUTION__DATABASE = eINSTANCE.getTeacherSolution_Database();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEACHER_SOLUTION__CATEGORY = eINSTANCE.getTeacherSolution_Category();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.StudentSolutionImpl <em>Student Solution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.StudentSolutionImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getStudentSolution()
		 * @generated
		 */
		EClass STUDENT_SOLUTION = eINSTANCE.getStudentSolution();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDENT_SOLUTION__AUTHOR = eINSTANCE.getStudentSolution_Author();

		/**
		 * The meta object literal for the '<em><b>Query Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDENT_SOLUTION__QUERY_RESULTS = eINSTANCE.getStudentSolution_QueryResults();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.SolutionContentImpl <em>Solution Content</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.SolutionContentImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getSolutionContent()
		 * @generated
		 */
		EClass SOLUTION_CONTENT = eINSTANCE.getSolutionContent();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLUTION_CONTENT__PATH = eINSTANCE.getSolutionContent_Path();

		/**
		 * The meta object literal for the '<em><b>Solution</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_CONTENT__SOLUTION = eINSTANCE.getSolutionContent_Solution();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLUTION_CONTENT__CONTENT = eINSTANCE.getSolutionContent_Content();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.LectureImpl <em>Lecture</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.LectureImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getLecture()
		 * @generated
		 */
		EClass LECTURE = eINSTANCE.getLecture();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LECTURE__NAME = eINSTANCE.getLecture_Name();

		/**
		 * The meta object literal for the '<em><b>Selected Tasks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LECTURE__SELECTED_TASKS = eINSTANCE.getLecture_SelectedTasks();

		/**
		 * The meta object literal for the '<em><b>Participants</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LECTURE__PARTICIPANTS = eINSTANCE.getLecture_Participants();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LECTURE__DATABASE = eINSTANCE.getLecture_Database();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LECTURE__ID = eINSTANCE.getLecture_Id();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.impl.QueryResultImpl <em>Query Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.impl.QueryResultImpl
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getQueryResult()
		 * @generated
		 */
		EClass QUERY_RESULT = eINSTANCE.getQueryResult();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_RESULT__DATE = eINSTANCE.getQueryResult_Date();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_RESULT__TYPE = eINSTANCE.getQueryResult_Type();

		/**
		 * The meta object literal for the '<em><b>Solution</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUERY_RESULT__SOLUTION = eINSTANCE.getQueryResult_Solution();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_RESULT__LOCATION = eINSTANCE.getQueryResult_Location();

		/**
		 * The meta object literal for the '<em><b>Summary</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_RESULT__SUMMARY = eINSTANCE.getQueryResult_Summary();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_RESULT__DESCRIPTION = eINSTANCE.getQueryResult_Description();

		/**
		 * The meta object literal for the '{@link de.unioldenburg.se.innoplus.data.QueryResultType <em>Query Result Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.unioldenburg.se.innoplus.data.QueryResultType
		 * @see de.unioldenburg.se.innoplus.data.impl.DataPackageImpl#getQueryResultType()
		 * @generated
		 */
		EEnum QUERY_RESULT_TYPE = eINSTANCE.getQueryResultType();

	}

} //DataPackage
