/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution Content</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionContent#getPath <em>Path</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionContent#getSolution <em>Solution</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.SolutionContent#getContent <em>Content</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionContent()
 * @model
 * @generated
 */
public interface SolutionContent extends EObject {
	/**
	 * Returns the value of the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * can point to one file XOR one folder
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Path</em>' attribute.
	 * @see #setPath(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionContent_Path()
	 * @model required="true"
	 * @generated
	 */
	String getPath();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getPath <em>Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Path</em>' attribute.
	 * @see #getPath()
	 * @generated
	 */
	void setPath(String value);

	/**
	 * Returns the value of the '<em><b>Solution</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Solution#getContents <em>Contents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solution</em>' container reference.
	 * @see #setSolution(Solution)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionContent_Solution()
	 * @see de.unioldenburg.se.innoplus.data.Solution#getContents
	 * @model opposite="contents" required="true" transient="false"
	 * @generated
	 */
	Solution getSolution();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getSolution <em>Solution</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Solution</em>' container reference.
	 * @see #getSolution()
	 * @generated
	 */
	void setSolution(Solution value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(Object)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getSolutionContent_Content()
	 * @model
	 * @generated
	 */
	Object getContent();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.SolutionContent#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(Object value);

} // SolutionContent
