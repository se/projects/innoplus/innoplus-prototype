/**
 */
package de.unioldenburg.se.innoplus.data.impl;

import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.StudentSolution;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.PersonImpl#getId <em>Id</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.PersonImpl#getSubmittedSolutions <em>Submitted Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.PersonImpl#getLectures <em>Lectures</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.impl.PersonImpl#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PersonImpl extends MinimalEObjectImpl.Container implements Person {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubmittedSolutions() <em>Submitted Solutions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubmittedSolutions()
	 * @generated
	 * @ordered
	 */
	protected EList<StudentSolution> submittedSolutions;

	/**
	 * The cached value of the '{@link #getLectures() <em>Lectures</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectures()
	 * @generated
	 * @ordered
	 */
	protected EList<Lecture> lectures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.PERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.PERSON__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<StudentSolution> getSubmittedSolutions() {
		if (submittedSolutions == null) {
			submittedSolutions = new EObjectContainmentWithInverseEList<StudentSolution>(StudentSolution.class, this, DataPackage.PERSON__SUBMITTED_SOLUTIONS, DataPackage.STUDENT_SOLUTION__AUTHOR);
		}
		return submittedSolutions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Lecture> getLectures() {
		if (lectures == null) {
			lectures = new EObjectWithInverseResolvingEList.ManyInverse<Lecture>(Lecture.class, this, DataPackage.PERSON__LECTURES, DataPackage.LECTURE__PARTICIPANTS);
		}
		return lectures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InnoplusDatabase getDatabase() {
		if (eContainerFeatureID() != DataPackage.PERSON__DATABASE) return null;
		return (InnoplusDatabase)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatabase(InnoplusDatabase newDatabase, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newDatabase, DataPackage.PERSON__DATABASE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDatabase(InnoplusDatabase newDatabase) {
		if (newDatabase != eInternalContainer() || (eContainerFeatureID() != DataPackage.PERSON__DATABASE && newDatabase != null)) {
			if (EcoreUtil.isAncestor(this, newDatabase))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDatabase != null)
				msgs = ((InternalEObject)newDatabase).eInverseAdd(this, DataPackage.INNOPLUS_DATABASE__PERSONS, InnoplusDatabase.class, msgs);
			msgs = basicSetDatabase(newDatabase, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DataPackage.PERSON__DATABASE, newDatabase, newDatabase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.PERSON__SUBMITTED_SOLUTIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubmittedSolutions()).basicAdd(otherEnd, msgs);
			case DataPackage.PERSON__LECTURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLectures()).basicAdd(otherEnd, msgs);
			case DataPackage.PERSON__DATABASE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetDatabase((InnoplusDatabase)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DataPackage.PERSON__SUBMITTED_SOLUTIONS:
				return ((InternalEList<?>)getSubmittedSolutions()).basicRemove(otherEnd, msgs);
			case DataPackage.PERSON__LECTURES:
				return ((InternalEList<?>)getLectures()).basicRemove(otherEnd, msgs);
			case DataPackage.PERSON__DATABASE:
				return basicSetDatabase(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DataPackage.PERSON__DATABASE:
				return eInternalContainer().eInverseRemove(this, DataPackage.INNOPLUS_DATABASE__PERSONS, InnoplusDatabase.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DataPackage.PERSON__ID:
				return getId();
			case DataPackage.PERSON__SUBMITTED_SOLUTIONS:
				return getSubmittedSolutions();
			case DataPackage.PERSON__LECTURES:
				return getLectures();
			case DataPackage.PERSON__DATABASE:
				return getDatabase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DataPackage.PERSON__ID:
				setId((String)newValue);
				return;
			case DataPackage.PERSON__SUBMITTED_SOLUTIONS:
				getSubmittedSolutions().clear();
				getSubmittedSolutions().addAll((Collection<? extends StudentSolution>)newValue);
				return;
			case DataPackage.PERSON__LECTURES:
				getLectures().clear();
				getLectures().addAll((Collection<? extends Lecture>)newValue);
				return;
			case DataPackage.PERSON__DATABASE:
				setDatabase((InnoplusDatabase)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DataPackage.PERSON__ID:
				setId(ID_EDEFAULT);
				return;
			case DataPackage.PERSON__SUBMITTED_SOLUTIONS:
				getSubmittedSolutions().clear();
				return;
			case DataPackage.PERSON__LECTURES:
				getLectures().clear();
				return;
			case DataPackage.PERSON__DATABASE:
				setDatabase((InnoplusDatabase)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DataPackage.PERSON__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case DataPackage.PERSON__SUBMITTED_SOLUTIONS:
				return submittedSolutions != null && !submittedSolutions.isEmpty();
			case DataPackage.PERSON__LECTURES:
				return lectures != null && !lectures.isEmpty();
			case DataPackage.PERSON__DATABASE:
				return getDatabase() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //PersonImpl
