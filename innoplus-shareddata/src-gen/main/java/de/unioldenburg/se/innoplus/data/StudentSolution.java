/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student Solution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.StudentSolution#getAuthor <em>Author</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.StudentSolution#getQueryResults <em>Query Results</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getStudentSolution()
 * @model
 * @generated
 */
public interface StudentSolution extends Solution {
	/**
	 * Returns the value of the '<em><b>Author</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Person#getSubmittedSolutions <em>Submitted Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Author</em>' container reference.
	 * @see #setAuthor(Person)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getStudentSolution_Author()
	 * @see de.unioldenburg.se.innoplus.data.Person#getSubmittedSolutions
	 * @model opposite="submittedSolutions" required="true" transient="false"
	 * @generated
	 */
	Person getAuthor();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.StudentSolution#getAuthor <em>Author</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' container reference.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(Person value);

	/**
	 * Returns the value of the '<em><b>Query Results</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.QueryResult}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.QueryResult#getSolution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query Results</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getStudentSolution_QueryResults()
	 * @see de.unioldenburg.se.innoplus.data.QueryResult#getSolution
	 * @model opposite="solution" containment="true"
	 * @generated
	 */
	EList<QueryResult> getQueryResults();

} // StudentSolution
