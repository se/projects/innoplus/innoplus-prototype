/**
 */
package de.unioldenburg.se.innoplus.data;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.QueryResult#getDate <em>Date</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.QueryResult#getType <em>Type</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.QueryResult#getSolution <em>Solution</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.QueryResult#getLocation <em>Location</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.QueryResult#getSummary <em>Summary</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.QueryResult#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult()
 * @model
 * @generated
 */
public interface QueryResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult_Date()
	 * @model required="true"
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.QueryResult#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.unioldenburg.se.innoplus.data.QueryResultType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see de.unioldenburg.se.innoplus.data.QueryResultType
	 * @see #setType(QueryResultType)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult_Type()
	 * @model required="true"
	 * @generated
	 */
	QueryResultType getType();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.QueryResult#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see de.unioldenburg.se.innoplus.data.QueryResultType
	 * @see #getType()
	 * @generated
	 */
	void setType(QueryResultType value);

	/**
	 * Returns the value of the '<em><b>Solution</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.StudentSolution#getQueryResults <em>Query Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solution</em>' container reference.
	 * @see #setSolution(StudentSolution)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult_Solution()
	 * @see de.unioldenburg.se.innoplus.data.StudentSolution#getQueryResults
	 * @model opposite="queryResults" required="true" transient="false"
	 * @generated
	 */
	StudentSolution getSolution();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.QueryResult#getSolution <em>Solution</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Solution</em>' container reference.
	 * @see #getSolution()
	 * @generated
	 */
	void setSolution(StudentSolution value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' attribute.
	 * @see #setLocation(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult_Location()
	 * @model
	 * @generated
	 */
	String getLocation();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.QueryResult#getLocation <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' attribute.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(String value);

	/**
	 * Returns the value of the '<em><b>Summary</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A very short summary for the feedback (no line breaks allowed).
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Summary</em>' attribute.
	 * @see #setSummary(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult_Summary()
	 * @model required="true"
	 * @generated
	 */
	String getSummary();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.QueryResult#getSummary <em>Summary</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Summary</em>' attribute.
	 * @see #getSummary()
	 * @generated
	 */
	void setSummary(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A longer text to describe the feedback in detail and with hints how to solve the identified issue (line breaks are allowed).
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getQueryResult_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.QueryResult#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

} // QueryResult
