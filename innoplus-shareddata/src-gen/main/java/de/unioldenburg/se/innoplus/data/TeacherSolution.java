/**
 */
package de.unioldenburg.se.innoplus.data;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Teacher Solution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getDatabase <em>Database</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getCategory <em>Category</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTeacherSolution()
 * @model
 * @generated
 */
public interface TeacherSolution extends Solution {
	/**
	 * Returns the value of the '<em><b>Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSampleSolutions <em>Sample Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' container reference.
	 * @see #setDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTeacherSolution_Database()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSampleSolutions
	 * @model opposite="sampleSolutions" required="true" transient="false"
	 * @generated
	 */
	InnoplusDatabase getDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getDatabase <em>Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' container reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(InnoplusDatabase value);

	/**
	 * Returns the value of the '<em><b>Category</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getSolutions <em>Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' reference.
	 * @see #setCategory(SolutionCategory)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getTeacherSolution_Category()
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getSolutions
	 * @model opposite="solutions" required="true"
	 * @generated
	 */
	SolutionCategory getCategory();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getCategory <em>Category</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' reference.
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(SolutionCategory value);

} // TeacherSolution
