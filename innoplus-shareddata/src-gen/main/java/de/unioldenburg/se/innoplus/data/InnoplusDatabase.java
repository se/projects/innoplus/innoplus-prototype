/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Innoplus Database</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getChildDatabases <em>Child Databases</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getParentDatabase <em>Parent Database</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getPersons <em>Persons</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getLectures <em>Lectures</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTaskCategories <em>Task Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSolutionCategories <em>Solution Categories</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getSampleSolutions <em>Sample Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getTasks <em>Tasks</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase()
 * @model
 * @generated
 */
public interface InnoplusDatabase extends EObject {
	/**
	 * Returns the value of the '<em><b>Child Databases</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.InnoplusDatabase}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getParentDatabase <em>Parent Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Databases</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_ChildDatabases()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getParentDatabase
	 * @model opposite="parentDatabase" containment="true"
	 * @generated
	 */
	EList<InnoplusDatabase> getChildDatabases();

	/**
	 * Returns the value of the '<em><b>Parent Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getChildDatabases <em>Child Databases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Database</em>' container reference.
	 * @see #setParentDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_ParentDatabase()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getChildDatabases
	 * @model opposite="childDatabases" transient="false"
	 * @generated
	 */
	InnoplusDatabase getParentDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getParentDatabase <em>Parent Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Database</em>' container reference.
	 * @see #getParentDatabase()
	 * @generated
	 */
	void setParentDatabase(InnoplusDatabase value);

	/**
	 * Returns the value of the '<em><b>Persons</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Person}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Person#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persons</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_Persons()
	 * @see de.unioldenburg.se.innoplus.data.Person#getDatabase
	 * @model opposite="database" containment="true"
	 * @generated
	 */
	EList<Person> getPersons();

	/**
	 * Returns the value of the '<em><b>Lectures</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Lecture}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Lecture#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lectures</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_Lectures()
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getDatabase
	 * @model opposite="database" containment="true"
	 * @generated
	 */
	EList<Lecture> getLectures();

	/**
	 * Returns the value of the '<em><b>Task Categories</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.TaskCategory}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.TaskCategory#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Categories</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_TaskCategories()
	 * @see de.unioldenburg.se.innoplus.data.TaskCategory#getDatabase
	 * @model opposite="database" containment="true"
	 * @generated
	 */
	EList<TaskCategory> getTaskCategories();

	/**
	 * Returns the value of the '<em><b>Solution Categories</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.SolutionCategory}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.SolutionCategory#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Solution Categories</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_SolutionCategories()
	 * @see de.unioldenburg.se.innoplus.data.SolutionCategory#getDatabase
	 * @model opposite="database" containment="true"
	 * @generated
	 */
	EList<SolutionCategory> getSolutionCategories();

	/**
	 * Returns the value of the '<em><b>Sample Solutions</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.TeacherSolution}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.TeacherSolution#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sample Solutions</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_SampleSolutions()
	 * @see de.unioldenburg.se.innoplus.data.TeacherSolution#getDatabase
	 * @model opposite="database" containment="true"
	 * @generated
	 */
	EList<TeacherSolution> getSampleSolutions();

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Task}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Task#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getInnoplusDatabase_Tasks()
	 * @see de.unioldenburg.se.innoplus.data.Task#getDatabase
	 * @model opposite="database" containment="true"
	 * @generated
	 */
	EList<Task> getTasks();

} // InnoplusDatabase
