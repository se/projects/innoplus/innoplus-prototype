/**
 */
package de.unioldenburg.se.innoplus.data;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Person#getId <em>Id</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Person#getSubmittedSolutions <em>Submitted Solutions</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Person#getLectures <em>Lectures</em>}</li>
 *   <li>{@link de.unioldenburg.se.innoplus.data.Person#getDatabase <em>Database</em>}</li>
 * </ul>
 *
 * @see de.unioldenburg.se.innoplus.data.DataPackage#getPerson()
 * @model abstract="true"
 * @generated
 */
public interface Person extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getPerson_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Person#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Submitted Solutions</b></em>' containment reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.StudentSolution}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.StudentSolution#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Submitted Solutions</em>' containment reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getPerson_SubmittedSolutions()
	 * @see de.unioldenburg.se.innoplus.data.StudentSolution#getAuthor
	 * @model opposite="author" containment="true"
	 * @generated
	 */
	EList<StudentSolution> getSubmittedSolutions();

	/**
	 * Returns the value of the '<em><b>Lectures</b></em>' reference list.
	 * The list contents are of type {@link de.unioldenburg.se.innoplus.data.Lecture}.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.Lecture#getParticipants <em>Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lectures</em>' reference list.
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getPerson_Lectures()
	 * @see de.unioldenburg.se.innoplus.data.Lecture#getParticipants
	 * @model opposite="participants"
	 * @generated
	 */
	EList<Lecture> getLectures();

	/**
	 * Returns the value of the '<em><b>Database</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.unioldenburg.se.innoplus.data.InnoplusDatabase#getPersons <em>Persons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' container reference.
	 * @see #setDatabase(InnoplusDatabase)
	 * @see de.unioldenburg.se.innoplus.data.DataPackage#getPerson_Database()
	 * @see de.unioldenburg.se.innoplus.data.InnoplusDatabase#getPersons
	 * @model opposite="persons" required="true" transient="false"
	 * @generated
	 */
	InnoplusDatabase getDatabase();

	/**
	 * Sets the value of the '{@link de.unioldenburg.se.innoplus.data.Person#getDatabase <em>Database</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' container reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(InnoplusDatabase value);

} // Person
