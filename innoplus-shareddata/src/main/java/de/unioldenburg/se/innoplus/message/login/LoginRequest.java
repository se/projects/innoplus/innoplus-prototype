package de.unioldenburg.se.innoplus.message.login;

import java.util.Objects;

import de.unioldenburg.se.innoplus.message.ClientToServer;

public class LoginRequest extends ClientToServer {
	private static final long serialVersionUID = -9013574825148972163L;

	protected final String lectureID;

	public LoginRequest(String clientID, String lectureID) {
		super(clientID);
		this.lectureID = Objects.requireNonNull(lectureID);
	}

	public String getLectureID() {
		return this.lectureID;
	}

	@Override
	public String toString() {
		return "LoginRequest{ clientID=\"" + clientID + "\", lectureID=\"" + lectureID + "\" }";
	}
}
