package de.unioldenburg.se.innoplus;

import org.apache.commons.codec.digest.DigestUtils;

public class InnoplusUtils {

	/**
	 * Calculates the MD5 hash for the given input
	 * @param input the input as String
	 * @return the corresponding MD5 hash as String
	 */
	public static String md5(String input) {
		return DigestUtils.md5Hex(input);
	}

	/** Utility wrapper for {@link System#getenv} which allows a default value **/
	public static String getenv(final String key, final String default_) {
		var env = System.getenv(key);
		if (env == null)
			env = default_;
		return env;
	}
}
