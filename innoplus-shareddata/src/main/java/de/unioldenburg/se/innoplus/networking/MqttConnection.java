package de.unioldenburg.se.innoplus.networking;

import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.Message;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import de.unioldenburg.se.innoplus.message.ServerToClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import static java.util.Objects.requireNonNull;
import static de.unioldenburg.se.innoplus.InnoplusUtils.getenv;

/**
 * Connection based on the <a href="https://en.wikipedia.org/wiki/MQTT">Message Queuing Telemetry Transport (MQTT)</a>.
 *
 * This implementation uses the following strategy to transfer {@link Message messages} between a server and multiple
 * clients. Each client owns a unique topic {@code client/<clientId>} and publishes their messages on {@code server}.
 * A server subscribes to {@code server} to receive client messages and replies to individual clients by publishing on
 * {@code client/<clientId>} (using {@link Message#getClientID()}).
 *
 * <br>
 *
 * Several parameters can be configured using the following environment variables:
 * <dl>
 * <dt>{@code MQTT_URL}</dt>
 * <dd>Broker URL (defaults to {@value #DEFAULT_BROKER_URL}</dd>
 * <dt>{@code MQTT_USER}</dt>
 * <dd>optional username used for broker authentication</dd>
 * <dt>{@code MQTT_PASSWORD}</dt>
 * <dd>optional password used for broker authentication</dd>
 * <dt>{@code MQTT_TOPIC_ROOT}</dt>
 * <dd>optional topic prefix for all publish/subscribe operations</dd>
 * </dl>
 *
 * Use the factory methods {@link #createServer} and {@link #createClient} to build a preconfigured instance.
 *
 * @param <In> Message types which may be received from this connection
 * @param <Out> Message types which may be sent through this connection
 */
public final class MqttConnection<In extends Message, Out extends Message> implements Connection<In, Out> {

	/**
	 * Creates a new instance configured as a server s.t. it receives message sent by {@link #createClient(String) clients}.
	 * @return the instance
	 * @throws IOException if the connection to the MQTT broker could not be established
	 */
	public static MqttConnection<ClientToServer, ServerToClient> createServer() throws IOException {
		return createServer(new Configuration(true, getenv("MQTT_SERVER_ID", "innoplus-server")));
	}

	/**
	 * Creates a new instance configured as a server with the supplied configuration.
	 * @return the instance
	 * @throws IOException if the connection to the MQTT broker could not be established
	 */
	public static MqttConnection<ClientToServer, ServerToClient> createServer(Configuration config) throws IOException {
		return new MqttConnection<>(config);
	}

	/**
	 * Creates a new instance configured as a client s.t. it receives messages from a {@link #createServer() server}.
	 * @return the instance
	 * @throws IOException if the connection to the MQTT broker could not be established
	 */
	public static MqttConnection<ServerToClient, ClientToServer> createClient(String clientId) throws IOException {
		return createClient(new Configuration(false, clientId));
	}

	/**
	 * Creates a new instance configured as a client with the supplied configuration.
	 * @return the instance
	 * @throws IOException if the connection to the MQTT broker could not be established
	 */
	public static MqttConnection<ServerToClient, ClientToServer> createClient(Configuration config) throws IOException {
		return new MqttConnection<>(config);
	}

	/** Default broker url (used when no explicit broker is supplied via MQTT_BROKER_URL) **/
	private static final String DEFAULT_BROKER_URL = "ssl://innoplus-dev.Informatik.Uni-Oldenburg.DE:8883";

	/** MQTT service level, currently using the maximum value to guarantee one-time delivery **/
	private static final int SERVICE_LEVEL = 2;

	public static final class Configuration {

		/** Current broker url used for publish/subscribe operations **/
		public final String brokerUrl;

		/** Current MQTT client id **/
		public final String clientId;

		/** Optional username (may be null) **/
		public final String username;

		/** Optional password (may be null) **/
		public final String password;

		/** Root topic for publish/subscribe **/
		public final String topicRoot;

		/** Topic for incoming messages **/
		public final String subscribeTopic;

		/** Yields the appropriate topic for an outgoing message **/
		public final Function<String, String> publishTopic;

		/**
		 * Creates a new configuration with default settings.
		 *
		 * @param isServer   whether the connection is used in server mode
		 */
		public Configuration(boolean isServer, String clientId) {
			this(isServer, getenv("MQTT_URL", DEFAULT_BROKER_URL), clientId);
		}

		/**
		 * Creates a new configuration with a custom broker and default settings.
		 *
		 * @param isServer   whether the connection is used in server mode
		 */
		public Configuration(boolean isServer, String brokerUrl, String clientId) {
			this(isServer, brokerUrl, clientId, getenv("MQTT_USER", null), getenv("MQTT_PASSWORD", null));
		}

		/**
		 * Creates a new configuration with a custom broker + credentials and default settings.
		 *
		 * @param isServer   whether the connection is used in server mode
		 */
		public Configuration(boolean isServer, String brokerUrl, String clientId, String username, String password) {
			this(isServer, brokerUrl, clientId, username, password, null);
		}

		// Overload for test purposes
		/** Creates a new configuration with the supplied settings. **/
		protected Configuration(boolean isServer, String brokerUrl, String clientId, String username, String password,
				String topicRoot) {

			this.brokerUrl = requireNonNull(brokerUrl, "Broker URL is required");
			this.clientId = requireNonNull(clientId, "Client-ID is required");
			this.username = username;
			this.password = password;

			if (topicRoot == null)
				topicRoot = getenv("MQTT_TOPIC_ROOT", "");

			if (!topicRoot.endsWith("/"))
				topicRoot += '/';

			this.topicRoot = topicRoot;

			if (isServer) {
				this.subscribeTopic = getServerTopic(clientId);
				this.publishTopic = this::getClientTopic;
			} else {
				this.subscribeTopic = getClientTopic(clientId);
				this.publishTopic = this::getServerTopic;
			}
		}

		/** Returns the server's topic **/
		public String getServerTopic(String clientId) {
			return topicRoot + "server";
		}

		/** Returns the client's topic **/
		public String getClientTopic(String clientId) {
			return topicRoot + "client/" + clientId;
		}
	}

	/** Mqtt implementation handle **/
	private final MqttClient client;

	/** Configuration **/
	private final Configuration config;

	/** Currently registered listeners **/
	private final Map<Class<? extends In>, MessageListener> listeners;

	/** Stellt eine Verbindung zu einem definierten Server **/
	private MqttConnection(final Configuration config) throws IOException {
		this.config = config;
		this.listeners = new ConcurrentHashMap<>();

		// Configure connection properties to prefer exact one-time delivery over speed
		var options = new MqttConnectOptions();
		options.setCleanSession(true);
		options.setAutomaticReconnect(true);

		// Add optional user
		var user = config.username;
		if (user != null && !user.isBlank())
			options.setUserName(user);

		// Add optional password
		var password = config.password;
		if (password != null && !password.isBlank())
			options.setPassword(password.toCharArray());

		// Add persistence for pending messages
		// Currently in-memory, maybe use file persistence?
		var persistence = new MemoryPersistence();

		System.out.printf("Connecting to \"%s\" as \"%s\" on \"%s\"\n", config.brokerUrl, config.clientId, config.subscribeTopic);

		try {
			// Establish the actual connection
			client = new MqttClient(config.brokerUrl, config.clientId, persistence);
			client.connect(options);
			client.subscribe(config.subscribeTopic, this::handleMqttMessage);
		} catch (MqttException e) {
			throw new IOException(e);
		}
	}

	@Override
	public void send(Out message) throws IOException {
		var payload = message.toBytes();
		var topic = config.publishTopic.apply(message.getClientID());
		System.out.printf("%s - Sending message on %s\n", config.clientId, topic);
		try {
			this.client.publish(topic, payload, SERVICE_LEVEL, false);
		} catch (MqttException e) {
			throw new IOException(e);
		}
	}

	@Override
	public <T extends In> void addListener(Class<T> clazz, MessageListener<? super T> listener) {
		System.out.printf("%s > Registered listener for %s\n", config.clientId, clazz);
		requireNonNull(clazz);
		requireNonNull(listener);
		this.listeners.put(clazz, listener);
	}

	private final void handleMqttMessage(String topic, MqttMessage message) throws IOException, ClassNotFoundException {

		try {
			var bytes = message.getPayload();
			System.out.printf("%s > %s: %s (%s bytes)\n", config.clientId, topic, message.getId(), bytes.length);
			var content = Message.from(bytes);

			// Dispatch class to an associated handler
			// Check superclasses if there is no direct match
			Class<?> clazz = content.getClass();
			do {
				var listener = this.listeners.get(clazz);
				if (listener != null) {
					listener.handle(content);
					return;
				}
				clazz = clazz.getSuperclass();
			} while (clazz != null);

			System.out.printf("%s > Ignoring message without associated handler: %s\n", config.clientId, content);

		} catch (Exception e) {
			// Logging only because this is a callback running in a different thread
			e.printStackTrace();
		} catch (Throwable e) {
			 // Just to ensure that it is logged
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void removeListener(Class<? extends In> clazz) {
		requireNonNull(clazz);
		if (this.listeners.remove(clazz) == null)
			throw new IllegalArgumentException("No handler registered for class " + clazz.getName());
	}

	@Override
	public void close() throws Exception {
		if (this.client != null) {
			// Await completion of pending transmissions
			if (this.client.isConnected())
				this.client.disconnect();
			this.client.close();
		}
	}
}
