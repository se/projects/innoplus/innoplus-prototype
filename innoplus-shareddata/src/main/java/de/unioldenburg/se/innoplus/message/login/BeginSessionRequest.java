package de.unioldenburg.se.innoplus.message.login;

import de.unioldenburg.se.innoplus.message.ClientToServer;

/**
 * Initial message from server to client requesting a list of all
 * available lectures s.t. a user can pick their current one.
 * The selection is used in the subsequent {@link LoginRequest}.
 */
public class BeginSessionRequest extends ClientToServer {
	private static final long serialVersionUID = -6666680454532268049L;

	public BeginSessionRequest(String clientID) {
		super(clientID);
	}

	@Override
	public String toString() {
		return "BeginSessionRequest{ clientID=\"" + clientID + "\" }";
	}
}
