package de.unioldenburg.se.innoplus.message;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.mapping.ecore2xml.Ecore2XMLPackage;
import org.eclipse.emf.mapping.ecore2xml.util.Ecore2XMLResource;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UML22UMLExtendedMetaData;
import org.eclipse.uml2.uml.resource.UML22UMLResource;
import org.eclipse.uml2.uml.resource.UMLResource;

public class EcoreIO {

	public static void saveEObject(EObject object, OutputStream stream) throws IOException {
		// handle resource before
		Resource initialResource = object != null ? object.eResource() : null;
		URI initialUri = null;
		XMIResource currentResource;
		if (initialResource != null) {
			// TODO: fix/check!!
			// handle existing resource
			if (initialResource instanceof XMIResource == false) {
				throw new IllegalStateException("strange resource: " + initialResource);
			}
			if (initialResource.getResourceSet() == null) {
				throw new IllegalStateException("resource withou set?? " + initialResource);
			}
			initialUri = initialResource.getURI();
			currentResource = (XMIResource) initialResource;
		} else {
			// create new resource set
		    ResourceSet resSet = createResourceSet();

		    // create new resource
			URI uri = URI.createURI("http:///save.xmi");
			currentResource = (XMIResource) resSet.getResource(uri, false);
			if (currentResource == null) {
				currentResource = (XMIResource) resSet.createResource(uri);
			}
			if (object != null) {
				currentResource.getContents().add(object);
			}
		}

		// save
		Map<String, Object> options = createOptions();
		try {
			currentResource.save(stream, options);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}

		/* handle resource after
		 * - to reduce side effects
		 * - no resource before and after XOR same path before and after
		 */
		if (initialResource != null) {
			initialResource.setURI(initialUri);
		} else {
			if (object != null) {
				currentResource.getContents().remove(object);
			}
		}
	}

	public static void saveEObject(EObject object, String path) {
		// check input
		if (object == null) {
			throw new IllegalArgumentException();
		}
		if (path == null) {
			throw new IllegalArgumentException();
		}

		// handle resource before
		Resource initialResource = object.eResource();
		URI initialUri = null;
		XMIResource currentResource;
		if (initialResource != null) {
			// handle existing resource
			if (initialResource instanceof XMIResource == false) {
				throw new IllegalStateException("strange resource: " + initialResource);
			}
			if (initialResource.getResourceSet() == null) {
				throw new IllegalStateException("resource withou set?? " + initialResource);
			}
			initialUri = initialResource.getURI();
			currentResource = (XMIResource) initialResource;
		} else {
			// create new resource set
		    ResourceSet resSet = new ResourceSetImpl();
		    Map<String, Object> mapExtensions = resSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
			mapExtensions.put("xmi", new XMIResourceFactoryImpl());
		    mapExtensions.put("ecore", new EcoreResourceFactoryImpl());

		    // create new resource
			URI uri = URI.createFileURI(new File(path).getAbsolutePath());
			currentResource = (XMIResource) resSet.getResource(uri, false);
			if (currentResource == null) {
				currentResource = (XMIResource) resSet.createResource(uri);
			}
			currentResource.getContents().add(object);
		}

		// save
		Map<String, Object> mapOptions = new HashMap<>();
		try {
			currentResource.save(mapOptions);
		} catch (IOException e) {
			e.printStackTrace();
		}

		/* handle resource after
		 * - to reduce side effects
		 * - no resource before and after XOR same path before and after
		 */
		if (initialResource != null) {
			initialResource.setURI(initialUri);
		} else {
			currentResource.getContents().remove(object);
		}
	}

	private static Map<String, Object> createOptions() {
		Map<String, Object> mapOptions = new HashMap<>();
		return mapOptions;
	}

	private static ResourceSet createResourceSet() {

		ResourceSet resSet = new ResourceSetImpl();
		Map<String, Object> mapExtensions = resSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
		mapExtensions.put("xmi", new XMIResourceFactoryImpl());
		mapExtensions.put("ecore", new EcoreResourceFactoryImpl());
		return resSet;
	}

	public static EObject loadEObject(String path) {
		ResourceSet resSet = createResourceSet();
		Map<String, Object> options = createOptions();

		Resource resource = resSet.createResource(URI.createFileURI(path));
		try {
			resource.load(options);
			return resource.getContents().get(0);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EObject loadEObject(InputStream stream) throws IOException {
		ResourceSet resSet = createResourceSet();
		Map<String, Object> options = createOptions();

		URI uri = URI.createURI("http:///load.xmi");
		Resource resource = resSet.createResource(uri);
		try {
			resource.load(stream, options);
			if (resource.getContents().isEmpty()) {
				return null;
			} else if (resource.getContents().size() == 1) {
				return resource.getContents().get(0);
			} else {
				throw new IllegalStateException("too much root entries in the XMI file");
			}
		} catch (IOException e) {
			e.printStackTrace();
//			return null;
			throw e;
		}
	}

	/**
	 * Registers Resources that are needed to load every element of a .uml file
	 * @param resourceSet
	 * @param umlResource
	 */
	private static void registerUMLPathMaps(ResourceSet resourceSet, URI umlResource) {
		//var uriMap = resourceSet.getURIConverter().getURIMap();

		URIConverter.URI_MAP.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP),
				umlResource.appendSegment("libraries").appendSegment(""));

		URIConverter.URI_MAP.put(URI.createURI(UMLResource.METAMODELS_PATHMAP),
				umlResource.appendSegment("metamodels").appendSegment(""));

		URIConverter.URI_MAP.put(URI.createURI(UMLResource.PROFILES_PATHMAP),
				umlResource.appendSegment("profiles").appendSegment(""));

	}

	protected static void registerUMLExtensions() {
		Map<String, Object> extensionFactoryMap = Resource.Factory.Registry.INSTANCE
				.getExtensionToFactoryMap();

		extensionFactoryMap.put(UMLResource.FILE_EXTENSION,
				UMLResource.Factory.INSTANCE);

		extensionFactoryMap.put(Ecore2XMLResource.FILE_EXTENSION,
				Ecore2XMLResource.Factory.INSTANCE);

		extensionFactoryMap.put(UML22UMLResource.FILE_EXTENSION,
				UML22UMLResource.Factory.INSTANCE);
	}

	protected static void registerUMLPackages(ResourceSet resourceSet) {

		EPackage.Registry packageRegistry = resourceSet.getPackageRegistry();

		packageRegistry.put(EcorePackage.eNS_URI, EcorePackage.eINSTANCE);
		packageRegistry.put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		packageRegistry.put(Ecore2XMLPackage.eNS_URI, Ecore2XMLPackage.eINSTANCE);
	}


	public static Model loadUmlModel(URI resourceURI, URI umlResource) throws IOException {

		// before loading the uml model we need to register some stuff
		ResourceSet resourceSet = createResourceSet();
		registerUMLPathMaps(resourceSet, umlResource);
		registerUMLPackages(resourceSet);
		registerUMLExtensions();

		// load given URI resource
		resourceSet.getResource(resourceURI, true);

		var resources = resourceSet.getResources();
		EcoreUtil.resolveAll(resources.get(0));
		return  (Model) EcoreUtil.getObjectByType(resources.get(0)
				.getContents(), UMLPackage.Literals.MODEL);
	}

	/** old method for backwards compatibility **/
	public static Model loadUmlModel(InputStream stream) throws IOException {
		ResourceSet resSet = createResourceSet();

		// Register UML2 Stuff
		Map<String, Object> mapExtensions = resSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
		resSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		mapExtensions.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap()
				.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);

		Map<String, Object> options = createOptions();

		URI uri = URI.createURI("http:///load.xmi");
		Resource resource = resSet.createResource(uri);
		try {
			resource.load(stream, options);
			if (resource.getContents().isEmpty()) {
				return null;
			} else if (resource.getContents().size() == 1) {
				return (Model) resource.getContents().get(0);
			} else {
				throw new IllegalStateException("too much root entries in the XMI file");
			}
		} catch (IOException e) {
			e.printStackTrace();
//			return null;
			throw e;
		}
	}
}
