package de.unioldenburg.se.innoplus.message.login;

import java.util.Objects;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.message.EcoreIO;
import de.unioldenburg.se.innoplus.message.ServerToClient;

public class BeginSessionResponse extends ServerToClient {
	private static final long serialVersionUID = 3303786647729721656L;

	/** Database contains only a list of all availabe lectures (shallow instances) */
	protected transient InnoplusDatabase initialClientData;

	public BeginSessionResponse(String clientID, InnoplusDatabase initialClientData) {
		super(clientID);
		this.initialClientData = Objects.requireNonNull(initialClientData);
	}

	public InnoplusDatabase getInitialClientData() {
		return initialClientData;
	}

	@Override
	public String toString() {
		return "BeginSessionResponse{ clientID=\"" + clientID + "\", initialClientData=\"" + initialClientData + "\" }";
	}

	private void readObject(ObjectInputStream is) throws ClassNotFoundException, IOException {
		// perform the default deserialization for all non-transient, non-static fields
		is.defaultReadObject();
		this.initialClientData = (InnoplusDatabase) EcoreIO.loadEObject(is);
	}

	private void writeObject(ObjectOutputStream os) throws IOException {
		// perform the default serialization for all non-transient, non-static fields
		os.defaultWriteObject();
		EcoreIO.saveEObject(this.initialClientData, os);
	}
}
