package de.unioldenburg.se.innoplus.message.login;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.message.EcoreIO;
import de.unioldenburg.se.innoplus.message.ServerToClient;

public class LoginResponse extends ServerToClient {
	private static final long serialVersionUID = 5987014822515744583L;

	protected final boolean loginResult;
	protected transient InnoplusDatabase taskDatabase;

	public LoginResponse(String clientID, boolean loginResult, InnoplusDatabase taskDatabase) {
		super(clientID);
		this.loginResult = loginResult;
		this.taskDatabase = taskDatabase;
	}

	public boolean isLoginResult() {
		return loginResult;
	}

	public InnoplusDatabase getTaskDatabase() {
		return taskDatabase;
	}

	@Override
	public String toString() {
		return "LoginResponse{ clientID=\"" + clientID + "\", loginResult=\"" + loginResult + "\", taskDatabase=\"" + taskDatabase + "\" }";
	}

	private void readObject(ObjectInputStream is) throws ClassNotFoundException, IOException {
		// perform the default deserialization for all non-transient, non-static fields
		is.defaultReadObject();
		this.taskDatabase = (InnoplusDatabase) EcoreIO.loadEObject(is);
	}

	private void writeObject(ObjectOutputStream os) throws IOException {
		// perform the default serialization for all non-transient, non-static fields
		os.defaultWriteObject();
		EcoreIO.saveEObject(this.taskDatabase, os);
	}
}
