package de.unioldenburg.se.innoplus.message.feedback;

import java.util.Map;

public class JavaFeedbackRequest extends FeedbackRequest {
	private static final long serialVersionUID = -3334739168346798208L;

	protected final String javaVersion;
	protected final int javaTabWidth;
	protected final Map<String, String> javaFiles; // file path (relative to the root/project folder) --> content of the file

	public JavaFeedbackRequest(String clientID, String taskName, String javaVersion, int javaTabWidth, Map<String, String> javaFiles) {
		super(clientID, taskName);
		this.javaVersion = javaVersion;
		this.javaTabWidth = javaTabWidth;
		this.javaFiles = javaFiles;
	}

	public String getJavaVersion() {
		return javaVersion;
	}

	public int getJavaTabWidth() {
		return javaTabWidth;
	}

	public Map<String, String> getJavaFiles() {
		return javaFiles;
	}

	@Override
	public String toString() {
		return "JavaFeedbackRequest{" +
				" clientID=\"" + clientID +
				"\", javaVersion=\"" + javaVersion +
				"\", javaTabWidth=\"" + javaTabWidth +
				"\", taskName=\"" + taskName +
				"\", javaFiles=" + javaFiles +
			" }";
	}
}
