package de.unioldenburg.se.innoplus.message.feedback;

import de.unioldenburg.se.innoplus.data.StudentSolution;

public class UMLFeedbackResponse extends FeedbackResponse {
	private static final long serialVersionUID = 67812212456983094L;

	public UMLFeedbackResponse(String clientID, StudentSolution result) {
		super(clientID, result);
	}

	@Override
	public String toString() {
		return "UMLFeedbackResponse{ clientID=\"" + clientID + "\", result=\"" + result + "\" }";
	}
}
