package de.unioldenburg.se.innoplus.message.feedback;

import java.util.List;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.StudentSolution;

public class JavaFeedbackResponse extends FeedbackResponse {
	private static final long serialVersionUID = 6781228016556983094L;

	public JavaFeedbackResponse(String clientID, StudentSolution result) {
		super(clientID, result);
	}

	@Override
	public String toString() {
		return "JavaFeedbackResponse{ clientID=\"" + clientID + "\", result=\"" + result + "\" }";
	}
}
