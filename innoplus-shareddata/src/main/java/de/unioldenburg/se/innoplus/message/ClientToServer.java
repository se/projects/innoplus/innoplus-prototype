package de.unioldenburg.se.innoplus.message;

public abstract class ClientToServer extends Message {
	private static final long serialVersionUID = -62568313952686327L;

	public ClientToServer(String clientID) {
		super(clientID);
	}

}
