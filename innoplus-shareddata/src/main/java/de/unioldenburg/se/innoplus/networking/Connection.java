package de.unioldenburg.se.innoplus.networking;

import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.Message;
import de.unioldenburg.se.innoplus.message.ServerToClient;

import java.io.IOException;

/**
 * Connection between a server and a client.
 *
 * @param <In> Message types which may be received from this connection
 * @param <Out> Message types which may be sent through this connection
 */
public interface Connection<In extends Message, Out extends Message> extends AutoCloseable {

	/** Asynchronous callback for incoming messages **/
	interface MessageListener<MessageType extends Message> {
		void handle(MessageType message) throws IOException;
	}

	/**
	 * Register a new listener to handle incoming messages.
	 *
	 * @param clazz runtime (super)class of all messages handled by listener
	 * @param listener the listener
	 * @param <T> the message type handled by this listener
	 * @throws IOException if the listener could not be registered
	 */
	<T extends In> void addListener(Class<T> clazz, MessageListener<? super T> listener) throws IOException;

	/**
	 * Removes the listener bound to `clazz`.
	 *
	 * @param clazz runtime (super)class of all messages handled by the listener
	 * @throws IOException if the listener could not be removed
	 * @throws IllegalArgumentException if there is no such listener
	 */
	public void removeListener(Class<? extends In> clazz) throws IOException;

	/**
	 * Sends a new message though this connection.
	 *
	 * @param message the payload
	 * @throws IOException if an error occurs
	 */
	void send(Out message) throws IOException;
}
