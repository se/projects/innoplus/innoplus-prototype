package de.unioldenburg.se.innoplus.message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.Objects;

public abstract class Message implements Serializable {
	private static final long serialVersionUID = 7588791331338623255L;
//	private static final String separator = "===";

	protected final String clientID; // session ID, for now: the user hash!

	public Message(String clientID) {
		super();
		this.clientID = Objects.requireNonNull(clientID);
	}

	public String getClientID() {
		return clientID;
	}

	/**
	 * Serializes this object into a byte array.
	 *
	 * @return bytes representation created by an {@link ObjectOutputStream}
	 */
	public byte[] toBytes() throws IOException {
		var bytes = new ByteArrayOutputStream();

		try (var oos = new ObjectOutputStream(bytes)) {
			oos.writeObject(this);
		}

		return bytes.toByteArray();
	}

	/**
	 * Serializes this object into a string.
	 *
	 * @return {@link Base64}-encoding of {@link #toBytes()}
	 */
	public String toStringMessage() throws IOException {
		return Base64.getEncoder().encodeToString(this.toBytes());
	}

	/**
	 * Reads an instance of T from the given bytes.
	 *
	 * @param bytes byte representation created by a call to {@link #toBytes}
	 * @param <T> the expected message type
	 * @return the decoded message
	 */
	public static <T extends Message> T from(byte[] bytes) throws IOException, ClassNotFoundException {
		var in = new ByteArrayInputStream(bytes);

		try (var ois = new ObjectInputStream(in)) {
			var res = ois.readObject();
			if (ois.available() != 0)
				throw new IllegalStateException("More bytes available!");
			return (T) res;
		}
	}

	/**
	 * Reads an instance of T from the given string.
	 *
	 * @param message text representation created by a call to {@link #toStringMessage()}
	 * @param <T> the expected message type
	 * @return the decoded message
	 */
	public static <T extends Message> T from(String message) throws IOException, ClassNotFoundException {
		return from(Base64.getDecoder().decode(message));
	}
}
