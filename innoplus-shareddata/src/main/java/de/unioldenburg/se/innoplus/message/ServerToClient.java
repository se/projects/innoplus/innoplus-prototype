package de.unioldenburg.se.innoplus.message;

public abstract class ServerToClient extends Message {
	private static final long serialVersionUID = -3745437858153082531L;

	public ServerToClient(String clientID) {
		super(clientID);
	}

}
