package de.unioldenburg.se.innoplus.communication;

import java.io.Serializable;

public class CheckstyleError implements Serializable {
    private static final long serialVersionUID = 1L;
    public String file;
    public int line;
    public int column;
    public String severity;
    public String feedback;

    public CheckstyleError(String file, int line, int column, String severity, String feedback){
        this.file = file;
        this.line = line;
        this.column = column;
        this.severity = severity;
        this.feedback = feedback;
    }

    @Override
    public String toString(){
       return "file: " + this.file +
               "\nline: "  + this.line +
               "\ncolumn: "  + this.column +
               "\nseverity: "  + this.severity +
               "\nfeedback: " + this.feedback;
    }
}