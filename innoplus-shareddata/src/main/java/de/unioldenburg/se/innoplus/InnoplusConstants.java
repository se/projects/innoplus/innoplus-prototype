package de.unioldenburg.se.innoplus;

public class InnoplusConstants {
	public static final String TASK_JAVA = "Text2Java";
	public static final String SOLUTION_JAVA_CODE = "JavaCode";
	public static final String SAMPLE_SOLUTION_JAVA_JUNIT = "JUnit4JavaCode";

	public static final String TASK_UML = "Text_to_UML";
	public static final String SOLUTION_UML = "UML_diagram";
	// Task Names
	public static final String CODE_TASK_MERGE_ARRAYS = "Merge sorted Arrays";
	public static final String CODE_TASK_STAR_DATABASE = "Star Database";
	public static final String CODE_TASK_MERGE_LINKED_LIST = "Linked List";
	public static final String CODE_TASK_MERGE_CIRCULAR_LIST = "Circular List";
	public static final String CODE_TASK_RUNNING_GAME = "Running Game";
	public static final String CODE_TASK_GENERIC_CUTLERY = "Generic Cutlery Task";
	public static final String CODE_TASK_CUTLERY = "Cutlery Task";
	public static final String CODE_TASK_IO_TEST = "IO Test";
	public static final String CODE_TASK_STREAM_TEST = "Stream Test";
}
