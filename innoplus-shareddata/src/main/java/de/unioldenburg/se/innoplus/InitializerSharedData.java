package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.data.SolutionCategory;
import de.unioldenburg.se.innoplus.data.TaskCategory;

public class InitializerSharedData {

	public static InnoplusDatabase createDefaultData() {
		DataFactory factory = DataFactory.eINSTANCE;
		InnoplusDatabase data = factory.createInnoplusDatabase();

		// Java + JUnit
		TaskCategory taskJavaCode = factory.createTaskCategory();
		data.getTaskCategories().add(taskJavaCode);
		taskJavaCode.setName(InnoplusConstants.TASK_JAVA);

		SolutionCategory solutionJUnit = factory.createSolutionCategory();
		data.getSolutionCategories().add(solutionJUnit);
		solutionJUnit.setName(InnoplusConstants.SAMPLE_SOLUTION_JAVA_JUNIT);
		SolutionCategory solutionJava = factory.createSolutionCategory();
		data.getSolutionCategories().add(solutionJava);
		solutionJava.setName(InnoplusConstants.SOLUTION_JAVA_CODE);

		taskJavaCode.getSolutionCategories().add(solutionJUnit);

		// Lecture "PDA"
		Lecture lecturePDA = factory.createLecture();
		data.getLectures().add(lecturePDA);
		lecturePDA.setName("PDA");
		lecturePDA.setId("36e93f23b97226d889adb38483b273fa");

		// Lecture "ST1"
		Lecture lectureST1 = factory.createLecture();
		data.getLectures().add(lectureST1);
		lectureST1.setName("ST1");
		lectureST1.setId("2d54797bbfce8f01aa6db98f73835709");

		return data;
	}

}
