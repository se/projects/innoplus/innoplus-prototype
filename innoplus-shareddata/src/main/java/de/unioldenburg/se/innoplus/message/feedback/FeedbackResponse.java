package de.unioldenburg.se.innoplus.message.feedback;

import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.message.EcoreIO;
import de.unioldenburg.se.innoplus.message.ServerToClient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public abstract class FeedbackResponse extends ServerToClient {
	private static final long serialVersionUID = 3097496160913612541L;

	protected transient StudentSolution result;

	public FeedbackResponse(String clientID, StudentSolution result) {
		super(clientID);
		this.result = result;
	}

	public StudentSolution getResult() {
		return result;
	}

	private void readObject(ObjectInputStream is) throws ClassNotFoundException, IOException {
		// perform the default deserialization for all non-transient, non-static fields
		is.defaultReadObject();
		this.result = (StudentSolution) EcoreIO.loadEObject(is);

	}

	private void writeObject(ObjectOutputStream os) throws IOException {
		// perform the default serialization for all non-transient, non-static fields
		os.defaultWriteObject();
		EcoreIO.saveEObject(this.result, os);
	}
}
