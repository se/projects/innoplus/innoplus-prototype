package de.unioldenburg.se.innoplus.uml_api;

import de.unioldenburg.se.innoplus.data.QueryResult;
import org.eclipse.uml2.uml.Model;
import org.pf4j.ExtensionPoint;

import java.util.List;

public interface UMLEvaluator extends ExtensionPoint {

    List<QueryResult> evaluate (Model model);
}
