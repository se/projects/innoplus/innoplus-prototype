package de.unioldenburg.se.innoplus.message.feedback;

public class UMLFeedbackRequest extends FeedbackRequest {
	private static final long serialVersionUID = -3334739167246798201L;

	protected final String uml; // content of the *.uml file
	protected final String notation; // content of the *.notation file

	public UMLFeedbackRequest(String clientID, String taskName, String uml, String notation) {
		super(clientID, taskName);
		this.uml = uml;
		this.notation = notation;
	}


	public String getUml() {
		return uml;
	}


	public String getNotation() {
		return notation;
	}


	@Override
	public String toString() {
		return "UMLFeedbackRequest [" +
			" clientID=" + clientID +
			", taskName=" + taskName +
			", uml=" + uml +
			", notation=" + notation +
		"]";
	}


}
