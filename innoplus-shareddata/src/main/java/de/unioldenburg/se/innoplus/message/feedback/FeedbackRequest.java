package de.unioldenburg.se.innoplus.message.feedback;

import de.unioldenburg.se.innoplus.message.ClientToServer;

public abstract class FeedbackRequest extends ClientToServer {
	private static final long serialVersionUID = 3097496160913612541L;

	protected final String taskName;

	public FeedbackRequest(String clientID, String taskName) {
		super(clientID);
		this.taskName = taskName;
	}

	public String getTaskName() {
		return taskName;
	}
}
