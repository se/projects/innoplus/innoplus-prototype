package de.unioldenburg.se.innoplus.message;

import java.util.Objects;

/** Wrapper class for several example messages used in tests **/
public final class ExampleMessages {
	private ExampleMessages() {}

	public static final class STC1 extends ServerToClientDummy<String> {
		public STC1(String clientID, String member) {
			super(clientID, member);
		}
	}

	public static final class CTS1 extends ClientToServerDummy<Character> {
		public CTS1(String clientID, Character member) {
			super(clientID, member);
		}
	}

	public static final class CTS2 extends ClientToServerDummy<Boolean> {
		public CTS2(String clientID, Boolean member) {
			super(clientID, member);
		}
	}

	public static final class CTS3 extends ClientToServerDummy<Integer> {
		public CTS3(String clientID, Integer member) {
			super(clientID, member);
		}
	}

	public static class ClientToServerDummy<T> extends ClientToServer {

		private final T member;

		public ClientToServerDummy(String clientID, T number) {
			super(clientID);
			this.member = number;
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) return true;
			if (other == null || getClass() != other.getClass()) return false;
			var that = (ClientToServerDummy<?>) other;
			return Objects.equals(this.clientID, that.clientID)
					&& Objects.equals(this.member, that.member);
		}

		@Override
		public int hashCode() {
			return Objects.hash(member);
		}

		@Override
		public String toString() {
			return "ClientToServerDummy{ clientId=\"" + clientID + "\"member=<" + member + "> }";
		}
	}

	public static class ServerToClientDummy<T> extends ServerToClient {

		private final T member;

		public ServerToClientDummy(String clientID, T number) {
			super(clientID);
			this.member = number;
		}

		@Override
		public boolean equals(Object other) {
			if (this == other) return true;
			if (other == null || getClass() != other.getClass()) return false;
			var that = (ServerToClientDummy<?>) other;
			return Objects.equals(this.clientID, that.clientID)
					&& Objects.equals(this.member, that.member);
		}

		@Override
		public int hashCode() {
			return Objects.hash(member);
		}

		@Override
		public String toString() {
			return "ServerToClientDummy{ clientId=\"" + clientID + "\"member=<" + member + "> }";
		}
	}
}
