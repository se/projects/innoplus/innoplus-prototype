package de.unioldenburg.se.innoplus.message;

import de.unioldenburg.se.innoplus.InitializerSharedData;
import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.message.ExampleMessages.*;
import de.unioldenburg.se.innoplus.message.*;
import de.unioldenburg.se.innoplus.message.login.*;

import org.junit.*;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.function.Consumer;

import org.eclipse.emf.ecore.util.EcoreUtil;

public class MessageTest {

	private static final Message[] examples = {
		new STC1("Client1", "Hello, World!"),
		new CTS1("ABCDE", 'X')
	};

	@Test
	public void testByteSerialization() throws Exception {
		for (var exp : examples) {
			var bytes = exp.toBytes();
			assertNotNull(bytes);
			var act = Message.from(bytes);
			assertEquals(exp, act);
		}
	}

	@Test
	public void testStringSerialization() throws Exception {
		for (var exp : examples) {
			var text = exp.toStringMessage();
			assertNotNull(text);
			var act = Message.from(text);
			assertEquals(exp, act);
		}
	}

	@Test
	public void testBeginSessionSerialization() throws Exception {
		var factory = DataFactory.eINSTANCE;
		var db = factory.createInnoplusDatabase();
		var lecture = factory.createLecture();
		lecture.setId("ABC");
		lecture.setName("PDA");;
		lecture.setDatabase(db);
		var exp = new BeginSessionResponse("abcde", db);

		checkSerialization(exp, act -> {
			assertEquals(exp.getClientID(), act.getClientID());

			var actDb = act.getInitialClientData();
			if (!EcoreUtil.equals(db, actDb))
				fail(db + " != " + actDb);
		});
	}

	@Test
	public void testLoginSerialization() throws Exception {
		var db = InitializerSharedData.createDefaultData();
		var exp = new LoginResponse("abcde", true, db);

		checkSerialization(exp, act -> {
			assertEquals(exp.getClientID(), act.getClientID());
			assertEquals(exp.isLoginResult(), act.isLoginResult());

			var actDb = act.getTaskDatabase();
			if (!EcoreUtil.equals(db, actDb))
				fail(db + " != " + actDb);
		});
	}

	private static <T> void checkSerialization(T exp, Consumer<T> verifier) throws Exception {
		var bytes = new ByteArrayOutputStream();
		try (var oos = new ObjectOutputStream(bytes)) {
			oos.writeObject(exp);
		}
		try (var ois = new ObjectInputStream(new ByteArrayInputStream(bytes.toByteArray()))) {
			@SuppressWarnings("unchecked")
			var read = (T) ois.readObject();
			assertTrue(read != exp);
			verifier.accept(read);
		}
	}
}
