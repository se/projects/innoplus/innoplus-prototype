package de.unioldenburg.se.innoplus.networking;

import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.Message;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import org.junit.*;

import de.unioldenburg.se.innoplus.message.ExampleMessages.*;
import de.unioldenburg.se.innoplus.networking.MqttConnection.Configuration;

import java.util.Vector;
import java.util.List;

import static org.junit.Assert.*;

public final class MqttConnectionTest {

	private static final String TEST_BROKER = "ssl://innoplus-dev.Informatik.Uni-Oldenburg.DE:8883";

	private static final String TEST_TOPIC_ROOT = "test";

	private static final Configuration serverConfig = new Configuration(true, TEST_BROKER, "test-server", "server", "password", TEST_TOPIC_ROOT);

	private static final Configuration clientConfig1 = new Configuration(false, TEST_BROKER, "test-client-1", null, null, TEST_TOPIC_ROOT);

	private static final Configuration clientConfig2 = new Configuration(false, TEST_BROKER, "test-client-2", null, null, TEST_TOPIC_ROOT);

	@Test
	public void testSendReceive() throws Exception {
		var toServer = new CTS1(clientConfig1.clientId, 'x');
		var toClient = new STC1(clientConfig1.clientId, "Hello, World!");

		var recServer = new Vector<ClientToServer>();
		var recClient1 = new Vector<ServerToClient>();
		var recClient2 = new Vector<ServerToClient>();

		try (
				var server = MqttConnection.createServer(serverConfig);
				var client1 = MqttConnection.createClient(clientConfig1);
				var client2 = MqttConnection.createClient(clientConfig2);
		) {
			// Redirect incoming message into the lists defined above
			server.addListener(ClientToServer.class, recServer::add);
			client1.addListener(ServerToClient.class, recClient1::add);
			client2.addListener(ServerToClient.class, recClient2::add);

			client1.send(toServer);
			server.send(toClient);

			checkMessages(List.of(toServer), recServer);
			checkMessages(List.of(toClient), recClient1);
			checkMessages(List.of(), recClient2);
		}
	}

	@Test
	public void testDispatchByType() throws Exception {
		var msg1 = new CTS1(clientConfig1.clientId, 'X');
		var msg2 = new CTS2(clientConfig1.clientId, true);
		var msg3 = new CTS3(clientConfig1.clientId, 0xDEADBEEF);

		var rec1 = new Vector<CTS1>();
		var rec2 = new Vector<CTS2>();
		var rec3 = new Vector<ClientToServer>();

		try (
				var server = MqttConnection.createServer(serverConfig);
				var client = MqttConnection.createClient(clientConfig1);
		) {
			// Redirect incoming message into the lists defined above
			server.addListener(CTS1.class, rec1::add);
			server.addListener(CTS2.class, rec2::add);
			server.addListener(ClientToServer.class, rec3::add);

			client.send(msg1);
			client.send(msg2);
			client.send(msg3);

			checkMessages(List.of(msg1), rec1);
			checkMessages(List.of(msg2), rec2);
			checkMessages(List.of(msg3), rec3);
		}
	}

	@Test
	public void testRemoveListener() throws Exception {
		var msg = new CTS1(clientConfig1.clientId, 'X');
		var rec1 = new Vector<CTS1>();
		var rec2 = new Vector<ClientToServer>();

		try (
				var server = MqttConnection.createServer(serverConfig);
				var client = MqttConnection.createClient(clientConfig1);
		) {
			// Redirect incoming message into the lists defined above
			server.addListener(CTS1.class, rec1::add);
			server.addListener(ClientToServer.class, rec2::add);

			// First message is received in the specialised handler
			client.send(msg);
			checkMessages(List.of(msg), rec1);
			checkMessages(List.of(), rec2);

			// Remove specialised handler
			server.removeListener(CTS1.class);
			rec1.clear();

			// Second message is received in the general handler
			client.send(msg);
			checkMessages(List.of(msg), rec2);
			checkMessages(List.of(), rec1);
		}
	}

	@Test
	public void testIllegalArguments() throws Exception {

		try (var server = MqttConnection.createServer(serverConfig)) {

			assertThrows(NullPointerException.class, () -> server.send(null));

			assertThrows(NullPointerException.class, () -> server.addListener(null, msg -> {}));
			assertThrows(NullPointerException.class, () -> server.addListener(CTS1.class, null));

			// Redirect incoming message into the lists defined above
			assertThrows(IllegalArgumentException.class, () -> server.removeListener(CTS1.class));
		}
	}

	/** Wrapper for `assertEquals` that waits until messages are received (max 10 seconds) **/
	public static <T extends Message> void checkMessages(List<T> expected, List<T> actual) throws InterruptedException {

		// Just to make it more likely that this thread doesn't need to sleep for a fixed time
		Thread.yield();

		// Wait for delayed messages
		for (int i = 0; actual.size() < expected.size(); i++) {
			assertTrue("Timout while waiting for messages", i < 100);
			Thread.sleep(100);
		}

		assertEquals(expected, actual);
	}
}
