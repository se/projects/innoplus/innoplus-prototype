# Aufgaben für Studenten

Dieses Verzeichnis enthält alle registrierten Aufgaben.

## Ordnerstruktur

Die zugehörigen Dateien sind wie folgt verteilt:

- `README.md` enthält die Aufgabenstellung
- `tests` enthält JUnit5-Tests, mit denen die Korrektheit studentischer
  Lösungen überprüft werden kann.
- `student_solutions` enthält mehrere Unterordner, in denen mögliche
  Lösungen von Studenten implementiert wurden.
