import java.util.ArrayList;
import java.util.List;

/** Korrekte L�sung mithilfe des JDK's **/
public class Circle {

	private final List<String> list = new ArrayList<>();
	private int pos;

	/**
	* Erstellt eine neue Liste mit dem ersten Element `data`,
	* wodurch die Liste niemals leer ist.
	*/
	public Circle(String data) {
		list.add(data);
	}

	/** Liefert das aktuelle Element in der Liste **/
	public String getCurrent() {
		return list.get(pos);
	}

	/** Packt `data` hinter dem aktuellen Element in die Liste **/
	public void insert(String data) {
		list.add(pos + 1, data);
	}

	/** Setzt das aktuelle Element um eins weiter **/
	public void next() {
		pos = (pos + 1) % size();
	}

	/** Setzt das aktuelle Element um eins zur�ck **/
	public void previous() {
		pos = (pos + size() - 1) % size();
	}

	/** Liefert die Anzahl der Elemente in der Liste **/
	public int size() {
		return list.size();
	}

	/**
	* Erzeugt ein neues Array mit allen Elementen der Liste,
	* beginnend beim aktuellen Element.
	*/
	public String[] toArray() {
		return list.toArray(String[]::new);
	}
}
