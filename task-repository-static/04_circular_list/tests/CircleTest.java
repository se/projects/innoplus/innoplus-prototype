import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class CircleTest {

	@StudentClass
	public interface Circle{

		/** Liefert das aktuelle Element in der Liste **/
		public String getCurrent();

		/** Packt `data` hinter dem aktuellen Element in die Liste **/
		public void insert(String data);

		/** Setzt das aktuelle Element um eins weiter **/
		public void next();

		/** Setzt das aktuelle Element um eins zur�ck **/
		public void previous();

		/** Liefert die Anzahl der Elemente in der Liste **/
		public int size();

		/**
		 * Erzeugt ein neues Array mit allen Elementen der Liste,
		 * beginnend beim aktuellen Element.
		 */
		public String[] toArray();
	}

	public static StudentConstructor<CircleTest.Circle> circleCtor;

	@DisplayName("Initial Configuration is not correct.")
	@Test
	public void testInit() {
		Circle c = circleCtor.create("Hello");
		assertEquals(1, c.size(), "Unexpected Circle size!");
		assertEquals("Hello", c.getCurrent(), "Circle had unexpected elements.");
	}

	@DisplayName("Iterating through a Circle with one element failed.")
	@Test
	public void testIteration() {
		Circle c = circleCtor.create("Hello");
		c.next();
		assertEquals("Hello", c.getCurrent(), "Unexpected element!");
		c.previous();
		assertEquals("Hello", c.getCurrent(), "Unexpected element!");
	}

	@DisplayName("Filling in the Circle failed.")
	@Test
	public void testFillInPlace() {
		Circle c = circleCtor.create("1");
		c.insert("3");
		assertEquals(2, c.size(), "Unexpected Circle size.");
		assertEquals("1", c.getCurrent(), "Unexpected element!");
		c.insert("2");
		assertEquals("1", c.getCurrent(), "Unexpected element!");
		assertEquals(3, c.size(), "Unexpected Circle size!");

		c.next();
		assertEquals("2", c.getCurrent(), "Unexpected element!");

		c.next();
		assertEquals("3", c.getCurrent(), "Unexpected element!");

		c.next();
		assertEquals("1", c.getCurrent(), "Unexpected element!");
	}

	@DisplayName("Iterating the Circle forward failed.")
	@Test
	public void testFillForward() {
		Circle c = circleCtor.create("A");
		assertEquals("A", c.getCurrent(), "Unexpected element!");

		c.insert("B");
		assertEquals(2, c.size(), "Unexpected Circle size!");
		assertEquals("A", c.getCurrent(), "Unexpected element!");

		c.next();
		assertEquals("B", c.getCurrent(), "Unexpected element!");

		c.insert("C");
		assertEquals(3, c.size(), "Unexpected Circle size!");
		assertEquals("B", c.getCurrent(), "Unexpected element!");

		c.next();
		assertEquals("C", c.getCurrent(), "Unexpected element!");

		c.next();
		assertEquals("A", c.getCurrent(), "Unexpected element!");
		c.next();
		assertEquals("B", c.getCurrent(), "Unexpected element!");
		c.next();
		assertEquals("C", c.getCurrent(), "Unexpected element!");
	}

	@DisplayName("Iterating the Circle backward failed.")
	@Test
	public void testFillBackward() {
		Circle c = circleCtor.create("A");
		assertEquals("A", c.getCurrent(), "Unexpected element!");

		c.insert("B");
		assertEquals(2, c.size(), "Unexpected Circle size!");
		assertEquals("A", c.getCurrent(), "Unexpected element!");

		c.previous();
		assertEquals("B", c.getCurrent(), "Unexpected element!");

		c.insert("C");
		assertEquals(3, c.size(), "Unexpected Circle size!");
		assertEquals("B", c.getCurrent(), "Unexpected element!");

		c.previous();
		assertEquals("A", c.getCurrent(), "Unexpected element!");

		c.previous();
		assertEquals("C", c.getCurrent(), "Unexpected element!");
		c.previous();
		assertEquals("B", c.getCurrent(), "Unexpected element!");
		c.previous();
		assertEquals("A", c.getCurrent(), "Unexpected element!");
	}

	@DisplayName("Converting the Circle to an array failed.")
	@Test
	public void testToArray() {
		Circle c = circleCtor.create("a");
		assertArrayEquals(new String[] { "a" }, c.toArray(), "Array conversion went wrong!");

		c.insert("e");
		c.insert("d");
		c.insert("c");
		c.insert("b");
		assertArrayEquals(new String[] { "a", "b", "c", "d", "e" }, c.toArray(), "Array conversion went wrong!");
	}
}
