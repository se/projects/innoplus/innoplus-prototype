# Kreisförmige Liste

Bei dieser Aufgabe sollen sie eine "kreisförmige" Liste implementieren, also
eine Liste ohne Anfang und ohne Ende, bei der auf jedes Element ein weiteres folgt.
Es wird eine Position gespeichert, an der sich die Liste aktuell befindet. Von dort
aus kann die Liste um ein Element weiter "gedreht" werden, wobei am Ende der
Liste wiederum am Anfang fortgesetzt wird.

Definieren sie dafür eine Klasse`Circle` mit den folgenden Methoden:

```java
public class Circle {

	/**
	* Erstellt eine neue Liste mit dem ersten Element `data`,
	* wodurch die Liste niemals leer ist.
	*/
	public Circle(String data);

	/** Liefert das aktuelle Element in der Liste **/
	public String getCurrent();

	/** Packt `data` hinter dem aktuellen Element in die Liste **/
	public void insert(String data);

	/** Setzt das aktuelle Element um eins weiter **/
	public void next();

	/** Setzt das aktuelle Element um eins zurück **/
	public void previous();

	/** Liefert die Anzahl der Elemente in der Liste **/
	public int size();

	/**
	* Erzeugt ein neues Array mit allen Elementen der Liste,
	* beginnend beim aktuellen Element.
	*/
	public String[] toArray();
}
```
