import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import parser.Parser;
import testutils.StudentCodeParser;
import testutils.StudentSolutionLoader;
import testutils.TestResultExtractor;

import static testutils.Assertions.*;

@ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class})
public class GenericCutleryTest {

    @StudentCodeParser
    Parser parser;

    static final String FOOD = "Food";
    static final String SOFT_FOOD = "SoftFood";
    static final String SOLID_FOOD = "SolidFood";
    static final String LIQUID_FOOD = "LiquidFood";
    static final String CAKE = "Cake";
    static final String SOUP = "Soup";
    static final String PUDDING = "Pudding";

    static final String CUTLERY = "Cutlery";

    static final String FORK = "Fork";
    static final String SPOON = "Spoon";
    static final String SPORK = "Spork";

    static final String PASTRY_FORK = "PastryFork";
    static final String SOUP_SPOON = "SoupSpoon";
    static final String TRAVEL_SPORK = "TravelSpork";

    @DisplayName("Testing " + FOOD)
    @Test
    void testFood() {
        var food = assertInterfaceExist(parser, FOOD);
        assertHasMethod(food, "getName");
    }

    @DisplayName("Testing " + SOFT_FOOD)
    @Test
    void testSoftFood() {
        var food = assertInterfaceExist(parser, SOFT_FOOD);
        assertExtends(food, FOOD);
    }

    @DisplayName("Testing " + SOLID_FOOD)
    @Test
    void testSolidFood() {
        var food = assertInterfaceExist(parser, SOFT_FOOD);
        assertExtends(food, FOOD);
    }

    @DisplayName("Testing " + LIQUID_FOOD)
    @Test
    void testLiquidFood() {
        var food = assertInterfaceExist(parser, LIQUID_FOOD);
        assertExtends(food, FOOD);
    }

    @DisplayName("Testing " + CAKE)
    @Test
    void testCake() {
        var food = assertInterfaceExist(parser, CAKE);
        assertExtends(food, SOFT_FOOD);
        assertExtends(food, SOLID_FOOD);
    }


    @DisplayName("Testing " + SOUP)
    @Test
    void testSoup() {
        var food = assertInterfaceExist(parser, SOUP);
        assertExtends(food, SOFT_FOOD);
        assertExtends(food, LIQUID_FOOD);
    }

    @DisplayName("Testing " + PUDDING)
    @Test
    void testPudding() {
        var food = assertInterfaceExist(parser, PUDDING);
        assertExtends(food, SOFT_FOOD);
    }

    @DisplayName("Testing " + CUTLERY)
    @Test
    void testCutlery() {
        var cutlery = assertInterfaceExist(parser, CUTLERY);
        var food = assertInterfaceExist(parser, FOOD);
        assertHasTypeParameter(cutlery, food);
    }

    @DisplayName("Testing " + FORK)
    @Test
    void testFork() {
        var cutlery = assertInterfaceExist(parser, FORK);
        var food = assertInterfaceExist(parser, SOLID_FOOD);
        assertHasTypeParameter(cutlery, food);
        assertExtends(cutlery, CUTLERY);
    }

    @DisplayName("Testing " + SPOON)
    @Test
    void testSpoon() {
        var cutlery = assertInterfaceExist(parser, SPOON);
        var food = assertInterfaceExist(parser, LIQUID_FOOD);
        assertHasTypeParameter(cutlery, food);
        assertExtends(cutlery, CUTLERY);
    }

    @DisplayName("Testing " + SPORK)
    @Test
    void testSpork() {
        var cutlery = assertInterfaceExist(parser, SPORK);
        var food = assertInterfaceExist(parser, SOFT_FOOD);
        assertHasTypeParameter(cutlery, food);
        assertExtends(cutlery, CUTLERY);
    }
}
