// Common "wrong" solution which creates a valid result
// but ignores the given precondition (a, b sorted)
public class Sorting {

    public static int[] merge(int[] a, int[] b) {

        int[] res = new int[a.length + b.length];
        int idx = 0;

        // Append a to b
        for (int i = 0; i < a.length; i++)
            res[idx++] = a[i];

        for (int i = 0; i < b.length; i++)
            res[idx++] = b[i];

        // Sort the combined array using Bubblesort
        for (int i = res.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if (res[j] > res[j + 1]) {
                    int tmp = res[j];
                    res[j] = res[j + 1];
                    res[j + 1] = tmp;
                }
            }
        }

        return res;
    }
}
