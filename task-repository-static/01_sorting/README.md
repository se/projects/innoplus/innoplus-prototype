# Aufgabe zum Verbinden sortierter Arrays

Bei dieser Aufgabe sollen die Studenten den Merge-Schritt bei MergeSort
realisieren, indem sie eine Methode mit der folgenden Signatur
implementieren:
```java
public static int[] merge(int[] a, int[] b);
```
