# Verkette Listen

Gegeben sei folgende Java-Implementierung einer verketteten Liste:

```java
class Element {
	private int value;
	private Element next;

	public Element(int value, Element next) {
		this.value = value;
		this.next = next;
	}

	public Element getNext() {
		return next;
	}

	public void setNext(Element next) {
		this.next = next;
	}

	public int getValue() {
		return value;
	}
}

public class LinkedIntList {
	private Element start = null;

	/** Fügt ein neues Element am Anfang der Liste ein **/
	public void add(int value) {
		this.start = new Element(value, start);
	}

	public Element getStart() {
		return this.start;
	}

	/** Verdoppelt alle Einträge mit dem gegebenen Wert **/
	public void duplicate(int value) {
		// IMPLEMENT ME
	}
}
```

Diese Vorgaben dürfen nicht geändert werden!
Implementieren sie die Methode `LinkedIntList.duplicate` wie folgt:

Ein Aufruf dieser Methode soll bewirken, dass alle aktuellen Elemente der Liste
mit dem Wert des übergebenen Parameters `value` an ihrer jeweiligen Position
doppelt eingefügt werden.

Beispiel: Die Liste

```
5 -> 4 -> 3 -> 4 -> 4 -> 2 -> 2 -> 1
```

wird durch den Aufruf der Methode `duplicate(4)` zu der Liste

```
5 -> 4 -> 4 -> 3 -> 4 -> 4 -> 4 -> 4 -> 2 -> 2 -> 1
```

Hinweis: Die Klausuraufgabe gibt weitere Hinweise, diese wurden aber wegen der
         fehlenden Zeitbeschränkung ausgelassen.
