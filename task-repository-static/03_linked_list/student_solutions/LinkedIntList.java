
public class LinkedIntList {
	private Element start = null;

	/** Fügt ein neues Element am Anfang der Liste ein **/
	public void add(int value) {
		this.start = new Element(value, start);
	}

	public Element getStart() {
		return this.start;
	}

	/** Verdoppelt alle Einträge mit dem gegebenen Wert **/
	public void duplicate(int value) {
		for (Element cur = start; cur != null; cur = cur.getNext()) {
			if (cur.getValue() == value) {
				Element next = new Element(value, cur.getNext());
				cur.setNext(next);
				cur = next;
			}
		}
	}
}
