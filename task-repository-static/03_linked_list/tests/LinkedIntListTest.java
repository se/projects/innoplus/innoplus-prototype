import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import org.junit.jupiter.api.extension.ExtendWith;


@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class LinkedIntListTest {

	@StudentClass
	public static interface LinkedIntList {

		/** Fügt ein neues Element am Anfang der Liste ein **/
		public void add(int value);

		/** Liefert das erste Element der Liste */
		public Element getStart();

		/** Verdoppelt alle Einträge mit dem gegebenen Wert **/
		public void duplicate(int value);
	}

	@StudentClass
	public static interface Element {

		public Element getNext();

		public int getValue();
	}

	public static StudentConstructor<LinkedIntList> listCtor;

	@DisplayName("Alterations to methods were made that are not allowed.")
	@Test
	public void testNoModification() {
		LinkedIntList list = createList();
		Class<?> clazz = list.getClass();

		for (Method method : clazz.getDeclaredMethods()) {
			String name = method.getName();

			switch (name) {
				case "add":
					assertEquals(List.of(Integer.TYPE), List.of(method.getParameterTypes()),
							"It is not allowed to modify the add method.");
					assertEquals(Void.TYPE, method.getReturnType(), "It is not allowed to modify the add method.");
					break;

				case "duplicate":
					assertEquals(List.of(Integer.TYPE), List.of(method.getParameterTypes()),
							"It is not allowed to modify the duplicate method.");
					assertEquals(Void.TYPE, method.getReturnType(), "It is not allowed to modify the duplicate method.");
					break;

				case "getStart":
					assertEquals(List.of(), List.of(method.getParameterTypes()), "It is not allowed to modify the getStart method.");
					assertEquals(Element.class, method.getReturnType(), "It is not allowed to modify the getStart method.");
					break;

				case "equals":
				case "hashCode":
				case "toString":
					// Ignore default methods
					break;
				default:
					if ((method.getModifiers() & Modifier.PRIVATE) == 0)
						fail("Auxiliary method`" + name + "` should` be private!");

					break;
			}
		}
	}

	@DisplayName("Default state is not correct.")
	@Test
	public void testDefaultState() {
		LinkedIntList list = createList();
		assertNull(list.getStart(), "The LinkedIntList should be empty directly after initialization");
	}

	@DisplayName("Empty LinkedIntList was not empty after duplication.")
	@Test
	public void testEmpty() {
		LinkedIntList list = createList();
		list.duplicate(1);
		assertNull(list.getStart(), "The LinkedIntList should be empty directly after initialization");
	}

	@DisplayName("Duplication of a single Element at the beginning of LinkedIntList failed.")
	@Test
	public void testSingleStart() {
		LinkedIntList list = createList(1);
		list.duplicate(1);
		checkList(list, 1, 1);
	}

	@DisplayName("Duplication of an Element in the middle of a LinkedIntList failed.")
	@Test
	public void testSingleMiddle() {
		LinkedIntList list = createList(1, 2, 3);
		list.duplicate(2);
		checkList(list, 1, 2, 2, 3);
	}

	@DisplayName("Duplication of an Element at the end of a LinkedIntList failed.")
	@Test
	public void testSingleEnd() {
		LinkedIntList list = createList(1, 2, 3);
		list.duplicate(3);
		checkList(list, 1, 2, 3, 3);
	}

	@DisplayName("An Element was modified although it was not present.")
	@Test
	public void testNone() {
		LinkedIntList list = createList(1, 2);
		list.duplicate(3);
		checkList(list, 1, 2);
	}

	@DisplayName("The example from the exercise description failed.")
	@Test
	public void testExample() {
		LinkedIntList list = createList(5, 4, 3, 4, 4, 2, 2, 1);
		list.duplicate(4);
		checkList(list, 5, 4, 4, 3, 4, 4, 4, 4, 2, 2, 1);
	}

	/** Erzeugt eine neue Liste mit den gegebenen Werten **/
	private static LinkedIntList createList(int... values) {
		LinkedIntList list = listCtor.create();
		for (int i = values.length - 1; i >= 0; i--) {
			list.add(values[i]);
		}
		return list;
	}

	/** Vergleicht den Inhalt von list mit den erwarteten Werten values **/
	private static void checkList(LinkedIntList list, int... values) {
		Element cur = list.getStart();

		for (int val : values) {
			assertNotNull(cur, () -> createError("Expected further Elements!", list, values));
			assertEquals(val, cur.getValue(), () -> createError("Invalid Elements inside the LinkedIntList!", list, values));
			cur = cur.getNext();
		}

		assertNull(cur, createError("Invalid Element at the end of the LinkedIntList.", list, values));
	}

	/** Hilfsmethode, die eine brauchbare Fehlermeldung erzeugt **/
	private static String createError(String msg, LinkedIntList list, int... expected) {
		String res = msg + " [";
		boolean first = true;

		for (Element cur = list.getStart(); cur != null; cur = cur.getNext()) {
			if (first)
				first = false;
			else
				res += ", ";

			res += cur.getValue();
		}

		res += "] != [";

		first = true;
		for (int val : expected) {
			if (first)
				first = false;
			else
				res += ", ";

			res += val;
		}

		res += "]";

		return res;
	}
}
