import java.util.ArrayList;

public class StarDatabase {
	private final ArrayList<Star> stars = new ArrayList<>();

	/**Fügt einen neuen Stern hinzu **/
	public void add(Star star) {
		this.stars.add(star);
	}

	/** Entfernt den Stern am gegebenen Index **/
	public void remove(int index) {
		this.stars.remove(index);
	}

	/** Liefert den Stern am gegebenen Index **/
	public Star get(int index) {
		return this.stars.get(index);
	}

	/** Liefert die Anzahl der Sterne **/
	public int size() {
		return this.stars.size();
	}

	/** Findet den Stern mit gegebener ID (wenn vorhanden, ansonsten null) **/
	public Star find(String id) {
		for (Star star : this.stars) {
			if (star.getId().equals(id))
				return star;
		}
		return null;
	}

	/** Liefert alle Sterne mit eine scheinbaren Helligkeit in [low, high] **/
	public Star[] getMagnitudeRange(double low, double high) {
		int count = 0;

		for (Star star : this.stars) {
			double mag = star.getApparentMagnitude();
			if (low <= mag && mag <= high)
				count++;
		}

		Star[] result = new Star[count];
		count = 0;

		for (Star star : this.stars) {
			double mag = star.getApparentMagnitude();
			if (low <= mag && mag <= high)
				result[count++] = star;
		}

		return result;
	}
}
