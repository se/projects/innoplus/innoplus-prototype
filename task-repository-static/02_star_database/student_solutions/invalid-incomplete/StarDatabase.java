// Lösung nur teilweise implementiert

import java.util.ArrayList;

public class StarDatabase {
	private final ArrayList<Star> stars = new ArrayList<>();

	/**Fügt einen neuen Stern hinzu **/
	public void add(Star star) {
		this.stars.add(star);
	}

	/** Entfernt den Stern am gegebenen Index **/
	public void remove(int index) {
		this.stars.remove(index);
	}

	/** Liefert den Stern am gegebenen Index **/
	public Star get(int index) {
		return this.stars.get(index);
	}

	/** Liefert die Anzahl der Sterne **/
	public int size() {
		return this.stars.size();
	}
}
