import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class Cutlery1Test {

    @StudentClass
    public interface CutleryTest1 {
        @StudentMethod
        public void main(String... args);
        @StudentMethod
        public Material getMat(String name);
    }

    public static StudentConstructor<CutleryTest1> cut1;

    @StudentClass
    public interface Cutlery {
        @StudentMethod
        void setMaterial(Material material);

        @StudentMethod
        Material getMaterial();
    }

    @StudentClass
    public interface Knife extends Cutlery {}

    @StudentClass
    public interface Fork extends Cutlery {}

    @StudentClass
    public interface Spoon extends Cutlery {}

    @StudentClass
    public interface TableSpoon extends Spoon {}

    @StudentClass
    public interface TeaSpoon extends Spoon {}

    @StudentClass
    public interface DinnerFork extends Fork {}

    @StudentClass
    public interface PastryFork extends Fork {}

    @StudentClass
    public interface Material {}

    @StudentClass
    public interface Artifact {
        @StudentMethod
        public Material getMaterial();

        @StudentMethod
        public void setMaterial(Material material);

        @Override
        @StudentMethod
        public String toString();
    }

    @StudentClass
    public interface SteakKnife extends Artifact, Knife {}

    public static StudentConstructor<SteakKnife> steakKnifeConstructor;

    @StudentClass
    public interface ThreeProngedPastryFork extends Artifact, PastryFork {}

    public static StudentConstructor<ThreeProngedPastryFork> threeProngedPastryForkConstructor;

    @StudentClass
    public interface Spork extends Artifact, Spoon, Fork {}

    public static StudentConstructor<Spork> sporkConstructor;

    String[] materials = new String[]{"SILVER", "STAINLESS_STEEL", "WOOD", "PLASTIC"};

    @Disabled
    @DisplayName("Testing the functionality of the main-Method")
    @Test
    public void testMain() {
        final var expectedOutput = List.of(
                "TeaSpoon made from STAINLESS_STEEL",
                "SteakKnife made from SILVER",
                "Spork made from PLASTIC",
                "Spork made from WOOD",
                "My very own ThreeProngedPastryFork made from SILVER"
        );
        final var cut = cut1.create();
        assertEquals(expectedOutput, captureOutput(1000, cut::main));
    }

    @DisplayName("Testing the SteakKnife-Implementation")
    @Test
    public void testSteakKnifeImplementation() {
        final var cutlery = steakKnifeConstructor.create(material(materials[0]));
        for (var mat : materials) {
            cutlery.setMaterial(material(mat));
            assertEquals(material(mat), cutlery.getMaterial(),
                    "The Getter or Setter for Material does not work.");
            assertEquals("SteakKnife made from " + mat, cutlery.toString(),
                    "The toString-Method does not work as expected. Maybe a typo?");
        }
    }

    @DisplayName("Testing the ThreeProngedPastryFork-Implementation")
    @Test
    public void testThreeProngedPastryForkImplementation() {
        final var cutlery = threeProngedPastryForkConstructor.create(material(materials[0]));
        for (var mat : materials) {
            cutlery.setMaterial(material(mat));
            assertEquals("ThreeProngedPastryFork made from " + mat, cutlery.toString());
        }
    }

    @DisplayName("Testing the Spork-Implementation")
    @Test
    public void testSporkImplementation() {
        final var cutlery = sporkConstructor.create(material(materials[0]));
        for (var mat : materials) {
            cutlery.setMaterial(material(mat));
            assertEquals("Spork made from " + mat, cutlery.toString());
        }
    }

    private static Material material(String name) {
        final var cut = cut1.create();
        Material mat = null;
        try {
            mat = cut.getMat(name);
        } catch (IllegalArgumentException e) {
            fail("Material " + name + " is missing!");
        }
        return mat;
    }

    /**
     * Captures the output of the given {@link Runnable}s
     * @param timeout max run time of the runnables
     * @param runnables the runnables to be run
     * @return the captured output as List of Strings (each entry a line)
     */
    private static List<String> captureOutput(long timeout, List<Runnable> runnables) {
        try (final var outputStream = new ByteArrayOutputStream()) {
            final var threads = runnables.stream()
                    .map(Thread::new)
                    .collect(Collectors.toList());

            final var old = System.out;
            System.setOut(new PrintStream(outputStream));

            threads.forEach(Thread::start);

            // Let this thread wait for the runnables to run for timeout amount of time
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e){
                unexpectedException(e);
            }

            threads.forEach(Thread::interrupt);
            threads.stream().filter(Thread::isAlive).forEach(Thread::stop);

            System.setOut(old);

            final var output = outputStream.toString();

            // String.split() always returns an array with at least one element (the not splitted string)
            if (output.length() == 0) {
                return Collections.emptyList();
            }

            return Arrays.stream(output.split("\\n")).collect(Collectors.toList());
        } catch (IOException e) {
            unexpectedException(e);
        }
        return Collections.emptyList();
    }

    /**
     * Captures the output of the given {@link Runnable}
     * @param timeout max run time of the runnable
     * @param runnable the runnable to be run
     * @return the captured output as List of Strings (each entry a line)
     */
    private static List<String> captureOutput(long timeout, Runnable runnable) {
        return captureOutput(timeout, List.of(runnable));
    }

    /**
     * helper method to report unexpected failure
     * @param e the unexpected exception
     */
    private static void unexpectedException(Exception e) {
        fail("Unexpected Exception thrown!", e);
    }
}
