# Streams

Gegeben sind die Klasse AquaticAnimal mit ihren Spezialisierungen Fish, Starfish, Turtle und Squid.
Außerdem gegeben ist die Klasse StreamTest mit drei oeffentlichen Methoden zur Erzeugung von Stream<AquaticAnimal>-Instanzen (createStream1-3).

Schreiben Sie nun in der Klasse StreamTest folgende fuenf Methoden:

```java
public static Stream<AquaticAnimal> filterFins(Stream<AquaticAnimal> stream)
```
Wirft alles aus dem Stream, was keine Flossen (fins) hat.

```java	
public static Stream<Squid> filterSquids(Stream<AquaticAnimal> stream)
```

Wirft alles aus dem Stream, was kein Squid ist. Hinweis: Erinnern Sie sich an die Typpruefung mittels instanceof.

```java	
public static double averageAppendages(Stream<AquaticAnimal> stream)
```
Summiert fuer jedes Element des Streams die Anzahl der Flossen, Gliedmassen und Tentakeln (fins, limbs, tentacles) und gibt den Durchschnittswert dieser Summen zurueck.
	
```java
public static String filterLimbs(Stream<AquaticAnimal> stream)
```
Wirft alles aus dem Stream, was weniger als 5 Gliedmassen (limbs) hat, ersetzt dann jede Squid-Instanz durch eine neue Fish-Instanz, ersetzt dann jedes Element durch seine String-Repraesentation (toString()), sortiert den Stream alphabetisch, und gibt anschliessend den Gesamt-String zurueck.
	
```java
public static int countEyes(Stream<AquaticAnimal> stream)
```
Behaelt nur Tiere im Stream, die weniger als 6 Gliedmassen (limbs) oder mindestens eine Tentakel (tentacle) haben, behaelt davon wiederum nur die ersten 10, und gibt hiervon die Gesamtsumme der Augen aus.

```java
public class StreamTest {
	
	public static void main(String[] args) {
		System.out.println(filterFins(createStream1()).count()); // 3
		System.out.println(filterFins(createStream2()).count()); // 15
		System.out.println(filterFins(createStream3()).count()); // 30
		
		System.out.println(filterSquids(createStream1()).count()); // 1
		System.out.println(filterSquids(createStream2()).count()); // 0
		System.out.println(filterSquids(createStream3()).count()); // 10

		System.out.println(averageAppendages(createStream1())); // 5.0
		System.out.println(averageAppendages(createStream2())); // 4.035714285714286
		System.out.println(averageAppendages(createStream3())); // 5.0

		System.out.println(filterLimbs(createStream1())); // *****F
		System.out.println(filterLimbs(createStream2())); // *
		System.out.println(filterLimbs(createStream3())); // **************************************************FFFFFFFFFF

		System.out.println(countEyes(createStream1())); // 10
		System.out.println(countEyes(createStream2())); // 18
		System.out.println(countEyes(createStream3())); // 18
	}
	
	public static Stream<AquaticAnimal> filterFins(Stream<AquaticAnimal> stream) {
		// TODO: implement this
        return stream;
	}
	
	public static Stream<Squid> filterSquids(Stream<AquaticAnimal> stream) {
		// TODO: implement this
        return stream;
	}
	
	public static double averageAppendages(Stream<AquaticAnimal> stream) {
		// TODO: implement this
        return 0.0;
	}
	
	public static String filterLimbs(Stream<AquaticAnimal> stream) {
		// TODO: implement this
        return "";
	}
	
	public static int countEyes(Stream<AquaticAnimal> stream) {
		// TODO: implement this
        return 0;
	}
	
	public static Stream<AquaticAnimal> createStream1() {
		return createStream(3, 5, 2, 1);
	}
	
	public static Stream<AquaticAnimal> createStream2() {
		return createStream(15, 1, 12, 0);
	}
	
	public static Stream<AquaticAnimal> createStream3() {
		return createStream(30, 50, 20, 10);
	}
	
	protected static Stream<AquaticAnimal> createStream(int fish, int starfish, int turtles, int squids) {
		Random rand = new Random();
		List<AquaticAnimal> list = new ArrayList<>(fish + starfish + turtles + squids);
		while (fish + starfish + turtles + squids > 0) {
			int next = rand.nextInt(4);
			if (next == 0 && fish > 0) {
				list.add(new Fish());
				fish--;
			} else if (next == 1 && starfish > 0) {
				list.add(new Starfish());
				starfish--;
			} else if (next == 2 && turtles > 0) {
				list.add(new Turtle());
				turtles--;
			} else if (next == 3 && squids > 0) {
				list.add(new Squid());
				squids--;
			}
		}
		return list.stream();
	}

	public static class AquaticAnimal {
		protected int fins;
		protected int limbs;
		protected int tentacles;
		protected int eyes;
		public AquaticAnimal(int fins, int limbs, int tentacles, int eyes) {
			super();
			this.fins = fins;
			this.limbs = limbs;
			this.tentacles = tentacles;
			this.eyes = eyes;
		}
		public int getFins() {
			return fins;
		}
		public void setFins(int fins) {
			this.fins = fins;
		}
		public int getLimbs() {
			return limbs;
		}
		public void setLimbs(int limbs) {
			this.limbs = limbs;
		}
		public int getTentacles() {
			return tentacles;
		}
		public void setTentacles(int tentacles) {
			this.tentacles = tentacles;
		}
		public int getEyes() {
			return eyes;
		}
		public void setEyes(int eyes) {
			this.eyes = eyes;
		}
	}
	
	public static class Fish extends AquaticAnimal {
		public Fish() {
			super(4, 0, 0, 2);
		}
		@Override
		public String toString() {
			return "F";
		}
	}
	
	public static class Starfish extends AquaticAnimal {
		public Starfish() {
			super(0, 5, 0, 0);
		}
		@Override
		public String toString() {
			return "*";
		}
	}
	
	public static class Turtle extends AquaticAnimal {
		public Turtle() {
			super(0, 4, 0, 2);
		}
		@Override
		public String toString() {
			return "T";
		}
	}
	
	public static class Squid extends AquaticAnimal {
		public Squid() {
			super(0, 8, 2, 2);
		}
		@Override
		public String toString() {
			return "S";
		}
	}

}
```
