import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import parser.Parser;
import testutils.StudentCodeParser;
import testutils.StudentSolutionLoader;
import testutils.TestResultExtractor;

import static testutils.Assertions.*;

@ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class})
public class CutleryTest1Test {

    @StudentCodeParser
    Parser parser;

    static final String MATERIAL = "Material";
    static final String ARTIFACT = "Artifact";
    static final String CUTLERY = "Cutlery";
    static final String KNIFE = "Knife";
    static final String FORK = "Fork";
    static final String SPOON = "Spoon";
    static final String TABLE_SPOON = "TableSpoon";
    static final String TEA_SPOON = "TeaSpoon";
    static final String DINNER_FORK = "DinnerFork";
    static final String PASTRY_FORK = "PastryFork";
    static final String STEAK_KNIFE = "SteakKnife";
    static final String THREE_PRONGED_PASTRY_FORK = "ThreeProngedPastryFork";
    static final String SPORK = "Spork";


    @DisplayName("Testing " + MATERIAL)
    @Test
    void testMaterial() {
        final var mat = assertEnumExist(parser, MATERIAL);
        assertHasEntry(mat, "SILVER");
        assertHasEntry(mat, "STAINLESS_STEEL");
        assertHasEntry(mat, "WOOD");
        assertHasEntry(mat, "PLASTIC");
    }

    @DisplayName("Testing " + ARTIFACT)
    @Test
    void testArtifact() {
        final var art = assertClassExist(parser, ARTIFACT);
        assertIsAbstract(art);
    }

    @DisplayName("Testing " + CUTLERY)
    @Test
    void testCutlery() {
        assertInterfaceExist(parser, CUTLERY);
    }

    @DisplayName("Testing " + KNIFE)
    @Test
    void testKnife() {
        final var cut = assertInterfaceExist(parser, KNIFE);
        assertExtends(cut, CUTLERY);
    }

    @DisplayName("Testing " + FORK)
    @Test
    void testFork() {
        final var cut = assertInterfaceExist(parser, FORK);
        assertExtends(cut, CUTLERY);
    }

    @DisplayName("Testing " + SPOON)
    @Test
    void testSpoon() {
        final var cut = assertInterfaceExist(parser, SPOON);
        assertExtends(cut, CUTLERY);
    }

    @DisplayName("Testing " + TABLE_SPOON)
    @Test
    void testTableSpoon() {
        final var cut = assertInterfaceExist(parser, TABLE_SPOON);
        assertExtends(cut, SPOON);
    }

    @DisplayName("Testing " + TEA_SPOON)
    @Test
    void testTeaSpoon() {
        final var cut = assertInterfaceExist(parser, TEA_SPOON);
        assertExtends(cut, SPOON);
    }

    @DisplayName("Testing " + DINNER_FORK)
    @Test
    void testDinnerFork() {
        final var cut = assertInterfaceExist(parser, DINNER_FORK);
        assertExtends(cut, FORK);
    }

    @DisplayName("Testing " + PASTRY_FORK)
    @Test
    void testPastryFork() {
        final var cut = assertInterfaceExist(parser, PASTRY_FORK);
        assertExtends(cut, FORK);
    }

    @DisplayName("Testing " + STEAK_KNIFE)
    @Test
    void testSteakKnife() {
        final var art = assertClassExist(parser, STEAK_KNIFE);
        assertExtends(art, ARTIFACT);
        assertImplements(art, KNIFE);
    }

    @DisplayName("Testing " + THREE_PRONGED_PASTRY_FORK)
    @Test
    void testThreeProngedPastryFork() {
        final var art = assertClassExist(parser, THREE_PRONGED_PASTRY_FORK);
        assertExtends(art, ARTIFACT);
        assertImplements(art, PASTRY_FORK);
    }

    @DisplayName("Testing " + SPORK)
    @Test
    void testSpork() {
        final var art = assertClassExist(parser, SPORK);
        assertExtends(art, ARTIFACT);
        assertImplements(art, SPOON);
        assertImplements(art, FORK);
    }
}
