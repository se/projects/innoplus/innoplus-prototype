public class RunningGame {

	public static void main(String[] args) {
		new RunningGame().start();
		System.out.println();
		new RunningGame().start2();
	}
	
	public void start() {
		Goal left = new Goal("left");
		Goal right = new Goal("right");
		left.setOther(right);
		right.setOther(left);
		Thread[] threads = new Thread[] {
				new Thread(new Hare(left)),
				new Thread(new Hedgehog(left)),
				new Thread(new Hedgehog(right))
		};
		for (Thread thread : threads) {
			thread.start();
		}
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) { }
		for (Thread thread : threads) {
			thread.interrupt();
		}
		/*
Hare: *running*
Hare: I'm here at the left goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the right goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the left goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the right goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the left goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the right goal!
Hedgehog: Neener-neener, I'm here already!
		 */
	}
	
	public void start2() {
		Goal first = new Goal("first");
		Goal second = new Goal("second");
		Goal third = new Goal("third");
		first.setOther(second);
		second.setOther(third);
		third.setOther(first);
		Thread[] threads = new Thread[] {
				new Thread(new Hare(first)),
				new Thread(new Hedgehog(first)),
				new Thread(new Hedgehog(second)),
				new Thread(new Hedgehog(third))
		};
		for (Thread thread : threads) {
			thread.start();
		}
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) { }
		for (Thread thread : threads) {
			thread.interrupt();
		}
	}
	/*
Hare: *running*
Hare: I'm here at the first goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the second goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the third goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the first goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the second goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the third goal!
Hedgehog: Neener-neener, I'm here already!
	 */
	 
	public static class Goal {
		
		String name;
		Goal other;
		
		public Goal(String name) {
			super();
			this.name = name;
		}

		public Goal getOther() {
			return other;
		}

		public void setOther(Goal other) {
			this.other = other;
		}

		public String getName() {
			return name;
		}
		
	}

	public static class Hare implements Runnable {
		
		Goal goal;
		
		public Hare(Goal goal) {
			super();
			this.goal = goal;
		}

		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				System.out.println("Hare: *running*");
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					break;
				}
				System.out.println("Hare: I'm here at the " + goal.name + " goal!");
				synchronized(goal) {
					goal.notify();
				}
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					break;
				}
				goal = goal.other;
			}
		}
		
	}

	public static class Hedgehog implements Runnable {

		Goal goal;

		public Hedgehog(Goal goal) {
			super();
			this.goal = goal;
		}

		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				synchronized(goal) {
					try {
						goal.wait();
					} catch (InterruptedException e) {
						break;
					}
				}
				System.out.println("Hedgehog: Neener-neener, I'm here already!");
			}
		}

	}

}
