package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.query.QueryExecutor;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.staticanalysis.StaticQueryFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class StaticQueryTest {
    static Stream<Arguments> studentSolutionProvider() {
        return Stream.of(
                arguments(
                        Paths.get(".", "..", "task-repository", "09_generic_cutlery", "student_solutions", "valid-optimal"),
                        InnoplusConstants.CODE_TASK_GENERIC_CUTLERY, 5)

        );
    }

    @ParameterizedTest
    @MethodSource("studentSolutionProvider")
    public void checkStudentSolutions(Path testFolder, String taskName, int expectedResults) throws IOException {

        // create data for our DataModel to test JUnit Queries
        var data = createJUnitTestData();
        assertNotNull(data);

        DataFactory factory = DataFactory.eINSTANCE;

        // query management
        var manager = new QueryManager();
        manager.register(taskName, new StaticQueryFactory(new DockerDriver()));
        assertNotNull(manager);

        // create student solution
        var studentSolution = factory.createStudentSolution();
        studentSolution.setDate(new Date());
        studentSolution.setTask(
                data.getTasks().stream()
                        .filter(t -> taskName.equals(t.getName()))
                        .findFirst().orElseThrow(IllegalStateException::new)
        );

        // load Java files to test
        // TODO UTIL METHOD
        var javaFiles = new HashMap<String, String>();
        for (var fileIt = Files.walk(testFolder).iterator(); fileIt.hasNext();){
            var path = fileIt.next();
            if (Files.isDirectory(path))
                continue;
            javaFiles.put(testFolder.relativize(path).toString(), Files.readString(path));
        }


        var solutionContent = factory.createSolutionContent();
        solutionContent.setContent(javaFiles);
        studentSolution.setContents(solutionContent);


        // Create and execute Queries
        var executor = new QueryExecutor();
        var queries = manager.map(studentSolution);

        assertNotNull(queries);
        assertFalse(queries.isEmpty());

        List<QueryExecutor.Result> results = null;
        for (int i = 0; i < 1; i++) {
            results = executor.executeQueries(queries, studentSolution);
        }

    }


    // TODO A Test Data Initializer might be cooler
    /** We do not need the whole Initial Data provided by the Initializer */
    private InnoplusDatabase createJUnitTestData(){
        var factory = DataFactory.eINSTANCE;
        var testData = factory.createInnoplusDatabase();

        var taskCategoryJavaCode = Initializer.generateTaskCategory("JavaCodeTask", testData);
        var solutionCategoryJava = Initializer.generateSolutionCategory("JavaCode", testData);

        // Create Tasks that we want to test
        var genericCutlery = factory.createTask();
        genericCutlery.setName(InnoplusConstants.CODE_TASK_GENERIC_CUTLERY);
        genericCutlery.setDescription("Empty Description");
        testData.getTasks().add(genericCutlery);
        genericCutlery.setCategory(taskCategoryJavaCode);

        var staticAnalysis = Paths.get(".", "..", "task-repository", "09_generic_cutlery", "static_analysis", "GenericCutleryTest.java")
                .toAbsolutePath()
                .normalize();
        assertTrue(Files.exists(staticAnalysis));

        Initializer.generateSampleSolution(genericCutlery, solutionCategoryJava, testData)
                .setContents(Initializer.generateSolutionContent(staticAnalysis,Map.of("static", staticAnalysis.toString())));


        return testData;
    }
}
