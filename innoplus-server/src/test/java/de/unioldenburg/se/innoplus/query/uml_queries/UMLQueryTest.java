package de.unioldenburg.se.innoplus.query.uml_queries;

import de.unioldenburg.se.innoplus.DynamicInitializer;
import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.query.QueryExecutor;
import de.unioldenburg.se.innoplus.query.QueryManager;
import org.eclipse.core.runtime.Path;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

class UMLQueryTest {

    static Stream<Arguments> studentSolutionProvider() {
        return Stream.of(
                arguments("GUI Modellierung", "GUI.uml")
        );
    }

    @ParameterizedTest
    @MethodSource("studentSolutionProvider")
    void evaluate(String taskName, String umlFileName) throws Exception {
        // load data
        var dataManager = new DataManager("src/test/resources/", new DynamicInitializer(), false);
        var data = dataManager.getData();

        var task = data.getTasks().stream()
                .filter(t -> t.getName().equals(taskName))
                .findFirst()
                .orElseThrow(IllegalStateException::new);

        var studentSolution = DataFactory.eINSTANCE.createStudentSolution();
        var solutionContent = DataFactory.eINSTANCE.createSolutionContent();
        solutionContent.setContent(
                new File(getClass().getClassLoader().getResource("modeling"+ Path.SEPARATOR + umlFileName).toURI())
        );
        studentSolution.setContents(solutionContent);
        studentSolution.setTask(task);

        var manager = new QueryManager();
        manager.register("GUI Modellierung", new UMLQueryFactory());
        var queries = manager.map(studentSolution);
        var executor = new QueryExecutor();
        var results = executor.executeQueries(queries, studentSolution);
        for (var result : results){
            System.out.println(result.getResult().getSummary());
            System.out.println(result.getResult().getType());
        }
    }
}