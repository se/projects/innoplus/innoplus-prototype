package de.unioldenburg.se.innoplus;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.data.Student;
import de.unioldenburg.se.innoplus.message.EcoreIO;
import de.unioldenburg.se.innoplus.persistence.DataManager;

class DataManagerTest {

	@Test
	void test() throws IOException {
		String folder = "src/test/resources/";
		DataManager manager = new DataManager(folder, new DynamicInitializer(), false);
		InnoplusDatabase data = manager.getData();

		// 2 lectures
		assertEquals(2, data.getLectures().size());

		// check lecture "PDA"
		Lecture pda = getPDA(data);
		testPDA(data);

		// create a new student for PDA
		Student student = DataFactory.eINSTANCE.createStudent();
		data.getPersons().add(student);
		pda.getParticipants().add(student);
		student.setId("someID");

		// filter the data + check PDA again
		InnoplusDatabase dataPart = manager.extractTasksFor(student, pda);
		pda = getPDA(dataPart);
		testPDA(dataPart);

		// save
		try (FileOutputStream stream = new FileOutputStream(new File(folder, "data.txt"))) {
			EcoreIO.saveEObject(dataPart, stream);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
		// load
		try (FileInputStream stream = new FileInputStream(new File(folder, "data.txt"))) {
			InnoplusDatabase dataReload = (InnoplusDatabase) EcoreIO.loadEObject(stream);
			pda = getPDA(dataReload);
			testPDA(dataReload);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	private Lecture getPDA(InnoplusDatabase data) {
		// lecture "PDA"
		Optional<Lecture> any = data.getLectures().stream().filter(l -> "PDA".equals(l.getName())).findAny();
		assertTrue(any.isPresent());
		return any.get();
	}

	private void testPDA(InnoplusDatabase data) {
		Lecture pda = getPDA(data);

		// PDA has 1 task
		assertEquals(4, pda.getSelectedTasks().size());
	}

}
