package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.query.QueryExecutor;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.checkstyle.CheckStyleQueryFactory;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CheckStyleTest {

    static Stream<Arguments> studentSolutionProvider() {
        return Stream.of(
//                arguments(
//                        Paths.get(".", "..", "task-repository", "01_sorting", "student_solutions", "valid-optimal"),
//                        Initializer.CODE_TASK_MERGE_ARRAYS, 5)
//                 arguments(
//                         Paths.get(".", "..", "task-repository", "02_star_database", "student_solutions", "invalid-incomplete"),
//                         Initializer.CODE_TASK_STAR_DATABASE, 5),
//                 arguments(
//                         Paths.get(".", "..", "task-repository", "03_linked_list", "student_solutions"),
//                         Initializer.CODE_TASK_MERGE_LINKED_LIST, 5),
//                 arguments(
//                         Paths.get(".", "..", "task-repository", "04_circular_list", "student_solutions", "valid_jdk"),
//                         Initializer.CODE_TASK_MERGE_CIRCULAR_LIST, 5)
                arguments(
                        Paths.get(".", "..", "task-repository", "05_hare_and_hedgehog", "student_solutions", "valid-optimal"),
                        InnoplusConstants.CODE_TASK_RUNNING_GAME, 5)

        );
    }

    @ParameterizedTest
    @MethodSource("studentSolutionProvider")
    public void checkStudentSolutions(Path testFolder, String taskName, int expectedResults) throws IOException {

        // create data for our DataModel to test JUnit Queries
        var data = createJUnitTestData();
        assertNotNull(data);

        DataFactory factory = DataFactory.eINSTANCE;

        // query management
        var manager = new QueryManager();
        manager.register(taskName, new CheckStyleQueryFactory(new DockerDriver()));
        assertNotNull(manager);

        // create student solution
        var studentSolution = factory.createStudentSolution();
        studentSolution.setDate(new Date());
        studentSolution.setTask(
                data.getTasks().stream()
                        .filter(t -> taskName.equals(t.getName()))
                        .findFirst().orElseThrow(IllegalStateException::new)
        );

        // load Java files to test
        // TODO UTIL METHOD
        var javaFiles = new HashMap<String, String>();
        for (var fileIt = Files.walk(testFolder).iterator(); fileIt.hasNext();){
            var path = fileIt.next();
            if (Files.isDirectory(path))
                continue;
            javaFiles.put(testFolder.relativize(path).toString(), Files.readString(path));
        }


        var solutionContent = factory.createSolutionContent();
        solutionContent.setContent(javaFiles);
        studentSolution.setContents(solutionContent);


        // Create and execute Queries
        var executor = new QueryExecutor();
        var queries = manager.map(studentSolution);
        assertNotNull(queries);
        assertFalse(queries.isEmpty());
        List<QueryExecutor.Result> results = null;
        var times = new ArrayList<OffsetDateTime>();
        times.add(OffsetDateTime.now());
        for (int i = 0; i < 3; i++) {
            results = executor.executeQueries(queries, studentSolution);
            times.add(OffsetDateTime.now());
        }
        assertNotNull(results);
        assertFalse(results.isEmpty());

        // check result
        results.forEach(result -> {
            System.out.println("---------------------------------");
            System.out.println(result.getResult().getType());
            System.out.println(result.getResult().getSummary());
            System.out.println(result.getResult().getDescription());
            System.out.println(result.getResult().getDate());
            System.out.println(result.getResult().getLocation());
            System.out.println("---------------------------------");
        });
        assertNotNull(results.get(0).getResult());
        QueryResult result = results.get(0).getResult();
        assertNotNull(result.getDate());

        times.forEach(System.out::println);
    }


    // TODO A Test Data Initializer might be cooler
    /** We do not need the whole Initial Data provided by the Initializer */
    private InnoplusDatabase createJUnitTestData(){
        var factory = DataFactory.eINSTANCE;
        var testData = factory.createInnoplusDatabase();

        var taskCategoryJavaCode = Initializer.generateTaskCategory("JavaCodeTask", testData);
        var solutionCategoryJava = Initializer.generateSolutionCategory("JavaCode", testData);

        // Create Tasks that we want to test
        var mergeArrays = factory.createTask();
        mergeArrays.setName(InnoplusConstants.CODE_TASK_MERGE_ARRAYS);
        mergeArrays.setDescription("Empty Description");
        testData.getTasks().add(mergeArrays);
        mergeArrays.setCategory(taskCategoryJavaCode);

        var starDatabase = factory.createTask();
        starDatabase.setName(InnoplusConstants.CODE_TASK_STAR_DATABASE);
        starDatabase.setDescription("Empty Description");
        testData.getTasks().add(starDatabase);
        starDatabase.setCategory(taskCategoryJavaCode);

        var linkedList = factory.createTask();
        linkedList.setName(InnoplusConstants.CODE_TASK_MERGE_LINKED_LIST);
        linkedList.setDescription("Empty Description");
        testData.getTasks().add(linkedList);
        linkedList.setCategory(taskCategoryJavaCode);

        var circularList = factory.createTask();
        circularList.setName(InnoplusConstants.CODE_TASK_MERGE_CIRCULAR_LIST);
        circularList.setDescription("Empty Description");
        testData.getTasks().add(circularList);
        circularList.setCategory(taskCategoryJavaCode);

        var runningGame = factory.createTask();
        runningGame.setName(InnoplusConstants.CODE_TASK_RUNNING_GAME);
        runningGame.setDescription("Empty Description");
        testData.getTasks().add(runningGame);
        runningGame.setCategory(taskCategoryJavaCode);

        // create Teacher Solutions for our Tasks
        var testPath = Paths.get(".", "..", "task-repository", "01_sorting", "code_checks", "custom-checks.xml")
                .toAbsolutePath()
                .normalize();
        assertTrue(Files.exists(testPath));

        Initializer.generateSampleSolution(
                mergeArrays, solutionCategoryJava, testData)
                .setContents(Initializer.generateSolutionContent(testPath,
                        Map.of("check", testPath.toString()))
                );

        testPath = Paths.get(".", "..", "task-repository", "02_star_database", "code_checks", "sun_checkstyle.xml")
                .toAbsolutePath()
                .normalize();
        assertTrue(Files.exists(testPath));
        Initializer.generateSampleSolution(starDatabase, solutionCategoryJava, testData)
                .setContents(Initializer.generateSolutionContent(testPath,
                        Map.of("check", testPath.toString()))
                );

        testPath = Paths.get(".", "..", "task-repository", "03_linked_list", "code_checks", "sun_checkstyle.xml")
                .toAbsolutePath()
                .normalize();
        assertTrue(Files.exists(testPath));
        Initializer.generateSampleSolution(linkedList, solutionCategoryJava, testData)
                .setContents(Initializer.generateSolutionContent(testPath,
                        Map.of("check", testPath.toString()))
                );

        testPath = Paths.get(".", "..", "task-repository", "04_circular_list", "code_checks", "sun_checkstyle.xml")
                .toAbsolutePath()
                .normalize();
        assertTrue(Files.exists(testPath));
        Initializer.generateSampleSolution(circularList, solutionCategoryJava, testData)
                .setContents(Initializer.generateSolutionContent(testPath,
                        Map.of("check", testPath.toString()))
                );

        testPath = Paths.get(".", "..", "task-repository", "05_hare_and_hedgehog", "code_checks", "custom-checks.xml")
                .toAbsolutePath()
                .normalize();
        assertTrue(Files.exists(testPath));
        Initializer.generateSampleSolution(runningGame, solutionCategoryJava, testData)
                .setContents(Initializer.generateSolutionContent(testPath,
                        Map.of("check", testPath.toString()))
                );


        return testData;
    }
}
