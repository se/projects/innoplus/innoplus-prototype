package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.StudIpAdapter.UserKind;

import java.io.IOException;

import org.junit.Test;

import static org.junit.Assert.*;

public class StudipAdapterTest {

	public static final String
		PDA = "36e93f23b97226d889adb38483b273fa", // PDA, WS 20/21
		ST1 = "2d54797bbfce8f01aa6db98f73835709", // ST1, WS 20/21

		ST1_STUDENT = "7bf927092164d4442bd55d73f58a006a",
		PDA_TUTOR   = "5fddbe3b3544a3ed2daf69755b4eabda",
		ST1_TEACHER = "64ebee080195ff2efbaa7d40c1b09415",
		UNKNOWN     = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

	@Test
	public void testUnknown() throws Exception {
		test(ST1, UNKNOWN, UserKind.Unknown);
	}

	@Test
	public void testST1Student() throws Exception {
		test(ST1, ST1_STUDENT, UserKind.Student);
	}

	@Test
	public void testPDATutor() throws Exception {
		test(PDA, PDA_TUTOR, UserKind.Tutor);
	}

	@Test
	public void testST1Lecturer() throws Exception {
		test(ST1, ST1_TEACHER, UserKind.Teacher);
	}

	// The API yields 0 (with status 200) for unknown lectures...
	// @Test
	// public void testUnknownLecture() throws Exception {
	// 	assertThrows(IllegalArgumentException.class, () -> test(UNKNOWN, ST1_STUDENT, UserKind.Unknown));
	// }

	private void test(String lectureID, String userHashed, UserKind status) throws Exception {
		var adapter = new StudIpAdapter();
		var result = adapter.validateCredentials(userHashed, lectureID);
		assertEquals(status, result);
	}
}
