package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.query.QueryExecutor;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.junit.JUnitQueryFactory;
import de.unioldenburg.se.innoplus.query.uml.UMLQueryFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class UMLQueryTest {

    static Stream<Arguments> studentSolutionProvider() {
        return Stream.of(
                arguments(
                        Paths.get(".", "..", "task-repository-static", "01_modelling", "student_solutions", "valid-optimal"),
                        StaticInitializer.UML_TASK_TEST),
                arguments(
                        Paths.get(".", "..", "task-repository-static", "01_modelling", "student_solutions", "suboptimal-different-names"),
                        StaticInitializer.UML_TASK_TEST),
                arguments(
                        Paths.get(".", "..", "task-repository-static", "01_modelling", "student_solutions", "invalid-missing-class"),
                        StaticInitializer.UML_TASK_TEST)
        );
    }

    @ParameterizedTest
    @MethodSource("studentSolutionProvider")
    public void checkStudentSolution(Path sPath, String taskID) throws Exception {
        var data = createJUnitTestData();
        assertNotNull(data);

        DataFactory factory = DataFactory.eINSTANCE;

        // query management
        var manager = new QueryManager();
        manager.register(taskID, new UMLQueryFactory());
        assertNotNull(manager);

        // create student solution
        var studentSolution = factory.createStudentSolution();
        studentSolution.setDate(new Date());
        studentSolution.setTask(
                data.getTasks().stream()
                        .filter(t -> taskID.equals(t.getName()))
                        .findFirst().orElseThrow(IllegalStateException::new)
        );

        var files = new HashMap<String, String>();
        for (var fileIt = Files.walk(sPath).iterator(); fileIt.hasNext();){
            var path = fileIt.next();
            if (Files.isDirectory(path))
                continue;
            files.put(sPath.relativize(path).toString(), Files.readString(path));
        }

        var solutionContent = factory.createSolutionContent();
        solutionContent.setContent(files);
        studentSolution.setContents(solutionContent);


        // Create and execute Queries
        var executor = new QueryExecutor();
        var queries = manager.map(studentSolution);
        assertNotNull(queries);
        assertFalse(queries.isEmpty());
        List<QueryExecutor.Result> results = executor.executeQueries(queries, studentSolution);
        assertNotNull(results);
        assertFalse(results.isEmpty());

        results.forEach(result -> {
            System.out.println("---------------------------------");
            System.out.println(result.getResult().getType());
            System.out.println(result.getResult().getSummary());
            System.out.println(result.getResult().getDescription());
            System.out.println(result.getResult().getDate());
            System.out.println(result.getResult().getLocation());
            System.out.println("---------------------------------");
        });

    }

    /**
     * We do not need the whole Initial Data provided by the Initializer
     */
    private InnoplusDatabase createJUnitTestData() throws IOException {
        var factory = DataFactory.eINSTANCE;
        var testData = factory.createInnoplusDatabase();

        var taskCategory = Initializer.generateTaskCategory(InnoplusConstants.TASK_UML, testData);
        var solutionCategory = Initializer.generateSolutionCategory(InnoplusConstants.SOLUTION_UML, testData);

        // Create Tasks that we want to test
        var umlTestTask = factory.createTask();
        umlTestTask.setName(StaticInitializer.UML_TASK_TEST);
        umlTestTask.setDescription("Empty Description");
        testData.getTasks().add(umlTestTask);
        umlTestTask.setCategory(taskCategory);

        var teacherSolutions = Paths.get(".", "..", "task-repository-static", "01_modelling", "teacher_solutions");

        var umlFiles = new HashMap<String, String>();
        for (var fileIt = Files.walk(teacherSolutions).iterator(); fileIt.hasNext();){
            var path = fileIt.next();
            if (Files.isDirectory(path))
                continue;
            umlFiles.put(teacherSolutions.relativize(path).toString(), Files.readString(path));
        }
        assertTrue(Files.exists(teacherSolutions));
        Initializer.generateSampleSolution(umlTestTask, solutionCategory, testData)
                .setContents(Initializer.generateSolutionContent(teacherSolutions, umlFiles));

        return testData;

    }

}
