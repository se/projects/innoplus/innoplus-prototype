package de.unioldenburg.se.innoplus;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Student;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.statistics.StatisticsManager;

public class StatisticsTest {
	private final static String path = "src/test/resources/statistics/";

	@Test
	public void testCsvStatistic() throws IOException {
		InnoplusDatabase data = new StaticInitializer().createDefaultData();

		// create some solutions with some hints for testing
		solution(data, 5, "2021-01-10 9:02");
		solution(data, 2, "2021-01-10 9:03");
		solution(data, 1, "2021-01-10 9:04");
		solution(data, 1, "2021-01-11 1:05");
		solution(data, 1, "2021-01-11 9:06");
		solution(data, 1, "2021-01-11 9:07");
		solution(data, 0, "2021-01-13 9:08");


		StatisticsManager manager = StatisticsManager.createDefault(path, data);
		manager.calculate(data);

		// very poor check ...
		assertTrue(Files.exists(Paths.get(path, "users.csv")));
	}

	private void solution(InnoplusDatabase data, int numberErrors, String time) {
		StudentSolution solution = DataFactory.eINSTANCE.createStudentSolution();
		solution.setTask(data.getTasks().get(0));
		solution.setAuthor(data.getPersons().stream().filter(p -> p instanceof Student).findFirst().get());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			solution.setDate(format.parse(time));
		} catch (ParseException e) {
			fail(time);
		}

		// create some QueryResults for testing
		result(solution, QueryResultType.WARNING);
		result(solution, QueryResultType.WARNING);
		for (int i = 0; i < numberErrors; i++) {
			result(solution, QueryResultType.ERROR);
		}
	}

	protected QueryResult result(StudentSolution solution, QueryResultType type) {
		QueryResult result = DataFactory.eINSTANCE.createQueryResult();
		result.setDate(solution.getDate());
		result.setSolution(solution);
		result.setType(type);
		result.setSummary("My summary");
		result.setDescription("My description");
		result.setLocation("location.java::-:");
		return result;
	}

	@Test
	public void testDataPersistence() throws IOException {
		// 1. create initial data
		DataManager data = new DataManager(InnoplusServer.PATH_DATA, new DynamicInitializer(), false);
		int sizeBefore = data.getData().getLectures().size();

		// 2. create new lecture (and save it)
		Lecture newLecture = DataFactory.eINSTANCE.createLecture();
		newLecture.setName("TestLecture");
		data.getData().getLectures().add(newLecture);
		data.saveCurrentData();

		// 3. reload data => one lecture more!
		data = new DataManager(InnoplusServer.PATH_DATA, new DynamicInitializer(), true);
		int sizeAfter = data.getData().getLectures().size();
		assertEquals(sizeBefore + 1, sizeAfter);
	}

}
