package de.unioldenburg.se.innoplus;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StudipUserValidation {

	@Test
	public void testPDA() {
		String lectureID = "36e93f23b97226d889adb38483b273fa"; // PDA, WS 20/21
		String userName = "salu7291"; // 7bf927092164d4442bd55d73f58a006a
		test(lectureID, userName, 0);
	}

	@Test
	public void testST1Student() {
		String lectureID = "2d54797bbfce8f01aa6db98f73835709"; // ST1, WS 20/21
		String userName = "salu7291"; // 7bf927092164d4442bd55d73f58a006a
		test(lectureID, userName, 1);
	}
	@Test
	public void testST1Lecturer() {
		String lectureID = "2d54797bbfce8f01aa6db98f73835709"; // ST1, WS 20/21
		String userName = "surg2130";
		test(lectureID, userName, 3);
	}

	private void test(String lectureID, String userName, int status) {
		// MD5
		String userHashed = DigestUtils.md5Hex(userName);
//		System.out.println(userHashed);

		URI uri;
		try {
			uri = new URI("https://elearning.uni-oldenburg.de/plugins.php/innovationplus/api/" + lectureID + "/" + userHashed);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			fail();
			return;
		}
		HttpRequest request = HttpRequest.newBuilder()
				.version(HttpClient.Version.HTTP_2)
				.uri(uri)
				.header("Content-Type", "application/json")
				.GET()
				.build();

		HttpClient client = HttpClient.newHttpClient();
		HttpResponse<String> response;
		try {
			response = client.send(request, BodyHandlers.ofString());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			fail();
			return;
		}
		assertNotNull(response);

//		// summary
//		System.out.println(response);
//		// print response headers
//        response.headers().map().forEach((k, v) -> System.out.println(k + ":" + v));
//        // print status code
//        System.out.println(response.statusCode());
//        // whole body
//        System.out.println("Body-Begin");
//        System.out.println(response.body());
//        System.out.println("Body-End");

        int result;
        try {
        	result = Integer.parseInt(response.body());
        } catch (Throwable e) {
        	e.printStackTrace();
        	fail(e.getMessage());
        	return;
        }
       	assertEquals(status, result);
	}

}
