package de.unioldenburg.se.innoplus.util;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

public class PropertiesFileTest {

    final String BASE_PATH = Path.of("src", "test", "resources", "properties_file_test").toString();

    @Test
    public void testStoreAPropFile() throws IOException {
        final var file = createPath("test_store.properties");
        assertTrue(file.toFile().delete(), "Could not delete old file, so test must fail!");
        final var prop = new PropertiesFile(file);
        prop.setValue("test", "test");
        prop.setValue("number", 1234);
        // store with modification timestamp
        prop.setStoreModificationTimestamp(true);
        assertTrue(prop.store());
        assertTrue(Pattern.compile(PropertiesFile.TIMESTAMP_COMMENT_REGEX).matcher(Files.readString(file)).find());
        // store without modification timestamp
        prop.setStoreModificationTimestamp(false);
        assertTrue(prop.store());
        assertFalse(Pattern.compile(PropertiesFile.TIMESTAMP_COMMENT_REGEX).matcher(Files.readString(file)).find());
    }

    @Test
    public void testStoreAPropFileWithAComment() throws IOException {
        final var file = createPath("test_store_comment.properties");
        assertTrue(file.toFile().delete(), "Could not delete old file, so test must fail!");
        final var prop = new PropertiesFile(file);
        prop.setValue("test", "test");
        prop.setValue("number", 1234);
        final var comment = "A Comment";
        // store with modification timestamp
        prop.setStoreModificationTimestamp(true);
        assertTrue(prop.store(comment));
        assertTrue(Files.readAllLines(file).contains('#' + comment));
        assertTrue(Pattern.compile(PropertiesFile.TIMESTAMP_COMMENT_REGEX).matcher(Files.readString(file)).find());
        // store without modification timestamp
        prop.setStoreModificationTimestamp(false);
        assertTrue(prop.store(comment));
        assertTrue(Files.readAllLines(file).contains('#' + comment));
        assertFalse(Pattern.compile(PropertiesFile.TIMESTAMP_COMMENT_REGEX).matcher(Files.readString(file)).find());
    }

    @Test
    public void testLoadAPropFile() {
        final var file = createPath("test_load.properties");
        final var prop = new PropertiesFile(file);
        assertTrue(prop.load());
        assertTrue(prop.getValue("test").isPresent());
        assertTrue(prop.getValue("number", Integer.class).isPresent());
    }

    @Test
    void testGetNull() {
        final var file = createPath("test_null.properties");
        final var propName = "nothing";
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, "something?");
        assertThrows(IllegalArgumentException.class, () -> prop.getValue(propName, null));
    }

    @Test
    void testGetBoolean() {
        final var clazz = Boolean.class;
        final Boolean propValue = true;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetByte() {
        final var clazz = Byte.class;
        final Byte propValue = 12;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetShort() {
        final var clazz = Short.class;
        final Short propValue = 12345;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetInteger() {
        final var clazz = Integer.class;
        final Integer propValue = 1234567;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetLong() {
        final var clazz = Long.class;
        final Long propValue = 123456789L;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetFloat() {
        final var clazz = Float.class;
        final Float propValue = 12.45F;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetDouble() {
        final var clazz = Double.class;
        final Double propValue = 12.45;
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetCharacter() {
        final var clazz = Character.class;
        final Character propValue = 'a';
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetString() {
        final var clazz = String.class;
        final String propValue = "Hallo";
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetUUID() {
        final var clazz = UUID.class;
        final UUID propValue = UUID.randomUUID();
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue);
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetUUIDWithoutHyphen() {
        final var clazz = UUID.class;
        final UUID propValue = UUID.randomUUID();
        final var propName = "a" + clazz.getSimpleName();
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        prop.setValue(propName, propValue.toString().replace("-", ""));
        final var gettedPropValue = prop.getValue(propName, clazz);
        assertTrue(gettedPropValue.isPresent());
        assertEquals(propValue, gettedPropValue.get());
    }

    @Test
    void testGetDefault() {
        final String propValue = "Hallo";
        final var propName = "aDefault";
        final var file = createPath("test_" + propName + ".properties");
        final var prop = new PropertiesFile(file);
        final var gettedPropValue = prop.getValue(propName, propValue);
        assertEquals(propValue, gettedPropValue);
    }

    protected Path createPath(String fileName) {
        return Path.of(BASE_PATH, fileName);
    }
}
