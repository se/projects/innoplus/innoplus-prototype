package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.query.QueryExecutor;
import de.unioldenburg.se.innoplus.query.QueryExecutor.Result;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.junit.JUnitQueryFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class JavaCodeTest {

	static Stream<Arguments> studentSolutionProvider() {
			return Stream.of(
//					arguments(
//							Paths.get(".", "..", "task-repository", "01_sorting", "student_solutions",
//									"valid-optimal"),
//							"Merge sorted Arrays",
//							QueryResultType.ERROR
//					)
					// arguments(
					// 		Paths.get(".", "..", "task-repository", "01_sorting", "student_solutions",
					// 				"valid-different-name"),
					// 		"Merge sorted Arrays",
					// 		QueryResultType.SUCCESS
					// ),
					// arguments(
					// 		Paths.get(".", "..", "task-repository", "01_sorting", "student_solutions",
					// 				"valid-ignoring-sortedness"),
					// 		"Merge sorted Arrays",
					// 		QueryResultType.SUCCESS
					// ),
					// arguments(
					// 		Paths.get(".", "..", "task-repository", "01_sorting", "student_solutions", "valid-library"),
					// 		"Merge sorted Arrays",
					// 		QueryResultType.SUCCESS
					// ),
//					arguments(
//							Paths.get(".", "..", "task-repository", "03_linked_list", "student_solutions"),
//							"Linked List",
//							QueryResultType.SUCCESS
//					),
// 					arguments(
// 							Paths.get(".", "..", "task-repository", "02_star_database", "student_solutions",
// 									"invalid-incomplete"),
// 							"Star Database",
// 							QueryResultType.ERROR
//					)
// 					),
//					arguments(
//							Paths.get(".", "..", "task-repository", "02_star_database", "student_solutions",
//									"invalid-compiler-error"),
//							"Star Database",
//							QueryResultType.ERROR
//					)
// 					arguments(
// 							Paths.get(".", "..", "task-repository", "04_circular_list", "student_solutions",
// 									"valid_jdk"),
// 							"Circular List",
// 							QueryResultType.ERROR
// 					)
					arguments(
							Paths.get(".", "..", "task-repository", "10_io_test", "student_solutions", "valid-optimal"),
							InnoplusConstants.CODE_TASK_IO_TEST,
							QueryResultType.ERROR
					)
			);
	}

	@ParameterizedTest
	@MethodSource("studentSolutionProvider")
	public void checkStudentSolution(Path testFolder, String taskName, QueryResultType expectedResult)
			throws IOException
	{
		// create data for our DataModel to test JUnit Queries
		var data = createJUnitTestData();
		assertNotNull(data);

		DataFactory factory = DataFactory.eINSTANCE;

		// query management
		var manager = new QueryManager();
		manager.register(taskName, new JUnitQueryFactory(new DockerDriver()));
		assertNotNull(manager);

		// create student solution
		var studentSolution = factory.createStudentSolution();
		studentSolution.setDate(new Date());
		System.out.println();
		studentSolution.setTask(
				data.getTasks().stream()
						.filter(t -> taskName.equals(t.getName()))
						.findFirst().orElseThrow(IllegalStateException::new)
		);

		var javaFiles = new HashMap<String, String>();
		for (var fileIt = Files.walk(testFolder).iterator(); fileIt.hasNext();){
			var path = fileIt.next();
			if (Files.isDirectory(path))
				continue;
			javaFiles.put(testFolder.relativize(path).toString(), Files.readString(path));
		}

		var solutionContent = factory.createSolutionContent();
		solutionContent.setContent(javaFiles);
		studentSolution.setContents(solutionContent);

		// Create and execute Queries
		var executor = new QueryExecutor();
		var queries = manager.map(studentSolution);
		assertNotNull(queries);
		assertFalse(queries.isEmpty());

		var results = executor.executeQueries(queries, studentSolution);

		assertNotNull(results);
		assertFalse(results.isEmpty());

		// check result
		// assertNotEquals("No Solution provided", results.get(0).getResult().getSummary());
		// assertNotEquals("Compiling the code failed", results.get(0).getResult().getSummary());
		// assertEquals(expectedResult, results.get(0).getResult().getType());
		System.out.println("-----------------------------------------");
		results.forEach(result -> {
			System.out.println(result.getResult().getLocation());
			System.out.println(result.getResult().getSummary());
			System.out.println(result.getResult().getDescription());
		});
	}

	/** We do not need the whole Initial Data provided by the Initializer */
	private InnoplusDatabase createJUnitTestData(){
		var factory = DataFactory.eINSTANCE;
		var testData = factory.createInnoplusDatabase();

		var taskCategoryJavaCode = Initializer.generateTaskCategory("JavaCodeTask", testData);
		var solutionCategoryJava = Initializer.generateSolutionCategory("JavaCode", testData);

		// Create Tasks that we want to test
		var mergeArrays = factory.createTask();
		mergeArrays.setName("Merge sorted Arrays");
		mergeArrays.setDescription("Empty Description");
		testData.getTasks().add(mergeArrays);
		mergeArrays.setCategory(taskCategoryJavaCode);

		var starDatabase = factory.createTask();
		starDatabase.setName("Star Database");
		starDatabase.setDescription("Empty Description");
		testData.getTasks().add(starDatabase);
		starDatabase.setCategory(taskCategoryJavaCode);

		var linkedList = factory.createTask();
		linkedList.setName("Linked List");
		linkedList.setDescription("Empty Description");
		testData.getTasks().add(linkedList);
		linkedList.setCategory(taskCategoryJavaCode);

		var circleList = factory.createTask();
		circleList.setName("Circular List");
		circleList.setDescription("Empty Description");
		testData.getTasks().add(circleList);
		circleList.setCategory(taskCategoryJavaCode);

		var hareAndHedgehog = factory.createTask();
		hareAndHedgehog.setName("Hare and Hedgehog");
		hareAndHedgehog.setDescription("Empty Description");
		testData.getTasks().add(hareAndHedgehog);
		hareAndHedgehog.setCategory(taskCategoryJavaCode);

		var ioTest = factory.createTask();
		ioTest.setName(InnoplusConstants.CODE_TASK_IO_TEST);
		ioTest.setDescription("Empty Description");
		testData.getTasks().add(ioTest);
		ioTest.setCategory(taskCategoryJavaCode);


		// create Teacher Solutions for our Tasks
		var testPath = Paths.get(".", "..", "task-repository", "01_sorting", "tests", "SortingTest.java");
		assertTrue(Files.exists(testPath));

		Initializer.generateSampleSolution(
				mergeArrays, solutionCategoryJava, testData)
				.setContents(Initializer.generateSolutionContent(testPath, null));

		testPath = Paths.get(".", "..", "task-repository", "02_star_database", "tests", "StarsTest.java");
		assertTrue(Files.exists(testPath));
		Initializer.generateSampleSolution(starDatabase, solutionCategoryJava, testData)
				.setContents(Initializer.generateSolutionContent(testPath, null));

		testPath = Paths.get(".", "..", "task-repository", "03_linked_list", "tests", "LinkedIntListTest.java");
		assertTrue(Files.exists(testPath));
		Initializer.generateSampleSolution(linkedList, solutionCategoryJava, testData).setContents(
					Initializer.generateSolutionContent(testPath, null));

		testPath = Paths.get(".", "..", "task-repository", "04_circular_list", "tests", "CircleTest.java");
		assertTrue(Files.exists(testPath));
		Initializer.generateSampleSolution(circleList, solutionCategoryJava, testData).setContents(
				Initializer.generateSolutionContent(testPath, null));


		testPath = Paths.get(".", "..", "task-repository", "05_hare_and_hedgehog", "tests", "RunningGameTest.java");
		assertTrue(Files.exists(testPath));
		Initializer.generateSampleSolution(hareAndHedgehog, solutionCategoryJava, testData).setContents(
				Initializer.generateSolutionContent(testPath, null));


		testPath = Paths.get(".", "..", "task-repository", "10_io_test", "tests", "IOTaskTest.java");
		assertTrue(Files.exists(testPath));
		Initializer.generateSampleSolution(ioTest, solutionCategoryJava, testData).setContents(
				Initializer.generateSolutionContent(testPath, null));

		return testData;
	}
}
