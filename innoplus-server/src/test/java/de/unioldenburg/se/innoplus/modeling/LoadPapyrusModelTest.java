package de.unioldenburg.se.innoplus.modeling;

import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
class LoadPapyrusModelTest {

	@Test
	void test() {
		ResourceSet resourceSet = new ResourceSetImpl();

		//UMLResourcesUtil.init().
		resourceSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap()
				.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);

		/*
		 * Initialize the ResourceSet
		 */

		/*
		 * https://www.eclipse.org/forums/index.php/t/487392/	!!
		 * https://www.eclipse.org/forums/index.php/t/353280/
		 * https://stackoverflow.com/questions/34055936/how-can-i-load-a-papyrus-uml-model-by-using-java
		 */
		/*
		 * https://stackoverflow.com/questions/34055936/how-can-i-load-a-papyrus-uml-model-by-using-java (used: works!)
		 * http://wiki.eclipse.org/MDT/UML2/FAQ#What.27s_required_to_load_a_UML_.28.uml.29_resource_from_a_standalone_application.3F
		 * https://www.eclipse.org/forums/index.php/t/151386/
		 * http://www.eclipse.org/modeling/mdt/uml2/docs/guides/UML2_2.0_Migration_Guide/guide.html (lange scrollen!)
		 */

		//UMLResourcesUtil.init(resourceSet);

//		// interessanterweise funktioniert es mittlerweile ohne diesen Code (was aber sehr gut ist)
//		String file = "/Users/johannes/.m2/repository/org/eclipse/uml2/uml/resources/5.0.0-v20140602-0749/resources-5.0.0-v20140602-0749.jar";
//		final URI uriJar = URI.createURI("jar:file:" + file + "!/");
//
//		Map<URI, URI> uriMap = resourceSet.getURIConverter().getURIMap();
//		uriMap.put(URI.createURI(UMLResource.UML_PRIMITIVE_TYPES_LIBRARY_URI), uriJar.appendSegment("libraries").appendSegment(""));
//		uriMap.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uriJar.appendSegment("libraries").appendSegment(""));
//		uriMap.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uriJar.appendSegment("metamodels").appendSegment(""));
//		uriMap.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uriJar.appendSegment("profiles").appendSegment(""));
////		uriMap.put(URI.createURI("pathmap://Papyrus.profile.uml"), URI.createURI("file:/D:/.../Papyrus.profile.uml/"));

////		// auch diese Zeilen sind nicht notwendig
////		Map<String, Object> extensionMap = Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap(); // hier funktioniert das globale Setzen!
//		Map<String, Object> extensionMap = resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap();
//		extensionMap.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
//		// extensionMap.put(org.eclipse.emf.ecore.resource.Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());

		/*
		 * Load the *.uml file
		 */
		XMLResource resource = (XMLResource) resourceSet.getResource(URI.createFileURI("src/test/resources/modeling/InnoPlus.uml"), true);
		assertNotNull(resource);
		Map<String, Object> mapOptions = new HashMap<>(); // no special options
		try {
			resource.load(mapOptions);
		} catch (IOException e) {
			e.printStackTrace();
		}


		EObject clas1 = resource.getContents().get(0);
		EObject clas2 = resource.getContents().get(0);
//		boolean isAbstract = (boolean) clas.eGet(clas.eClass().getEStructuralFeature("abstract"));
		for (EStructuralFeature feature : clas1.eClass().getEAllStructuralFeatures()) {
			Object value1 = clas1.eGet(feature);
			Object value2 = clas2.eGet(clas2.eClass().getEStructuralFeature(feature.getName()));
			if (Objects.equals(value1, value2) == false) {
				System.err.println("difference found!");
			}
		}
//		clas1.eContents();

		/*
		 * Test the loaded UML model
		 */
		// print simple statistic
		Model model = (Model) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.Literals.MODEL);
		assertNotNull(model, "Model: load failed!");

		// owned types / classes
		Set<Class> ownedClasses = model.getOwnedTypes().stream().filter(t -> t instanceof Class).map(t -> (Class) t).collect(toSet());
		for (Class elem : ownedClasses) {
//			boolean abstractValue = elem.isAbstract();
			System.out.println("owned type: " + elem);
		}
		assertEquals(16, ownedClasses.size());
		System.out.println("\n");

		// owned elements
		for (Element elem : model.allOwnedElements()) {
			// check, that each element has an ID (in form of the XMI-ID)
			String xmiID = resource.getID(elem);
			assertNotNull(xmiID, "" + elem);

			System.out.println("owned element " + xmiID + ": " + elem);
		}
	}

}
