package de.unioldenburg.se.innoplus;

import static de.unioldenburg.se.innoplus.StudipAdapterTest.PDA;
import static de.unioldenburg.se.innoplus.StudipAdapterTest.PDA_TUTOR;
import static de.unioldenburg.se.innoplus.StudipAdapterTest.ST1;
import static de.unioldenburg.se.innoplus.StudipAdapterTest.ST1_STUDENT;
import static de.unioldenburg.se.innoplus.StudipAdapterTest.ST1_TEACHER;
import static de.unioldenburg.se.innoplus.StudipAdapterTest.UNKNOWN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import de.unioldenburg.se.innoplus.data.*;
import de.unioldenburg.se.innoplus.message.feedback.UMLFeedbackRequest;
import de.unioldenburg.se.innoplus.message.feedback.UMLFeedbackResponse;
import org.junit.jupiter.api.Test;

import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.feedback.JavaFeedbackRequest;
import de.unioldenburg.se.innoplus.message.login.BeginSessionRequest;
import de.unioldenburg.se.innoplus.message.login.BeginSessionResponse;
import de.unioldenburg.se.innoplus.message.login.LoginRequest;
import de.unioldenburg.se.innoplus.message.login.LoginResponse;
import de.unioldenburg.se.innoplus.networking.Connection;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class InnoplusServerTest {

	private final TestConnection con;
	private final InnoplusServer server;

	public InnoplusServerTest() throws Exception {
		this.con = new TestConnection();
		this.server = new InnoplusServer(con);
	}

	@Test
	public void testInvalidLogin() throws Exception {
		login(UNKNOWN, PDA, false);
	}

	@Test
	public void testValidLoginStudent() throws Exception {
		var person = login(ST1_STUDENT, ST1, true);
		assertTrue(person instanceof Student);
		assertFalse(person.getLectures().isEmpty());

		var opt = person.getLectures()
				.stream()
				.filter(l -> "ST1".equals(l.getName()))
				.findAny();
		assertTrue(opt.isPresent(), person.getLectures()::toString);
	}

	@Test
	public void testValidLoginTutor() throws Exception {
		var person = login(PDA_TUTOR, PDA, true);
		assertTrue(person instanceof Teacher);
	}

	@Test
	public void testValidLoginTeacher() throws Exception {
		var person = login(ST1_TEACHER, ST1, true);
		assertTrue(person instanceof Teacher);
	}

	@Test
	public void testDuplicateLoginSame() throws Exception {
		var person1 = login(ST1_TEACHER, ST1, true);
		assertNotNull(person1);

		var size = person1.getLectures().size();

		var person2 = login(ST1_TEACHER, ST1, true);
		assertTrue(person1 == person2);
		assertEquals(size, person1.getLectures().size());
	}

	@Test
	public void testDuplicateLoginDifferent() throws Exception {
		var person1 = login(PDA_TUTOR, PDA, true);
		var person2 = login(PDA_TUTOR, ST1, true);
		assertTrue(person1 == person2);
		var lect = person1.getLectures();
		assertEquals(2, lect.size());
		var l1 = lect.get(0).getId();
		var l2 = lect.get(1).getId();

		if (PDA.equals(l1))
			assertEquals(ST1, l2);
		else {
			assertEquals(ST1, l1);
			assertEquals(PDA, l2);
		}
	}

	static Stream<Arguments> studentSolutionProvider() {
		return Stream.of(
				arguments(
						StaticInitializer.CODE_TASK_STAR_DATABASE,
						Paths.get(".", "..", "task-repository-static", "02_star_database", "student_solutions", "invalid-incomplete")
				),
				arguments(
						StaticInitializer.CODE_TASK_MERGE_ARRAYS,
						Paths.get(".", "..", "task-repository-static", "01_sorting", "student_solutions", "valid-optimal")
				),
				arguments(
						StaticInitializer.CODE_TASK_MERGE_LINKED_LIST,
						Paths.get(".", "..", "task-repository-static", "03_linked_list", "student_solutions")
				),
				arguments(
						StaticInitializer.CODE_TASK_MERGE_CIRCULAR_LIST,
						Paths.get(".", "..", "task-repository-static", "04_circular_list", "student_solutions", "valid_jdk")
				)
		);
	}

	@ParameterizedTest
	@MethodSource("studentSolutionProvider")
	public void requestJavaCodeFeedback(String task, Path solution)throws Exception{
		// load Java files to test
		var javaFiles = new HashMap<String, String>();
		for (var fileIt = Files.walk(solution).iterator(); fileIt.hasNext();) {
			var path = fileIt.next();
			if (Files.isDirectory(path))
				continue;
			javaFiles.put(solution.relativize(path).toString(), Files.readString(path));

		}
		var request = new JavaFeedbackRequest(ST1_STUDENT, task, "", 8, javaFiles);
		server.handle(request);
	}

	@Test
	public void requestUMLFeedback() throws Exception {
	    var sPath = Paths.get(".", "..", "task-repository", "01_modelling", "student_solutions", "valid-optimal");
		var files = new HashMap<String, String>();
		for (var fileIt = Files.walk(sPath).iterator(); fileIt.hasNext();){
			var path = fileIt.next();
			if (Files.isDirectory(path))
				continue;
			files.put(sPath.relativize(path).toString(), Files.readString(path));
		}
		server.handle(
				new UMLFeedbackRequest(
						ST1_STUDENT,
						StaticInitializer.UML_TASK_TEST,
						files.entrySet().iterator().next().getValue(),
						""
				)
		);
	}

	private Person login(String userHash, String lecureId, boolean expected) throws Exception {
		con.receive(new LoginRequest(userHash, lecureId));
		var sent = con.getSentMessages();
		assertEquals(1, sent.size());

		// Validate response
		var resp = (LoginResponse) sent.get(0);
		assertEquals(userHash, resp.getClientID());
		assertEquals(expected, resp.isLoginResult());

		// Check that user was created on success
		var opt = server.getDatabase().getPerson(userHash);

		assertEquals(expected, opt.isPresent());
		assertEquals(expected, resp.getTaskDatabase() != null);

		if (!expected)
			return null;

		var expLecture = server.getDatabase()
			.getLecture(lecureId)
			.get();


		// Database should include the requested lecture + tasks on success
		var data = resp.getTaskDatabase();
		assertEquals(1, data.getPersons().size());
		var person = data.getPersons().get(0);

		assertEquals(data, person.getDatabase());
		assertEquals(userHash, person.getId());
		assertTrue(person.getSubmittedSolutions().isEmpty());

		// Only include data for lectures requested by the current user
		assertTrue(data.getLectures().containsAll(person.getLectures()));
		assertEquals(1, person.getLectures().size());

		var actLecture = person.getLectures().get(0);
		assertTrue(expLecture != actLecture);
		assertEquals(data, actLecture.getDatabase());
		assertEquals(expLecture.getId(), actLecture.getId());
		assertEquals(expLecture.getName(), actLecture.getName());
		assertEquals(data.getPersons(), actLecture.getParticipants());

		assertEquals(data.getTasks(), actLecture.getSelectedTasks());
		var expTasks = expLecture.getSelectedTasks();
		assertEquals(expTasks.size(), data.getTasks().size());

		// Should always have a task to ensure proper test coverage
		assumeTrue(expTasks.size() > 0, () -> "Missing example tasks for lecture: " + lecureId);

		for (int i = 0; i < expTasks.size(); i++) {
			var expTask = expTasks.get(i);
			var actTask = data.getTasks().get(i);

			assertTrue(expTask != actTask);
			assertEquals(data, actTask.getDatabase());
			assertEquals(expTask.getName(), actTask.getName());
			assertEquals(expTask.getDescription(), actTask.getDescription());
			assertEquals(List.of(actLecture), actTask.getUsedInLectures());

			// Hidden details
			assertNull(actTask.getAuthor());
			assertTrue(actTask.getSolutions().isEmpty());

			var expCat = expTask.getCategory();
			var actCat = actTask.getCategory();
			assertTrue(expCat != actCat);
			assertEquals(data, actCat.getDatabase());
			assertEquals(expCat.getName(), actCat.getName());
			assertEquals(data.getTasks(), actCat.getTasks());

			for (var a : actCat.getTasks()) {
				var e = expCat.getTasks()
						.stream()
						.filter(t -> t.getName().equals(a.getName()))
						.findAny();
				assertTrue(e.isPresent());
				assertTrue(a != e.get());
				assertTrue(data.getTasks().contains(a));
			}

			assertTrue(actCat.getSolutionCategories().isEmpty());
		}
		return opt.orElse(null);
	}

	@Test
	public void testBeginSessionStudent() throws Exception {

		var data = beginSession(PDA_TUTOR);

		// No top/bottom references
		assertNull(data.getParentDatabase());
		assertTrue(data.getChildDatabases().isEmpty());

		// No solutions
		assertTrue(data.getSampleSolutions().isEmpty());
		assertTrue(data.getSolutionCategories().isEmpty());

		// Database should contain no users
		assertTrue(data.getPersons().isEmpty());

		// Include all known lectures (as copies!)
		var expData = server.getDatabase().getData();
		assertTrue(expData != data);

		var expLect = expData.getLectures();
		var actLect = data.getLectures();

		assertEquals(expLect.size(), actLect.size());

		for (int i = 0; i < expLect.size(); i++) {
			var exp = expLect.get(i);
			var act = actLect.get(i);

			// Expose ID + Name
			assertEquals(exp.getId(), act.getId());
			assertEquals(exp.getName(), act.getName());

			// Hide everything else
			assertTrue(data == act.getDatabase());
			assertTrue(act.getParticipants().isEmpty());
			assertTrue(act.getSelectedTasks().isEmpty());
		}
	}

	private InnoplusDatabase beginSession(String userHash) throws Exception {
		con.receive(new BeginSessionRequest(userHash));
		var sent = con.getSentMessages();
		assertEquals(1, sent.size());

		var resp = (BeginSessionResponse) sent.get(0);
		assertEquals(userHash, resp.getClientID());
		var data = resp.getInitialClientData();
		assertNotNull(data);

		if (true) {
			for (var person : data.getPersons())  {
				System.out.println(person);
			}
		}

		return data;
	}

	/** Dummy connection implementation to be used in tests **/
	public class TestConnection implements Connection<ClientToServer, ServerToClient> {

		private final Map<Class<?>, MessageListener> listeners = new HashMap<>();
		private List<ServerToClient> sentMessages = new java.util.ArrayList<>();
		private boolean closed;

		@Override
		public <T extends ClientToServer> void addListener(Class<T> clazz, MessageListener<? super T> listener) throws IOException {
			assertNotNull(clazz);
			assertNotNull(listener);
			listeners.put(clazz, listener);
		}

		@Override
		public void removeListener(Class<? extends ClientToServer> clazz) throws IOException {
			assertNotNull(clazz);
			if(listeners.remove(clazz) == null)
				fail("No such listener!");
		}

		@Override
		public void send(ServerToClient message) throws IOException {
			assertFalse(closed);
			this.sentMessages.add(message);
		}

		/** Returns a list of all messages {@link #send sent} since the last call to this method **/
		public List<ServerToClient> getSentMessages() {
			var res = this.sentMessages;
			this.sentMessages = new java.util.ArrayList<>();
			return res;
		}

		/** Dispatches a new incoming message to the appropriate listener registered via {@link #addListener} **/
		public void receive(ClientToServer message) throws IOException {
			assertFalse(closed);
			Class<?> clazz = message.getClass();
			do {
				var listener = this.listeners.get(clazz);
				if (listener != null) {
					listener.handle(message);
					return;
				}
				clazz = clazz.getSuperclass();
			} while (clazz != null);

			fail("No listener for message: " + message);
		}

		@Override
		public void close() {
			closed = true;
		}
	}
}
