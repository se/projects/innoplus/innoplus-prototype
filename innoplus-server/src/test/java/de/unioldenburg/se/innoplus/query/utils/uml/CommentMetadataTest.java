package de.unioldenburg.se.innoplus.query.utils.uml;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;

import org.junit.jupiter.api.Test;

class CommentMetadataTest {

	@Test
	void testExtractFrom() {
		// TODO: How to create elements?
	}

	@Test
	void testGetSynonyms() {
		var input = Json.createObjectBuilder()
			.add("synonyms", Json.createObjectBuilder()
				.add("de", Json.createArrayBuilder()
					.add("foo")
					.add("bar")
				)
				.add("en", Json.createArrayBuilder()
					.add("tea")
				)
			)
			.build();

		var meta = new CommentMetadata(input, null);
		var syns = meta.getSynonyms();
		assertNotNull(syns);

		assertEquals(2, syns.size());
		assertEquals(List.of("foo", "bar"), syns.get("de"));
		assertEquals(List.of("tea"), syns.get("en"));

		assertFalse(meta.isExclusiveMatch());
	}

	@Test
	void testIsExclusiveMatch() {
		var input = Json.createObjectBuilder()
			.add("synonyms", Json.createObjectBuilder()
				.add("de", Json.createArrayBuilder()
					.add("foo")
					.add("bar")
				)
				.add("exclusive", true)
			)
			.build();

		var meta = new CommentMetadata(input, null);
		assertTrue(meta.isExclusiveMatch());
	}

	@Test
	void testEmpty() {
		var input = JsonObject.EMPTY_JSON_OBJECT;
		var meta = new CommentMetadata(input, null);
		assertTrue(meta.getSynonyms().isEmpty());
		assertFalse(meta.isExclusiveMatch());
	}

}
