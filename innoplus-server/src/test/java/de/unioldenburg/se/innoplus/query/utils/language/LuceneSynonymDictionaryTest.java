package de.unioldenburg.se.innoplus.query.utils.language;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class LuceneSynonymDictionaryTest {

	public static Object[][] getEquivalentParams() {
		return new Object[][] {
			{ null, null, true },
			{ "Apple", null, false },
			{ "Apple", "Apple", true },
			{ "Apple", "Toaster", false },
			{ "Immobilienmakler", "Immobilienhändler", true }
		};
	}

	@ParameterizedTest
	@MethodSource("getEquivalentParams")
	void testAreEquivalent(String a, String b, boolean exp) {
		var dict = NameMatcher.instance.getDictionary();
		var res = dict.areEquivalent(a, b);
		assertEquals(exp, res);
	}

}
