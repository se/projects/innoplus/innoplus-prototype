package de.unioldenburg.se.innoplus.statistics;

import static java.util.stream.Collectors.toList;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;

public class StatisticsManager {
	protected final List<Statistic> statistics = new ArrayList<>();
	protected final Path pathFolder;

	public StatisticsManager(String path) {
		pathFolder = Paths.get(path);
        try {
			Files.createDirectories(pathFolder);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static StatisticsManager createDefault(String path, InnoplusDatabase data) {
		StatisticsManager manager = new StatisticsManager(path);

		// generate labels: https://www.baeldung.com/java-random-string
		int leftLimit = 97; // letter 'a'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = 10;
		Random random = new Random();

		// statistics for all users
		manager.register(new CountUserStatistic());

		// individual statistics for each task
		for (Task task : data.getTasks()) {
			long studentSolutions = task.getSolutions().stream().filter(s -> s instanceof StudentSolution).count();

			// ... with at least one submitted solution
			if (studentSolutions >= 1) {
				manager.register(new SessionsOverTimeStatistic(task));
			}
		}

		// individual statistics for each user
		Map<String, Person> map = new HashMap<>(data.getPersons().size()); // (random label -> Person)
		List<Task> tasksToAnalyze = new ArrayList<>(); // collect tasks which are used by at least one person
		Set<Task> taskSet = new HashSet<>();
		for (Person person : data.getPersons()) {
			// calculate replacement label
			String label = null;
			while (label == null || map.containsKey(label)) {
				label = random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
						.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
			}
			map.put(label, person);

			// calculate used tasks
			person.getSubmittedSolutions().stream()
					.filter(s -> s.getTask() != null)
					.forEach(s -> taskSet.add(s.getTask()));


			// register statistics
			manager.register(new IndividualUserRequestsStatistic(person, label));
			manager.register(new IndividualUserTasksStatistic(person, label, tasksToAnalyze));
		}

		// this approach looks a bit complicated, but allows to filter the analyzed persons (future work) and only their tasks are analyzed!
		tasksToAnalyze.addAll(taskSet.stream()
				.sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
				.collect(toList()));

		return manager;
	}

	public void register(Statistic statistic) {
		statistics.add(statistic);
	}

	public void calculate(InnoplusDatabase data) {
		for (Statistic s : statistics) {
			String type = s.getStatisticType();
			String name = s.getName(data);
			try (OutputStream storage = new FileOutputStream(pathFolder.resolve(name + "." + type).toFile())) {
				s.calculate(data, storage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
