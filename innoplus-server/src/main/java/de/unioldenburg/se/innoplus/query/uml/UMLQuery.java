package de.unioldenburg.se.innoplus.query.uml;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Solution;
import de.unioldenburg.se.innoplus.data.TeacherSolution;
import de.unioldenburg.se.innoplus.message.EcoreIO;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.uml.strategies.*;
import de.unioldenburg.se.innoplus.query.utils.language.Dictionary;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.*;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static de.unioldenburg.se.innoplus.Initializer.createQueryResult;

public class UMLQuery implements Query {

    /** Teacher Solutions **/
    private final List<Model> teacherSolutions = new ArrayList<>();

    private final List<QueryResult> queryResults = new ArrayList<>();

    private final Dictionary dictionary = Dictionary.getInstance();

    public UMLQuery(TeacherSolution teacherSolution){

        var path = Paths.get(teacherSolution.getContents().getPath());
        try {
            for (var fileIt = Files.walk(path).iterator(); fileIt.hasNext(); ) {
                var file = fileIt.next();
                if (!file.getFileName().toString().endsWith(".uml"))
                    continue;
                teacherSolutions.add(EcoreIO.loadUmlModel(new FileInputStream(file.toFile())));
            }
        } catch (IOException e) {
            logger.info("Unable to create UMLQuery. ", e);
        }

    }

    @Override
    public void evaluate(Solution studentSolution) {
        // Load StudentSolution as UML Model
        // we agreed that we only ever evaluate one uml File with one Diagram
        var content = ((Map<String, String>) studentSolution.getContents().getContent())
                .entrySet()
                .iterator()
                .next();

        // Parse Student UML Model
        Model studentModel;
        try (var studentInputStream = new ByteArrayInputStream(content.getValue().getBytes())){
            studentModel = EcoreIO.loadUmlModel(studentInputStream);
        } catch (IOException e) {
            logger.info("Unable to load StudentSolution", e);
            queryResults.add(createQueryResult(
                    QueryResultType.ERROR,
                    content.getKey() + " is not a Valid UML Model. ",
                    content.getKey() + " is not a Valid UML Model. ",
                    "::-:"
                    )
            );
            return;
        }

        // evaluate Model
        var modelErrorMap = new HashMap<Model, List<QueryResult>>();
        Model bestMatch = null;
        for (var tSolution : teacherSolutions){
            var errors = new ArrayList<QueryResult>();

            // matched Elements Lists
            var matchedElements = new ArrayList<Element>();
            var matchedElementsTeacher = new ArrayList<Element>();

            // check if present in TeacherSolution but not student -> Student is missing a Element
            for (var studentElement : studentModel.getPackagedElements()){
                Element tElement = presentInSolution(tSolution, studentElement);
                if(tElement != null){
                    matchedElements.add(studentElement);
                    matchedElementsTeacher.add(tElement);
                } else {
                    // if (studentElement.getName() == null){
                    //     System.out.println("hall");
                    // }
                    errors.add(createQueryResult(
                            QueryResultType.WARNING,
                            "There is no match for Type: " + studentElement.eClass().getName() + " with Name: " + studentElement.getName() + ".",
                            "There is no match for Type: " + studentElement.eClass().getName() + " with Name: " + studentElement.getName() + ".",
                            "::-:"
                    ));
                }
            }

            // check if present in Student but not teacher -> Element might not be necessary
            for (var teacherElement : tSolution.getPackagedElements()){
                if((presentInSolution(studentModel, teacherElement) == null)){
                    logger.info("error for type : {} ", teacherElement);
                    // if (teacherElement.getName() == null){
                    //     System.out.println("hallo welt");
                    // }
                    errors.add(createQueryResult(
                            QueryResultType.ERROR,
                            "No corresponding element for element : " + teacherElement.getName() + " found.",
                            "No corresponding element for element : " + teacherElement.getName() + " found.",
                            "::-:"
                    ));
                }
            }

            // extract Information from matched Elements
            errors.addAll(extractHints(matchedElements, matchedElementsTeacher));

            // Evaluate Solution Quality
            modelErrorMap.put(tSolution, errors);
            if(bestMatch == null) {
                bestMatch = tSolution;
            }
            // new best case if there are less errors. Not the best idea tbh ... but do able
            else if(errors.size() < modelErrorMap.get(bestMatch).size()){
                bestMatch = tSolution;
            }
        }

        this.queryResults.addAll(modelErrorMap.get(bestMatch));
        // No Errors were found so success
        if (queryResults.isEmpty())
            queryResults.add(createQueryResult(
                    QueryResultType.SUCCESS,
                    "No Errors were found",
                    "No Errors were found",
                    "::-:"
                    )
            );

    }

    private Element presentInSolution(Model solution, NamedElement element){
        if (element == null){
            return null;
        }
        // TODO Strategy Factory
        Element match = null;
        for (var solutionElement : solution.getPackagedElements()){
            if (solutionElement instanceof Class && element instanceof Class){
                var strategy = new ClassStrategy();
                if(strategy.match((Class) solutionElement, (Class) element)){
                    match = solutionElement;
                }
            }
            else if (solutionElement instanceof Association && element instanceof Association){
                var strategy = new AssociationStrategy();
                if(strategy.match((Association) solutionElement, (Association) element)){
                    match = solutionElement;
                }
            }
            else if (solutionElement instanceof Enumeration && element instanceof Enumeration){
                var strategy = new EnumStrategy();
                if(strategy.match((Enumeration) solutionElement, (Enumeration) element)){
                    match = solutionElement;
                }
            }
            else if (solutionElement instanceof Interface && element instanceof Interface){
                var strategy = new InterfaceStrategy();
                if (strategy.match((Interface) solutionElement, (Interface) element)){
                    match = solutionElement;
                }
            }
            else if (solutionElement instanceof Generalization && element instanceof Generalization){
                var strategy = new GeneralizationStrategy();
                if (strategy.match((Generalization) solutionElement, (Generalization) element)){
                    match = solutionElement;
                }
            }
            else{
                // No match
            }
        }
        return match;
    }

    private List<QueryResult> extractHints(List<Element> matchedElements, List<Element> solution){
        var errors = new ArrayList<QueryResult>();

        for (int i = 0; i < matchedElements.size(); i++) {
            var element = matchedElements.get(i);
            var solutionElement = solution.get(i);
            if (solutionElement instanceof Class && element instanceof Class){
                var strategy = new ClassStrategy();
                errors.addAll(strategy.extractResults((Class) solutionElement, (Class) element));
            }
            else if (solutionElement instanceof Association && element instanceof Association){
                var strategy = new AssociationStrategy();
                errors.addAll(strategy.extractResults((Association) solutionElement,(Association) element));
            }
            else if (solutionElement instanceof Enumeration && element instanceof Enumeration){
                var strategy = new EnumStrategy();
                errors.addAll(strategy.extractResults((Enumeration) solutionElement, (Enumeration) element));
            }
            else if (solutionElement instanceof Interface && element instanceof Interface){
                var strategy = new InterfaceStrategy();
                errors.addAll(strategy.extractResults((Interface) solutionElement, (Interface) element));
            }
        }
        return errors;
    }

    @Override
    public List<Query> getSubQueries() {
        return Collections.emptyList();
    }

    @Override
    public List<QueryResult> getResult() {
        return queryResults;
    }

    @Override
    public Object getTraceabilityElement() {
        return null;
    }
}
