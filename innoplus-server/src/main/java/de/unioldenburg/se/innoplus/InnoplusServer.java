package de.unioldenburg.se.innoplus;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import de.unioldenburg.se.innoplus.data.*;
import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.feedback.JavaFeedbackRequest;
import de.unioldenburg.se.innoplus.message.feedback.JavaFeedbackResponse;
import de.unioldenburg.se.innoplus.message.feedback.UMLFeedbackRequest;
import de.unioldenburg.se.innoplus.message.feedback.UMLFeedbackResponse;
import de.unioldenburg.se.innoplus.message.login.BeginSessionRequest;
import de.unioldenburg.se.innoplus.message.login.BeginSessionResponse;
import de.unioldenburg.se.innoplus.message.login.LoginRequest;
import de.unioldenburg.se.innoplus.message.login.LoginResponse;
import de.unioldenburg.se.innoplus.networking.Connection;
import de.unioldenburg.se.innoplus.networking.Connection.MessageListener;
import de.unioldenburg.se.innoplus.networking.MqttConnection;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.statistics.StatisticsManager;

import static de.unioldenburg.se.innoplus.InnoplusUtils.getenv;

/** Global entrypoint which initializes required data and starts listening for incoming requests **/
public class InnoplusServer {
	public static void main(String[] args) throws Exception {
		System.out.println("Starting server...");
		var server = new InnoplusServer();
	}

	/** the logger */
	private final static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
			.getLogger(InnoplusServer.class);

	private final static Initializer INITIALIZER = new DynamicInitializer();
	private static final String PATH_BASE = getenv("DATA_DIRECTORY", "src/main/resources");
	public final static String PATH_DATA = PATH_BASE + "/data/";
	public final static String PATH_STATISTICS = PATH_BASE + "/statistics/";

	private final DataManager data;
	private final QueryManager manager;
	private final Connection<ClientToServer, ServerToClient> con;
	private final StudIpAdapter studip;

	/** Initializes a new server using the default configuration **/
	public InnoplusServer() throws IOException {
		this(MqttConnection.createServer());
		INITIALIZER.initQueries(manager, data.getData());
	}

	/** Initializes a new server using the connection and a default configuration **/
	public InnoplusServer(Connection<ClientToServer, ServerToClient> con) throws IOException {
		this(
			new DataManager(PATH_DATA, INITIALIZER, false),
				INITIALIZER.createDefaultQueryManager(),
			con
		);
	}

	/** Initializes a new server using the provided configuration **/
	public InnoplusServer(DataManager data, QueryManager manager, Connection<ClientToServer, ServerToClient> con) throws IOException {
		this.data = data;
		this.manager = manager;
		this.con = con;
		this.studip = new StudIpAdapter();
		registerHandlers();
	}

	/** Registers all known handlers for incoming messages at the current connection **/
	private void registerHandlers() throws IOException {
		// Casts is required because Java's type interference sometimes doesn't work....
		this.con.addListener(LoginRequest.class, (MessageListener<LoginRequest>) this::handle);
		this.con.addListener(BeginSessionRequest.class, (MessageListener<BeginSessionRequest>) this::handle);
		this.con.addListener(JavaFeedbackRequest.class, (MessageListener<JavaFeedbackRequest>) this::handle);
		this.con.addListener(UMLFeedbackRequest.class, (MessageListener<UMLFeedbackRequest>) this::handle);
	}

	/**
	 * Handles an incoming login request for a new user.
	 *
	 * The server validates the supplied credentials using the {@link StudIpAdapter Stud.IP API}
	 * and replies with a {@link LoginResponse}. This method inserts the user in the internal
	 * database on success.
	 *
	 * @param request the request
	 * @throws IOException if the response could not be sent
	 */
	protected synchronized void handle(LoginRequest request) throws IOException {
		Person person = null;
		Lecture lecture = null;
		var clientId = request.getClientID();
		var lectureId = request.getLectureID();

		var kind = studip.validateCredentials(clientId, lectureId);
		var valid = kind != StudIpAdapter.UserKind.Unknown;
		var optUser = determineSender(request);

		// Person already logged in (maybe for a different lecture)
		if (optUser.isPresent()) {
			person = optUser.get();
			var optLecture =  person.getLectures()
								.stream()
								.filter(l -> l.getId().equals(lectureId))
								.findAny();

			// Add to lecture if missing
			if (valid) {
				if (optLecture.isPresent()) {
					lecture = optLecture.get();
				} else {
					lecture = findLecture(lectureId);
					lecture.getParticipants().add(person);
					person.getLectures().add(lecture);
				}
			}

			// Remove from lecture if the person isn't enlisted anymore
			else if (optLecture.isPresent() && !valid) {
				lecture = optLecture.get();
				lecture.getParticipants().remove(person);
			}
		}
		// First login for a new user
		else if (valid) {
			person = kind == StudIpAdapter.UserKind.Student
				? DataFactory.eINSTANCE.createStudent()
				: DataFactory.eINSTANCE.createTeacher();

			person.setId(clientId);
			this.data.getData().getPersons().add(person);

			lecture = findLecture(lectureId);
			lecture.getParticipants().add(person);
		}

		var data = valid ? this.data.extractTasksFor(person, lecture) : null;
		this.con.send(new LoginResponse(clientId, valid, data));
	}

	/**
	 * Handles an incoming request to start a new session for the current user.
	 *
	 * The server replies with a {@link BeginSessionResponse} that contains
	 * a list of all available lectures
	 *
	 * @param request the request
	 * @throws IOException if the response could not be sent
	 */
	protected synchronized void handle(BeginSessionRequest request) throws IOException {

		var lectures = this.data.extractLectureList();
		for (var l : lectures.getLectures()) {
			logger.info("Lecture {} has tasks {}", l.getName(), l.getSelectedTasks().stream().map(Task::getName).collect(Collectors.toList()));
		}
		for (var t : lectures.getTasks()) {
			logger.info("{}", t.getName());
		}
		this.con.send(new BeginSessionResponse(request.getClientID(), lectures));
	}

	protected void handle(JavaFeedbackRequest request) throws IOException {
		//logger.info("Creating Java Feedback for request : {}", request);
		java.util.List<Query> queries;
		StudentSolution solution;

		var sent = DataFactory.eINSTANCE.createStudentSolution();
		synchronized(this) {
			try {
				var optPerson = determineSender(request);
				if (optPerson.isEmpty()) {
					// FIXME: Return something like HTTP's BAD_REQUEST?
					logger.debug("No Person for Hash: {} found", request.getClientID());
					return;
				}

				// TODO create + remember solutions correctly!
				Optional<Task> task = data.getTask(request.getTaskName());
				if (task.isEmpty()) {
					logger.debug("No Task found for Name: {}", request.getTaskName());
					// FIXME: Return something like HTTP's BAD_REQUEST?
					return;
				}
				solution = DataFactory.eINSTANCE.createStudentSolution();
				var content = DataFactory.eINSTANCE.createSolutionContent();
				content.setContent(request.getJavaFiles());
				solution.setDate(new Date());
				solution.setAuthor(optPerson.get());
				solution.setContents(content);
				solution.setTask(task.get()); // remembers the solution in-memory #38

				queries = manager.map(solution);
			}
			catch (Exception e){
			    logger.info("Unexpected Error");
			    logger.info(e);
				this.con.send(new JavaFeedbackResponse(request.getClientID(), sent));
			    return;
			}
		}

		try {
			queries.stream().forEach(
					q -> {
						q.evaluate(solution);
						sent.getQueryResults().addAll(q.getResult());
					});
		} catch (Exception e){
			logger.info("Unexpected Error during evaluation");
			logger.info(e);
			this.con.send(new JavaFeedbackResponse(request.getClientID(), sent));
			return;
		}

		logger.info("evaluated all queries, Returning {} QueryResults", sent.getQueryResults().size());
		sent.getQueryResults().forEach(qr -> logger.debug("Sending QueryResult : {}", qr));
		// create + send response
		JavaFeedbackResponse response = new JavaFeedbackResponse(request.getClientID(), sent);
		logger.info("Sending Response: {}", response);
		this.con.send(response);

		synchronized(this) {
			// update statistics
			StatisticsManager manager = StatisticsManager.createDefault(PATH_STATISTICS, data.getData());
			manager.calculate(data.getData());

			// persist the data
			data.saveCurrentData();
		}
	}

	protected void handle(UMLFeedbackRequest request) throws IOException {
		logger.debug("UML request: {}", request);

		java.util.List<Query> queries;
		StudentSolution solution;

		var sent = DataFactory.eINSTANCE.createStudentSolution();
		try {
			var optPerson = determineSender(request);
			if (optPerson.isEmpty()) {
				// FIXME: Return something like HTTP's BAD_REQUEST?
				logger.debug("No Person for Hash: {} found", request.getClientID());
				return;
			}

			// TODO create + remember solutions correctly!
			Optional<Task> task = data.getTask(request.getTaskName());
			if (task.isEmpty()) {
				logger.debug("No Task found for Name: {}", request.getTaskName());
				// FIXME: Return something like HTTP's BAD_REQUEST?
				return;
			}

			solution = DataFactory.eINSTANCE.createStudentSolution();
			var content = DataFactory.eINSTANCE.createSolutionContent();
			content.setContent(Map.of("", request.getUml()));
			solution.setDate(new Date());
			solution.setAuthor(optPerson.get());
			solution.setContents(content);
			solution.setTask(task.get());

			queries = manager.map(solution);

		} catch (Exception e){
			logger.info("Unexpected Error", e);
			sent.getQueryResults().add(createServerErrorResponse());
			this.con.send(new UMLFeedbackResponse(request.getClientID(), sent));
			return;
		}

		try {
			queries.stream().forEach(
					q -> {
						q.evaluate(solution);
						sent.getQueryResults().addAll(q.getResult());
					});
		} catch (Exception e){
			logger.info("Unexpected Error during evaluation", e);
			sent.getQueryResults().add(createServerErrorResponse());
			this.con.send(new UMLFeedbackResponse(request.getClientID(), sent));
			return;
		}


		var response = new UMLFeedbackResponse(request.getClientID(), sent);
		logger.info("Sending Response: {}", response);
		this.con.send(response);
	}

	private static QueryResult createServerErrorResponse(){
		return Initializer.createQueryResult(QueryResultType.WARNING,
				"Internal Server Error",
				"Internal Server Error",
				"::-:"
		);
	}

	/**
	 * Finds the {@link Person} who issued the supplied request.
	 *
	 * @param request the request
	 * @return the user (if existing)
	 */
	private final Optional<Person> determineSender(ClientToServer request) {
		var clientId = request.getClientID();
		return this.data.getPerson(clientId);
	}

	/**
	 * Finds the {@link Lecture} identified by the supplied id.
	 *
	 * @param lectureId the id
	 * @return the lecture
	 * @throws IllegalArgumentException if there is no such lecture
	 */
	private final Lecture findLecture(final String lectureId) {
		var opt = this.data.getLecture(lectureId);

		if (opt.isEmpty())
			throw new IllegalArgumentException("No such lecture: " + lectureId);

		return opt.get();
	}


	public DataManager getDatabase() {
		return this.data;
	}
}
