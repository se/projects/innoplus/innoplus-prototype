package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.Dictionary;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;
import org.eclipse.uml2.uml.Operation;

import java.util.Collections;
import java.util.List;

public class OperationStrategy implements MatchingStrategy<Operation> {
    @Override
    public boolean match(Operation element, Operation other) {
        return NameMatcher.instance.areEquivalent(element, other);
        // if(Dictionary.getInstance().areEquivalent(element.getName(), other.getName())){
        //     // TODO check interface of the operation
        //     if (element.getReturnResult() != null && other.getReturnResult() != null){
        //         return element.getReturnResult().getType().equals(other.getReturnResult().getType());
        //     }
        // }
        // return false;
    }

    @Override
    public List<QueryResult> extractResults(Operation element, Operation other) {
        return Collections.emptyList();
    }
}
