package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.Initializer;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;
import org.eclipse.uml2.uml.Interface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InterfaceStrategy implements MatchingStrategy<Interface> {

    @Override
    public boolean match(Interface element, Interface other) {
        return NameMatcher.instance.areEquivalent(element, other);
    }

    @Override
    public List<QueryResult> extractResults(Interface element, Interface other) {
        var results = new ArrayList<QueryResult>();

        var opStrategy = new OperationStrategy();
        var matches = new Boolean[element.getOwnedOperations().size()];

        for (int i = 0; i < element.getOwnedOperations().size(); i++) {
            var elementOperation = element.getOwnedOperations().get(i);
            matches[i] = false;

            for (int j = 0; j < other.getOwnedOperations().size(); j++) {
                var otherOperation = element.getOwnedOperations().get(i);
                if(opStrategy.match(elementOperation, otherOperation)){
                    matches[i] = true;
                }
            }
        }

        if (Arrays.asList(matches).contains(false)){
            long missingOperations = Arrays.asList(matches).stream().filter(b -> !b).count();
            results.add(Initializer.createQueryResult(
                    QueryResultType.WARNING,
                    missingOperations + " operations for the Interface: " + other.getName() + " are missing.",
                    missingOperations + " operations for the Interface: " + other.getName() + " are missing.",
                    "::-:")
            );
        }

        return results;
    }
}
