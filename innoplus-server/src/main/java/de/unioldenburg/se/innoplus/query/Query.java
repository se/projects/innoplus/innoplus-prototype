package de.unioldenburg.se.innoplus.query;

import java.util.List;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.Solution;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;

/**
 * Properties of Queries:
 * - A query can have an internal state.
 * - Each query is executed zero or one times.
 */
public interface Query {

	// 0. (no circles of (non?) executed queries: might be difficult) TODO
	// public List<Query> getPreconditions();

	/** Logger for every Query Instance **/
	static final org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager.getLogger(Query.class);

	/**
	 * Starts the evaluation of the query (Step 1).
	 * @param studentSolution the current solution to check
	 */
	public void evaluate(Solution studentSolution);


	/**
	 * After Step 1, sub queries might exist for this query (Step 2), which must be executed now, but outside of this query.
	 * @return the queries are not yet executed!
	 */
	public List<Query> getSubQueries();


	/**
	 * After executing all sub queries (Step 2), the result of this query is available (Step 3).
	 * @return the final result of the query (must not be null)
	 */
	public List<QueryResult> getResult();

	/**
	 * After executing all sub queries (Step 2), the information about traceability for this query are available (Step 3).
	 * @return the information required for traceability (might be null)
	 */
	public Object getTraceabilityElement();

}
