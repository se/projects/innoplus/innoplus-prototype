package de.unioldenburg.se.innoplus.query.docker;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.zerodep.ZerodepDockerHttpClient;
import de.unioldenburg.se.innoplus.util.FileUtils;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Set;

public class DockerDriver {

	/** Class which consumes the generated image **/
	public interface BuildResultConsumer {

		/**
		 * Consume the generated image.
		 *
		 * @param buildResult  the image bytes (this stream is closed after this method is
		 *               called)
		 */
		void accept(InputStream buildResult);
	}

	protected final static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
			.getLogger(DockerDriver.class);

	/** Tag for the builder image **/
	public static final String IMAGE_TAG = "maven-setup:latest";

	/** Client used to issue commands to the Docker host */
	private final DockerClient client;



	public DockerDriver(){
		logger.trace("Creating new DockerDriver!");

		var conf = DefaultDockerClientConfig.createDefaultConfigBuilder().
				build();

		var httpClient = new ZerodepDockerHttpClient.Builder()
				.dockerHost(conf.getDockerHost())
				.sslConfig(conf.getSSLConfig())
				.build();

		this.client = DockerClientImpl.getInstance(conf, httpClient);

	}

	/**
	 * Yields the base image which will be used for customized containers to build
	 * images.
	 *
	 * @return the image id
	 **/
	public String getImage(Map<String, String> arguments, Set<Path> contents, Set<String> resources){
		logger.info("Using build image tagged as \"{}\"", IMAGE_TAG);
		logger.info("With Build Arguments : {}", arguments);


		// tar the context and the dockerfile
		var byteOut = new ByteArrayOutputStream();
		try (var tarOut = new TarArchiveOutputStream(byteOut)) {

			for(var content : contents){
			    var target = content.toAbsolutePath().normalize();
				if (Files.isDirectory(target)){
					FileUtils.tarFolder(tarOut, target, target.getParent());
				}
			    else{
					try (var fis = new FileInputStream(target.toFile())) {
						var bytesIn = fis.readAllBytes();
						FileUtils.createArchiveEntry(tarOut, bytesIn, target.getFileName());
					}
				}
			}

			for(var res : resources){
				var bytesIn = IOUtils.resourceToByteArray(res, ClassLoader.getSystemClassLoader());
				var name = Paths.get(res).getFileName();
				FileUtils.createArchiveEntry(tarOut, bytesIn, name);
			}

		} catch (IOException e) {
		    logger.info("DockerFile not found", e);
		}

		// The Builder Pattern is messed up! Order Matters here. We need to set the BaseDirectory before the Dockerfile
		var cmd = client.buildImageCmd()
                .withTarInputStream(new ByteArrayInputStream(byteOut.toByteArray()))
				.withPull(true)
				.withTags(java.util.Set.of(IMAGE_TAG))
				.withRemove(true);

		for(var entry : arguments.entrySet()){
			var value = Paths.get(entry.getValue()).getFileName();
			cmd.withBuildArg(entry.getKey(), value.toString());
		}
		var context = cmd.start();

		logger.debug("Started image build...");

		var id = context.awaitImageId();
		logger.debug("Image-ID: {}", id);
		return id;
	}

	/**
	 * Instantiates a new container capable of building a customized image according
	 * to the given configuration.
	 *
	 * @return the container id
	 * @throws IOException if an error occurs
	 */
	public String createContainer(String image, BuildContext context) throws IOException{
		//logger.debug("createContainer({})", context);

		var byteOut = new ByteArrayOutputStream();
		try (var tarOut = new TarArchiveOutputStream(byteOut)) {
			for(var entry : context.studentSolution.entrySet()){
				var bytesIn = IOUtils.toInputStream(entry.getValue(), StandardCharsets.UTF_8).readAllBytes();
				var target = Paths.get(entry.getKey());
				FileUtils.createArchiveEntry(tarOut, bytesIn, target) ;
			}
		}

		var createResult = client
				.createContainerCmd(image)
				// needs to be an executable
				.withEntrypoint(context.mvnCommand)
				.withNetworkDisabled(true)
				.withHostConfig(
						HostConfig.newHostConfig()
							.withMemory(500L * 1024 * 1024)
							.withCpuPercent(30L)
				)
				.withAttachStdout(true)
				.withAttachStderr(true)
				.exec();

		client.copyArchiveToContainerCmd(createResult.getId())
				.withTarInputStream(new ByteArrayInputStream(byteOut.toByteArray()))
				// for some reason this ignores the set working directory
				.withRemotePath(context.remotePath)
				.exec();

		// The container is identified by this unique hash
		final var id = createResult.getId();

		// Dump warnings just to be sure
		for (var warning : createResult.getWarnings())
			logger.warn(warning);

		return id;
	}

	/**
	 * Returns the instance of the docker client.
	 * @return docker client
	 */
	public DockerClient getClient(){
		return client;
	}
}
