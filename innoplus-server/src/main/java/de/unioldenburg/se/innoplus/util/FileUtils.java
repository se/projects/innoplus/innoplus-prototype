package de.unioldenburg.se.innoplus.util;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {


    public static boolean isEmpty(Path path){
        if (Files.isDirectory(path)) {
            try (var directory = Files.list(path)) {
                return !directory.iterator().hasNext();
            }
            catch (IOException e){
                return false;
            }
        }
        return false;
    }

    public static void tarFolder(TarArchiveOutputStream outputStream, Path folder, Path base) throws IOException {
        for(var file : Files.newDirectoryStream(folder)){
            if (Files.isDirectory(file)){
                tarFolder(outputStream, file, base);
                continue;
            }
            var bytesIn = new FileInputStream(file.toFile()).readAllBytes();
            var fileName = base.relativize(file);
            createArchiveEntry(outputStream, bytesIn, fileName);
        }
    }

    public static void createArchiveEntry(TarArchiveOutputStream tarOut, byte[] bytesIn, Path file) throws IOException {
        var tarEntry = (TarArchiveEntry) tarOut.createArchiveEntry(file.toFile(), file.toString());
        tarEntry.setSize(bytesIn.length);
        tarOut.putArchiveEntry(tarEntry);
        tarOut.write(bytesIn);
        tarOut.closeArchiveEntry();
    }
}
