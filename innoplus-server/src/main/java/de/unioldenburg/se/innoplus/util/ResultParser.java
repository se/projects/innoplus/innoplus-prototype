package de.unioldenburg.se.innoplus.util;

import de.unioldenburg.se.innoplus.data.QueryResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

// TODO maybe interface for extract Methods and abstractXML Parser class, but probably an overkill
public abstract class ResultParser implements AutoCloseable {
    protected final List<String> filenames;
    protected final InputStream result;

    protected ResultParser(List<String> filenames, InputStream result){
        this.filenames = filenames;
        this.result = result;
    }

    /** Extracts Query Results from either a CheckStyle or JUnit Report */
    public abstract List<QueryResult> extractResults();

    @Override
    public void close() throws IOException {
        this.result.close();
    }
}
