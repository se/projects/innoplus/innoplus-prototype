package de.unioldenburg.se.innoplus;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;

/** Adapter for the official Stud.IP REST API **/
public class StudIpAdapter {

	/** Type of user **/
	public enum UserKind {
		/** Not identified or not existing **/
		Unknown,
		Student,
		Tutor,
		Teacher
	}

	/** Base url for API requests **/
	private static final String apiBase = "https://elearning.uni-oldenburg.de/plugins.php/innovationplus/api/";

	/** Enable debug output? **/
	private static final boolean DEBUG_HTTP = false;

	/** HTTP client for API requests **/
	private final HttpClient client;

	public StudIpAdapter() {
		// Set custom default options
		this.client = HttpClient.newBuilder()
						.version(HttpClient.Version.HTTP_2)
						.connectTimeout(Duration.ofSeconds(10))
						.followRedirects(Redirect.NORMAL)
						.build();
	}

	/**
	 * Checks whether the client exists and is enrolled for a certain lecture.
	 *
	 * @param userHash  MD5 hash of the username (e.g. abcd1234)
	 * @param lectureID the lecture id (e.g. PDA, WS 20/21:
	 *                  36e93f23b97226d889adb38483b273fa)
	 * @return true if the client is a member of the lecture, false otherwise
	 * @throws IOException           if the request failed
	 * @throws IllegalStateException if the API returns an unexpected response
	 **/
	public UserKind validateCredentials(final String userHash, final String lectureID) throws IOException {
		// Endpoint is <apiBase> / <lectureId> / MD5(<clientID>)
		var res = makeRequest(lectureID + '/' + userHash);
		var num = Integer.parseInt(res);

		// Sanity check
		if (num < 0 || num >= UserKind.values().length)
			throw new IllegalStateException("Unexpected API response: " + num);

		return UserKind.values()[num];
	}

	/**
	 * Issues a new request targeting <{@link apiBase}>/<path>
	 *
	 * @param path subpath starting from apiBase
	 * @return the response body
	 * @throws IOException if the request failed
	 **/
	private final String makeRequest(String path) throws IOException {
		try {
			var uri = URI.create(apiBase + path);
			if (DEBUG_HTTP)
				System.out.printf("GET %s\n", uri);

			var req = HttpRequest.newBuilder(uri)
							.GET()
							.build();

			var res = client.send(req, BodyHandlers.ofString());
			var body = res.body();

			if (DEBUG_HTTP) {
				System.out.printf("Status: %d\n", res.statusCode());
				res.headers().map().forEach((header, values) -> {
					System.out.printf("- %s: %s\n", header, values);
				});
				System.out.printf("Begin body\n%s\nEnd body\n\n", body);
			}

			// Fail for other status codes than 2xx
			if (res.statusCode() / 100 != 2)
				throw new IOException("Request failed (" + res.statusCode() + "):\n" + body);

			return body;
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}
}
