package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;
import org.eclipse.uml2.uml.Generalization;

import java.util.Collections;
import java.util.List;

public class GeneralizationStrategy implements MatchingStrategy<Generalization> {
    @Override
    public boolean match(Generalization element, Generalization other) {
        return NameMatcher.instance.areEquivalent(element.getGeneral(), other.getGeneral())
                && NameMatcher.instance.areEquivalent(element.getSpecific(), other.getSpecific());
    }

    @Override
    public List<QueryResult> extractResults(Generalization element, Generalization other) {
        return Collections.emptyList();
    }
}
