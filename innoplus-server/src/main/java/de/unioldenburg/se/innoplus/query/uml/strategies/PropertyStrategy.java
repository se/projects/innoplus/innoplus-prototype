package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.Dictionary;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;
import org.eclipse.uml2.uml.Property;

import java.util.Collections;
import java.util.List;

public class PropertyStrategy implements MatchingStrategy<org.eclipse.uml2.uml.Property> {

    @Override
    public boolean match(Property element, Property other) {
        return NameMatcher.instance.areEquivalent(element, other);
    }

    @Override
    public List<QueryResult> extractResults(Property element, Property other) {
        return Collections.emptyList();
    }
}
