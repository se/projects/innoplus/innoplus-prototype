package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.Initializer;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.Dictionary;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;
import org.eclipse.uml2.uml.Class;

import java.util.ArrayList;
import java.util.List;

public class ClassStrategy implements MatchingStrategy<Class> {


    public ClassStrategy(){

    }

    @Override
    public boolean match(Class element, Class other) {
        return NameMatcher.instance.areEquivalent(element, other);
    }

    @Override
    public List<QueryResult> extractResults(Class element, Class other) {
        var results = new ArrayList<QueryResult>();
        var propertyStrategy = new PropertyStrategy();
        for (int i = 0; i < element.getAttributes().size(); i++){
            int matches = 0;
            var tProperty = element.getAttributes().get(i);
            for (int j = 0; j < other.getAttributes().size(); j++) {
                var sProperty = other.getAllAttributes().get(j);
                if (propertyStrategy.match(tProperty, sProperty)){
                    // System.out.println("----------sP " + sProperty);
                    // System.out.println("----------tP " + tProperty);
                    matches++;
                }
            }
            if (matches == 0){
                results.add(Initializer.createQueryResult(
                        QueryResultType.WARNING,
                        "A property for the class: " + other.getName() + " is missing.",
                        "A property for the class: " + other.getName() + " is missing.",
                        "::-:")
                );
            }
        }
        var opStrategy = new OperationStrategy();
        for (int i = 0; i < element.getOwnedOperations().size(); i++) {
            int matches = 0;
            var tOperation = element.getOwnedOperations().get(i);
            for (int j = 0; j < other.getOwnedOperations().size(); j++) {
                var sOperation = element.getOwnedOperations().get(i);
                if (opStrategy.match(tOperation, sOperation)){
                    if (opStrategy.match(tOperation, sOperation)){
                        matches++;
                    }
                }
                if (matches == 0){
                    results.add(Initializer.createQueryResult(
                            QueryResultType.WARNING,
                            "A operation for the class: " + other.getName() + " is missing.",
                            "A operation for the class: " + other.getName() + " is missing.",
                            "::-:")
                    );
                }
            }
        }
        // TODO we can do this recursively but i have to check if it goes up to object
        for (int i = 0; i < element.getSuperClasses().size(); i++){
            int matches = 0;
            var tParentClass = element.getSuperClasses().get(i);
            for (int j = 0; j < other.getSuperClasses().size(); j++) {
                var sParentClass = other.getSuperClasses().get(j);
                if (match(tParentClass, sParentClass)){
                    matches++;
                }
            }
            if (matches==0){
                results.add(Initializer.createQueryResult(
                        QueryResultType.WARNING,
                        "A superclass for the class: " + other.getName() + " is missing.",
                        "A superclass for the class: " + other.getName() + " is missing.",
                        "::-:")
                );
            }
        }
        return results;
    }
}
