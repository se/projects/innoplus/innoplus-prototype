package de.unioldenburg.se.innoplus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.unioldenburg.se.innoplus.data.*;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.checkstyle.*;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.junit.JUnitQueryFactory;
import de.unioldenburg.se.innoplus.query.staticanalysis.StaticQueryFactory;
import de.unioldenburg.se.innoplus.query.uml.UMLQueryFactory;
import de.unioldenburg.se.innoplus.util.FileUtils;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class Initializer {

	private final static org.apache.logging.log4j.Logger logger =
		org.apache.logging.log4j.LogManager.getLogger(Initializer.class);

	public abstract QueryManager createDefaultQueryManager();

	public static TaskCategory generateTaskCategory(String categoryName, InnoplusDatabase data){
		var taskCategory = DataFactory.eINSTANCE.createTaskCategory();
		data.getTaskCategories().add(taskCategory);
		taskCategory.setName(categoryName);
		return taskCategory;
	}

	public static SolutionCategory generateSolutionCategory(String categoryName, InnoplusDatabase data){
		// TODO might not be the smartest idea to add i to solutionCategories here
		var solutionCategory = DataFactory.eINSTANCE.createSolutionCategory();
		data.getSolutionCategories().add(solutionCategory);
		solutionCategory.setName(categoryName);
		return solutionCategory;
	}

	public static TeacherSolution generateSampleSolution(Task task, SolutionCategory solutionCategory,
														InnoplusDatabase data){
		var sampleSolution = DataFactory.eINSTANCE.createTeacherSolution();
		data.getSampleSolutions().add(sampleSolution);
		sampleSolution.setDate(new Date());
		sampleSolution.setTask(task);
		sampleSolution.setCategory(solutionCategory);
		return sampleSolution;
	}

	public static SolutionContent generateSolutionContent(Path path, Object content){
		var solutionContent = DataFactory.eINSTANCE.createSolutionContent();
		solutionContent.setPath(path.toString());
		solutionContent.setContent(content);
		return solutionContent;
	}

	public static QueryResult createQueryResult(QueryResultType type, String summary, String description,
												String location)
	{
		var qResult = DataFactory.eINSTANCE.createQueryResult();
		qResult.setDate(Date.from(Instant.now()));
		qResult.setType(type);
		qResult.setSummary(summary);
		qResult.setDescription(description);
		qResult.setLocation(location);
		return qResult;
	}

	public abstract InnoplusDatabase createDefaultData() throws IOException;

	public abstract void initQueries(QueryManager queryManager, InnoplusDatabase data);
}
