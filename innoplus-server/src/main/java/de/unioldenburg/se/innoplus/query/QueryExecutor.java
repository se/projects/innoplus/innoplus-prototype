package de.unioldenburg.se.innoplus.query;

import java.util.ArrayList;
import java.util.List;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.Solution;

public class QueryExecutor {

	public class Result {
		private final QueryResult result;
		private final Object traceability;

		public Result(QueryResult result, Object traceability) {
			super();
			this.result = result;
			this.traceability = traceability; // might be null
		}

		public QueryResult getResult() {
			return result;
		}
		public Object getTraceability() {
			return traceability;
		}
	}


	public List<Result> executeQueries(List<Query> queries, Solution studentSolution) {
		// TODO, creating an empty ArrayList is kind of bad
		List<Result> result = new ArrayList<>();
		executeQueriesLogic(queries, studentSolution, result);
		return result;
	}

	protected void executeQueriesLogic(List<Query> queries, Solution studentSolution, List<Result> result) {
		if (queries == null || queries.isEmpty()) {
			return;
		}

		/* features
		 * - execute all queries: done
		 * - hierarchy of queries: depth-first, done
		 * - limit results to show: TODO
		 * - break, if error occurred (as indicated by the QueryResult): TODO
		 * - ... ?
		 */

		for (int i = 0; i < queries.size(); i++) {
			// execute query
			Query query = queries.get(i);
			query.evaluate(studentSolution);

			// remember position to add the result
			int index = result.size();

			// sub-queries (and adds its result!)
			executeQueriesLogic(query.getSubQueries(), studentSolution, result);

			// add the result at the remembered position
			// TODO see TODO above, fails if the ArrayList is empty
			// result.set(index, new Result(query.getResult(), query.getTraceabilityElement()));
			for(var qResult : query.getResult()){
				result.add(index, new Result(qResult, query.getTraceabilityElement()));
			}
		}
	}
}
