package de.unioldenburg.se.innoplus.query.checkstyle;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Solution;
import de.unioldenburg.se.innoplus.query.CodeQuery;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.docker.BuildContext;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.util.CheckstyleResultParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static de.unioldenburg.se.innoplus.Initializer.createQueryResult;

public class CheckStyleQuery extends CodeQuery implements Query {
    private List<QueryResult> queryResults;

    public CheckStyleQuery(String image, DockerDriver driver) {
        super(image, driver);
        logger.debug("created new CheckStyleQuery");
    }

    @Override
    public void evaluate(Solution studentSolution) {
        this.queryResults = new ArrayList<>();
        var content = (Map) studentSolution.getContents().getContent();

        if (studentSolution.getContents().getContent() == null){
            queryResults.add(createQueryResult(
                    QueryResultType.ERROR,
                    "No Solution provided",
                    "No Solution provided",
                    "::-:")
            );
            return;
        }
        // try to run the code
        AtomicReference<InputStream> archiveConsumer = new AtomicReference<>();
        try {
            execute(
                    new BuildContext(
                            content,
                            workingDir + "/" + "feedback",
                            workingDir + "/student_solutions",
                            "java", "-classpath",
                            "custom-checks.jar:checkstyle-8.43-all.jar:innoplus-shareddata-1.1.0-SNAPSHOT.jar",
                            "com.puppycrawl.tools.checkstyle.Main", "-c" , "config.xml", "student_solutions/"
                    ),
                    archiveConsumer::set
            );
        } catch (IOException e) {
            queryResults.add(createQueryResult(QueryResultType.ERROR,
                    "Compilation Error",
                    "Compiling the code failed",
                    "::-:")
            );
            return;
        }
        try{
            var parser = new CheckstyleResultParser(content.keySet(), archiveConsumer.get());
            this.queryResults.addAll(parser.extractResults());
        } catch (Exception e) {
            // Something failed while trying to extract the Result -> files were not generated so a compilation error
            logger.debug("Exception occurred while parsing checkstyle Feedback", e);
            queryResults.add(createQueryResult(
                    QueryResultType.ERROR,
                    "Compilation Error",
                    "Compilation Error",
                    "::-:")
            );
        }
    }

    @Override
    public List<Query> getSubQueries() {
        // Don't think we need SubQueries here. If we do, an EmptyList is still a better default than null
        return Collections.emptyList();
    }

    @Override
    public List<QueryResult> getResult() {
        if (queryResults == null)
            throw new IllegalStateException("Cannot fetch the Result before executing the Query");
        return queryResults;
    }

    @Override
    public Object getTraceabilityElement() {
        return null;
    }

}
