package de.unioldenburg.se.innoplus.query;

import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;

import java.util.List;

public abstract class QueryFactory {

	/** the logger */
	protected final static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
			.getLogger(QueryFactory.class);

	public abstract List<Query> createQueries(StudentSolution solution);
}
