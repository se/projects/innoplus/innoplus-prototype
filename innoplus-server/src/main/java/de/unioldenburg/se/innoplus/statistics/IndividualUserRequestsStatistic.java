package de.unioldenburg.se.innoplus.statistics;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.QueryResultType;

public class IndividualUserRequestsStatistic implements Statistic {
	protected final Person person;
	protected final String replacementLabel;

	public IndividualUserRequestsStatistic(Person person, String replacementLabel) {
		super();
		this.person = Objects.requireNonNull(person);
		this.replacementLabel = Objects.requireNonNull(replacementLabel);
	}

	@Override
	public String getStatisticType() {
		return "csv";
	}

	@Override
	public String getName(InnoplusDatabase data) {
		return "user-" + replacementLabel + "-requests";
	}

	@Override
	public void calculate(InnoplusDatabase data, OutputStream storage) throws IOException {
		// create the data structure as required by the CSV library
		List<String[]> csv = StatisticsHelper.createCsvTemplate();

		// headline
		csv.add(new String[] {"Category", "Value"});

		/*
		 * Content
		 */

		csv.add(new String[] {"Requests", "" + person.getSubmittedSolutions().size()});

		csv.add(new String[] {"Sessions", "" + StatisticsHelper.groupRequestsBySessions(person.getSubmittedSolutions()).size()});

		long hintSuccess = person.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.SUCCESS).count();
		long hintError = person.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.ERROR).count();
		long hintWarning = person.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.WARNING).count();
		long hintInformation = person.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.INFORMATION).count();
		long all = hintSuccess + hintError + hintWarning + hintInformation;

		csv.add(new String[] {"% Success", StatisticsHelper.percentage(all, hintSuccess)});
		csv.add(new String[] {"% Error", StatisticsHelper.percentage(all, hintError)});
		csv.add(new String[] {"% Warning", StatisticsHelper.percentage(all, hintWarning)});
		csv.add(new String[] {"% Information", StatisticsHelper.percentage(all, hintInformation)});

		// store the data as CSV
		StatisticsHelper.saveAsCsv(csv, storage);
	}
}
