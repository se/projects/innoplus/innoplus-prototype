package de.unioldenburg.se.innoplus;

import de.unioldenburg.se.innoplus.data.*;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.checkstyle.CheckStyleQueryFactory;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.junit.JUnitQueryFactory;
import de.unioldenburg.se.innoplus.query.staticanalysis.StaticQueryFactory;
import de.unioldenburg.se.innoplus.query.uml_queries.UMLQueryFactory;
import de.unioldenburg.se.innoplus.util.PropertiesFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Stream;


public class DynamicInitializer extends Initializer {

    private static final org.apache.logging.log4j.Logger logger =
            org.apache.logging.log4j.LogManager.getLogger(DynamicInitializer.class);

    public static final String BASE_DIR = Path.of(".", "..").toString();
    public static final String TASK_BASE_DIR = "task-repository";
    public static final String LECTURES_BASE_DIR = "lectures";
    public static final String TASK_DESCRIPTION_FILE = "description.properties";
    public static final String TASK_TASK_FILE = "task.md";
    public static final String LECTURE_DESCRIPTION_FILE = "description.properties";
    public static final String LECTURE_TASKS_FOLDER = "tasks";
    public static final String CHECK_BASE_DIR = "checks";
    public static final String STYLE_CHECKS_DIR = "style";
    public static final String RUNTIME_CHECKS_DIR = "runtime";
    public static final String STATIC_CHECKS_DIR = "static";
    public static final List<String> CHECK_TYPES = List.of(STYLE_CHECKS_DIR, RUNTIME_CHECKS_DIR, STATIC_CHECKS_DIR);
    public static final String MODELLING_ESTATE_DIR = "real_estate";
    public static final String MODELLING_QUERIES_DIR = "queries";
    public static final String TC_PROGRAMMING = InnoplusConstants.TASK_JAVA;
    public static final String TC_MODELLING = InnoplusConstants.TASK_UML;
    public static final List<String> TASK_CATEGORIES = List.of(TC_PROGRAMMING, TC_MODELLING);
    public static final Map<String, String> TASK_CATEGORY_FOLDER = Map.of(
            TC_PROGRAMMING, "programming",
            TC_MODELLING, "modelling");

    protected final DataFactory factory = DataFactory.eINSTANCE;
    protected final InnoplusDatabase data = factory.createInnoplusDatabase();
    protected final Map<String, TaskCategory> taskCategories = new HashMap<>();
    protected final Map<String, Path> indexedTasks = new HashMap<>();
    protected final Map<String, Task> allTasks = new HashMap<>();
    protected final Map<String, Teacher> allTeachers = new HashMap<>();
    protected final Map<String, Lecture> allLectures = new HashMap<>();
    protected final Map<Task, TeacherSolution> allSampleSolutions = new HashMap<>();


    protected static class Description extends PropertiesFile {

        public static final String NAME = "name";
        public static final String ID = "id";

        protected String name;

        public Description(Path propertiesFilePath, String name) {
            super(propertiesFilePath);
            this.name = name;
        }

        public boolean load() {
            final var success = super.load();
            if(!success) {
                logger.warn("Could not load {} for {}. Trying to create one.",
                        this.getClass().getSimpleName(), name);
            }
            return success;
        }

        public boolean store() {
            final var success = super.store();
            if(!success) {
                logger.warn("Could not store {} for {}!", this.getClass().getSimpleName(), name);
            }
            return success;
        }

        public <T> T getValueAndSetDefault(String key, T defaultValue) {
            final var value = getValue(key, defaultValue);
            setValue(key, value);
            return value;
        }
    }

    protected static class LectureDescription extends Description {

        public static final String LECTURER = "lecturer";
        public static final String TASKS = "tasks";


        public LectureDescription(Path propertiesFilePath, String name) {
            super(propertiesFilePath, name);
        }
    }

    protected static class TaskDescription extends Description {

        public static final String CREATOR = "creator";
        public static final String CATEGORY = "category";


        public TaskDescription(Path propertiesFilePath, String name) {
            super(propertiesFilePath, name);
        }
    }

    public DynamicInitializer() { }

    @Override
    public QueryManager createDefaultQueryManager() {
        final var manager = new QueryManager();
        final var driver = new DockerDriver();
        final var jqf = new JUnitQueryFactory(driver);
        final var cqf = new CheckStyleQueryFactory(driver);
        final var sqf = new StaticQueryFactory(driver);
        final var uqf = new UMLQueryFactory();

        for (final var task : data.getTasks()) {
            final var taskName = task.getName();
            switch (task.getCategory().getName()) {
                case TC_PROGRAMMING:
                    try {
                        final var checks = (Map<String, String>) allSampleSolutions.get(task)
                                .getContents().getContent();
                        if (checks.containsKey(RUNTIME_CHECKS_DIR)) {
                            manager.register(taskName, jqf);
                            logger.debug("Registered {} query for Task '{}' ({}) in Query Manager",
                                    RUNTIME_CHECKS_DIR, taskName, task.getId());
                        }
                        if (checks.containsKey(STYLE_CHECKS_DIR)) {
                            manager.register(taskName, cqf);
                            logger.debug("Registered {} query for Task '{}' ({}) in Query Manager",
                                    STYLE_CHECKS_DIR, taskName, task.getId());
                        }
                        if (checks.containsKey(STATIC_CHECKS_DIR)) {
                            manager.register(taskName, sqf);
                            logger.debug("Registered {} query for Task '{}' ({}) in Query Manager",
                                    STATIC_CHECKS_DIR, taskName, task.getId());
                        }
                    } catch (NullPointerException e) {
                        logger.warn("Task '{}' ({}) had no checks!", taskName, task.getId());
                    }
                    break;
                case TC_MODELLING:
                    manager.register(taskName, uqf);
                    logger.debug("Registered {} query for Task '{}' ({}) in Query Manager",
                            TC_MODELLING, taskName, task.getId());
                    break;
                default:
                    throw new IllegalStateException("No QueryManager exist for task " + taskName);
            }
        }

        return manager;
    }

    @Override
    public InnoplusDatabase createDefaultData() {
        indexTasks();
        createTaskCategories();
        createLecturesAndItsTasks();
        data.getLectures().addAll(allLectures.values());
        data.getTasks().addAll(allTasks.values());
        data.getPersons().addAll(allTeachers.values());
        data.getTaskCategories().addAll(taskCategories.values());
        data.getSampleSolutions().addAll(allSampleSolutions.values());
        for (final var l : data.getLectures()) {
            logger.info("Loaded {} Tasks for Lecture '{}' ({})",
                    l.getSelectedTasks().size(), l.getName(), l.getId());
        }
        return data;
    }

    @Override
    public void initQueries(QueryManager queryManager, InnoplusDatabase data) {
        logger.info("Initializing Queries");

        for (var task : data.getTasks()){
            // TODO: remove this for modelling
            if (!task.getCategory().getName().equals(TC_PROGRAMMING)){
                continue;
            }

            var dummySolution = DataFactory.eINSTANCE.createStudentSolution();
            dummySolution.setTask(task);
            logger.info("Initializing Task: '{}' ({})", task.getName(), task.getId());
            queryManager.map(dummySolution);
        }

        logger.info("Finished Query Initialization");
    }

    protected void indexTasks() {
        for (final var category : TASK_CATEGORIES) {
            final var allTasksOfCategory = Path.of(BASE_DIR, TASK_BASE_DIR, TASK_CATEGORY_FOLDER.get(category))
                    .toFile().listFiles(File::isDirectory);
            if (allTasksOfCategory != null) {
                for (final var task : allTasksOfCategory) {
                    final var taskDescription = createTaskDescription(task.toPath());
                    taskDescription.load();
                    final var taskId = taskDescription.getValueAndSetDefault(TaskDescription.ID,
                            UUID.randomUUID().toString());
                    indexedTasks.put(taskId, task.toPath());
                    // set category for task if not set or wrong
                    final var taskCategory = taskDescription.getValue(TaskDescription.CATEGORY);
                    if (taskCategory.isEmpty() || !taskCategory.get().equals(category)) {
                        taskDescription.setValue(TaskDescription.CATEGORY, category);
                    }
                    taskDescription.store();
                }
            }
        }
    }

    protected String loadTaskTask(Path taskPath) {
        final var path = taskPath.resolve(TASK_TASK_FILE);
        try {
            return Files.readString(path);
        } catch (IOException e) {
            logger.warn("Failed to load Tasks '{}' Task!", taskPath.getFileName(), e);
        }
        return "";
    }

    protected void createTaskCategories() {
        for (final var categoryName : TASK_CATEGORIES) {
            final var category = factory.createTaskCategory();
            category.setName(categoryName);
            final var solution = generateSolutionCategory(category.getName(), data);
            category.getSolutionCategories().add(solution);
            taskCategories.put(category.getName(), category);
        }
    }

    protected void createLecturesAndItsTasks() {
        final var lectureNames = Path.of(BASE_DIR, LECTURES_BASE_DIR).toFile().listFiles(File::isDirectory);
        if (lectureNames != null) {
            for (final var lectureName : lectureNames) {
                final var lecture = createLecture(lectureName.toPath());
                if (lecture.isEmpty()) {
                    continue;
                }

                // task map to filter duplicate entries
                final var lectureTasks = new HashMap<String, Task>();

                // add all tasks in lectures 'tasks' directory
                final var lectureTasksDir = lectureName.toPath().resolve(LECTURE_TASKS_FOLDER)
                        .toFile().listFiles(File::isDirectory);
                if (lectureTasksDir != null) {
                    for (final var lectureTask : lectureTasksDir) {
                        final var taskDescription = createTaskDescription(lectureTask.toPath());
                        taskDescription.load();
                        final var taskId = taskDescription.getValueAndSetDefault(TaskDescription.ID,
                                UUID.randomUUID().toString());
                        taskDescription.store();

                        if (allTasks.containsKey(taskId)) {
                            lectureTasks.put(taskId, allTasks.get(taskId));
                        } else {
                            final var taskPath = indexedTasks.containsKey(taskId) ?
                                    indexedTasks.get(taskId) : lectureTask.toPath();
                            final var task = createTask(taskPath);
                            task.ifPresent(t -> {
                                // add task category from task description
                                final var taskCategory = taskDescription.getValue(TaskDescription.CATEGORY);
                                taskCategory.ifPresent(c -> {
                                    final var tc = taskCategories.get(c);
                                    if (tc != null) {
                                        t.setCategory(tc);
                                        lectureTasks.put(t.getId(), t);
                                    } else {
                                        logger.warn("TaskCategory {} is unknown -> ignoring task {} ({})!",
                                                c, t.getName(), t.getId());
                                    }
                                });
                            });
                        }
                    }
                }

                // add all tasks in lecture description 'tasks' entry (comma separated)
                final var lectureDescription = createLectureDescription(lectureName.toPath());
                lectureDescription.load();
                final var tasks = lectureDescription.getValue(LectureDescription.TASKS);
                if (tasks.isPresent()) {
                    for (var taskId : tasks.get().split(",")) {
                        taskId = taskId.trim();
                        if (allTasks.containsKey(taskId)) {
                            lectureTasks.put(taskId, allTasks.get(taskId));
                        } else if (indexedTasks.containsKey(taskId)) {
                            final var task = createTask(indexedTasks.get(taskId));
                            task.ifPresent(t -> lectureTasks.put(t.getId(), t));
                        }
                    }
                }

                // add all registered tasks to lecture
                lecture.get().getSelectedTasks().addAll(lectureTasks.values());
            }
        }
    }

    protected Optional<Lecture> createLecture(Path lecturePath) {
        final var lecture = factory.createLecture();
        final var lectureName = lecturePath.getFileName().toString();
        final var lectureDescription = createLectureDescription(lecturePath);
        if (!lectureDescription.load()) {
            logger.error("Could not load Lecture '{}'!", lectureName);
            return Optional.empty();
        }
        lecture.setId(lectureDescription.getValueAndSetDefault(LectureDescription.ID, UUID.randomUUID().toString()));
        lecture.setName(lectureDescription.getValue(LectureDescription.NAME, lectureName));
        lectureDescription.store();
        final var lecturer = lectureDescription.getValue(LectureDescription.LECTURER);
        if (lecturer.isPresent()) {
            lecture.getParticipants().add(getTeacher(lecturer.get()));
        } else {
            logger.info("Lecture '{}' has no lecturer.", lectureName);
        }
        allLectures.put(lecture.getId(), lecture);
        return Optional.of(lecture);
    }

    protected TaskDescription createTaskDescription(Path taskPath) {
        return new TaskDescription(taskPath.resolve(TASK_DESCRIPTION_FILE), taskPath.toAbsolutePath().toString());
    }

    protected LectureDescription createLectureDescription(Path lecturePath) {
        return new LectureDescription(lecturePath.resolve(LECTURE_DESCRIPTION_FILE),
                lecturePath.toAbsolutePath().toString());
    }

    protected Optional<Task> createTask(Path taskPath) {
        final var task = factory.createTask();
        final var taskName = taskPath.getFileName().toString();
        final var taskDescription = new TaskDescription(taskPath.resolve(TASK_DESCRIPTION_FILE), taskName);
        if (!taskDescription.load()) {
            logger.warn("Could not load Task '{}'!", taskName);
            return Optional.empty();
        }
        task.setId(taskDescription.getValueAndSetDefault(TaskDescription.ID, UUID.randomUUID().toString()));
        task.setName(taskDescription.getValue(TaskDescription.NAME, taskName));
        taskDescription.getValue(TaskDescription.CATEGORY)
                .ifPresent(c -> task.setCategory(taskCategories.get(c)));
        task.setDescription(loadTaskTask(taskPath));
        taskDescription.getValue(TaskDescription.CREATOR)
                .ifPresent(id -> task.setAuthor(getTeacher(id)));
        final Map<String, BiFunction<Task, Path, Optional<TeacherSolution>>> solutionCreator = Map.of(
                TC_PROGRAMMING, this::createProgrammingTaskSolution,
                TC_MODELLING, this::createModellingSolution
        );
        if (task.getCategory() != null) {
            final var solution = solutionCreator.get(task.getCategory().getName()).apply(task, taskPath);
            solution.ifPresent(s -> allSampleSolutions.put(task, s));
        }
        indexedTasks.put(task.getId(), taskPath);
        allTasks.put(task.getId(), task);
        return Optional.of(task);
    }

    protected Optional<TeacherSolution> createProgrammingTaskSolution(Task task, Path taskPath) {
        if (task.getCategory().getSolutionCategories().isEmpty()) {
            return Optional.empty();
        }
        final var solution = generateSampleSolution(task, task.getCategory().getSolutionCategories().get(0), data);
        final var checksDir = taskPath.resolve(CHECK_BASE_DIR);
        if (!Files.exists(checksDir)) {
            return Optional.empty();
        }
        final var checks = new HashMap<String, String>();
        for (final var checkType : CHECK_TYPES) {
            final var currentCheckDir = checksDir.resolve(checkType);
            if (Files.exists(currentCheckDir)) {
                try (Stream<Path> walk = Files.walk(currentCheckDir, 1)) {
                    walk.filter(Files::isRegularFile)
                            .findFirst()
                            .ifPresent(c -> checks.put(checkType, c.toAbsolutePath().normalize().toString()));
                } catch (IOException e) {
                    logger.warn(e);
                }
            }
        }
        solution.setContents(
                generateSolutionContent(
                        // TODO: make runtime check path not necessary anymore
                        Path.of(checks.containsKey(RUNTIME_CHECKS_DIR) ?
                                checks.get(RUNTIME_CHECKS_DIR) : checks.get(STATIC_CHECKS_DIR)),
                        checks
                )
        );
        return Optional.of(solution);
    }

    protected Optional<TeacherSolution> createModellingSolution(Task task, Path taskPath) {
        if (task.getCategory().getSolutionCategories().isEmpty()) {
            return Optional.empty();
        }
        final var solution = generateSampleSolution(task, task.getCategory().getSolutionCategories().get(0), data);
        var folder = taskPath.resolve(MODELLING_QUERIES_DIR);
        if (!Files.exists(folder)) {
            logger.warn("UMLQueries folder '{}' does not exist for task '{}' ({})!",
                    folder, task.getName(), task.getId());
            return Optional.empty();
        }
        try (Stream<Path> walk = Files.walk(folder, 1)) {
            walk.filter(Files::isRegularFile)
                    .filter(p -> p.getFileName().toString().endsWith("jar"))
                    .findFirst()
                    .ifPresent(jarFile -> {
                        solution.setContents(
                                generateSolutionContent(folder.resolve(jarFile.getFileName()).toAbsolutePath(), null)
                        );
                    });
        } catch (IOException e) {
            logger.warn(e);
            // TODO: return empty Optional?
        }
        return Optional.of(solution);
    }

    /* old modelling solution
    protected Optional<TeacherSolution> createModellingSolution(Task task, Path taskPath) {
        if (task.getCategory().getSolutionCategories().isEmpty()) {
            return Optional.empty();
        }
        final var solution = generateSampleSolution(task, task.getCategory().getSolutionCategories().get(0), data);
        var folder = taskPath.resolve(MODELLING_ESTATE_DIR);
        if (!Files.exists(folder)) {
            return Optional.empty();
        }
        try (Stream<Path> walk = Files.walk(folder, 1)) {
            walk.filter(Files::isRegularFile)
                    .filter(p -> p.getFileName().toString().endsWith("uml"))
                    .findFirst()
                    .ifPresent(uml -> {
                        final var umlRealEstateFiles = new HashMap<String, String>();
                        final var umlRel = folder.relativize(uml).toString();
                        try {
                            final var umlContent = Files.readString(uml);
                            // logger.info("{} = {}", umlRel, umlContent);
                            umlRealEstateFiles.put(umlRel, umlContent);
                            solution.setContents(
                                    generateSolutionContent(folder, umlRealEstateFiles)
                            );
                        } catch (IOException e) {
                            logger.warn(e);
                        }
                    });
        } catch (IOException e) {
            logger.warn(e);
            // TODO: return empty Optional?
        }
        return Optional.of(solution);
    }
     */

    protected Teacher getTeacher(String id) {
        return allTeachers.computeIfAbsent(id, k -> {
            final var teacher = factory.createTeacher();
            teacher.setId(k);
            return teacher;
        });
    }
}
