package de.unioldenburg.se.innoplus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.unioldenburg.se.innoplus.data.*;
import de.unioldenburg.se.innoplus.query.QueryManager;
import de.unioldenburg.se.innoplus.query.checkstyle.*;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.junit.JUnitQueryFactory;
import de.unioldenburg.se.innoplus.query.staticanalysis.StaticQueryFactory;
import de.unioldenburg.se.innoplus.query.uml.UMLQueryFactory;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * unused leftover for archive and as backup
 * must be modified to be used again
 */
// TODO (01.10.21): delete soon
public class StaticInitializer extends Initializer {

	private final static org.apache.logging.log4j.Logger logger =
			org.apache.logging.log4j.LogManager.getLogger(StaticInitializer.class);

	// Tasks
	public static final String CODE_TASK_MERGE_ARRAYS = "Merge sorted Arrays";
	public static final String CODE_TASK_STAR_DATABASE = "Star Database";
	public static final String CODE_TASK_MERGE_LINKED_LIST = "Linked List";
	public static final String CODE_TASK_MERGE_CIRCULAR_LIST = "Circular List";
	public static final String UML_TASK_TEST = "Test UML";

	// Checks for Tasks TODO put them into resources or something nice
	public static final Path JUNIT_CHECK_PATH_MERGE_ARRAYS = Paths.get(".", "..", "task-repository-static", "01_sorting", "tests", "SortingTest.java");
	public static final Path JUNIT_CHECK_PATH_STAR_DATABASE = Paths.get(".", "..", "task-repository-static", "02_star_database", "tests", "StarsTest.java");
	public static final Path JUNIT_CHECK_PATH_LINKED_LIST = Paths.get(".", "..", "task-repository-static", "03_linked_list", "tests", "LinkedIntListTest.java");
	public static final Path JUNIT_CHECK_PATH_CIRCULAR_LIST = Paths.get(".", "..", "task-repository-static", "04_circular_list", "tests", "CircleTest.java");
	public static final Path JUNIT_CHECK_PATH_RUNNING_GAME = Paths.get(".", "..", "task-repository-static", "05_hare_and_hedgehog", "tests", "RunningGameTest.java");
	public static final Path JUNIT_CHECK_PATH_GENERIC_CUTLERY = Paths.get(".", "..", "task-repository-static", "09_generic_cutlery", "static_analysis", "GenericCutleryTest.java");
	public static final Path JUNIT_CHECK_PATH_CUTLERY = Paths.get(".", "..", "task-repository-static", "06_cutlery_1", "static_analysis", "CutleryTest1Test.java");
	public static final Path JUNIT_CHECK_PATH_IO_TEST = Paths.get(".", "..", "task-repository-static", "10_io_test", "tests", "IOTaskTest.java");
	public static final Path JUNIT_CHECK_PATH_STREAM_TEST = Paths.get(".", "..", "task-repository-static", "08_stream_test", "tests", "StreamTestTest.java");


	public static final Path UML_TEACHER_SOLUTION_TEST = Paths.get(".", "..", "task-repository-static", "01_modelling", "teacher_solutions");


	public InnoplusDatabase createDefaultData() throws IOException {
		DataFactory factory = DataFactory.eINSTANCE;
		InnoplusDatabase data = factory.createInnoplusDatabase();

		// Java + JUnit
		TaskCategory taskJavaCode = factory.createTaskCategory();
		data.getTaskCategories().add(taskJavaCode);
		taskJavaCode.setName("Text2Java");

		var taskUML = factory.createTaskCategory();
		data.getTaskCategories().add(taskUML);
		taskUML.setName(InnoplusConstants.TASK_UML);

		// Junit Solution Category I
		var solutionJUnit = generateSolutionCategory("JUnit4JavaCode", data); // TODO: why are InnoplusConstants not used?

		// UML Solution Category
		var solutionUML = Initializer.generateSolutionCategory(InnoplusConstants.SOLUTION_UML, data);

		// Java Solution Category why do we need this?
		var solutionJava = generateSolutionCategory("JavaCode", data);

		//taskJavaCode.getSolutionCategories().add(solutionJUnit);
		taskJavaCode.getSolutionCategories().add(solutionJava);
		//taskJavaCode.getSolutionCategories().add(solutionCheckStyle);

		// Lecture "ST1"
		Lecture lectureST1 = factory.createLecture();
		data.getLectures().add(lectureST1);
		lectureST1.setName("ST1");
		lectureST1.setId("2d54797bbfce8f01aa6db98f73835709");

		// Lecture "PDA" with Lecturer and Student
		Lecture lecture = factory.createLecture();
		data.getLectures().add(lecture);
		lecture.setName("PDA");
		lecture.setId("36e93f23b97226d889adb38483b273fa");

		Lecture lectureOMP2021 = factory.createLecture();
		data.getLectures().add(lectureOMP2021);
		lectureOMP2021.setName("OMP");
		lectureOMP2021.setId("5121d92f28848eddf57dbdef148c0fcb");

		Teacher lecturer = factory.createTeacher();
		data.getPersons().add(lecturer);
		lecturer.setId("64ebee080195ff2efbaa7d40c1b09415");
		lecture.getParticipants().add(lecturer);
		lectureST1.getParticipants().add(lecturer);
		lectureOMP2021.getParticipants().add(lecturer);

		Student student = factory.createStudent();
		data.getPersons().add(student);
		student.setId("7bf927092164d4442bd55d73f58a006a");
		lecture.getParticipants().add(student);

		// register the first task
		Task task = factory.createTask();
		task.setName(CODE_TASK_MERGE_ARRAYS);
		task.setDescription(loadDescriptionFile("01_sorting"));
		data.getTasks().add(task);
		task.setCategory(taskJavaCode);
		task.setAuthor(lecturer);
		lecture.getSelectedTasks().add(task);

		var task2 = factory.createTask();
		task2.setName(CODE_TASK_STAR_DATABASE);
		task.setDescription(loadDescriptionFile("02_star_database"));
		data.getTasks().add(task2);
		task2.setCategory(taskJavaCode);
		task2.setAuthor(lecturer);
		lecture.getSelectedTasks().add(task2);

		var task3 = factory.createTask();
		task3.setName(CODE_TASK_MERGE_LINKED_LIST);
		task3.setDescription(loadDescriptionFile("03_linked_list"));
		data.getTasks().add(task3);
		task3.setCategory(taskJavaCode);
		task3.setAuthor(lecturer);
		lecture.getSelectedTasks().add(task3);

		var task4 = factory.createTask();
		task4.setName(CODE_TASK_MERGE_CIRCULAR_LIST);
		task4.setDescription(loadDescriptionFile("04_circular_list"));
		data.getTasks().add(task4);
		task4.setCategory(taskJavaCode);
		task4.setAuthor(lecturer);
		lecture.getSelectedTasks().add(task4);

		var task5 = factory.createTask();
		task5.setName(InnoplusConstants.CODE_TASK_RUNNING_GAME);
		task5.setDescription(loadDescriptionFile("05_hare_and_hedgehog"));
		data.getTasks().add(task5);
		task5.setCategory(taskJavaCode);
		task5.setAuthor(lecturer);
		lectureOMP2021.getSelectedTasks().add(task5);

		var taskGenericCutlery = factory.createTask();
		taskGenericCutlery.setName(InnoplusConstants.CODE_TASK_GENERIC_CUTLERY);
		taskGenericCutlery.setDescription(loadDescriptionFile("09_generic_cutlery"));
		data.getTasks().add(taskGenericCutlery);
		taskGenericCutlery.setCategory(taskJavaCode);
		taskGenericCutlery.setAuthor(lecturer);
		lectureOMP2021.getSelectedTasks().add(taskGenericCutlery);

		var taskCutlery = factory.createTask();
		taskCutlery.setName(InnoplusConstants.CODE_TASK_CUTLERY);
		taskCutlery.setDescription(loadDescriptionFile("06_cutlery_1"));
		data.getTasks().add(taskCutlery);
		taskCutlery.setCategory(taskJavaCode);
		taskCutlery.setAuthor(lecturer);
		lectureOMP2021.getSelectedTasks().add(taskCutlery);

		var task08 = factory.createTask();
		task08.setName(InnoplusConstants.CODE_TASK_STREAM_TEST);
		task08.setDescription(loadDescriptionFile("08_stream_test"));
		data.getTasks().add(task08);
		task08.setCategory(taskJavaCode);
		task08.setAuthor(lecturer);
		lectureOMP2021.getSelectedTasks().add(task08);

		var task10 = factory.createTask();
		task10.setName(InnoplusConstants.CODE_TASK_IO_TEST);
		task10.setDescription(loadDescriptionFile("10_io_test"));
		data.getTasks().add(task10);
		task10.setCategory(taskJavaCode);
		task10.setAuthor(lecturer);
		lectureOMP2021.getSelectedTasks().add(task10);

		// Teacher Solution JUnit I don't think we need this here
		var task1JunitSolution = generateSampleSolution(task, solutionJava, data);

		var codeCheck = Paths.get(".", "..", "task-repository-static", "01_sorting", "code_checks", "sun_checkstyle.xml")
				.toAbsolutePath()
				.normalize();
		assertTrue(Files.exists(codeCheck));

		task1JunitSolution.setContents(
				Initializer.generateSolutionContent(
						JUNIT_CHECK_PATH_MERGE_ARRAYS,
						Map.of("check", codeCheck.toString())
				)
		);

		var task2JunitSolution = generateSampleSolution(task2, solutionJava, data);
		codeCheck = Paths.get(".", "..", "task-repository-static", "02_star_database", "code_checks", "sun_checkstyle.xml")
				.toAbsolutePath()
				.normalize();

		assertTrue(Files.exists(codeCheck));

		task2JunitSolution.setContents(
				Initializer.generateSolutionContent(
						JUNIT_CHECK_PATH_STAR_DATABASE,
						Map.of("check", codeCheck.toString())
				));

		var task3JunitSolution = generateSampleSolution(task3, solutionJava, data);

		codeCheck = Paths.get(".", "..", "task-repository-static", "03_linked_list", "code_checks", "sun_checkstyle.xml")
				.toAbsolutePath()
				.normalize();

		assertTrue(Files.exists(codeCheck));

		task3JunitSolution.setContents(
				Initializer.generateSolutionContent(
						JUNIT_CHECK_PATH_LINKED_LIST,
						Map.of("check", codeCheck.toString())
				));

		var task4JunitSolution = generateSampleSolution(task4, solutionJava, data);

		codeCheck = Paths.get(".", "..", "task-repository-static", "04_circular_list", "code_checks", "sun_checkstyle.xml")
				.toAbsolutePath()
				.normalize();

		assertTrue(Files.exists(codeCheck));

		task4JunitSolution.setContents(
				Initializer.generateSolutionContent(
						JUNIT_CHECK_PATH_CIRCULAR_LIST,
						Map.of("check", codeCheck.toString())
				));

		var task5JunitSolution = generateSampleSolution(task5, solutionJava, data);

		codeCheck = Paths.get(".", "..", "task-repository-static", "05_hare_and_hedgehog", "code_checks", "custom-checks.xml")
				.toAbsolutePath()
				.normalize();

		assertTrue(Files.exists(codeCheck));

		task5JunitSolution.setContents(
				Initializer.generateSolutionContent(JUNIT_CHECK_PATH_RUNNING_GAME, Map.of("check", codeCheck.toString()))
		);

		var genericCutlerySolution = generateSampleSolution(taskGenericCutlery, solutionJava, data);

		codeCheck = Paths.get(".", "..", "task-repository-static", "09_generic_cutlery", "code_checks", "custom-checks.xml")
				.toAbsolutePath()
				.normalize();

		var staticAnalysis = Paths.get(".", "..", "task-repository-static", "09_generic_cutlery", "static_analysis", "GenericCutleryTest.java");
		assertTrue(Files.exists(codeCheck));
		assertTrue(Files.exists(staticAnalysis));

		genericCutlerySolution.setContents(
				Initializer.generateSolutionContent(JUNIT_CHECK_PATH_GENERIC_CUTLERY, Map.of("check", codeCheck.toString(), "static", staticAnalysis.toString()))
		);


		var cutlerySolution = generateSampleSolution(taskCutlery, solutionJava, data);

		codeCheck = Paths.get(".", "..", "task-repository-static", "06_cutlery_1", "code_checks", "custom-checks.xml")
				.toAbsolutePath()
				.normalize();

		staticAnalysis = Paths.get(".", "..", "task-repository-static", "06_cutlery_1", "static_analysis", "CutleryTest1Test.java");

		assertTrue(Files.exists(codeCheck));
		assertTrue(Files.exists(staticAnalysis));

		cutlerySolution.setContents(
				Initializer.generateSolutionContent(JUNIT_CHECK_PATH_CUTLERY, Map.of("check", codeCheck.toString(), "static", staticAnalysis.toString()))
		);

		var task08JunitSolution = generateSampleSolution(task08, solutionJava, data);
		codeCheck = Paths.get(".", "..", "task-repository-static", "08_stream_test", "code_checks", "custom-checks.xml")
				.toAbsolutePath()
				.normalize();

		assertTrue(Files.exists(codeCheck));

		task08JunitSolution.setContents(
				Initializer.generateSolutionContent(JUNIT_CHECK_PATH_STREAM_TEST, Map.of("check", codeCheck.toString()))
		);

		var task10JunitSolution = generateSampleSolution(task10, solutionJava, data);
		codeCheck = Paths.get(".", "..", "task-repository-static", "10_io_test", "code_checks", "custom-checks.xml")
				.toAbsolutePath()
				.normalize();

		assertTrue(Files.exists(codeCheck));

		task10JunitSolution.setContents(
				Initializer.generateSolutionContent(JUNIT_CHECK_PATH_IO_TEST, Map.of("check", codeCheck.toString()))
		);

		{
			var umlRealEstateTask = factory.createTask();
			umlRealEstateTask.setName("Immobilien");
			umlRealEstateTask.setDescription("Immobilien");
			data.getTasks().add(umlRealEstateTask);
			umlRealEstateTask.setCategory(taskUML);
			umlRealEstateTask.setAuthor(lecturer);
			lectureST1.getSelectedTasks().add(umlRealEstateTask);

			var folder = Paths.get(".", "..", "task-repository-static", "uml", "real_estate");
			var umlRealEstateFiles = new HashMap<String, String>();

			var uml = folder.resolve("Immobilien").resolve("Immobilien.uml");
			var umlRel = folder.relativize(uml).toString();
			var umlContent = Files.readString(uml);
			// logger.info("{} = {}", umlRel, umlContent);
			umlRealEstateFiles.put(umlRel, umlContent);

			assertTrue(Files.exists(folder));

			generateSampleSolution(umlRealEstateTask, solutionUML, data)
					.setContents(Initializer.generateSolutionContent(folder, umlRealEstateFiles));
		}
		return data;
	}

	public QueryManager createDefaultQueryManager() {
		QueryManager manager = new QueryManager();
		var driver = new DockerDriver();
		var jqf = new JUnitQueryFactory(driver);
		var cqf = new CheckStyleQueryFactory(driver);
		var sqf = new StaticQueryFactory(driver);
		var uqf = new UMLQueryFactory();
		manager.register(CODE_TASK_MERGE_ARRAYS, jqf);
		manager.register(CODE_TASK_STAR_DATABASE, jqf);
		manager.register(CODE_TASK_MERGE_LINKED_LIST, jqf);
		manager.register(CODE_TASK_MERGE_CIRCULAR_LIST, jqf);
		manager.register(InnoplusConstants.CODE_TASK_RUNNING_GAME, jqf);
		manager.register(InnoplusConstants.CODE_TASK_IO_TEST, jqf);
		manager.register(InnoplusConstants.CODE_TASK_STREAM_TEST, jqf);
		// Checkstyle Stuff
		manager.register(CODE_TASK_MERGE_ARRAYS, cqf);
		manager.register(CODE_TASK_STAR_DATABASE, cqf);
		manager.register(CODE_TASK_MERGE_LINKED_LIST, cqf);
		manager.register(CODE_TASK_MERGE_CIRCULAR_LIST, cqf);
		manager.register(InnoplusConstants.CODE_TASK_GENERIC_CUTLERY, cqf);
		manager.register(InnoplusConstants.CODE_TASK_RUNNING_GAME, cqf);
		manager.register(InnoplusConstants.CODE_TASK_IO_TEST, cqf);
		manager.register(InnoplusConstants.CODE_TASK_STREAM_TEST, cqf);
		manager.register(InnoplusConstants.CODE_TASK_CUTLERY, cqf);

		// Static Analysis
		manager.register(InnoplusConstants.CODE_TASK_GENERIC_CUTLERY, sqf);
		manager.register(InnoplusConstants.CODE_TASK_CUTLERY, sqf);

		//UML Stuff
		manager.register(UML_TASK_TEST, uqf);
		manager.register("Immobilien", uqf);
		return manager;
	}

	public void initQueries(QueryManager queryManager, InnoplusDatabase data) {
		logger.info("Initializing Queries");

		for(var task : data.getTasks()){
			if (!task.getCategory().getName().equals(InnoplusConstants.SOLUTION_JAVA_CODE)){
				continue;
			}

			var dummySolution = DataFactory.eINSTANCE.createStudentSolution();
			dummySolution.setTask(task);
			logger.info("Initializing Task: ", task.getName());
			queryManager.map(dummySolution);
		}

		logger.info("Finished Query Initialization");
	}

	/** Reads the content of a tasks `README.md` located in the `task-repository-static` **/
	private static String loadDescriptionFile(String task) throws IOException {
		var path = Paths.get("..", "task-repository-static", task, "README.md").toAbsolutePath().normalize();
		logger.debug("Reading task description: {}", path);
		return Files.readString(path);
	}
}
