package de.unioldenburg.se.innoplus.query.utils.uml;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonException;
import javax.json.JsonObject;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;

/** Wrapper handling metadata associated to UML entities via comments **/
public final class CommentMetadata {

	/**
	 * Extracts metadata associated to the given element (if any)
	 *
	 * @param el the element to check
	 * @return the metadata or null if the is no such comment
	 **/
	public static CommentMetadata extractFrom(final Element el) {

		// Fetch associated comments (if any)
		EList<? extends Comment> comms = el.getOwnedComments();
		if (comms == null)
			return null;

		// Check if any comment contains actual metadata
		for (var com : comms) {

			// Empty comment?
			if (!com.isSetBody())
				continue;

			var body = com.getBody();

			// Crude heuristic to detect JSON content
			if (!body.startsWith("{"))
				return null;

			// Return the first actual metadata
			try {
				var reader = new StringReader(body);
				var parser = Json.createReader(reader);
				var obj = parser.readObject();
				return new CommentMetadata(obj, el);

			} catch (JsonException e) {
				// Ignore exceptions if the content isn't actually JSON
				e.printStackTrace();
			}
		}

		return null;
	}

	/** annotated element **/
	private final Element source;

	/** internal metadata representation **/
	private final JsonObject representation;

	/** synonyms attached to the element (lazily initialized) **/
	private Map<String, List<String>> synonyms;

	/** whether an internal dictionary must not be used to extends synonyms **/
	private boolean exclusiveMatch;

	/** Internal constructor **/
	protected CommentMetadata(JsonObject representation, Element source) {
		super();
		this.representation = representation;
		this.source = source;
	}

	/** Retrieves the annotated element **/
	public Element getSource() {
		return source;
	}

	/**
	 * Retrieves a synonym mapping attached to the internal node.
	 *
	 * @return the mapping or an empty map if there is no custom mapping
	 */
	public Map<String, List<String>> getSynonyms() {
		if (this.synonyms != null)
			return this.synonyms;

		var synMap = representation.getJsonObject("synonyms");
		if (synMap == null) {
			this.synonyms = Collections.emptyMap();
			return this.synonyms;
		}

		this.synonyms = new HashMap<String, List<String>>();

		synMap.forEach((lang, s) -> {

			if (!(s instanceof JsonArray))
				return;

			var arr = s.asJsonArray();
			var values = new ArrayList<String>(arr.size());

			for (int i = 0; i < arr.size(); i++) {
				values.add(arr.getString(i));
			}

			this.synonyms.put(lang, values);
		});

		this.exclusiveMatch = synMap.getBoolean("exclusive", false);

		return this.synonyms;
	}

	public boolean isExclusiveMatch() {
		// Lazily initialize the member
		getSynonyms();
		return exclusiveMatch;
	}

	@Override
	public String toString() {
		return "CommentMetadata [representation=" + representation + ", source=" + source + "]";
	}
}
