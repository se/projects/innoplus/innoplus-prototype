package de.unioldenburg.se.innoplus.persistence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import de.unioldenburg.se.innoplus.Initializer;
import de.unioldenburg.se.innoplus.data.*;
import de.unioldenburg.se.innoplus.message.EcoreIO;

public class DataManager {

	protected final Path pathFolder;
	protected final String pathStr;
	protected InnoplusDatabase data;

	public DataManager(String pathFolder, Initializer initializer, boolean reloadExistingData) throws IOException {
		super();

		// ensure directly to store EMF data
		this.pathFolder = Paths.get(Objects.requireNonNull(pathFolder));
		try {
			Files.createDirectories(this.pathFolder);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// register the package internally
		DataPackage.eINSTANCE.getDataFactory();

		var path = this.pathFolder.resolve("data.xmi");
		this.pathStr = path.toString();
		if (reloadExistingData && Files.exists(path)) {
			// load previous data
			data = (InnoplusDatabase) EcoreIO.loadEObject(pathStr);
		} else {
			// create initial data
			data = initializer.createDefaultData();
			saveCurrentData();
		}
	}

	public void saveCurrentData() {
		EcoreIO.saveEObject(data, pathStr);
	}

	public InnoplusDatabase getData() {
		return data;
	}

	public Optional<Person> getPerson(String id) {
		Objects.requireNonNull(id);
		return data.getPersons()
			.stream()
			.filter(p -> id.equals(p.getId()))
			.findAny();
	}

	public Optional<Lecture> getLecture(final String lectureId) {
		return this.data.getLectures()
				.stream()
				.filter(l -> lectureId.equals(l.getId()))
				.findAny();
	}

	public Optional<Task> getTask(String taskName) {
		return this.data.getTasks()
				.stream()
				.filter(t -> taskName.equals(t.getName()))
				.findAny();
	}

	/** Extracts a shallow list of all known lectures **/
	public InnoplusDatabase extractLectureList() {
		var dataCopy = DataFactory.eINSTANCE.createInnoplusDatabase();

		for (var lecture : this.data.getLectures()) {
			var lectureCopy = DataFactory.eINSTANCE.createLecture();
			lectureCopy.setId(lecture.getId());
			lectureCopy.setName(lecture.getName());
			lectureCopy.setDatabase(dataCopy);
		}

		return dataCopy;
	}

	/** Returns the TeacherSolutions for a given Task */
	public static List<TeacherSolution> getTeacherSolutions(Task task){
		return task.getSolutions().stream()
				.filter(solution -> solution instanceof TeacherSolution)
				.map(solution -> (TeacherSolution) solution)
				.filter(tSolution -> tSolution.getTask().getName().equals(task.getName()))
				.collect(Collectors.toList());
	}

	/** Returns the TeacherSolutions for a given Task */
	public static List<StudentSolution> getStudentSolution(Task task){
		return task.getSolutions().stream()
				.filter(solution -> solution instanceof StudentSolution)
				.map(solution -> (StudentSolution) solution)
				.filter(tSolution -> tSolution.getTask().getName().equals(task.getName()))
				.collect(Collectors.toList());
	}

	/** Extracts a copy of all tasks associated to lecture */
	public InnoplusDatabase extractTasksFor(final Person person, final Lecture selectedLecture) {

		// Copy data that is available to the client
		// QUESTION: This could probably be simplified when revising the model
		var dataCopy = DataFactory.eINSTANCE.createInnoplusDatabase();
		var personCopy = (person instanceof Person)
						? DataFactory.eINSTANCE.createStudent()
						: DataFactory.eINSTANCE.createTeacher();

		personCopy.setId(person.getId());
		personCopy.setDatabase(dataCopy);
		dataCopy.getPersons().add(personCopy);

		// Caches to avoid duplicating objects
		// QUESTION: Can a task ever be assigned to multiple lectures?
		var categoryCopies = new HashMap<TaskCategory, TaskCategory>();

		// Copy all lectures
		for (var lecture : person.getLectures()) {
			var lectureCopy = DataFactory.eINSTANCE.createLecture();
			lectureCopy.setId(lecture.getId());
			lectureCopy.setName(lecture.getName());
			lectureCopy.setDatabase(dataCopy);
			dataCopy.getLectures().add(lectureCopy);

			// Other lectures are just shallow copies
			if (!selectedLecture.getId().equals(lecture.getId()))
				continue;

			lectureCopy.getParticipants().add(personCopy);

			// Copy all tasks
			for (var task : lecture.getSelectedTasks()) {
				var taskCopy = DataFactory.eINSTANCE.createTask();
				taskCopy.setName(task.getName());
				taskCopy.setDescription(task.getDescription());
				taskCopy.getUsedInLectures().add(lectureCopy);
				taskCopy.setDatabase(dataCopy);

				// Copy the category
				var categoryCopy_ = categoryCopies.computeIfAbsent(task.getCategory(), category -> {
					var categoryCopy = DataFactory.eINSTANCE.createTaskCategory();
					categoryCopy.setName(category.getName());
					categoryCopy.setDatabase(dataCopy);
					dataCopy.getTaskCategories().add(categoryCopy);
					return categoryCopy;
				});

				categoryCopy_.getTasks().add(taskCopy);
				taskCopy.setCategory(categoryCopy_);
				dataCopy.getTasks().add(taskCopy);

				lectureCopy.getSelectedTasks().add(taskCopy);
			}
		}

		return dataCopy;
	}
}
