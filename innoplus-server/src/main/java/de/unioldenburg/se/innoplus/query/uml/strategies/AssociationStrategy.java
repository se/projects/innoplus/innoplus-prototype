package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;
import org.eclipse.uml2.uml.Association;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AssociationStrategy implements MatchingStrategy<Association> {

    @Override
    public boolean match(Association element, Association other) {
        // gg Java Arrays.asList(boolean[]) does not exist
        var matches = new Boolean[other.getEndTypes().size()];
        Arrays.fill(matches, false);

        for(var tType : element.getEndTypes()){
            for (int i=0; i < other.getEndTypes().size(); i++){
                if(NameMatcher.instance.areEquivalent(tType, other.getEndTypes().get(i))){
                    matches[i] = true;
                }
            }
        }

        return !Arrays.asList(matches).contains(false);
    }

    @Override
    public List<QueryResult> extractResults(Association element, Association other) {

        return Collections.emptyList();
    }
}
