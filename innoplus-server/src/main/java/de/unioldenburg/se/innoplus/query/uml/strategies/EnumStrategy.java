package de.unioldenburg.se.innoplus.query.uml.strategies;

import de.unioldenburg.se.innoplus.Initializer;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.query.uml.MatchingStrategy;
import de.unioldenburg.se.innoplus.query.utils.language.NameMatcher;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class EnumStrategy implements MatchingStrategy<Enumeration> {

	private static final String EMPTY_LOCATION = "::-:";

	@Override
	public boolean match(Enumeration t, Enumeration s) {
		return NameMatcher.instance.areEquivalent(t, s);
	}

	@Override
	public List<QueryResult> extractResults(Enumeration t, Enumeration s) {
		var errors = false;
		var results = new ArrayList<QueryResult>();

		if (!t.getVisibility().equals(s.getVisibility())) {
			var res = Initializer.createQueryResult(
				QueryResultType.WARNING,
				"Missmatched visibility for " + t.getName(),
				"Expected " + t.getVisibility() + " but was " + s.getVisibility(),
				EMPTY_LOCATION
			);
			results.add(res);
		}

		if (t.isAbstract() != s.isAbstract()) {
			var res = Initializer.createQueryResult(
				QueryResultType.WARNING,
				"Missmatched abstract specifier for " + t.getName(),
				"Expected " + t.getVisibility() + " but was " + s.getVisibility(),
				EMPTY_LOCATION
			);
			results.add(res);
		}

		var matched = new HashSet<Element>();

		for (var exp : t.getOwnedLiterals()) {
			var act = s.getOwnedLiterals()
					.stream()
					.filter(a -> NameMatcher.instance.areEquivalent(exp, a))
					.findAny();

			if (act.isEmpty()) {
				var res = Initializer.createQueryResult(
					QueryResultType.ERROR,
					"Missing literal \"" + exp.getName() + "\" for " + t.getName(),
					"Expected literal" + exp + " is not defined",
					EMPTY_LOCATION
				);
				results.add(res);
			}
			else {
				matched.add(act.get());
			}
		}

		if (errors)
			return results;

		// Optional warnings:

		for (var act : s.getOwnedLiterals()) {
			if (matched.contains(act))
				continue;

			var res = Initializer.createQueryResult(
				QueryResultType.INFORMATION,
				"Unknown literal \"" + act.getName() + "\" for " + t.getName(),
				"Literal" + act + " is not required",
				EMPTY_LOCATION
			);
			results.add(res);
		}

		return results;
	}
}
