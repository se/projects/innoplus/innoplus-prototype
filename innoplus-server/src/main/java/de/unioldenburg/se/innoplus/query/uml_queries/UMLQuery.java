package de.unioldenburg.se.innoplus.query.uml_queries;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Solution;
import de.unioldenburg.se.innoplus.message.EcoreIO;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.uml_api.UMLEvaluator;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.util.encoders.UTF8;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static de.unioldenburg.se.innoplus.Initializer.createQueryResult;

public class UMLQuery implements Query {

    private static final Logger logger = LogManager.getLogger(UMLQuery.class);

    private final List<QueryResult> results = new ArrayList<>();
    private final List<UMLEvaluator> umlEvaluators;

    private final org.eclipse.emf.common.util.URI resourceURI;

    public UMLQuery(List<UMLEvaluator> umlEvaluators){
        this.umlEvaluators = umlEvaluators;

        // TODO UML Resource ab fuck
        var umlResource = getClass()
                .getClassLoader()
                .getResource("uml2/org.eclipse.uml2.uml.resources_5.5.0.v20210228-1829.jar")
                .toString();
        resourceURI = org.eclipse.emf.common.util.URI.createURI("jar:" + umlResource + "!/");

        logger.debug("UMLQuery created");
    }

    @Override
    public void evaluate(Solution studentSolution) {
        results.clear();
        var model = ((Map) studentSolution.getContents().getContent()).values().stream().findFirst();
        Model solution;
        try {
            solution = EcoreIO.loadUmlModel(IOUtils.toInputStream(model.get().toString(), "UTF-8"));
            //solution.setURI(resourceURI.toString());
            //System.out.println(solution);
            //solution = EcoreIO.loadUmlModel(org.eclipse.emf.common.util.URI.createFileURI(model.get().toString()), resourceURI);
        } catch (IOException e) {
            results.add(createQueryResult(
                    QueryResultType.ERROR,
                    "Model could not be loaded",
                    "Model could not be loaded",
                    "::-:"));
            logger.warn("A Model could not be loaded!", e);
            return;
        }
        for (final var evaluator : umlEvaluators) {
            results.addAll(evaluator.evaluate(solution));
        }
        if (results.isEmpty()){
            results.add(createQueryResult(QueryResultType.SUCCESS, "No errors found.", "No errors found.", "::-:"));
        }
    }

    @Override
    public List<Query> getSubQueries() {
        return Collections.emptyList();
    }

    @Override
    public List<QueryResult> getResult() {
        return results;
    }

    @Override
    public Object getTraceabilityElement() {
        return null;
    }
}
