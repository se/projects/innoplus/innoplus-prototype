package de.unioldenburg.se.innoplus.util;

import de.unioldenburg.se.innoplus.Initializer;
import de.unioldenburg.se.innoplus.communication.CheckstyleError;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CheckstyleResultParser {
    InputStream result;
    HashMap filenameMap = new HashMap();
    Map<String, QueryResultType> errorMap = Map.of(
            "info", QueryResultType.INFORMATION,
            "warning", QueryResultType.WARNING,
            "error", QueryResultType.ERROR
    );

    public CheckstyleResultParser(Set<String> filenames, InputStream result) {
        this.result = result;
        filenames.forEach(name -> filenameMap.put(Paths.get(name).getFileName().toString(), name));
    }

    public List<QueryResult> extractResults() throws Exception{
        List<QueryResult> results;
        try(var inStream = new ObjectInputStream(result)){
            results = ((List<CheckstyleError>) inStream.readObject()).stream()
                    .map(error -> Initializer.createQueryResult(
                            errorMap.get(error.severity),
                            error.feedback,
                            "",
                            filenameMap.get(error.file) + ":" + error.line + ":" + error.column + "-:"))
                    .collect(Collectors.toList());
        }

        if (results.isEmpty()){
            results.add(Initializer.createQueryResult(
                    QueryResultType.SUCCESS,
                    "No Code Style violations were found",
                    "",
                    "::-:")
            );
        }
        return results;
    }
}
