package de.unioldenburg.se.innoplus.query.checkstyle;

import de.unioldenburg.se.innoplus.DynamicInitializer;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.QueryFactory;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

// TODO Maybe Make A CodeQueryFactory and make QueryFactory an interface
public class CheckStyleQueryFactory extends QueryFactory {

    /** stores references to already build images */
    private final HashMap<Task, String> imageCache = new HashMap<>();

    /** docker driver used for initializing images **/
    private final DockerDriver driver;

    public static final String DOCKER_BUILD_ARG_CHECKSTYLE_CHECK = "CHECKSTYLE_CHECK";
    public static final String DOCKER_BUILD_ARG_CUSTOM_CHECKS = "CUSTOM_CHECKS";

    private static final String dockerfile = "docker/checkstyle/Dockerfile";

    public CheckStyleQueryFactory(DockerDriver driver){
        this.driver = driver;
    }


    @Override
    public List<Query> createQueries(StudentSolution solution) {
        // TODO Man is this ugly
        // create and return queries

        List<Query> queries = new ArrayList<>();
        var task = solution.getTask();


        Set<Path> content;
        try {
            content = Files.walk(Paths.get(".", "..", "innoplus-customchecks", "output"))
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            throw new IllegalStateException("A Required File was not found.", e);
        }
        for (var teacherSolution : DataManager.getTeacherSolutions(task)) {

            // check if there is a code check for the solution
            var checkPath = Paths.get(((Map<String, String>) teacherSolution.getContents().getContent())
                    .get(DynamicInitializer.STYLE_CHECKS_DIR));
            content.add(checkPath);
            if (!Files.exists(checkPath)) {
                logger.debug("There are no registered Checks for Task: {}", task.getName());
                continue;
            }

            var image = imageCache.computeIfAbsent(task,
                    t -> this.driver.getImage(
                            Map.of(
                                    DOCKER_BUILD_ARG_CHECKSTYLE_CHECK, checkPath.toString(),
                                    DOCKER_BUILD_ARG_CUSTOM_CHECKS, "inno-customchecks-1.0.jar"
                            ),
                            content,
                            Set.of(
                                    dockerfile
                            )
                    )
            );
            queries.add(new CheckStyleQuery(image, this.driver));
        }

        return queries;
    }

}
