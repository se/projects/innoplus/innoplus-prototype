package de.unioldenburg.se.innoplus.query.utils.language;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

import org.eclipse.uml2.uml.NamedElement;

import de.unioldenburg.se.innoplus.query.utils.uml.CommentMetadata;

/**
 * Utility class to match model elements using an internal dictionary.
 *
 * @see Dictionary
 **/
public final class NameMatcher {

	public static final NameMatcher instance;

	static {
		var cl = NameMatcher.class.getClassLoader();
		try (var is = cl.getResourceAsStream("openthesaurus.txt")) {
			var reader = new BufferedReader(new InputStreamReader(is));
			var dict = new OpenThesaurusDictionary(reader);
			instance = new NameMatcher(dict);
		} catch (IOException | ParseException e) {
			throw new RuntimeException("Failed to initialize NameMatcher instance");
		}
	}

	/** Dictionary for actual string matching **/
	private final Dictionary dict;

	/** Creates a new matcher that uses the supplied dictionary **/
	public NameMatcher(Dictionary dict) {
		super();
		this.dict = dict;
	}

	/**
	 * Checks if both element names are equivalent under the rules of the internal
	 * dictionary. Respects metadata associated with the supplied elements.
	 *
	 * @param teacherEl the expected element
	 * @param studentEl the actual element
	 * @return whether studentEl matches teacherEl according to the internal
	 *         {@link Dictionary dictionary}.
	 **/
	public boolean areEquivalent(NamedElement teacherEl, NamedElement studentEl) {
		var studentName = studentEl.getName();
		var teacherName = teacherEl.getName();

		// Consider two unnamed elements as equivalent
		if (studentName == null)
			return teacherName == null;

		if (teacherName == null)
			return false;

		// Prefer custom dictionaries supplied via comments
		var dict = findAnnotedDictionary(teacherEl);
		if (dict == null)
			dict = this.dict;

		return dict.areEquivalent(teacherName, studentName);
	}

	/**
	 * Checks if the supplied model element contains custom has an associated
	 * comment defining custom matching rules (e.g. a selection of synonyms).
	 *
	 * @param el the element to check
	 * @return a customized dictionary or null if the element has no associated
	 *         metadata.
	 **/
	private Dictionary findAnnotedDictionary(NamedElement el) {

		// Fetch associated comments (if any)
		var meta = CommentMetadata.extractFrom(el);
		return meta == null
			? null
			: new CommentDictionary(meta, this.dict);
	}

	/** Returns the internal dictionary **/
	public Dictionary getDictionary() {
		return dict;
	}
}
