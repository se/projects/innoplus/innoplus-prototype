package de.unioldenburg.se.innoplus.query.staticanalysis;

import de.unioldenburg.se.innoplus.DynamicInitializer;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.QueryFactory;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class StaticQueryFactory extends QueryFactory {

	/** stores references to already build images */
	private final HashMap<Task, String> imageCache = new HashMap<>();

	/** docker driver used for initializing images **/
	private final DockerDriver driver;

	/** Relative Path to the Classloader Stuff */
	private final Path STATIC_ANALYSIS_PATH = Paths.get(".", "..", "innoplus-static-analysis");

	private final Path JAVA_FILES = Paths.get("src", "main", "java");

	private final Path POM_XML = Paths.get("pom.xml");

	public static final String DOCKER_ARG_STATIC_TESTS = "STATIC_TESTS";
	public static final String DOCKER_ARG_JAVA_FILES = "JAVA_FILES";
	public static final String DOCKER_ARG_POM_XML = "POM_XML";

	private static final String dockerfile = "docker/static/Dockerfile";

	public StaticQueryFactory(DockerDriver driver){
		this.driver = driver;
	}

	@Override
	public List<Query> createQueries(StudentSolution solution) {
		// create and return queries
		List<Query> queries = new ArrayList<>();

		var task = solution.getTask();

		for (var teacherSolution : DataManager.getTeacherSolutions(task)) {
			// check if there is a static analysis for the solution
			var staticAnalysisPath = Paths.get(((Map<String, String>) teacherSolution.getContents().getContent())
					.get(DynamicInitializer.STATIC_CHECKS_DIR));
			if (!Files.exists(staticAnalysisPath)) {
				logger.info("There are no registered Static Analysis for Task: {}", task.getName());
				continue;
			}

			var image = imageCache.computeIfAbsent(
					task,
					t -> this.driver.getImage(
							Map.of(
									DOCKER_ARG_STATIC_TESTS, staticAnalysisPath.toString(),
									DOCKER_ARG_JAVA_FILES, "java",
									DOCKER_ARG_POM_XML, "pom.xml"),
							Set.of(
									staticAnalysisPath,
									STATIC_ANALYSIS_PATH.resolve(JAVA_FILES),
									STATIC_ANALYSIS_PATH.resolve(POM_XML)
							),
							Set.of(
									dockerfile
							)
					)
			);

			queries.add(
					new StaticQuery(
							image,
							this.driver
					)
			);
		}
		return queries;
	}

}
