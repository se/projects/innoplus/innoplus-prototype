package de.unioldenburg.se.innoplus.util;

import java.util.*;
import org.w3c.dom.*;

/**
 * Util Class to transform a {@link NodeList} into a {@link List}
 * From: https://stackoverflow.com/questions/19589231/can-i-iterate-through-a-nodelist-using-for-each-in-java
 */
public final class XmlUtil {

    public static List<Node> asList(NodeList n) {
        return n.getLength() == 0 ?
                Collections.<Node>emptyList() : new NodeListWrapper(n);
    }

    static final class NodeListWrapper extends AbstractList<Node>
            implements RandomAccess {
        private final NodeList list;

        NodeListWrapper(NodeList l) {
            list = l;
        }

        public Node get(int index) {
            return list.item(index);
        }

        public int size() {
            return list.getLength();
        }
    }

    public static String attributeToString(Element element, String attributeName) {
        return element.getAttribute(attributeName);
    }
}

