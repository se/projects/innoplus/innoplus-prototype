package de.unioldenburg.se.innoplus.query.uml;

import de.unioldenburg.se.innoplus.data.QueryResult;
import org.eclipse.uml2.uml.Element;

import java.util.HashMap;
import java.util.List;

public interface MatchingStrategy<T extends Element> {

    boolean match(T element, T other);

    List<QueryResult> extractResults(T element, T other);
}
