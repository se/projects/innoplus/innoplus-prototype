package de.unioldenburg.se.innoplus.util;

import de.unioldenburg.se.innoplus.Initializer;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestResultParser extends ResultParser{

    /** our tests */
    private final List<Node> nodes;

    /** the logger */
    protected final static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
            .getLogger(TestResultParser.class);

    public TestResultParser(InputStream result, List<String> filenames)
            throws ParserConfigurationException, IOException, SAXException
    {
        super(filenames, result);
        var doc = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(result);
        this.nodes = XmlUtil.asList(doc.getElementsByTagName("testcase"));
    }


    @Override
    public List<QueryResult> extractResults() {
        var queryResults = new ArrayList<QueryResult>();
        for (var node : this.nodes){
            var element = (Element) node;
            var summary = element.getElementsByTagName("summary").item(0).getTextContent();
            var description = element.getElementsByTagName("description").item(0).getTextContent();
            var trace = element.getElementsByTagName("trace").item(0).getTextContent();

            queryResults.add(Initializer.createQueryResult(
                    QueryResultType.ERROR,
                    summary,
                    description,
                    trace)
            );
        }

        // No Errors / failures found inside the test report
        if (queryResults.isEmpty())
            queryResults.add(Initializer.createQueryResult(QueryResultType.SUCCESS, "No functional Errors found", "", "::-:"));

        return queryResults;
    }

}
