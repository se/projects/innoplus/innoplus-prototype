package de.unioldenburg.se.innoplus.statistics;

import static java.util.stream.Collectors.groupingBy;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;

/**
 * Analyzes the reviewed solutions for one person, grouped by (selected) tasks, regarding the quality of the solution.
 * The quality of the solution is evaluated in terms of number of errors in the hints, grouped into four groups
 * @author Johannes Meier
 */
public class IndividualUserTasksStatistic implements Statistic {
	private final static int ERROR_VERY_BAD = 10;
	private final static int ERROR_BAD = 5;
	private final static int ERROR_OK = 1;
	private final static int ERROR_FINE = 0;

	/**
	 * The person whose statistics will be analyzed
	 */
	protected final Person person;
	/**
	 * Label for anonymization
	 */
	protected final String replacementLabel;
	/**
	 * The tasks for which submitted solutions are counted
	 */
	protected final List<Task> tasksToAnalyze;

	public IndividualUserTasksStatistic(Person person, String replacementLabel, List<Task> tasksToAnalyze) {
		super();
		this.person = Objects.requireNonNull(person);
		this.replacementLabel = Objects.requireNonNull(replacementLabel);
		this.tasksToAnalyze = Objects.requireNonNull(tasksToAnalyze);
	}

	@Override
	public String getStatisticType() {
		return "csv";
	}

	@Override
	public String getName(InnoplusDatabase data) {
		return "user-" + replacementLabel + "-tasks";
	}

	@Override
	public void calculate(InnoplusDatabase data, OutputStream storage) throws IOException {
		// create the data structure as required by the CSV library
		List<String[]> csv = StatisticsHelper.createCsvTemplate();

		// headline
		csv.add(new String[] {"Task",
				"all (requests)", "all (earliest time)",
				">= " + ERROR_VERY_BAD + " errors (requests)", ">= " + ERROR_VERY_BAD + " errors (earliest time)",
				">= " + ERROR_BAD + " errors (requests)", ">= " + ERROR_BAD + " errors (earliest time)",
				">= " + ERROR_OK + " error (requests)", ">= " + ERROR_OK + " error (earliest time)",
				">= " + ERROR_FINE + " error (requests)", ">= " + ERROR_FINE + " error (earliest time)",
			});

		/*
		 * Content
		 */

		// group the submitted solutions (with date, task and hints) of the person by task
		Map<Task, List<StudentSolution>> solutionsGroupedByTask = person.getSubmittedSolutions().stream()
				.filter(s -> s.getQueryResults().isEmpty() == false && s.getTask() != null && s.getDate() != null)
				.collect(groupingBy(s -> s.getTask()));

		for (int i = 0; i < tasksToAnalyze.size(); i++) {
			Task task = tasksToAnalyze.get(i); // keep the given order of tasks

			// determine matching solutions for this task and this person
			List<StudentSolution> s = solutionsGroupedByTask.get(task);
			if (s == null) {
				s = Collections.emptyList();
			}

			/* group submitted solutions
			 * - all Dates of already created QueryResults are earlier than "System.currentTimeMillis() + 1000 * 60", i.e. smaller!
			 */
			int verybadNumber = 0;
			long verybadTime = System.currentTimeMillis() + 1000 * 60;
			int badNumber = 0;
			long badTime = System.currentTimeMillis() + 1000 * 60;
			int okNumber = 0;
			long okTime = System.currentTimeMillis() + 1000 * 60;
			int fineNumber = 0;
			long fineTime = System.currentTimeMillis() + 1000 * 60;
			int allNumber = 0;
			long allTime = System.currentTimeMillis() + 1000 * 60;

			for (StudentSolution sol : s) {
				int[] types = StatisticsHelper.countTypes(sol.getQueryResults());

				int errors = types[1];
				long currentTime = sol.getDate().getTime();
				if (errors >= ERROR_VERY_BAD) {
					verybadNumber++;
					verybadTime = Math.min(verybadTime, currentTime);
				} else if (errors >= ERROR_BAD) {
					badNumber++;
					badTime = Math.min(badTime, currentTime);
				} else if (errors >= ERROR_OK) {
					okNumber++;
					okTime = Math.min(okTime, currentTime);
				} else if (errors >= ERROR_FINE) {
					fineNumber++;
					fineTime = Math.min(fineTime, currentTime);
				} else {
					throw new IllegalStateException();
				}

				allNumber++;
				allTime = Math.min(allTime, currentTime);
			}

			// print the result
			csv.add(new String[] {task.getName(),
					allNumber + "", format(allNumber, allTime),
					verybadNumber + "", format(verybadNumber, verybadTime),
					badNumber + "", format(badNumber, badTime),
					okNumber + "", format(okNumber, okTime),
					fineNumber + "", format(fineNumber, fineTime)
				});
		}

		// store the data as CSV
		StatisticsHelper.saveAsCsv(csv, storage);
	}

	private String format(int number, long time) {
		if (number <= 0) {
			return "";
		} else {
			Date date=new Date(time);
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        return format.format(date);
		}
	}
}
