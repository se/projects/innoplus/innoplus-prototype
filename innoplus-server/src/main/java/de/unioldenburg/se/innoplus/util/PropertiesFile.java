package de.unioldenburg.se.innoplus.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;

public class PropertiesFile {

    private static final Logger logger = LogManager.getLogger(PropertiesFile.class);
    public static final String TIMESTAMP_COMMENT_REGEX =
            "#\\w{3}\\s\\w{3}\\s\\d{2}\\s\\d{2}\\:\\d{2}\\:\\d{2}\\s\\w{3,4}\\s\\d{4}[\\n]";

    protected final Properties properties = new Properties();
    protected final Path propertiesFilePath;
    protected final Map<Class, Function<String, Object>> converters = new HashMap<>();
    protected boolean storeModificationTimestamp = false;

    public PropertiesFile(Path propertiesFilePath, boolean storeModificationTimestamp) {
        this.propertiesFilePath = propertiesFilePath;
        this.storeModificationTimestamp = storeModificationTimestamp;
        createDefaultConverters();
    }

    public PropertiesFile(Path propertiesFilePath) {
        this(propertiesFilePath, false);
    }

    public boolean load() {
        return load(propertiesFilePath);
    }

    public boolean load(Path propertiesFilePath) {
        final var propFile = propertiesFilePath.toFile();
        try {
            properties.load(new FileReader(propFile));
        } catch (IOException e) {
            logger.warn("Could not load PropertiesFile {}!", propFile, e);
            return false;
        }
        return true;
    }

    public boolean store() {
        return store(propertiesFilePath);
    }

    public boolean store(String comments) {
        return store(propertiesFilePath, comments);
    }

    public boolean store(Path propertiesFilePath) {
        return store(propertiesFilePath, null);
    }

    public boolean store(Path propertiesFilePath, String comments) {
        final var propFile = propertiesFilePath.toFile();
        try {
            final var buffer = new StringWriter();
            properties.store(buffer, comments);
            // remove the timestamp comment from properties file if desired
            var propertiesString = buffer.toString();
            if (!storeModificationTimestamp) {
                propertiesString = propertiesString.replaceAll(TIMESTAMP_COMMENT_REGEX, "");
            }
            Files.writeString(propertiesFilePath, propertiesString);
        } catch (IOException e) {
            logger.warn("Could not store PropertiesFile {}", propFile, e);
            return false;
        }
        return true;
    }

    public Optional<String> getValue(String key) {
        return getValue(key, String.class);
    }

    public <T> Optional<T> getValue(String key, Class<T> type) {
        final var value = properties.getProperty(key);
        if (value == null) {
            return Optional.empty();
        }
        return Optional.of(convert(value, type));
    }

    public <T> T getValue(String key, T defaultValue) {
        final var value = getValue(key, defaultValue.getClass());
        if (value.isEmpty()) {
            return defaultValue;
        }
        return (T) value.get();
    }

    public void setValue(String key, Object value) {
        properties.put(key, value.toString());
    }

    public void createConverter(Class toType, Function<String, Object> converter) {
        converters.put(toType, converter);
    }

    public void deleteConverter(Class toType) {
        converters.remove(toType);
    }

    public boolean isStoreModificationTimestamp() {
        return storeModificationTimestamp;
    }

    public void setStoreModificationTimestamp(boolean storeModificationTimestamp) {
        this.storeModificationTimestamp = storeModificationTimestamp;
    }

    protected <T> T convert(String value, Class<T> toType) {
        if (converters.containsKey(toType)) {
            return (T) converters.get(toType).apply(value);
        }
        throw new IllegalStateException("A converter from String to " + toType.getCanonicalName() + " is not defined!");
    }

    protected void createDefaultConverters() {
        createConverter(null, v -> { throw new IllegalArgumentException("Null is not a valid type!"); });
        createConverter(Boolean.class, Boolean::valueOf);
        createConverter(Byte.class, Byte::valueOf);
        createConverter(Short.class, Short::valueOf);
        createConverter(Integer.class, Integer::valueOf);
        createConverter(Long.class, Long::valueOf);
        createConverter(Float.class, Float::valueOf);
        createConverter(Double.class, Double::valueOf);
        createConverter(Character.class, v -> v.charAt(0));
        createConverter(String.class, v -> v);
        // source: https://stackoverflow.com/a/18987428 (15.09.21)
        createConverter(UUID.class, v -> UUID.fromString(v.replaceAll(
                "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})",
                "$1-$2-$3-$4-$5")));
    }
}
