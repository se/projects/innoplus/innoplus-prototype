package de.unioldenburg.se.innoplus.query.docker;

import java.util.Map;

/** POD storing data associated with a single container **/
public class BuildContext {

    /** Path to the Student Solution*/
    public final Map<String, String> studentSolution;

    /** Result File Path */
    public final String resultFile;

    /** Maven Command to execute */
    public final String[] mvnCommand;

    public final String remotePath;

    public BuildContext(Map<String, String> studentSolution, String resultFile, String remotePath,String... mvnCommand)
    {
        this.studentSolution = studentSolution;
        this.resultFile = resultFile;
        this.mvnCommand = mvnCommand;
        this.remotePath = remotePath;
    }

    @Override
    public String toString(){
        return "StudentSolution : " + studentSolution + "\n Maven Command : " + mvnCommand;
    }
}
