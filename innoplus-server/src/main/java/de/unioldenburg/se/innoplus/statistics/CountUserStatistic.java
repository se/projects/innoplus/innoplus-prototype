package de.unioldenburg.se.innoplus.statistics;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Student;
import de.unioldenburg.se.innoplus.data.Teacher;

public class CountUserStatistic implements Statistic {

	@Override
	public String getStatisticType() {
		return "csv";
	}

	@Override
	public String getName(InnoplusDatabase data) {
		return "users";
	}

	@Override
	public void calculate(InnoplusDatabase data, OutputStream storage) throws IOException {
		// create the data structure as required by the CSV library
		List<String[]> csv = StatisticsHelper.createCsvTemplate();

		// headline
		csv.add(new String[] {"User", "Number", "Requests", "Sessions",
				"% Success", "% Error",
				"% Warning", "% Information"});

		// all user
		handlePersons(data.getPersons(), csv, "all user");

		// students
		handlePersons(data.getPersons().stream().filter(p -> p instanceof Student).collect(toList()), csv, "students");

		// lecturer
		handlePersons(data.getPersons().stream().filter(p -> p instanceof Teacher).collect(toList()), csv, "lecturer");

		// store the data as CSV
		StatisticsHelper.saveAsCsv(csv, storage);
	}

	private void handlePersons(List<Person> stream, List<String[]> csv, String name) {
		long count = stream.size();
		int requests = 0;
		int sessions = 0;

		long hintSuccess = 0;
		long hintError = 0;
		long hintWarning = 0;
		long hintInformation = 0;

		for (Person p : stream) {
			requests += p.getSubmittedSolutions().size();
			sessions += StatisticsHelper.groupRequestsBySessions(p.getSubmittedSolutions()).size();

			hintSuccess += p.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.SUCCESS).count();
			hintError += p.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.ERROR).count();
			hintWarning += p.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.WARNING).count();
			hintInformation += p.getSubmittedSolutions().stream().flatMap(s -> s.getQueryResults().stream()).filter(r -> r.getType() == QueryResultType.INFORMATION).count();
		}

		long all = hintSuccess + hintError + hintWarning + hintInformation;

		csv.add(new String[] {name, count + "", requests + "", sessions + "",
				StatisticsHelper.percentage(all, hintSuccess), StatisticsHelper.percentage(all, hintError),
				StatisticsHelper.percentage(all, hintWarning), StatisticsHelper.percentage(all, hintInformation)});
	}

}
