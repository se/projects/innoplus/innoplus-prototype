package de.unioldenburg.se.innoplus.query;

import com.github.dockerjava.api.model.Frame;
import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.query.docker.BuildContext;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.query.docker.Utils;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

/**
 * Code Queries share the same maven Project Structure so a same Parent class is kind of useful
 * I am using it to initialize the maven project.
 * This will probably change a little bit with docker
 */
public abstract class CodeQuery{

    /** the logger */
    private final static org.apache.logging.log4j.Logger logger = org.apache.logging.log4j.LogManager
            .getLogger(DockerDriver.class);

    /** Id of the build Docker Image */
    protected final String image;

    // TODO maybe only reference the client
    /** reference to the docker driver to enable execution */
    protected final DockerDriver driver;

    /** Absolute path of the project inside the docker container */
    protected final String workingDir;


    public CodeQuery(String image, DockerDriver driver){
        // Determine metadata for the image
        this.driver = driver;
        this.image = image;

        var imageMeta = this.driver.getClient().inspectImageCmd(image).exec();
        this.workingDir = Objects.requireNonNull(imageMeta.getConfig()).getWorkingDir();

        logger.debug("Using Image = {}", imageMeta);
    }

    protected int execute(final BuildContext context, DockerDriver.BuildResultConsumer archiveConsumer)
            throws IOException
    {
        final var id = driver.createContainer(image, context);
        logger.debug("Using container: {}", id);
        logger.debug("context : {}", context);

        var client = driver.getClient();

        try {
            // Start the build process
            client.startContainerCmd(id)
                    .exec();
            logger.debug("Started build process!");

            // Dump the entire container output when running in debug mode
            final var logDockerOutput = logger.isDebugEnabled();
            if (logDockerOutput)
                fetchLog(id, logger::debug);

            // Wait until the container exits and check the exit code
            var buildRes = client.waitContainerCmd(id).start().awaitStatusCode();

            // Detect & log build errors
            if (buildRes != 0) {
                logger.warn("Failed to build image! (exit code {})", buildRes);

                // Dump the log if an error occurred and the output wasn't logged above
                if (!logDockerOutput && logger.isWarnEnabled())
                    fetchLog(id, logger::warn);
            }

            logger.debug("Build finished, fetching image...");

            // Extract the result from the container
            logger.debug("extracting resultFile : {}", context.resultFile);
            try (var archive = new TarArchiveInputStream(client.copyArchiveFromContainerCmd(id, context.resultFile).exec())) {
                // get the archive entry
                var entry = archive.getNextTarEntry();
                logger.debug("Found {}", entry.getName());

                if (entry.isDirectory())
                    throw new IllegalStateException("Invalid entry: " + entry);

                // allocate byte buffer with size of entry
                byte[] buf = new byte[(int) entry.getSize()];
                // read archive into the buffer
                IOUtils.readFully(archive, buf);

                archiveConsumer.accept(new ByteArrayInputStream(buf));

                // Sanity check
                if (archive.available() != 0) {
                    entry = archive.getNextTarEntry();
                    if (entry != null) {
                        throw new IllegalStateException("Another tar entry found: " + entry.getName());
                    }
                }
            }
            return buildRes;
        } finally {
            client.removeContainerCmd(id).exec();
            logger.debug("Removed temporary container: {}!", id);
        }

    }

    /**
     * Fetches the log from the container and transfers it into the supplied reader.
     *
     * @implSpec this method blocks until the referenced container terminates!
     *
     * @param id     the container id
     * @param reader the log handler
     * @throws IOException if an error occurs
     */
    private final void fetchLog(final String id, final java.util.function.Consumer<Frame> reader) throws IOException {
        this.driver.getClient().logContainerCmd(id).withStdOut(true).withStdErr(true).withFollowStream(true)
                .exec(new Utils.EmptyResultCallback<Frame>() {
                    @Override
                    public void onNext(Frame frame) {
                        reader.accept(frame);
                    }
                }).close();
    }
}
