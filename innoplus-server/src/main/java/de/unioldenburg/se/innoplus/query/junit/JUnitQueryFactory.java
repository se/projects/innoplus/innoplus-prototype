package de.unioldenburg.se.innoplus.query.junit;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import de.unioldenburg.se.innoplus.DynamicInitializer;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.persistence.DataManager;

import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.QueryFactory;
import de.unioldenburg.se.innoplus.query.checkstyle.CheckStyleQuery;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import org.apache.commons.io.IOUtils;

public class JUnitQueryFactory extends QueryFactory {

	/** stores references to already build images */
	private final HashMap<Task, String> imageCache = new HashMap<>();

	/** docker driver used for initializing images **/
	private final DockerDriver driver;

	/** Relative Path to the Classloader Stuff */
	private final Path JUNIT_CLASSLOADER = Paths.get(".", "..", "JUnit-ClassLoader");

	private final Path JAVA_FILES = Paths.get("src", "main", "java");

	private final Path POM_XML = Paths.get("pom.xml");

	public static final String DOCKER_ARG_TEACHER_SOLUTION = "TEACHER_SOLUTION";
	public static final String DOCKER_ARG_JAVA_FILES = "JAVA_FILES";
	public static final String DOCKER_ARG_POM_XML = "POM_XML";

	private static final String dockerfile = "docker/junit/Dockerfile";

	public JUnitQueryFactory(DockerDriver driver){
		this.driver = driver;
	}

	@Override
	public List<Query> createQueries(StudentSolution solution) {
		// create and return queries
		List<Query> queries = new ArrayList<>();

		var task = solution.getTask();



		for (var teacherSolution : DataManager.getTeacherSolutions(task)) {
			var solutionPath = Paths.get(((Map<String, String>) teacherSolution.getContents().getContent())
					.get(DynamicInitializer.RUNTIME_CHECKS_DIR));
			if (!Files.exists(solutionPath)) {
				logger.info("There are no registered Runtime Check for Task: {}", task.getName());
				continue;
			}
			var tSolution = Paths.get(teacherSolution.getContents().getPath()).toAbsolutePath();
			var image = imageCache.computeIfAbsent(
					task,
					t -> this.driver.getImage(
							Map.of(
									DOCKER_ARG_TEACHER_SOLUTION, solutionPath.toString(),
									DOCKER_ARG_JAVA_FILES, "java",
									DOCKER_ARG_POM_XML, "pom.xml"),
							Set.of(
									tSolution,
									JUNIT_CLASSLOADER.resolve(JAVA_FILES),
									JUNIT_CLASSLOADER.resolve(POM_XML)
							),
							Set.of(
									dockerfile
							)
					)
			);

			queries.add(
					new JUnitQuery(
							image,
							this.driver
					)
			);
		}
		return queries;
	}

}
