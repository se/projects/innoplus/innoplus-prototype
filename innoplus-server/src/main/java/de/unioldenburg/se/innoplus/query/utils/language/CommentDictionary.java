package de.unioldenburg.se.innoplus.query.utils.language;

import de.unioldenburg.se.innoplus.query.utils.uml.CommentMetadata;

/**
 * Utility class that implements custom dictionaries supplied via comments
 * attached to a model elements.
 **/
public final class CommentDictionary implements Dictionary {

	/** Metadata affecting this dictionary **/
	private final CommentMetadata metadata;

	/**
	 * Parent dictionary for string lookup (used if the comment is intended as an
	 * extension of the default dictionary), may be null.
	 **/
	private final Dictionary parent;

	public CommentDictionary(CommentMetadata metadata, Dictionary parent) {
		this.metadata = metadata;
		this.parent = parent;
	}

	@Override
	public boolean areEquivalent(String nameStudent, String nameTeacher) {

		// Check the list of synonyms
		var synObj = metadata.getSynonyms();
		for (var synByLang : synObj.entrySet()) {

			for (var cur : synByLang.getValue()) {
				if (cur.equals(nameStudent))
					return true;
			}
		}

		// Delegate to parent dictionaries (if possible and allowed)
		if (parent != null && !metadata.isExclusiveMatch())
			return this.parent.areEquivalent(nameStudent, nameTeacher);

		return false;
	}

}
