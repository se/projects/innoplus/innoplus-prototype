package de.unioldenburg.se.innoplus.statistics;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.opencsv.CSVWriter;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.StudentSolution;

public class StatisticsHelper {
	public static final long TIME_BETWEEN_REQUESTS_NEW_SESSION = 1000 * 60 * 60 * 6;
	public static final boolean PNG_INSTEADOF_SVG = false;

	public static List<String[]> createCsvTemplate() {
		return new ArrayList<>();
	}

	public static void saveAsCsv(List<String[]> data, OutputStream storage) {
		OutputStreamWriter osw = new OutputStreamWriter(storage, StandardCharsets.UTF_8);
		// write the stuff into the *.csv file: http://opencsv.sourceforge.net/
		try (CSVWriter writer = new CSVWriter(osw, ';', '\"')) {
			// https://stackoverflow.com/questions/13969254/unwanted-double-quotes-in-generated-csv-file
			writer.writeAll(data, false);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static List<List<StudentSolution>> groupRequestsBySessions(Collection<StudentSolution> requests) {
		// ignore requests without date!
		List<StudentSolution> all = requests.stream().filter(r -> r.getDate() != null).collect(toList());

		// sort regarding their time: older requests are at the begin of the list
		all.sort((o1, o2) -> Long.compare(o1.getDate().getTime(), o2.getDate().getTime()));

		// group
		List<List<StudentSolution>> result = new ArrayList<>();
		if (all.isEmpty()) {
			return result;
		}

		List<StudentSolution> currentSession = null;
		for (int i = 0; i < all.size(); i++) {
			StudentSolution solution = all.get(i);
			if (currentSession == null) {
				// first session / group
				currentSession = new ArrayList<>();
				result.add(currentSession);
			} else {
				long currentTime = solution.getDate().getTime();
				long previousTime = currentSession.get(currentSession.size() - 1).getDate().getTime();
				if (currentTime - TIME_BETWEEN_REQUESTS_NEW_SESSION > previousTime) {
					// new session
					currentSession = new ArrayList<>();
					result.add(currentSession);
				} else {
					// current solution belongs to the current session
				}
			}
			currentSession.add(solution);
		}
		return result;
	}

	public static boolean isFine(StudentSolution solution) {
		if (solution == null || solution.getQueryResults().isEmpty()) {
			return false;
		}
		int[] types = countTypes(solution.getQueryResults());
		// 0 Error, >= 1 Success, * Warning, * Information
		return types[1] <= 0 && types[4] >= 1;
	}

	public static int[] countTypes(Collection<QueryResult> results) {
		int[] result = new int[5];
		int error = 0;
		int warning = 0;
		int information = 0;
		int success = 0;
		if (results != null) {
			for (QueryResult r : results) {
				switch (r.getType()) {
				case ERROR:
					error++;
					break;
				case WARNING:
					warning++;
					break;
				case INFORMATION:
					information++;
					break;
				case SUCCESS:
					success++;
					break;
				default:
					throw new IllegalStateException("unknown type " + r.getType());
				}
			}
		}
		result[0] = error + warning + information + success;
		result[1] = error;
		result[2] = warning;
		result[3] = information;
		result[4] = success;
		return result;
	}

	public static String percentage(long dividend, long divisor) {
		if (dividend == 0) {
			return "0";
		}
		if (divisor == 0) {
			return "0.0";
		}
		return ((double) divisor / dividend) + "";
	}
}
