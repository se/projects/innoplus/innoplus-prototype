package de.unioldenburg.se.innoplus.statistics;

import java.io.IOException;
import java.io.OutputStream;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;

public interface Statistic {
	public String getStatisticType();
	public String getName(InnoplusDatabase data);
	public void calculate(InnoplusDatabase data, OutputStream storage) throws IOException;
}
