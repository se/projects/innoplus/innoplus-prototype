package de.unioldenburg.se.innoplus.query.utils.language;

/**
 * Matcher for names used by
 * {@link de.unioldenburg.se.innoplus.data.StudentSolution students} and
 * {@link de.unioldenburg.se.innoplus.data.TeacherSolution teachers}
 **/
public interface Dictionary {

	/** Creates a new instance of the default matcher **/
	public static Dictionary getInstance() {
		return String::equals;
	}

	/**
	 * Checks if both names are equivalent under the rules of this dictionary.
	 *
	 * Implementations may check for synonyms, ignore minor typos, ...
	 **/
	public boolean areEquivalent(String nameStudent, String nameTeacher);
}
