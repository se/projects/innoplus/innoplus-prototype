package de.unioldenburg.se.innoplus.query.uml_queries;

import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.QueryFactory;
import de.unioldenburg.se.innoplus.uml_api.UMLEvaluator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;
import org.pf4j.PluginRuntimeException;
import org.pf4j.PluginState;

import java.nio.file.Paths;
import java.sql.Array;
import java.util.*;

public class UMLQueryFactory extends QueryFactory {

    private static final Logger logger = LogManager.getLogger(UMLQueryFactory.class);

    private final PluginManager pluginManager = new DefaultPluginManager();
    private final Map<Task, List<UMLEvaluator>> umlEvaluators = new HashMap<>();

    @Override
    public List<Query> createQueries(StudentSolution solution) {
        final var task = solution.getTask();
        return List.of(new UMLQuery(umlEvaluators.computeIfAbsent(task, t -> {
            final var evaluators = new ArrayList<UMLEvaluator>();
            for (final var teacherSolution : DataManager.getTeacherSolutions(t)) {
                final var pluginPath = Paths.get(teacherSolution.getContents().getPath());
                logger.debug("Task '{}' ({}) has plugin-path: {}", t.getName(), t.getId(), pluginPath);
                try {
                    final var pluginId = pluginManager.loadPlugin(pluginPath);
                    logger.debug("Starting plugin: {}", pluginId);
                    final var pluginState = pluginManager.startPlugin(pluginId);
                    if (pluginState != PluginState.STARTED) {
                        logger.warn("Plugin '{}' could not be started! State is {}", pluginId, pluginState);
                    }
                    final  var ev = pluginManager.getExtensions(UMLEvaluator.class, pluginId);
                    logger.debug("Loaded {} evaluators for plugin '{}'.", ev.size(), pluginId);
                    evaluators.addAll(ev);
                } catch (PluginRuntimeException e) {
                    logger.warn("Plugin from path '{}' could not be loaded!", pluginPath, e);
                }
            }
            return evaluators;
        })));
    }
}
