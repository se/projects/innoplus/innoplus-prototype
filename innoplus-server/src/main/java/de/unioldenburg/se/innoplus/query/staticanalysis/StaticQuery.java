package de.unioldenburg.se.innoplus.query.staticanalysis;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Solution;
import de.unioldenburg.se.innoplus.query.CodeQuery;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.docker.BuildContext;
import de.unioldenburg.se.innoplus.query.docker.DockerDriver;
import de.unioldenburg.se.innoplus.util.TestResultParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static de.unioldenburg.se.innoplus.Initializer.createQueryResult;

public class StaticQuery extends CodeQuery implements Query {

	/** stores the QueryResults that this Query produces */
	private List<QueryResult> queryResults;

	/** */
	private static final String filename = "report.xml";

	public StaticQuery(String image, DockerDriver driver) {
		super(image, driver);
		logger.debug("Created Static Analysis Query");
	}

	/**
	 * Evaluates a given Student Solution. And writes the result into queryResults
	 * @param studentSolution the current solution to check
	 */
	@Override
	public void evaluate(Solution studentSolution) {
		queryResults = new ArrayList<>();
		var content = (Map) studentSolution.getContents().getContent();

		if(studentSolution.getContents().getContent() == null){
		    queryResults.add(createQueryResult(
		    		QueryResultType.ERROR,
					"No Solution provided",
					"No Solution provided",
					"::-:")
			);
		    return;
		}
		// could also be a list
		AtomicReference<InputStream> archiveConsumer = new AtomicReference<>();
		try {
			execute(
					new BuildContext(
							content,
							workingDir + "/junit-reports/" + filename,
							"work/inno-plus/input/",
							"mvn", "verify"
					),
					archiveConsumer::set
			);
		} catch (Exception e) {
			e.printStackTrace();
			queryResults.add(createQueryResult(
					QueryResultType.ERROR,
					"Compilation Error",
					"Compiling the code failed",
					"::-:")
			);
			return;
		}
		var files = new ArrayList<String>(content.keySet());
		// Error here is expected, if not present there was a compilation error
		try (var parser = new TestResultParser(archiveConsumer.get(), files)){
					this.queryResults.addAll(parser.extractResults());
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// Something failed while trying to extract the Result -> files were not generated so a compilation error
			logger.debug(e);
			logger.error("Failed to compile StudentCode");
			queryResults.add(createQueryResult(
					QueryResultType.ERROR,
					"Compilation Error",
					"Compilation Error",
					": :-: ")
			);
		}
	}

	@Override
	public List<Query> getSubQueries() {
		/* no sub queries for JUnit
		 * - or one Sub-Query for each test method??
		 */
		return Collections.emptyList();
	}

	@Override
	public List<QueryResult> getResult() {
		logger.debug("Getting results, result size: {}", queryResults.size());
	    if (queryResults == null)
	    	throw new IllegalStateException("Cannot fetch the Result before executing the Query");
		return queryResults;
	}

	@Override
	public Object getTraceabilityElement() {
		// TODO Auto-generated method stub
		return null;
	}
}
