package de.unioldenburg.se.innoplus.query;

import de.unioldenburg.se.innoplus.data.StudentSolution;

import java.util.*;
import java.util.stream.Collectors;

public class QueryManager {

    /** Map to store queries for a Topic. There can be multiple QueryTypes for a given Topic, eg. CheckStyle and JUnit
	 * therefore we store a List of QueryFactories.
	 * */
	protected final Map<String, List<QueryFactory>> evaluators = new HashMap<>();

	public QueryManager() {
		super();
	}

	public void register(String solutionCategory, QueryFactory queryFactory) {
		if (solutionCategory == null || queryFactory == null) {
			throw new IllegalArgumentException();
		}
		var factories = evaluators.computeIfAbsent(solutionCategory, sc -> new ArrayList<>());
		factories.add(queryFactory);
	}

	public List<Query> map(StudentSolution studentSolution) {
		String kind = studentSolution.getTask().getName();

		// use the factories ...
		var factories = evaluators.get(kind);
		if (factories == null) {
			throw new IllegalStateException("missing query factory '" + kind + "' for " + studentSolution);
		}

		// create new queries
		List<Query> queries = factories.stream()
				.map(factory -> factory.createQueries(studentSolution))
				.flatMap(Collection::stream)
				.collect(Collectors.toList());

		if (queries.isEmpty()) {
			throw new IllegalStateException("missing query");
		}

		return queries;
	}
}
