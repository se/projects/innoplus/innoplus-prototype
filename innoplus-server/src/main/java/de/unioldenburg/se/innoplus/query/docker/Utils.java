package de.unioldenburg.se.innoplus.query.docker;

/** Common utilities for the docker api **/
public class Utils {

	/** ResultCallback which implements all required methods as NOOPs **/
	public static interface EmptyResultCallback<T> extends com.github.dockerjava.api.async.ResultCallback<T> {

		@Override
		public default void close() throws java.io.IOException {
		}

		@Override
		public default void onStart(java.io.Closeable closeable) {
		}

		@Override
		public default void onNext(T object) {
		}

		@Override
		public default void onError(Throwable throwable) {
		}

		@Override
		public default void onComplete() {
		}
	}
}
