package de.unioldenburg.se.innoplus.statistics;

import static java.util.stream.Collectors.toList;

import java.awt.BasicStroke;
import java.awt.Rectangle;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.svg.SVGGraphics2D;

import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.data.Task;

/**
 * Renders the sessions and requests for one given task in form of a timeline
 * @author Johannes Meier
 */
public class SessionsOverTimeStatistic implements Statistic {
	protected final Task task;

	public SessionsOverTimeStatistic(Task task) {
		super();
		this.task = Objects.requireNonNull(task);
	}

	@Override
	public String getStatisticType() {
		if (StatisticsHelper.PNG_INSTEADOF_SVG) {
			return "png";
		} else {
			return "svg";
		}
	}

	@Override
	public String getName(InnoplusDatabase data) {
		return "task-" +
				task.getName().replaceAll(" ", "").replaceAll("\\.", "").toLowerCase() +
				"-over-time";
	}

	@Override
	public void calculate(InnoplusDatabase data, OutputStream storage) throws IOException {
		/*
		 * https://jfree.github.io/jfreechart-and-opencsv/
		 */
		TimeSeriesCollection dataset = new TimeSeriesCollection();

		// sessions
		List<StudentSolution> solutions = task.getSolutions().stream()
				.filter(s -> s instanceof StudentSolution)
				.map(s -> (StudentSolution) s)
				.collect(toList());

		// count the Sessions
		TimeSeries seriesSessions = new TimeSeries("started Sessions");
		dataset.addSeries(seriesSessions);
		List<List<StudentSolution>> groupedBySession = StatisticsHelper.groupRequestsBySessions(solutions);
		Map<Day, Integer> daysSessions = new HashMap<>();
		groupedBySession.forEach(se -> {
			Day d = new Day(se.get(0).getDate());
			if (daysSessions.containsKey(d)) {
				daysSessions.put(d, daysSessions.get(d) + 1);
			} else {
				daysSessions.put(d, 1);
			}
		});
		daysSessions.entrySet().forEach(d -> seriesSessions.addOrUpdate(d.getKey(), d.getValue()));

		// count the Requests
		TimeSeries seriesRequests = new TimeSeries("Requests");
		dataset.addSeries(seriesRequests);
		Map<Day, Integer> daysRequests = new HashMap<>();
		solutions.forEach(s -> {
			Day d = new Day(s.getDate());
			if (daysRequests.containsKey(d)) {
				daysRequests.put(d, daysRequests.get(d) + 1);
			} else {
				daysRequests.put(d, 1);
			}
		});
		daysRequests.entrySet().forEach(d -> seriesRequests.addOrUpdate(d.getKey(), d.getValue()));


		// configure JFreeChart for displaying the data
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
            "Sessions for the Task '" + task.getName() + "' over time", "Date", "Number of ...", dataset);
//        chart.addSubtitle(new TextTitle("Source: https://ourworldindata.org/excess-mortality-covid"));
        XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setAutoPopulateSeriesStroke(false);
        renderer.setDrawSeriesLineAsPath(false);
        // show small shapes: https://www.jfree.org/forum/viewtopic.php?t=13651
        renderer.setDefaultCreateEntities(true);
        renderer.setDefaultShapesVisible(true);
        renderer.setDefaultShapesFilled(true);
        renderer.setDefaultEntityRadius(renderer.getDefaultEntityRadius() + 2);
        renderer.setDefaultStroke(new BasicStroke(3.0f));
        renderer.setDefaultItemLabelGenerator(new StandardXYItemLabelGenerator());
        renderer.setDefaultItemLabelsVisible(true);

        // styles
        StandardChartTheme.createJFreeTheme().apply(chart);
        /* for drawing the item labels, use the same color as for the line/shapes
         * - TODO does not work, reason is unknown
         * - https://www.jfree.org/forum/viewtopic.php?t=13845
         */
        for (int i = 0; i < dataset.getSeriesCount(); i++) {
        	renderer.setSeriesItemLabelPaint(i, renderer.getSeriesPaint(i));
        }

        // Y: Integer values for numbers, no doubles required: https://www.jfree.org/jfreechart/faq.html#FAQ7
        NumberAxis axisY = (NumberAxis) plot.getRangeAxis();
        axisY.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        axisY.setAutoRangeIncludesZero(true);

        // X: show Days: https://www.jfree.org/forum/viewtopic.php?f=3&t=120629
        DateAxis axisX = (DateAxis) plot.getDomainAxis();
        axisX.setTickUnit(new DateTickUnit(DateTickUnitType.DAY, 1, new SimpleDateFormat("yyyy-MM-dd")), true, true);
        axisX.setTickMarkPosition(DateTickMarkPosition.START);

        if (StatisticsHelper.PNG_INSTEADOF_SVG) {
        	// render to chart to a PNG file
        	ChartUtils.writeChartAsPNG(storage, chart, 1000, 640);
        } else {
        	// render the chart to an SVG file
        	SVGGraphics2D g2 = new SVGGraphics2D(720, 480);
        	chart.draw(g2, new Rectangle(720, 480));
        	// SVGUtils.writeToHTML(new File("svg.html"), "JFreeChart CSV Demo", g2.getSVGElement());
        	// SVGUtils.writeToSVG(new File("chart.svg"), g2.getSVGElement());

        	// JFreeChartSVG does not provide an export option with an OutputStream, so implement it yourself ... 
    		OutputStreamWriter osw = new OutputStreamWriter(storage, StandardCharsets.UTF_8);
    		// parts are copied from org.jfree.svg.SVGUtils
    		try (BufferedWriter writer = new BufferedWriter(osw)) {
                writer.write("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
                writer.write(g2.getSVGElement() + "\n");
    			writer.flush();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        }
	}
}
