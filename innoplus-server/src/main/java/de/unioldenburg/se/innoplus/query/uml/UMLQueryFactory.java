package de.unioldenburg.se.innoplus.query.uml;

import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.persistence.DataManager;
import de.unioldenburg.se.innoplus.query.Query;
import de.unioldenburg.se.innoplus.query.QueryFactory;

import java.util.List;
import java.util.stream.Collectors;

public class UMLQueryFactory extends QueryFactory {

    @Override
    public List<Query> createQueries(StudentSolution solution) {
        var teacherSolution = DataManager.getTeacherSolutions(solution.getTask());

        return teacherSolution.stream()
                .map(ts -> new UMLQuery(ts))
                .collect(Collectors.toList());
    }
}
