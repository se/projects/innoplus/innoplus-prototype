import org.junit.jupiter.api.Test;

@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class Cutlery2Test {

    @StudentClass
    public interface Food {
        @StudentMethod
        String getName();
    }

    @StudentClass
    public interface SoftFood extends Food { }

    @StudentClass
    public interface SolidFood extends Food { }

    @StudentClass
    public interface LiquidFood extends Food { }

    @StudentClass
    public interface Cake extends SolidFood, SoftFood { }

    @StudentClass
    public interface Soup extends LiquidFood, SoftFood { }

    @StudentClass
    public interface Pudding extends SoftFood { }

    @StudentClass
    public interface Cutlery<T extends Food> {
        @StudentMethod
        void process(T food);
    }

    @StudentClass
    public interface Fork<T extends SolidFood> extends Cutlery<T> { }

    @StudentClass
    public interface Spoon<T extends LiquidFood> extends Cutlery<T> { }

    @StudentClass
    public interface Spork<T extends SoftFood> extends Cutlery<T> { }

    @StudentClass
    public interface PastryFork extends Fork<Cake> {
        @Override
        @StudentMethod
        public void process(Cake food);
    }
    StudentConstructor<PastryFork> pastryForkCtor;

    @StudentClass
    public interface SoupSpoon extends Spoon<Soup> {
        @Override
        @StudentMethod
        public void process(Soup food);
    }
    StudentConstructor<SoupSpoon> soupSpoonCtor;

    @StudentClass
    public interface TravelSpork<T extends SoftFood> extends Spork<T> {
        @Override
        @StudentMethod
        public void process(T food);
    }
    StudentConstructor<TravelSpork> travelSporkCtor;


    @Test
    public void testFunctionality(){

        PastryFork fork = pastryForkCtor.create();
        SoupSpoon spoon = soupSpoonCtor.create();
        TravelSpork<SoftFood> spork = travelSporkCtor.create();

        Cake cake = () -> "Cake";
        Soup soup = () -> "Soup";
        Pudding pudding = () -> "Pudding";

        fork.process(cake);
        spoon.process(soup);
        spork.process(pudding);
        spork.process(cake);
        spork.process(soup);
    }
}
