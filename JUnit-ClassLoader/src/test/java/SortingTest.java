import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class SortingTest {

    @StudentClass
    public static interface Merger {

        public int[] merge(int[] a, int[] b);
    }

    public static StudentConstructor<Merger> ctor = null;

    @DisplayName("Combining arrays of the same length failed.")
    @Test
    public void testDefault() {

        int[] a = { 1, 3, 5, 7 };
        int[] b = { 2, 4, 6, 8 };
        int[] exp = { 1, 2, 3, 4, 5, 6, 7, 8 };

        test(a, b, exp, "Wrong result when combining arrays of the same length. Eg. merge([1, 3, 5], [2, 4, 6]) was not [1, 2, 3, 4 ,5, 6]");
    }

    @DisplayName("Combining arrays of different length failed.")
    @Test
    public void testDefaultLess() {

        int[] a = { 1, 3, 5, 7 };
        int[] b = { 2, 4, 6 };
        int[] exp = { 1, 2, 3, 4, 5, 6, 7 };

        test(a, b, exp, "Wrong result when combining arrays of different length. Eg. merge([1, 3, 5], [2, 4, 6, 7]) was not [1, 2, 3, 4 ,5, 6, 7]");
    }

    @DisplayName("Combining arrays of different length failed.")
    @Test
    public void testDefaultMore() {

        int[] a = { 1, 3 };
        int[] b = { 2, 4, 6 };
        int[] exp = { 1, 2, 3, 4, 6 };

        test(a, b, exp, "Wrong result when combining arrays of different length. Eg. merge([1, 3, 5], [2, 4, 6, 7]) was not [1, 2, 3, 4 ,5, 6, 7]");
    }

    @DisplayName("Combining with an empty array failed.")
    @Test
    public void testEmpty() {

        int[] a = { 1, 3, 5, 7 };
        int[] b = { 2, 4, 6 };
        int[] EMPTY = new int[0];

        test(a, EMPTY, a, "Wrong result when combining with an empty array on the right. Eg. merge([1, 2], []) was not [1, 2]");

        test(EMPTY, b, b, "Wrong result when combining with an empty array on the left. Eg. merge([], [1, 2]) was not [1, 2]");

        test(EMPTY, EMPTY, EMPTY, "Wrong result when combining two empty arrays.");
    }

    private void test(int[] left, int[] right, int[] exp, String error) {
        Merger proxy = ctor.create();
        int[] res = proxy.merge(left, right);

        if (!Arrays.equals(exp, res))
            fail(error + "! merge( " + Arrays.toString(left) + ", " + Arrays.toString(right) + " ) => " + Arrays.toString(res));
    }
}
