import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class RunningGameTest {

    public static final long HARE_FIRST_SLEEP_TIME_MS = 1000;
    public static final long HARE_SECOND_SLEEP_TIME_MS = 500;

    @StudentClass
    public static interface RunningGame {
        public void start();
    }

    public static StudentConstructor<RunningGame> rgCtor;

    @StudentClass
    public static interface Goal{
       public Goal getOther();
       public void setOther(Goal other);
       public String getName();
    }

    public static StudentConstructor<Goal> goalCtor;

    @StudentClass
    public static interface Hedgehog extends Runnable{
    }

    public static StudentConstructor<Hedgehog> hedgehogCtor;

    @StudentClass
    public static interface Hare extends Runnable {
    }

    public static StudentConstructor<Hare> hareCtor;

    /*
    This classes were supposed to be used to test the notify() and wait() usage of classes Hare and Hedgehog
    but this does not work, because the synchronization objects are not the same but reflection Proxies

    public static class HedgehogTestRunner implements Runnable {

        Goal goal;

        public HedgehogTestRunner(Goal goal) {
            super();
            this.goal = goal;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("Hare: *running*");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    unexpectedException(e);
                    break;
                }
                System.out.println("Hare: I'm here at the " + goal.getName() + " goal!");
                synchronized (goal) {
                    goal.notifyAll();
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    unexpectedException(e);
                    break;
                }
                goal = goal.getOther();
            }
        }
    }

    public static class HareTestRunner implements Runnable {

        private final Goal goal;
        private int count = 0;

        public HareTestRunner(Goal goal) {
            this.goal = goal;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (goal) {
                    try {
                        System.out.println("test1");
                        goal.wait();
                        count++;
                        System.out.println("test2");
                    } catch (InterruptedException e) {
                        System.out.println("testint");
                        break;
                    }
                }
            }
        }

        public int getCount() {
            return count;
        }
    }

     */

    @DisplayName("Testing the setting and getting of Goal name.")
    @Test
    public void testGoalSetAndGetName() {
        final var goalName = RandomStringUtils.randomAlphanumeric(16);
        final var first = goalCtor.create(goalName);

        assertEquals(goalName, first.getName(),
                "Getter for name of Goal returns the wrong name.");
    }

    @DisplayName("Testing the setting and getting of Goal other.")
    @Test
    public void testGoalSetAndGetOtherGoal() {
        final var first = goalCtor.create("first");
        final var second = goalCtor.create("second");
        first.setOther(second);

        assertEquals(first.getOther(), second,
                "Getter for other of Goal returns the wrong Goal.");
    }

    @DisplayName("Testing that the output of the Hare is as required.")
    @Test
    public void testHareOutput() {
        final var timeout = 1100;
        final var goalName = RandomStringUtils.randomAlphanumeric(16);
        final var goal = goalCtor.create(goalName);
        final var hare = hareCtor.create(goal);

        final var output = captureOutput(timeout, hare);

        assertEquals(2, output.size(), "The output of the Hare is less or more then expected.");

        assertTrue(output.get(1).contains(goalName), "The Hare does not output the name of the Goal or " +
                "the order of output is wrong.");

        checkOutput(output.get(0), "Hare: *running*", "The first message of the Hare is not " +
                "as required or contains to many typos. This could be the reason why other tests fail too.");

        checkOutput(output.get(1), "Hare: I'm here at the " + goalName + " goal!", "The " +
                "second message of the Hare is not as required or contains to many typos. This could be the " +
                "reason why other tests fail too.");
    }

    @DisplayName("Testing that the output of the Hedgehog is as required.")
    @Test
    public void testHedgehogOutput() {
        final var timeout = 100;
        final var goal = goalCtor.create("testGoal");
        final var hedgehog = hedgehogCtor.create(goal);

        final var output = captureOutput(timeout, hedgehog);

        assertEquals(0, output.size(), "The output of the Hedgehog is more then expected. " +
                "Maybe the Hedgehog does not call wait() or its output is at the wrong position.");
    }

    /**
     * Helper class for "diy" parameterized test of Hare
     */
    static class HareTestArgument {
        public final long timeout;
        public final Goal goals;

        public HareTestArgument(long timeout, Goal goals) {
            this.timeout = timeout;
            this.goals = goals;
        }
    }

    @DisplayName("Testing the Hare functionality with different number of Goals and run times.")
    @Test
    public void testHare() {
        final var arguments = List.of(
                new HareTestArgument(100, generateGoals(1)),
                new HareTestArgument(1100, generateGoals(2)),
                new HareTestArgument(2300, generateGoals(3)),
                new HareTestArgument(5000, generateGoals(3)),
                new HareTestArgument(10000, generateGoals(4))
        );

        arguments.forEach(arg -> {
            final var  hare = hareCtor.create(arg.goals);
            final var expectedOutput = generateExpectedHareOutput(arg.timeout, arg.goals);

            final var actualOutput = captureOutput(arg.timeout, hare);

            assertTrue(actualOutput.size() <= expectedOutput.size(),
                    "The output of the Hare is less then expected. Are all required messages printed?");

            final var eOI = expectedOutput.iterator();

            actualOutput.forEach(ao -> {
                checkOutput(ao, eOI.next(),
                        "The output is not correct. Either the output string has too many typos or " +
                                "the Hare functionality is not as required.");
            });
        });
    }

    @DisplayName("Testing if core functionality is given. Like in the start method.")
    @Test
    public void testFunctionality() {
        final var timeout = 3000;
        final var first = goalCtor.create("first");
        final var second = goalCtor.create("second");
        first.setOther(second);
        second.setOther(first);
        final var hedgehog = hedgehogCtor.create(first);
        final var hedgehog2 = hedgehogCtor.create(second);
        final var hare = hareCtor.create(first);
        final var expectedOutput = generateExpectedStartOutput(timeout, first);

        final var actualOutput = captureOutput(timeout, List.of(hedgehog, hedgehog2, hare));

        assertTrue(actualOutput.size() <= expectedOutput.size(),
                "The output is less than expected. Are the sleep times as required and " +
                        "all required messages printed?");

        final var eOI = expectedOutput.iterator();

        actualOutput.forEach(ao -> {
            checkOutput(ao, eOI.next(),
                    "The output is not correct. Either the output string has too many typos or " +
                            "the Hare, Hedgehog or Goal functionality is not as required.");
        });
    }

    @DisplayName("Testing if start method is working as expected (produces the expected output).")
    @Test
    public void testStart() {
        final var timeout = 3000;
        final var first = goalCtor.create("left");
        final var second = goalCtor.create("right");
        first.setOther(second);
        second.setOther(first);
        final var game = rgCtor.create();
        final var expectedOutput = generateExpectedStartOutput(timeout, first);

        final var actualOutput = captureOutput(timeout, game::start);

        assertTrue(actualOutput.size() <= expectedOutput.size(),
                "The output of start method is less than expected. Are the sleep times as required and " +
                        "all required messages printed?");

        final var eOI = expectedOutput.iterator();

        actualOutput.forEach(ao -> {
            checkOutput(ao, eOI.next(),
                    "The output is not correct. Either the output string has too many typos or " +
                            "the Hare, Hedgehog or Goal functionality is not as required.");
        });
    }

    /**
     * Helper class for "diy" parameterized test of extensibility
     */
    public static class ExtensibilityTestArguments {
        public final long timeout;
        public final int numHares;
        public final int numGoals;
        public final long expectedOutputLinesMin;
        public final long expectedOutputLinesMax;

        public ExtensibilityTestArguments(long timeout, int numHares, int numGoals, long expectedOutputLinesMin,
                                          long expectedOutputLinesMax) {
            this.timeout = timeout;
            this.numHares = numHares;
            this.numGoals = numGoals;
            this.expectedOutputLinesMin = expectedOutputLinesMin;
            this.expectedOutputLinesMax = expectedOutputLinesMax;
        }

        public ExtensibilityTestArguments(long timeout, int numHares, int numGoals, long expectedOutputLines) {
            this(timeout, numHares, numGoals, expectedOutputLines, expectedOutputLines);
        }
    }

    // This test is complicated because it depends on the used hardware and the output string length
    @DisplayName("Testing if the game was designed extensible.")
    @Test
    public void testExtensibility(){
        final var arguments = List.of(
                new ExtensibilityTestArguments(2000, 2, 2, 8),
                new ExtensibilityTestArguments(2000, 2, 3, 8),
                new ExtensibilityTestArguments(2000, 3, 1, 11),
                new ExtensibilityTestArguments(3000, 1, 5, 6),
                new ExtensibilityTestArguments(5000, 5, 2, 45, 50)
        );

        arguments.forEach(arg -> {
            final var goals = generateGoals(arg.numGoals);
            final var agents = new ArrayList<Runnable>(arg.numHares + arg.numGoals);
            var goal = goals;
            for (int i = 0; i < arg.numHares; i++) {
                agents.add(hareCtor.create(goal));
                assert goal != null;
                goal = goal.getOther();
            }
            for (int i = 0; i < arg.numGoals; i++) {
                agents.add(hedgehogCtor.create(goal));
                assert goal != null;
                goal = goal.getOther();
            }

            final var output = captureOutput(arg.timeout, agents);

            assertTrue(arg.expectedOutputLinesMin <= output.size()
                            && output.size() <= arg.expectedOutputLinesMax,
                    String.format("The output is not correct. Either the output string has too many typos or " +
                            "the Hare, Hedgehog or Goal functionality is not as required. (min=%d, max=%d, actual=%d)",
                    arg.expectedOutputLinesMin, arg.expectedOutputLinesMax, output.size()));
        });
    }

    /**
     * Captures the output of the given {@link Runnable}s
     * @param timeout max run time of the runnables
     * @param runnables the runnables to be run
     * @return the captured output as List of Strings (each entry a line)
     */
    private static List<String> captureOutput(long timeout, List<Runnable> runnables) {
        try (final var outputStream = new ByteArrayOutputStream()) {
            final var threads = runnables.stream()
                    .map(Thread::new)
                    .collect(Collectors.toList());

            final var old = System.out;
            System.setOut(new PrintStream(outputStream));

            threads.forEach(Thread::start);

            // Let this thread wait for the runnables to run for timeout amount of time
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e){
                unexpectedException(e);
            }

            threads.forEach(Thread::interrupt);
            threads.stream().filter(Thread::isAlive).forEach(Thread::stop);

            System.setOut(old);

            final var output = outputStream.toString();

            // String.split() always returns an array with at least one element (the not splitted string)
            if (output.length() == 0) {
                return Collections.emptyList();
            }

            return Arrays.stream(output.split("\\n")).collect(Collectors.toList());
        } catch (IOException e) {
            unexpectedException(e);
        }
        return Collections.emptyList();
    }

    /**
     * Captures the output of the given {@link Runnable}
     * @param timeout max run time of the runnable
     * @param runnable the runnable to be run
     * @return the captured output as List of Strings (each entry a line)
     */
    private static List<String> captureOutput(long timeout, Runnable runnable) {
        return captureOutput(timeout, List.of(runnable));
    }

    /**
     * Generate a circle of Goals
     * @param num number of goals in circle
     * @return the circle of goals or null if num is less then 1
     */
    private static Goal generateGoals(int num) {
        if (num < 1) return null;
        final var head = goalCtor.create("0");
        Goal actualGoal = head;
        for (int i = 1; i < num; i++) {
            final var nextGoal = goalCtor.create(String.valueOf(i));
            actualGoal.setOther(nextGoal);
            actualGoal = nextGoal;
        }
        actualGoal.setOther(head);
        return head;
    }

    /**
     * Generates the expected output of a Hare
     * @param timeout the max time the hare has to run
     * @param firstGoal the goal at which the hare starts
     * @return the expected output as List of Strings (each entry a line)
     */
    private static List<String> generateExpectedHareOutput(long timeout, Goal firstGoal) {
        final var expectedOutput = new ArrayList<String>();
        expectedOutput.add("Hare: *running*");
        while (timeout >= HARE_FIRST_SLEEP_TIME_MS) {
            timeout -= HARE_FIRST_SLEEP_TIME_MS;
            expectedOutput.add("Hare: I'm here at the " + firstGoal.getName() + " goal!");
            if (timeout < HARE_SECOND_SLEEP_TIME_MS) break;
            timeout -= HARE_SECOND_SLEEP_TIME_MS;
            firstGoal = firstGoal.getOther();
            expectedOutput.add("Hare: *running*");
        }
        return expectedOutput;
    }

    /**
     * Generates the expected output of the start method of the running game
     * @param timeout time the game runs in ms
     * @param firstGoal the goal that the hare gets at its creation
     * @return the expected Output as List of Strings (each entry a line)
     */
    private static List<String> generateExpectedStartOutput(long timeout, Goal firstGoal) {
        final var expectedOutput = new ArrayList<String>();
        expectedOutput.add("Hare: *running*");
        while (timeout >= HARE_FIRST_SLEEP_TIME_MS) {
            timeout -= HARE_FIRST_SLEEP_TIME_MS;
            expectedOutput.add("Hare: I'm here at the " + firstGoal.getName() + " goal!");
            expectedOutput.add("Hedgehog: Neener-neener, I'm here already!");
            if (timeout < HARE_SECOND_SLEEP_TIME_MS) break;
            timeout -= HARE_SECOND_SLEEP_TIME_MS;
            firstGoal = firstGoal.getOther();
            expectedOutput.add("Hare: *running*");
        }
        return expectedOutput;
    }

    /**
     * Checks weather the output is nearly equal to the expected output
     * uses the Levenshtein Distance for equality check
     * @param actual the actual output
     * @param expected the expected output
     * @param maxDiff the max number of modifications allowed to be a valid expected output
     * @param errorMessage the error message that gets printed if the output is not equal enough
     */
    private static void checkOutput(String actual, String expected, long maxDiff, String errorMessage) {
        final var ld = new LevenshteinDistance();
        final var diff = ld.apply(actual, expected);
        assertTrue(diff < maxDiff, errorMessage + " Expected: " + expected + " Actual: " + actual);
    }

    /**
     * Checks weather the output is nearly equal to the expected output
     * with predefined max diff of 40% of expected output
     * uses the Levenshtein Distance for equality check
     * @param actual the actual output
     * @param expected the expected output
     * @param errorMessage the error message that gets printed if the output is not equal enough
     */
    private static void checkOutput(String actual, String expected, String errorMessage) {
        final var maxDiff = Math.round(expected.length() * 0.4);
        checkOutput(actual, expected, maxDiff, errorMessage);
    }

    /**
     * helper method to report unexpected failure
     * @param e the unexpected exception
     */
    private static void unexpectedException(Exception e) {
        fail("Unexpected Exception thrown!", e);
    }
}
