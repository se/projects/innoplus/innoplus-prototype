import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// @Disabled
@ExtendWith(StudentSolutionLoader.class)
public class StudentSolutionLoaderTest {

//    @StudentClass
//    public static interface Merger {
//
//        public int[] merge(int[] a, int[] b);
//    }
//
//    public static Constructor<Merger> ctor = null;
//
//    @Disabled
//    @Test
//	public void test() {
//        assertNotNull(ctor);
//        var proxy = ctor.create();
//
//        int[] a = { 1, 2 };
//        int[] b = { 0, 1 };
//        int[] exp = { 0, 1, 1, 2 };
//
//        assertArrayEquals(exp, proxy.merge(a, b));
//    }
//
    @StudentClass
    public interface Star {

        public String getName();

        public void setName(String name);

        public String getId();

        public void setId(String id);

        public String getType();

        public void setType(String type);

        public double getDistance();

        public void setDistance(double distance);

        public double getApparentMagnitude();

        public void setApparentMagnitude(double apparentMagnitude);
    }

    public static StudentConstructor<Star> starCtor;

    @StudentClass
    public interface StarDatabase {

        public Star createStar();

        /** Fügt einen neuen Stern hinzu **/
        public void add(Star star);

        /** Entfernt den Stern am gegebenen Index **/
        public void remove(int index);

        /** Liefert den Stern am gegebenen Index **/
        public Star get(int index);

        /** Liefert die Anzahl der Sterne **/
        public int size();

        /** Findet den Stern mit gegebener ID (wenn vorhanden, ansonsten null) **/
        public Star find(String id);

        /** Liefert alle Sterne mit eine scheinbaren Helligkeit in [low, high] **/
        public Star[] getMagnitudeRange(double low, double high);

        public Star select(boolean cond, Star a, Star b);
    }

    public static StudentConstructor<StarDatabase> starDatabaseCtor;

	@Test
	public void testDefault() {
		StarDatabase db = starDatabaseCtor.create();
		assertEquals(0, db.size());
		assertNull(db.find("DoesNotExist"));
	}

    @Test
	public void testInsert() {
		StarDatabase db = starDatabaseCtor.create();
        Star s1 = starCtor.create();
        db.add(s1);

        Star s2 = db.createStar();
        db.add(s2);
	}

    @Test
	public void testGetMagnitude() {
		StarDatabase db = starDatabaseCtor.create();
        db.getMagnitudeRange(0, 1);
	}

    @Test
    public void testToSring() {
        starCtor.create().toString();
    }

    @Test
    public void testSwap() {
        var db = starDatabaseCtor.create();
        var s1 = starCtor.create();
        var s2 = db.createStar();

        assertTrue(s1 == db.select(true, s1, s2));
        assertTrue(s2 == db.select(false, s1, s2));
    }

    @StudentClass
    public static interface A {
        public void foo();
    }

    @StudentClass
    public static interface B {
        public void bar();
    }

    @StudentClass
    public static interface C extends A, B {
        public void hello();
    }

    public static StudentConstructor<C> cCtor;

    @Test
    public void testInheritance() {
        var c = cCtor.create();
        c = cCtor.create();
        c.foo();
        c.bar();
        c.hello();
    }
}
