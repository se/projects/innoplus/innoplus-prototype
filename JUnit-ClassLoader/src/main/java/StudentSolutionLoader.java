import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;

import java.lang.reflect.ParameterizedType;
import java.nio.file.Paths;
import java.util.HashMap;

public class StudentSolutionLoader implements TestInstancePostProcessor {

	private final JavaClasses studentSolution;

	public StudentSolutionLoader() throws Exception {
		var input = Paths.get(".", "input").toAbsolutePath().normalize();
        this.studentSolution = new JavaClasses(input);
	}

    @Override
    public void postProcessTestInstance(Object test, ExtensionContext context) throws Exception {

        var teacherSolution = test.getClass();
        var typeCache = new HashMap<Class<?>, InterfaceConstructor<?>>(); // Test-Interface -> Student-Class-Adapter
        var typeHandler = new TypeHandler(typeCache);

        for (var teacherInterface : teacherSolution.getDeclaredClasses()) {
            StudentClass studentClassAnn = teacherInterface.getAnnotation(StudentClass.class);
            if (studentClassAnn == null)
                continue;

            if (!teacherInterface.isInterface())
                throw new IllegalStateException("Target of @" + StudentClass.class.getSimpleName() + " must be an interface!");

            var studentClass = !studentClassAnn.value().isBlank()
                    ? studentSolution.findClass(studentClassAnn.value())
                    : studentSolution.findImplementation(teacherInterface);

            var ctor = new InterfaceConstructor<>(studentSolution, typeHandler, studentClass, teacherInterface);
            typeCache.put(teacherInterface, ctor);
        }

        for (var inter : typeCache.values())
            inter.initialiseMethodAdapter();

        for (var field : teacherSolution.getDeclaredFields()) {

            // Insert the actual InterfaceCtor instance as value
            if (field.getType().equals(StudentConstructor.class)) {
        		var type = (ParameterizedType) field.getGenericType();
        		var clazz = (Class<?>) type.getActualTypeArguments()[0];

                var ctor = typeCache.get(clazz);
                field.set(test, ctor);
                continue;
            }

			// Insert the actual StudentArray factory instance as value
			if (field.getType().equals(StudentArray.Factory.class)) {
				var type = (ParameterizedType) field.getGenericType();
				var testClazz = (Class<?>) type.getActualTypeArguments()[0];
				var studentClazz = typeHandler.getStudentClass(testClazz);
				var ctor = StudentArray.createFactory(studentClazz, typeHandler);
				field.set(test, ctor);
				continue;
			}

            var studentClass = field.getAnnotation(StudentClass.class);
            var studentMethod = field.getAnnotation(StudentMethod.class);

            // Static methods
            if (studentClass == null) {
                if (studentMethod == null)
                    continue;

                var type = (ParameterizedType) field.getGenericType();

                var all = type.getActualTypeArguments();
                var ret = (Class<?>) all[all.length - 1];
                var params = new Class<?>[all.length - 1];

                for (int i = 0; i < params.length; i++) {
                    params[i] = (Class<?>) all[i];
                }

                var method = studentSolution.findMethod(ret, studentMethod.value(), params);
                field.set(test, MethodAdapters.wrapStaticMethod((Class<?>) type.getRawType(), method));
            } else {
                var clazz = studentSolution.findClass(studentClass.value());

                // Constructors
                if (studentMethod == null) {
                    field.set(test, MethodAdapters.generateCtorCall(typeHandler, clazz));
                } else {

                    var type = (ParameterizedType) field.getGenericType();

                    var all = type.getActualTypeArguments();
                    var ret = (Class<?>) all[all.length - 1];
                    var params = new Class<?>[all.length - 2];

                    for (int i = 0; i < params.length; i++) {
                        params[i] = (Class<?>) all[i + 1];
                    }

                    var method = studentSolution.findMethodIn(clazz, ret, studentMethod.value(), params);
                    field.set(test, MethodAdapters.wrapInstanceMethod((Class<?>) type.getRawType(), method));
                }
            }
        }
    }
}
