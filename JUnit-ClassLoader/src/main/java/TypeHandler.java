import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Utility class that enables funtion calls from tests into classes provided by
 * students. It provides methods to translate between types used in the tests
 * and those used in the actual implementation.
 *
 * Currently handled types:
 *
 * @see InterfaceConstructor.InterfaceProxy
 * @see StudentArray
 **/
final class TypeHandler {

	/** test interface => {@link InterfaceConstructor student class wrapper} **/
	private final Map<Class<?>, InterfaceConstructor<?>> interfaceWrappers;

	public TypeHandler(Map<Class<?>, InterfaceConstructor<?>> interfaceWrappers) {
		this.interfaceWrappers = interfaceWrappers;
	}

	/**
	 * Shortcut for {@link #getStudentClass(Class, Type) getStudentClass(testClazz,
	 * null) }
	 **/
	public Class<?> getStudentClass(Class<?> testClazz) {
		return getStudentClass(testClazz, null);
	}

	/**
	 * Shortcut for {@link #getStudentClass(Class, ParameterizedType)
	 * getStudentClass(testClazz, testType) } that casts {@link testType if
	 * appropriate}
	 **/
	public Class<?> getStudentClass(Class<?> testClazz, Type testType) {
		var type = testType instanceof ParameterizedType ? (ParameterizedType) testType : null;
		return getStudentClass(testClazz, type);
	}

	/**
	 * Maps from a type used in test code to the corresponding type in the student
	 * code. Resolves the underlying type for all internal wrapper classes.
	 *
	 * @param testClazz test type
	 * @param testType  generic type of testClazz used to handle generic types (may
	 *                  be null)
	 * @return {@code testClass} or the underlying type if {@code testClazz} is an
	 *         internal wrapper
	 */
	public Class<?> getStudentClass(Class<?> testClazz, ParameterizedType testType) {

		var wrapper = interfaceWrappers.get(testClazz);

		if (wrapper != null)
			return wrapper.studentClass;

		if (testType != null && StudentArray.class.equals(testType.getRawType())) {

			// Recursively resolve the element type of an array
			var elemType = testType.getActualTypeArguments()[0];
			var elemClazz = testClazz;

			if (elemType instanceof Class<?>) {
				elemClazz = (Class<?>) elemType;

			} else if (elemType instanceof ParameterizedType) {
				var paramRealType = (ParameterizedType) elemType;
				elemClazz = (Class<?>) paramRealType.getRawType();

			} else {
				// ???
			}

			var resolved = getStudentClass(elemClazz, elemType);

			// HACK: Use resolved.arrayType() for >= Java 12
			return Array.newInstance(resolved, 0).getClass();
		}

		return testClazz;
	}

	/**
	 * Maps from a type used in student code to the corresponding type in the test
	 * code. Returns internal wrapper classes if necessary.
	 *
	 * @param studentClazz student type
	 * @return {@code studentClass} or the wrapper representing {@code testClazz}
	 */
	public Class<?> getTestClass(Class<?> studentClass) {
		if (studentClass.isArray())
			return StudentClass.class;

		for (var v : this.interfaceWrappers.values()) {
			if (studentClass.equals(v.studentClass))
				return v.teacherInterface;
		}

		return studentClass;
	}

	/**
	 * Utility method to call into student methods.
	 *
	 * Wraps {@link Method#invoke} by translating the arguments and the returned
	 * value
	 *
	 * @param context    called instance
	 * @param toCall     method to call
	 * @param args       arguments for the function call (will be modified!)
	 * @param targetType expected type for the return value
	 * @return the result of
	 *         {@code translateToTest(targetType, toCall.invoke(context, translateToSudent(args)))}
	 *
	 * @see Method#invoke(Object, Object...)
	 */
	public Object callStudentMethod(Object context, Method toCall, Object[] args, Class<?> targetType)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		// Unwrap instances of wrappers
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				args[i] = translateToStudent(args[i]);
			}
		}

		var res = toCall.invoke(context, args);
		return translateToTest(targetType, res);
	}

	/**
	 * Ensures that {@code value} from student code matches the expected type in the
	 * test code. Wraps custom types into the internal wrapper classes.
	 **/
	public Object translateToTest(Class<?> testType, Object value) {
		if (value == null)
			return null;

		// Wrap results in wrapper return type:
		var actual = interfaceWrappers.get(testType);
		if (actual != null)
			return actual.getProxy(value);

		// Check for array types
		if (StudentArray.class.equals(testType))
			return StudentArray.wrap(value, this);

		return value;
	}

	/**
	 * Ensures that {@code value} from test code matches the expected type in the
	 * student code. Extracts the internal objects from internal wrapper classes.
	 **/
	public Object translateToStudent(Object value) {

		if (value instanceof Proxy) {
			var arg = Proxy.getInvocationHandler(value);
			return ((InterfaceConstructor<?>.InterfaceProxy) arg).instance;
		}

		if (value instanceof StudentArray)
			return ((StudentArray<?>) value).realArray;

		return value;
	}
}
