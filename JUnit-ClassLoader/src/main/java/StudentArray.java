import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;
import java.util.function.IntFunction;

/**
 * Wrapper for arrays returned by student code which uses the {@link Array array
 * reflection API} to access the underlying array. Use {@link #get},
 * {@link #set} and {@link #length()} instead of the index operator +
 * {@code length}.
 *
 * Also implements the read-only methods from {@link List} (except
 * {@link #sublist} and {@link #toArray}) to ease testing.
 **/
public class StudentArray<T> implements List<T> {

    /** Factory to create arrays of student types **/
    public static interface Factory<T> extends IntFunction<StudentArray<T>> {}

    /** Creates a new factory for the supplied type */
    public static <T> Factory<T> createFactory(Class<?> studentType, TypeHandler typeHandler) {
        return size -> new StudentArray<>(studentType, size, typeHandler);
    }

    /**
     * Weak cache of all known instances.
     *
     * Caching is required to retain identity comparisons expected from Java arrays.
     */
    private static final Map<Object, StudentArray<?>> instances = new WeakHashMap<>();

    /** Returns a wrapper for the supplied array **/
    @SuppressWarnings("unchecked")
    protected static <T> StudentArray<T> wrap(Object arr, TypeHandler typeHandler) {
        return (StudentArray<T>) instances.computeIfAbsent(arr, a -> new StudentArray<>(a, typeHandler));
    }

    // protected s.t. we can access it during API translation
    /** student array **/
    protected final Object realArray;

    /** whether {@link realArray} has multiple dimensions, e.g. for Star[][] */
    private final boolean nestedArray;

    /** type handler to wrap/unwrap array elements **/
    private final TypeHandler typeHandler;

    /** type used in test code to represent the {@link #realArray}'s elements */
    private final Class<?> testElemType;

    private StudentArray(Object array, TypeHandler typeHandler) {
        this.realArray = array;
        var studentElemType = realArray.getClass().getComponentType();
        this.nestedArray = studentElemType.isArray();
        this.typeHandler = typeHandler;
        this.testElemType = typeHandler.getTestClass(studentElemType);
    }

    /** Creates a new array, effectively {@code new <type>[size]} */
    private StudentArray(Class<?> studentType, int size, TypeHandler typeHandler) {
        this.realArray = Array.newInstance(studentType, size);
        this.nestedArray = studentType.isArray();
        this.typeHandler = typeHandler;
        this.testElemType = typeHandler.getTestClass(studentType);
        instances.put(this.realArray, this);
    }

    /** Returns the element at position {@code idx} **/
    @SuppressWarnings("unchecked")
    public T get(int idx) {
        var res = Array.get(realArray, idx);

        if (nestedArray)
            return (T) wrap(res, typeHandler);

        return (T) typeHandler.translateToTest(testElemType, res);
    }

    /** Sets the element at position {@code idx} **/
    @Override
    public T set(int idx, T value) {
        var elem = typeHandler.translateToStudent(value);
        Array.set(realArray, idx, elem);
        return value;
    }

    /** Returns the array length **/
    public int length() {
        return Array.getLength(realArray);
    }

    // Sane implementations for Object's methods

    @Override
    public String toString() {
        String res = "[";
        for (int i = 0, end = length(); i < end; i++) {
            if (i != 0)
                res += ", ";
            res += Array.get(realArray, i);
        }
        res += ']';
        return res;
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (int i = 0, end = length(); i < end; i++) {
            result = (31 * result) + Objects.hashCode(Array.get(realArray, i));
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (obj == this)
            return true;

        // Comparable to other arrays
        if (obj instanceof StudentArray) {
            var other = (StudentArray<?>) obj;
            var len = this.length();
            if (len != other.length())
                return false;

            while (len-- > 0) {
                var a = Array.get(this.realArray, len);
                var b = Array.get(other.realArray, len);
                if (!Objects.equals(a, b))
                    return false;
            }

            return true;
        }

        // Also comparable to other collections
        if (obj instanceof Collection<?>) {
            var it = ((Collection<?>) obj).iterator();

            for (int i = 0, end = length(); i < end; i++) {
                if (!it.hasNext())
                    return false;

                var a = Array.get(realArray, i);
                var b = typeHandler.translateToStudent(it.next());

                if (!Objects.equals(a, b))
                    return false;
            }

            return !it.hasNext();
        }

        return false;
    }

    // For-each compitability
    @Override
    public ListIterator<T> iterator() {
        return listIterator();
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(int start) {

        return new ListIterator<T>() {
            private int pos = start;

            @Override
            public boolean hasNext() {
                return pos != length();
            }

            @Override
            public T next() {
                return get(pos++);
            }

            @Override
            public boolean hasPrevious() {
                return pos != 0;
            }

            @Override
            public T previous() {
                return get(--pos);
            }

            @Override
            public int nextIndex() {
                return pos;
            }

            @Override
            public int previousIndex() {
                return pos - 1;
            }

            @Override
            public void remove() {
                assumeTrue(false, "Action not supported!");
            }

            @Override
            public void set(T e) {
                assumeTrue(false, "Action not supported!");
            }

            @Override
            public void add(T e) {
                assumeTrue(false, "Action not supported!");
            }
        };
    }

    // Implement some of the Collection API to ease testing

    @Override
    public int size() {
        return length();
    }

    @Override
    public boolean isEmpty() {
        return length() == 0;
    }

    @Override
    public boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @Override
    public boolean containsAll(Collection<?> col) {
        for (var entry : col) {
            if (!this.contains(entry))
                return false;
        }
        return true;
    }

    @Override
    public int indexOf(Object needle) {
        needle = typeHandler.translateToStudent(needle);

        for (int i = 0, end = length(); i < end; i++) {
            var cur = Array.get(realArray, i);
            if (Objects.equals(needle, cur))
                return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object needle) {
        needle = typeHandler.translateToStudent(needle);

        for (int i = length() - 1; i >= 0; i--) {

            var cur = Array.get(realArray, i);
            if (Objects.equals(needle, cur))
                return i;
        }
        return -1;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public Object[] toArray() {
        assumeTrue(false, "Action not supported!");
        return null;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public <U> U[] toArray(U[] a) {
        assumeTrue(false, "Action not supported!");
        return null;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public boolean add(T e) {
        assumeTrue(false, "Action not supported!");
        return false;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public boolean remove(Object o) {
        assumeTrue(false, "Action not supported!");
        return false;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public boolean addAll(Collection<? extends T> c) {
        assumeTrue(false, "Action not supported!");
        return false;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public boolean removeAll(Collection<?> c) {
        assumeTrue(false, "Action not supported!");
        return false;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public boolean retainAll(Collection<?> c) {
        assumeTrue(false, "Action not supported!");
        return false;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public void clear() {
        assumeTrue(false, "Action not supported!");
    }

    /** NOT IMPLEMENTED **/
    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        assumeTrue(false, "Action not supported!");
        return false;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public void add(int index, T element) {
        assumeTrue(false, "Action not supported!");
    }

    /** NOT IMPLEMENTED **/
    @Override
    public T remove(int index) {
        assumeTrue(false, "Action not supported!");
        return null;
    }

    /** NOT IMPLEMENTED **/
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        assumeTrue(false, "Action not supported!");
        return null;
    }
}
