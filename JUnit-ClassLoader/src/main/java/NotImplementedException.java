import java.lang.reflect.Method;

public class NotImplementedException extends org.opentest4j.TestAbortedException {

	public final Class<?> clazz;
	public final String method;

	public NotImplementedException(Class<?> c, String m) {
		this.clazz = c;
		this.method = m;
	}
}
