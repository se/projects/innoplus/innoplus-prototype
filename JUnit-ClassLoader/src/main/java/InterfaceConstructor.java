import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;

public final class InterfaceConstructor<T> implements StudentConstructor<T> {

    private final JavaClasses classManager;
    private final TypeHandler typeHandler;
    protected final Class<?> studentClass;
    protected final Class<?> teacherInterface;
    private final Map<Method, Method> vtable;

    public InterfaceConstructor(JavaClasses classManager, TypeHandler typeHandler, Class<?> studentClass, Class<?> teacherInterface) throws Exception {
        this.classManager = classManager;
        this.typeHandler = typeHandler;
        this.studentClass = studentClass;
        this.teacherInterface = teacherInterface;
        this.vtable = new java.util.HashMap<>();
    }

    protected void initialiseMethodAdapter() {

        for (var stub : teacherInterface.getMethods()) {

            var types = stub.getParameterTypes();
            var genericTypes = stub.getGenericParameterTypes();
            // System.out.printf("Before: %s -> %s\n", java.util.Arrays.toString(types), ret);

            // Translate wrappers to native type
            // Parameters:
            for (int i = 0; i < types.length; i++) {
                types[i] = typeHandler.getStudentClass(types[i], genericTypes[i]);
            }

            // Return type:
            var ret = typeHandler.getStudentClass(stub.getReturnType(), stub.getGenericReturnType());

            // System.out.printf("After: %s -> %s\n\n", java.util.Arrays.toString(types), ret);

            // Find & register implementation
            var impl = classManager.findMethodIn(studentClass, ret, stub.getName(), types);
            if (impl != null)
                // Ensure that the function is callable (e.g. for missing public)
                impl.setAccessible(true);
            vtable.put(stub, impl);
        }
    }

    private final Map<Object, T> proxyInstances = new java.util.WeakHashMap<>();

    @Override
    public T create(Object... args) {
        var instance = MethodAdapters.callCtor(typeHandler, studentClass, args);
        return getProxy(instance);
    }

    @SuppressWarnings("unchecked")
    public T createProxy(Object instance) {
        var handler = new InterfaceProxy(instance);

        var cl = studentClass.getClassLoader();
        var interfaces = new Class<?>[] { teacherInterface };

        var proxy = (T) Proxy.newProxyInstance(cl, interfaces, handler);
        proxyInstances.put(instance, proxy);
        return proxy;
    }

    public T getProxy(Object o) {
        return proxyInstances.computeIfAbsent(o, this::createProxy);
    }

    protected final class InterfaceProxy implements java.lang.reflect.InvocationHandler {
        protected final Object instance;

        public InterfaceProxy(Object instance) {
            this.instance = instance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            // System.out.printf("%s . %s (%s)\n", "proxy", method, Arrays.toString(args));
            var toCall = vtable.get(method);

            // Handle Java's Object methods
            if (toCall == null && List.of("toString", "hashCode", "equals", "getClass").contains(method.getName())) {
                toCall = studentClass.getMethod(method.getName(), method.getParameterTypes());
            }

            if(toCall == null) {
                System.out.println("test");
                throw new NotImplementedException(proxy.getClass(), method.getName());
            }

            try {
                return typeHandler.callStudentMethod(instance, toCall, args, method.getReturnType());

            } catch (InvocationTargetException e) {
                // Unwrap exception for a proper stack trace and to
                // avoid the "Unexpected exception thrown" error
                throw e.getCause();
            }
        }
    }
}
