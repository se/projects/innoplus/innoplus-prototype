import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

/** Namespaces for utility methods that translate between reflection to test code */
public final class MethodAdapters {

	private MethodAdapters() {}

	public static <T> StudentConstructor<T> generateCtorCall(TypeHandler typeHandler, Class<T> clazz) {
        return (Object[] args) -> callCtor(typeHandler, clazz, args);
    }

    public static <T> T callCtor(TypeHandler typeHandler, Class<T> clazz, Object... args) {

        for (int i = 0; i < args.length; i++)
            args[i] = typeHandler.translateToStudent(args[i]);

        var argTypes = new Class<?>[args.length];
        for (int i = 0; i < args.length; i++)
            argTypes[i] = args[i].getClass();

        try {
            var ctor = clazz.getDeclaredConstructor(argTypes);
			ctor.setAccessible(true);
            return ctor.newInstance(args);

        } catch (NoSuchMethodException e) {
            var str = Arrays.stream(args)
                    .map(arg -> arg.getClass().getSimpleName())
                    .collect(Collectors.joining(", "));
			// var str = Arrays.toString(args);
			// str = str.substring(1, str.length() - 1);
            throw new NotImplementedException(clazz, clazz.getSimpleName() + "(" + str + ')');
			// assumeTrue(false, "No matching constructor found: " + clazz.getSimpleName() + "(" + str + ')');

		} catch(InstantiationException e) {
			fail(clazz.getName() + " is abstract!");

		} catch(InvocationTargetException e) {
            fail(e);

        } catch(SecurityException | IllegalAccessException | IllegalArgumentException e) {
			assumeTrue(false, "Class could not be loaded: " + e);
		}

		throw new Error(); // Unreachable
    }

	public static Object wrapInstanceMethod(Class<?> clazz, Method method) {

		if (clazz == Function.class)
			return wrapInstanceFunction(method);

		if (clazz == BiFunction.class)
			return wrapInstanceBiFunction(method);

		fail("No wrapper for " + clazz);
		return null;
	}

    public static Function<Object, Object> wrapInstanceFunction(Method method) {
        return (a) -> {
            try {
                return method.invoke(a);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static BiFunction<Object, Object, Object> wrapInstanceBiFunction(Method method) {
        return (a, b) -> {
            try {
                return method.invoke(a, b);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static Object wrapStaticMethod(Class<?> clazz, Method method) {
        if (clazz == Function.class)
            return wrapStaticFunction(method);
        else if (clazz == BiFunction.class)
            return wrapStaticBiFunction(method);
        else
            throw new Error("Not implemented!");
    }

    public static Function<Object, Object> wrapStaticFunction(Method method) {
        return (a) -> {
            try {
                return method.invoke(null, a);
            } catch (Exception e) {
                //throw new RuntimeException("(" + file + ")" + e);
                throw new RuntimeException("(" + method.getDeclaringClass().getName() + " : " + method.getName() +")" + e);
            }
        };
    }

    public static BiFunction<Object, Object, Object> wrapStaticBiFunction(Method method) {
        return (a, b) -> {
            try {
                return method.invoke(null, a, b);
            } catch (Exception e) {
                throw new RuntimeException("(" + method.getDeclaringClass().getName() + " : " + method.getName() + ")" + e);
            }
        };
    }
}
