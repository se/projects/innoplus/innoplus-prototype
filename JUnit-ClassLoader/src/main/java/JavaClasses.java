import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class JavaClasses {

    private final Map<Path, Class<?>> classes;

    public JavaClasses(Path directory, Path... extra) throws MalformedURLException {

        classes = new HashMap<>();

        try (var classLoader = new URLClassLoader(new URL[] { directory.toUri().toURL() });
            var files = Files.walk(directory)
        ) {
            for (var fileIt = files.iterator(); fileIt.hasNext(); ) {
                var file = fileIt.next();
                if (!file.getFileName().toString().endsWith(".java"))
                    continue;

                var rel = directory.relativize(file);
                var args = new ArrayList<String>();
                args.add("javac");
                args.add(rel.toString());
                for (var e : extra)
                    args.add(e.toString());

                var exit = new ProcessBuilder(args)
                        .directory(directory.toFile())
                        .inheritIO()
                        .start()
                        .waitFor();

                if (exit != 0)
                    throw new RuntimeException("Compilation failed!");

                var clazzName = rel.toString();
                clazzName = clazzName.substring(0, clazzName.length() - ".java".length());
                clazzName = clazzName.replace('\\', '.');
                clazzName = clazzName.replace('/', '.');
                var clazz = classLoader.loadClass(clazzName);

                // We have to make sure that inner classes are loaded
                if (clazz.getClasses().length != 0){
                    for (var clazz_ : clazz.getClasses()) {
                        classes.put(Paths.get(clazz_.getName() + ".java"), clazz_);
                    }
                }
                classes.put(file, clazz);
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize", e);
        }
    }

    public Class<?> findClass(String expectedName /* , Class<?> parent, Class<?>... interfaces */) throws Exception {

        for (var clazz : classes.values()) {

            // 1. Versuch: Direkter Match
            if (expectedName.equalsIgnoreCase(clazz.getSimpleName()))
                return clazz;
            else if(expectedName.equals(clazz.getName()))
                return clazz;
            else if (expectedName.equals(clazz.getName().substring(clazz.getName().lastIndexOf(".") + 1))){
                return clazz;
            }
            // 2. Versuch: ?
        }

        throw new NoSuchElementException("Could not find " + expectedName);
    }

    /** Finds a class that is the closest match for the interface (might not be an exact match) **/
    public Class<?> findImplementation(Class<?> interface_) throws Exception {

        var matches = new ArrayList<Map.Entry<Class<?>, Integer>>(classes.size());
        for (var clazz : classes.values()) {
            var count = 0;

            if(interface_.getSimpleName().equals(clazz.getSimpleName())){
                count++;
            }
            for (var binding : interface_.getDeclaredMethods()) {
                var found = findMethodInImpl(clazz, binding.getReturnType(), binding.getName(), binding.getParameterTypes());
                if (found != null)
                    count++;
            }

            if (count > 0)
                matches.add(Map.entry(clazz, count));
        }

        if (matches.isEmpty())
            throw new NotImplementedException(interface_, null);

        if (matches.size() == 1)
            return matches.get(0).getKey();

        matches.sort(Comparator.comparing(Map.Entry::getValue));
        var best = matches.get(matches.size() - 1);
        var second = matches.get(matches.size() - 2);

        if (best.getValue() == second.getValue())
            throw new IllegalStateException("Multiple matches: " + matches);

        return best.getKey();
    }

    public Method findMethodIn(Class<?> clazz, Class<?> returnType, String expectedName, Class<?>... parameterTypes) {
        return findMethodInImpl(clazz, returnType, expectedName, parameterTypes);
    }

    public Method findMethod(Class<?> returnType, String expectedName, Class<?>... parameterTypes)
            throws NoSuchMethodException {

        for (var clazz : classes.values()) {
            var found = findMethodInImpl(clazz, returnType, expectedName, parameterTypes);
            if (found != null)
                return found;
        }

        throw new NotImplementedException(null, "Could not find method : " + expectedName);
    }

    private Method findMethodInImpl(Class<?> clazz, Class<?> returnType, String expectedName,
            Class<?>... parameterTypes) {

        // 1. Versuch: Direkter Match
        try {
            var method = clazz.getMethod(expectedName, parameterTypes);
            if (method != null)
                return method;
        } catch (NoSuchMethodException e) {
            // Ignored, try partial match below
        }

        // 2. Versuch
        Outer: for (var method : clazz.getDeclaredMethods()) {
            if (!match(returnType, method.getReturnType()))
                continue;

            var actParamTypes = method.getParameterTypes();
            if (actParamTypes.length != parameterTypes.length)
                continue;

            for (int i = 0; i < parameterTypes.length; i++) {
                if (!match(actParamTypes[i], parameterTypes[i]))
                    break Outer;
            }

            return method;
        }

        return null;
    }

    private static boolean match(Class<?> target, Class<?> source) {

        if (target.isAssignableFrom(source))
            return true;

        try {
            var type = target.getDeclaredField("TYPE");
            var pt = (Class<?>) type.get(null);
            if (pt != null && match(pt, source))
                return true;

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {}

        try {
            var type = source.getDeclaredField("TYPE");
            var pt = (Class<?>) type.get(null);
            if (pt != null && match(target, pt))
                return true;

        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {}

        return false;
    }

	public Map<Path, Class<?>> getClasses() {
		return this.classes;
	}

    public String findName(Class<?> returnType, String expectedName, Class<?>... parameterTypes)
            throws NoSuchMethodException {

        for (var clazz : classes.entrySet()) {
            var found = findMethodInImpl(clazz.getValue(), returnType, expectedName, parameterTypes);
            if (found != null)
                return clazz.getKey().toString();
        }

        throw new NoSuchMethodException("Could not find " + expectedName);
    }
}
