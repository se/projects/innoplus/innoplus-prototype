import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.junit.platform.commons.annotation.Testable;
import org.opentest4j.AssertionFailedError;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public class TestResultExtractor implements TestWatcher {

    private int testRun = 0;
    private int testMethods;

    // We use this for checking if something is part of the student code
    private final JavaClasses studentSolution;

    public TestResultExtractor() throws Exception{
        var input = Paths.get(".", "input").toAbsolutePath().normalize();
        this.studentSolution = new JavaClasses(input);
    }

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        checkFirst(context);
        writeToXml(
                context.getTestClass().get().getSimpleName(),
                context.getTestMethod().get().getName(),
                "No Implementation for Tested Methods found",
                "",
                "::-:",
                "Disabled"
        );

        checkLast(context);
    }


    @Override
    public void testSuccessful(ExtensionContext context) {
        checkFirst(context);
        checkLast(context);
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        checkFirst(context);
        if (cause instanceof NotImplementedException){
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    "Constructor / Method : " + ((NotImplementedException) cause).method + " is not implemented.",
                    "Constructor / Method : " + ((NotImplementedException) cause).method + " is not implemented.",
                    "::-:",
                    // ((NotImplementedException) cause).clazz == null ? "::-:" :
                    //         ((NotImplementedException) cause).clazz.getSimpleName() + "::-:" ,
                    "Disabled"
            );
        }
        else {
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    "No Implementation found for Method: " + context.getTestMethod().get().getName(),
                    "",
                    "::-:",
                    "Disabled"
            );
        }
        checkLast(context);
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        checkFirst(context);

        int lastError = -1;
        var stackTrace = cause.getStackTrace();
        var studentErrors = new ArrayList<StackTraceElement>();
        for (int i = 0; i < stackTrace.length; i++) {
            try {
                // We have to remove package Info from the Stacktrace Classname
                var sCName = stackTrace[i].getClassName().split("\\.");
                if(studentSolution.findClass(sCName[sCName.length - 1]) != null){
                    lastError = i;
                    studentErrors.add(stackTrace[i]);
                }
            } catch (Exception e) {
            }
        }

        String trace;
        var trimmedST = Arrays.copyOfRange(stackTrace, 0, lastError == -1 ? 0 : lastError + 1);
        if (studentErrors.isEmpty()){
            trace = "::-:";
        }
        else {
            var cName = studentErrors.get(0).getClassName().replaceAll("\\.", "\\/");
            var index = cName.indexOf("$");
            cName = index == -1 ? cName : cName.substring(0, index);
            trace = cName + ".java" + ":" + studentErrors.get(0).getLineNumber() + ":-:";
        }

        if(cause instanceof AssertionFailedError){
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    context.getDisplayName(),
                    cause.getMessage().contains("==>") ? cause.getMessage().substring(0, cause.getMessage().indexOf("==>")) : cause.getMessage(),
                    trace,
                    "AssertionError");
        }
        else {
            var description = cause + "\n" +Arrays.stream(trimmedST)
                    .map(s -> s.toString())
                    .collect(Collectors.joining("\n"));
            writeToXml(
                    context.getTestClass().get().getSimpleName(),
                    context.getTestMethod().get().getName(),
                    context.getDisplayName(),
                    description,
                    trace,
                    "Exception");
        }
        // TODO StackTrace till last Student Error

        checkLast(context);
    }

    // Util Methods
    private static void writeToXml(String filename, String testName, String summary, String description, String trace,
                                   String result)
    {
        try(var writer = new BufferedWriter(new FileWriter("junit-reports/report.xml", true))) {
            writer.write("    <testcase>" + testName + "\n");
            writer.write("        <result>" + result + "</result>\n");
            writer.write("        <summary>" + summary +  "</summary>" + "\n");
            writer.write("        <description>" + description + "</description>" + "\n");
            writer.write("        <trace>" + trace + "</trace>" + "\n");
            writer.write("    </testcase>" + "\n");
        } catch (IOException ignored) { }
    }

    private void checkFirst(ExtensionContext context){
        if (testRun != 0) return;
        testMethods = (int) Arrays.stream(context.getTestClass().get().getDeclaredMethods())
                .filter(method -> Arrays.stream(method.getAnnotations())
                        .anyMatch(annotation -> annotation.annotationType().isAnnotationPresent(Testable.class))
                ).count();
        try(var writer = new BufferedWriter(
                new FileWriter("junit-reports/report.xml")))
        {
            writer.write("<report>\n");
        } catch (IOException ignored){}
    }

    private void checkLast(ExtensionContext context){
        if (++testRun >= testMethods)
            try(var writer = new BufferedWriter(
                    new FileWriter("junit-reports/report.xml", true)))
            {
                writer.write("</report>");
            }
            catch (IOException ignored){ }
    }
}
