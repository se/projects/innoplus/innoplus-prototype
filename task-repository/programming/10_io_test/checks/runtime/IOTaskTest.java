import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class IOTaskTest {

    TextDocument[] docs;
    @Test
    public void testXmlReadWrite() throws Exception{
        createDocuments();
        for (TextDocument doc : docs) {
            Document xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            var tDoc = textDocumentCtor.create();
            tDoc.save(xml, doc);
            TextDocument doc2 = tDoc.load(xml);
            assertEquals(doc, doc2, "XML READ / write is not implemented correctly");
        }
    }

    @Test
    public void testDataReadWrite() throws Exception{
        createDocuments();
        for (var doc : docs){
            try (var out = new ByteArrayOutputStream()){
                var tDoc = textDocumentCtor.create();
                tDoc.save(new ObjectOutputStream(out), doc);
                ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
                TextDocument doc2 = tDoc.load(new ObjectInputStream(in));
                assertEquals(doc, doc2, "");
            }
        }
    }

    @Test
    public void testObjectReadWrite() throws Exception{
        createDocuments();
        for (var doc : docs){
            try (var out = new ByteArrayOutputStream()){
                var tDoc = textDocumentCtor.create();
                tDoc.save(new DataOutputStream(out), doc);
                var in = new ByteArrayInputStream(out.toByteArray());
                TextDocument doc2 = tDoc.load(new DataInputStream(in));
                assertEquals(doc, doc2, "");
            }
        }
    }

    private void createDocuments(){
        if (docs == null){
            docs = new TextDocument[]{
                    createDocument(5, 3, 2),
                    createDocument(3, 5, 1),
                    createDocument(4, 2, 4)
            };
        }
    }

    private TextDocument createDocument(int sections, int paragraphs, int depth) {
        TextDocument doc = textDocumentCtor.create();
        doc.setTitle("Document Title");
        createSections(doc, sections, paragraphs, depth);
        return doc;
    }

    private void createSections(Section parent, int sections, int paragraphs, int depth) {
        if (depth == 0) {
            return;
        }
        for (int s = 0; s < sections; s++) {
            Section sec = sectionCtor.create();
            sec.setTitle((s+1) + ". Title");
            createSections(sec, sections, paragraphs, depth - 1);
            for (int p = 0; p < paragraphs; p++) {
                Paragraph para = paragraphCtor.create();
                para.setText("Text");
                sec.add(para);
            }
            parent.add(sec);
        }
    }

    // Student Interface Stuff
    @StudentClass
    public interface TextDocument extends Section {

        @StudentMethod
        public void save(DataOutputStream out, TextDocument doc);

        @StudentMethod
        public TextDocument load(DataInputStream in) throws IOException;

        @StudentMethod
        public void save(ObjectOutputStream out, TextDocument doc) throws IOException;

        @StudentMethod
        public TextDocument load(ObjectInputStream in) throws ClassNotFoundException, IOException;

        @StudentMethod
        public void save(Document xml, TextDocument doc);

        @StudentMethod
        public TextDocument load(Document xml);

        @StudentMethod
        public String toString();
    }

    StudentConstructor<TextDocument> textDocumentCtor;

    @StudentClass
    public interface Content extends Serializable {

        @StudentMethod
        public boolean isSection();

        @StudentMethod
        public abstract boolean isParagraph();

        @StudentMethod
        public abstract Section asSection();

        @StudentMethod
        public abstract Paragraph asParagraph();

        @StudentMethod
        public Section getParent();

        @StudentMethod
        public void setParent(Section parent);

        @StudentMethod
        public String getPath();

        @StudentMethod
        public void print();
    }
    StudentConstructor<Content> contentCtor;

    @StudentClass
    public interface Section extends Content {

        static final long serialVersionUID = 1L;

        @StudentMethod
        public String getTitle();

        @StudentMethod
        public void load(Element secElement, Section sec);

        @StudentMethod
        public void save(Element secNode, Section sec, Document xml);

        @StudentMethod
        public void load(DataInputStream in, Section sec) throws IOException;

        @StudentMethod
        public void save(DataOutputStream out, Section sec) throws IOException;

        @StudentMethod
        public void setTitle(String title);

        @StudentMethod
        public List<Content> getContent();

        @StudentMethod
        public void add(Content c);

        @StudentMethod
        public void remove(Content c);

        @StudentMethod
        public boolean isSection();

        @StudentMethod
        public boolean isParagraph();

        @StudentMethod
        public Section asSection();

        @StudentMethod
        public Paragraph asParagraph();

        @StudentMethod
        int getPosition(Content c);

        @StudentMethod
        @Override
        public String toString();

        @StudentMethod
        @Override
        public int hashCode();


        @StudentMethod
        @Override
        public boolean equals(Object obj);

    }

    StudentConstructor<Section> sectionCtor;

    @StudentClass
    public interface Paragraph extends Content {

        @StudentMethod
        public String getText();

        @StudentMethod
        public void load(Element paraNode, Paragraph para);

        @StudentMethod
        public void save(Element paraNode, Paragraph para, Document xml);

        @StudentMethod
        public void load(DataInputStream in, Paragraph p) throws IOException;

        @StudentMethod
        public void save(DataOutputStream out, Paragraph para) throws IOException;

        @StudentMethod
        public void setText(String text);

        @StudentMethod
        public boolean isSection();

        @StudentMethod
        public boolean isParagraph();

        @StudentMethod
        public Section asSection();

        @StudentMethod
        public Paragraph asParagraph();

        @StudentMethod
        public String toString();

        @StudentMethod
        public int hashCode();

        @StudentMethod
        public boolean equals(Object obj);
    }

    StudentConstructor<Paragraph> paragraphCtor;
}
