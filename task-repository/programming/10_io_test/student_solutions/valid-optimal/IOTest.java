import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/*
Input/Output
Gegeben ist eine Klassenstruktur fuer Text-Dokumente.
Implementieren Sie die drei save- sowie die drei load-Methoden, so dass das aktuelle Objekt in seiner vollstaendigen Objektstruktur gespeichert und wieder geladen werden kann.
Sie duerfen die gegebenen Klassen nicht veraendern, abgesehen von zusaetzlichen Hilfsmethoden, die Sie bei Bedarf hinzufuegen duerfen.
 */
public class IOTest {
	
	public static void main(String[] args) {
		TextDocument[] docs = { createDocument1(), createDocument2(), createDocument3() };
		for (TextDocument doc : docs) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			try {
				TextDocument.save(new DataOutputStream(out), doc);
				ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
				TextDocument doc2 = TextDocument.load(new DataInputStream(in));
				System.out.println(doc.equals(doc2));
			} catch (IOException e) { }
			out = new ByteArrayOutputStream();
			try {
				TextDocument.save(new ObjectOutputStream(out), doc);
				ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
				TextDocument doc2 = TextDocument.load(new ObjectInputStream(in));
				System.out.println(doc.equals(doc2));
			} catch (IOException | ClassNotFoundException e) { }
			try {
				Document xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
				TextDocument.save(xml, doc);
				TextDocument doc2 = TextDocument.load(xml);
				System.out.println(doc.equals(doc2));
			} catch (ParserConfigurationException e) { }
		}
	}
	
	public static TextDocument createDocument1() {
		return createDocument(5, 3, 2);
	}
	
	public static TextDocument createDocument2() {
		return createDocument(3, 5, 1);
	}
	
	public static TextDocument createDocument3() {
		return createDocument(4, 2, 7);
	}
	
	protected static TextDocument createDocument(int sections, int paragraphs, int depth) {
		TextDocument doc = new TextDocument();
		doc.title = "Document Title";
		createSections(doc, sections, paragraphs, depth);
		return doc;
	}

	private static void createSections(Section parent, int sections, int paragraphs, int depth) {
		if (depth == 0) {
			return;
		}
		for (int s = 0; s < sections; s++) {
			Section sec = new Section();
			sec.title = (s+1) + ". Title";
			createSections(sec, sections, paragraphs, depth - 1);
			for (int p = 0; p < paragraphs; p++) {
				Paragraph para = new Paragraph();
				para.text = "Text";
				sec.add(para);
			}
			parent.add(sec);
		}
	}

	public static class TextDocument extends Section {
		
		private static final long serialVersionUID = 1L;
		
		public static void save(DataOutputStream out, TextDocument doc) throws IOException {
			// sample solution
			Section.save(out, doc);
		}
		
		public static TextDocument load(DataInputStream in) throws IOException {
			// sample solution
			TextDocument doc = new TextDocument();
			Section.load(in, doc);
			return doc;
		}
		
		public static void save(ObjectOutputStream out, TextDocument doc) throws IOException {
			// sample solution
			out.writeObject(doc);
		}
		
		public static TextDocument load(ObjectInputStream in) throws ClassNotFoundException, IOException {
			// sample solution
			return (TextDocument) in.readObject();
		}
		
		public static void save(Document xml, TextDocument doc) {
			// sample solution
			Element docNode = xml.createElement("document");
			xml.appendChild(docNode);
			Section.save(docNode, doc, xml);
		}
		
		public static TextDocument load(Document xml) {
			// sample solution
			TextDocument doc = new TextDocument();
			Section.load(xml.getDocumentElement(), doc);
			return doc;
		}
		
		@Override
		public String toString() {
			return "document";
		}

	}
	
	public static abstract class Content implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		protected Section parent;
		
		public abstract boolean isSection();
		public abstract boolean isParagraph();
		public abstract Section asSection();
		public abstract Paragraph asParagraph();
		
		public Section getParent() {
			return parent;
		}
		
		public void setParent(Section parent) {
			this.parent = parent;
		}
		
		public String getPath() {
			if (parent == null) {
				return "/" + toString();
			}
			return parent.getPath() + "/" + toString() + "[" + parent.getPosition(this) + "]";
		}
		
		public void print() {
			System.out.print(getPath());
			if (isSection()) {
				System.out.println(" '" + asSection().title + "'");
			} else if (isParagraph()) {
				System.out.println(" '" + asParagraph().text + "'");
			} else {
				System.out.println();
			}
			if (isSection()) {
				for (Content c : asSection().getContent()) {
					c.print();
				}
			}
		}
		
	}
	
	public static class Section extends Content {
		
		private static final long serialVersionUID = 1L;

		private static final int TYPE_SECTION = 0;
		private static final int TYPE_PARAGRAPH = 1;
		
		private static final String XML_SECTION = "section";
		private static final String XML_PARAGRAPH = "paragraph";
		private static final String XML_TITLE = "title";
		
		protected String title;
		protected List<Content> content = new ArrayList<>();
		
		public String getTitle() {
			return title;
		}
		
		// sample solution
		public static void load(Element secElement, Section sec) {
			sec.title = secElement.getAttribute(XML_TITLE);
			NodeList nodes = secElement.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				if (nodes.item(i) instanceof Element) {
					Element childNode = (Element) nodes.item(i);
					if (childNode.getNodeName().equals(XML_SECTION)) {
						Section s = new Section();
						Section.load(childNode, s);
						sec.add(s);
					} else if (childNode.getNodeName().equals(XML_PARAGRAPH)) {
						Paragraph p = new Paragraph();
						Paragraph.load(childNode, p);
						sec.add(p);
					}
				}
			}
		}

		// sample solution
		public static void save(Element secNode, Section sec, Document xml) {
			secNode.setAttribute(XML_TITLE, sec.title);
			for (Content c : sec.content) {
				if (c.isSection()) {
					Element childNode = xml.createElement(XML_SECTION);
					Section.save(childNode, c.asSection(), xml);
					secNode.appendChild(childNode);
				} else if (c.isParagraph()) {
					Element childNode = xml.createElement(XML_PARAGRAPH);
					Paragraph.save(childNode, c.asParagraph(), xml);
					secNode.appendChild(childNode);
				}
			}
		}

		// sample solution
		public static void load(DataInputStream in, Section sec) throws IOException {
			sec.title = in.readUTF();
			int size = in.readInt();
			for (int i = 0; i < size; i++) {
				int type = in.readInt();
				if (type == TYPE_SECTION) {
					Section s = new Section();
					Section.load(in, s);
					sec.add(s);
				} else if (type == TYPE_PARAGRAPH) {
					Paragraph p = new Paragraph();
					Paragraph.load(in, p);
					sec.add(p);
				}
			}
		}

		// sample solution
		public static void save(DataOutputStream out, Section sec) throws IOException {
			out.writeUTF(sec.title);
			out.writeInt(sec.content.size());
			for (Content c : sec.content) {
				if (c.isSection()) {
					out.writeInt(TYPE_SECTION);
					Section.save(out, c.asSection());
				} else if (c.isParagraph()) {
					out.writeInt(TYPE_PARAGRAPH);
					Paragraph.save(out, c.asParagraph());
				}
			}
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public List<Content> getContent() {
			return Collections.unmodifiableList(content);
		}
		
		public void add(Content c) {
			if (c.parent != null) {
				c.parent.remove(c);
			}
			content.add(c);
			c.parent = this;
		}
		
		public void remove(Content c) {
			content.remove(c);
			c.parent = null;
		}

		@Override
		public boolean isSection() {
			return true;
		}

		@Override
		public boolean isParagraph() {
			return false;
		}
		
		@Override
		public Section asSection() {
			return this;
		}

		@Override
		public Paragraph asParagraph() {
			return null;
		}

		protected int getPosition(Content c) {
			for (int pos = 0; pos < content.size(); pos++) {
				if (content.get(pos) == c) {
					return pos;
				}
			}
			return -1;
		}

		@Override
		public String toString() {
			return "section";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((content == null) ? 0 : content.hashCode());
			result = prime * result + ((title == null) ? 0 : title.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Section other = (Section) obj;
			if (content == null) {
				if (other.content != null)
					return false;
			} else if (!content.equals(other.content))
				return false;
			if (title == null) {
				if (other.title != null)
					return false;
			} else if (!title.equals(other.title))
				return false;
			return true;
		}

	}
	
	public static class Paragraph extends Content {
		
		private static final long serialVersionUID = 1L;
		
		protected String text;

		public String getText() {
			return text;
		}

		// sample solution
		public static void load(Element paraNode, Paragraph para) {
			para.text = paraNode.getTextContent();
		}

		// sample solution
		public static void save(Element paraNode, Paragraph para, Document xml) {
			paraNode.setTextContent(para.text);
		}

		// sample solution
		public static void load(DataInputStream in, Paragraph p) throws IOException {
			p.text = in.readUTF();
		}

		// sample solution
		public static void save(DataOutputStream out, Paragraph para) throws IOException {
			out.writeUTF(para.text);
		}

		public void setText(String text) {
			this.text = text;
		}

		@Override
		public boolean isSection() {
			return false;
		}

		@Override
		public boolean isParagraph() {
			return true;
		}
		
		@Override
		public Section asSection() {
			return null;
		}

		@Override
		public Paragraph asParagraph() {
			return this;
		}

		@Override
		public String toString() {
			return "paragraph";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Paragraph other = (Paragraph) obj;
			if (text == null) {
				if (other.text != null)
					return false;
			} else if (!text.equals(other.text))
				return false;
			return true;
		}

	}

}
