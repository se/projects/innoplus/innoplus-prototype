# Aufgabe zum Verbinden sortierter Arrays

Bei dieser Aufgabe[1] sollen die Studenten eine Sternendatenbank realisieren.
Dafür mussteN die folgenden Klassen implementiert werden:

---

### `Star`

Diese Klasse repräsentiert einen einzelnen Stern mit den folgenden Eigenschaften:

- Attribute:
  * Name (`name`)
  * ID (`id`)
  * Distanz zum Stern (`distance`)
  * Scheinbare Helligkeit (`apparentMagnitude`)
  * Typ (`type`)

- Konstruktoren
  * Default
  * Name + ID

- Methoden
  - Getter/Setter

---

### `StarDatabase`

Diese Klasse verwaltet eine Menge von Sternen über die folgenden
Methoden:

```java
// Fügt einen neuen Stern hinzu
public void add(Star star);

// Entfernt den Stern am gegebenen Index
public void remove(int index);

// Liefert den Stern am gegebenen Index
public Star get(int index);

// Liefert die Anzahl der Sterne
public int size();
```

Außerdem sollen die folgenden Anfragen[2] möglich sein:


```java
// Findet den Stern mit gegebener ID (wenn vorhanden, ansonsten null)
public Star find(String id);

// Liefert alle Sterne mit eine scheinbaren Helligkeit in [low, high]
public Star[] getMagnitudeRange(double low, double high);
```

[1] Aufgabe gekürzt aus der Vorlesung Programmierung, Datenstrukturen &
    Algorithmen übernommen

[2] die Anfragen wurden eigentlich in einer vorhergehenden Aufgabe auf Arrays
    implementiert und mussten hier nur auf die Klasse übertragen werden

## Ordnerstruktur

Die zugehörigen Dateien sind wie folgt verteilt:

- `tests` enthält JUnit5-Tests, mit denen die Korrektheit studentischer
  Lösungen überprüft werden kann.
- `student_solutions` enthält mehrere Unterordner, in denen mögliche
  Lösungen von Studenten implementiert wurden.
