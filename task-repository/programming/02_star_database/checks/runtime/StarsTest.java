import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;




@ExtendWith({StudentSolutionLoader.class , TestResultExtractor.class })
public class StarsTest {

        @StudentClass
        public interface Star {

            public String getName();

            public void setName(String name);

            public String getId();

            public void setId(String id);

            public String getType();

            public void setType(String type);

            public double getDistance();

            public void setDistance(double distance);

            public double getApparentMagnitude();

            public void setApparentMagnitude(double apparentMagnitude);
        }

        public static StudentConstructor<Star> starCtor;

        @StudentClass
        public interface StarDatabase {

            public Star createStar();

	    public Star createStar(String s1, String s2);

            /** Fügt einen neuen Stern hinzu **/
            public void add(Star star);

            /** Entfernt den Stern am gegebenen Index **/
            public void remove(int index);

            /** Liefert den Stern am gegebenen Index **/
            public Star get(int index);

            /** Liefert die Anzahl der Sterne **/
            public int size();

            /** Findet den Stern mit gegebener ID (wenn vorhanden, ansonsten null) **/
            public Star find(String id);

            /** Liefert alle Sterne mit eine scheinbaren Helligkeit in [low, high] **/
            public StudentArray<Star> getMagnitudeRange(double low, double high);

            public Star select(boolean cond, Star a, Star b);

			public void addAll(StudentArray<Star> star);

			public StudentArray<StudentArray<StudentArray<Star>>> getNested(Star s);

			public void acceptNested(StudentArray<StudentArray<StudentArray<Star>>> s);
        }

        public static StudentConstructor<StarDatabase> starDatabaseCtor;


	@DisplayName("Initial configuration was not correct.")
	@Test
	public void testDefault() {
		StarDatabase db = starDatabaseCtor.create();
		assertEquals(0, db.size(), "There should be no Stars inside the StarDatabase directly after initialization.");
		assertNull(db.find("DoesNotExist"), "Search returned a Star that should not be present after " +
				"initialization of the StarDatabase.");
	}

	@DisplayName("Inserting Stars into the StarDatabase failed.")
	@Test
	public void testInsert() {
		StarDatabase db = starDatabaseCtor.create();
		Star s0 = starCtor.create("Pluto", "Imposter1");
		Star s1 = starCtor.create("Venus", "<0> <0>");

		db.add(s0);
		assertEquals(1, db.size(), "The size of StarDatabase is wrong after insertion of a Star.");
		assertEquals(s0, db.get(0), "The inserted Star was not found.");

		db.add(s1);
		assertEquals(2, db.size(), "The size of StarDatabase is wrong after insertion of two Stars.");
		assertEquals(s0, db.get(0), "The first inserted Star was not found after insertion of second Star.");
		assertEquals(s1, db.get(1), "The second inserted Star was not found.");
	}

	@DisplayName("Removing Stars from the StarDatabase failed.")
	@Test
	public void testRemove() {
		StarDatabase db = starDatabaseCtor.create();
		Star s0 = starCtor.create("Pluto", "Imposter1");
		Star s1 = starCtor.create("Venus", "<0> <0>");
		Star s2 = starCtor.create("Mars", "Red");

		db.add(s0);
		db.add(s1);
		db.add(s2);

		db.remove(1);
		assertEquals(2, db.size(), "The size of StarDatabase is wrong after deletion of a Star.");
		assertEquals(s0, db.get(0), "The wrong Star was removed from the StarDatabase.");
		assertEquals(s2, db.get(1), "The wrong Star was removed from the StarDatabase.");

		db.remove(0);
		assertEquals(1, db.size(), "The size of StarDatabase is wrong after deletion of two Stars.");
		assertEquals(s2, db.get(0), "The wrong Star was removed from the StarDatabase.");
	}

	@DisplayName("Searching for Stars failed.")
	@Test
	public void testFind() {
		StarDatabase db = starDatabaseCtor.create();
		Star s0 = starCtor.create("Pluto", "Imposter1");
		Star s1 = starCtor.create("Venus", "<0> <0>");
		Star s2 = starCtor.create("Mars", "Red");

		assertNull(db.find(s1.getId()), "find(id) returned a Star even though the StarDatabase should be empty.");

		db.add(s0);
		db.add(s1);
		db.add(s2);

		assertEquals(s1, db.find(s1.getId()), "find(id) for a Star returned the wrong Star.");
		assertNull(db.find("DoesNotExist"), "find(id) found a Star that should not be in StarDatabase.");
	}

	@DisplayName("Searching Stars by magnitude failed.")
	@Test
	public void testGetMagnitudeRange() {
		StarDatabase db = starDatabaseCtor.create();
		Star s0 = starCtor.create("Pluto", "Imposter1");
		Star s1 = starCtor.create("Venus", "<0> <0>");
		Star s2 = starCtor.create("Mars", "Red");

		s0.setApparentMagnitude(11);
		s1.setApparentMagnitude(200);
		s2.setApparentMagnitude(4000);

		assertEquals(List.of(), db.getMagnitudeRange(0, 1_000_000),
				"getMagnitudeRange(low,high) returned Star(-s) even though the StarDatabase should be empty.");

		db.add(s0);
		db.add(s1);
		db.add(s2);

		assertEquals(List.of(s0, s1, s2), db.getMagnitudeRange(0, 1_000_000),
				"getMagnitudeRange(low,high) returned wrong number of Stars.");

		assertEquals(List.of(s0, s1), db.getMagnitudeRange(0, 1_000),
				"getMagnitudeRange(low,high) returned wrong number of Stars.");

		assertEquals(List.of(s2), db.getMagnitudeRange(1_000, 999_999),
				"getMagnitudeRange(low,high) returned wrong number of Stars.");
	}

}
