// Gültige Implementierungen für die Verwaltung des Arrays,
// aber es wird mit `null` initialsiert => NPE
import java.util.ArrayList;

public class StarDatabase {
	private Star[] stars;

	/**Fügt einen neuen Stern hinzu **/
	public void add(Star star) {
		Star[] res = new Star[this.stars.length + 1];
		for (int i = 0; i < this.stars.length; i++)
			res[i] = stars[i];

		res[stars.length] = star;
		this.stars = res;
	}

	/** Entfernt den Stern am gegebenen Index **/
	public void remove(int index) {
		Star[] res = new Star[this.stars.length - 1];

		for (int i = 0, cur = 0; i < this.stars.length; i++) {
			if (i != index)
				res[cur++] = stars[i];
		}

		this.stars = res;
	}

	/** Liefert den Stern am gegebenen Index **/
	public Star get(int index) {
		return this.stars[index];
	}

	/** Liefert die Anzahl der Sterne **/
	public int size() {
		return this.stars.length;
	}

	/** Findet den Stern mit gegebener ID (wenn vorhanden, ansonsten null) **/
	public Star find(String id) {
		for (Star star : this.stars) {
			if (star.getId().equals(id))
				return star;
		}
		return null;
	}

	/** Liefert alle Sterne mit eine scheinbaren Helligkeit in [low, high] **/
	public Star[] getMagnitudeRange(double low, double high) {
		int count = 0;

		for (Star star : this.stars) {
			double mag = star.getApparentMagnitude();
			if (low <= mag && mag <= high)
				count++;
		}

		Star[] result = new Star[count];
		count = 0;

		for (Star star : this.stars) {
			double mag = star.getApparentMagnitude();
			if (low <= mag && mag <= high)
				result[count++] = star;
		}

		return result; // <= Accidental typo
	}
}
