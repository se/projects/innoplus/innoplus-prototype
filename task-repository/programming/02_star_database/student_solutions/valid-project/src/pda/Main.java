package pda;

public class Main {

    public static void main(String[] args) {
	    StarDatabase db = new StarDatabase();
	    db.add(new Star("Venus", "v3n", "planet", 3.2, 3.2));
        db.add(new Star("Mars", "m0rs", "big-planet", 6.3, 10.5));

        System.out.println("Size = " + db.size());
        System.out.println(db.get(0).getName());
        System.out.println(db.get(1).getName());
    }
}
