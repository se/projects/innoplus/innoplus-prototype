// Optimal valid solution
public class Star {
	private String name, id, type;
	private double distance, apparentMagnitude;

	public Star() {}

	public Star(String name, String id) {
		this.name = name;
		this.id = id;
	}

	public Star(String name, String id, String type, double distance, double apparentMagnitude) {
		this.name = name;
		this.id = id;
		this.type = type;
		this.distance = distance;
		this.apparentMagnitude = apparentMagnitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getApparentMagnitude() {
		return apparentMagnitude;
	}

	public void setApparentMagnitude(double apparentMagnitude) {
		this.apparentMagnitude = apparentMagnitude;
	}
}
