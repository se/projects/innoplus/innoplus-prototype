// "Partially valid" solution that differs from the method
// signature and prints the result instead of returning it
public class Sorting {

    public static void merge(int[] a, int[] b) {

        int[] res = new int[a.length + b.length];
        int i = 0;
        int j = 0;

        for (int k = 0; k < res.length; k++) {

            if (i == a.length) {
                res[k] = b[j++];
            } else if (j == b.length) {
                res[k] = a[i++];
            } else if (a[i] < b[j]) {
                res[k] = b[j++];
            } else {
                res[k] = a[i++];
            }
        }

        for (int num : res) {
            System.out.print(res);
            System.out.print(' ');
        }
        System.out.println();
    }
}
