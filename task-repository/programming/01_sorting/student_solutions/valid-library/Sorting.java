// Same approach as valid-ignoring-sortedness but
// using library methods instead of manual loops, ...
public class Sorting {

    public static int[] merge(int[] a, int[] b) {
        int[] res = new int[a.length + b.length];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        java.util.Arrays.sort(res);
        return res;
    }
}
