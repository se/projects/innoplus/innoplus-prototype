// Optimal valid solution
public class Aufgabe1 {

    public static int[] merge(int[] a, int[] b) {

        int[] res = new int[a.length + b.length];
        int i = 0;
        int j = 0;

        for (int k = 0; k < res.length; k++) {

            if (i == a.length) {
                res[k] = b[j++];
            } else if (j == b.length) {
                res[k] = a[i++];
            } else if (a[i] < b[j]) {
                res[k] = a[i++];
            } else {
                res[k] = b[j++];
            }
        }

        return res;
    }
}
