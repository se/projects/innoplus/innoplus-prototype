package work;
// Common wrong solution which creates a valid res
// for the example in the exercise but causes an
// IndexOutOfBoundsException for others.
public class Sorting {

    public static int[] merge(int[] a, int[] b) {

        int[] res = new int[a.length + b.length];
        int i = 0;
        int j = 0;

        for (int k = 0; k < res.length; k++) {

            // Wrong assumption: j always less than b.length
            if (i < a.length && a[i] < b[j]) {
                res[k] = a[i];
                i++;
            } else {
                res[k] = b[j];
                j++;
            }
        }

        return res;
    }
}
