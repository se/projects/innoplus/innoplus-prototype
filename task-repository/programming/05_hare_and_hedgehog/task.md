# Hase und Igel

Basierend auf einer alten Volkserzählung (https://de.wikipedia.org/wiki/Der_Hase_und_der_Igel)

Der Hase (engl. hare) fordert den Igel (engl. hedgehog) zu einem Wettlauf zwischen zwei Zielpfosten (eng. goal post) auf. Was der Hase nicht weiß: Es gibt zwei Igel, die nahezu identisch aussehen. So rennt der Hase von Ziel zu Ziel, aber einer der beiden Igel ist immer schon vor ihm da.

Schreiben Sie folgende drei Klassen, um diesen "Wettlauf" zu simulieren:

Eine Klasse Goal, die einen Namen und ein Folgeziel (das als nächstes angesteuert werden soll) speichert.

Eine Klasse Hare, die ein Ziel speichert und Runnable implementiert. Die run-Methode soll so lange, wie der Thread nicht unterbrochen wird, von einem zum nächsten Ziel laufen:
- Ausgabe "Hare: *running*"
- eine Sekunde warten
- Ausgabe: "Hare: I'm here at the <Name des Ziels> goal!"
- jemand anderen am gleichen Ziel auffordern, sich zu melden
- eine halbe Sekunden warten
- das Folgeziel des aktuellen Ziels als das nächste Ziel festlegen.

Eine Klasse Hedgehog, die ebenfalls ein Ziel speichert und Runnable implementiert. Die run-Methode soll so lange, wie der Thread nicht unterbrochen wird, an den gegebenen Ziel warten, bis eine Aufforderung kommt, sich zu melden. Sobald diese Aufforderung kommt, erfolgt die Ausgabe "Hedgehog: Neener-neener, I'm here already!" und wartet dann wieder.

Damit soll das folgende Testprogramm die danach beschriebene Ausgabe erzeugen:

```java
public class RunningGame {

	public static void main(String[] args) {
		new RunningGame().start();
	}
	
	public void start() {
		Goal left = new Goal("left");
		Goal right = new Goal("right");
		left.setOther(right);
		right.setOther(left);
		Thread[] threads = new Thread[] {
				new Thread(new Hare(left)),
				new Thread(new Hedgehog(left)),
				new Thread(new Hedgehog(right))
		};
		for (Thread thread : threads) {
			thread.start();
		}
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) { }
		for (Thread thread : threads) {
			thread.interrupt();
		}
	}

}
```
````
Hare: *running*
Hare: I'm here at the left goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the right goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the left goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the right goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the left goal!
Hedgehog: Neener-neener, I'm here already!
Hare: *running*
Hare: I'm here at the right goal!
Hedgehog: Neener-neener, I'm here already!
````

Ihre Implementierung soll aber auch mit anderen Szenarien funktionieren, beispielsweise auch mit mehr als zwei Zielen und Igeln.
