public class Goal {

    private String name;
    private Goal other;

    public Goal(String name) {
        this.name = name;
    }

    public void setOther(Goal other) {
        this.other = other;
    }

    public Goal getOther() {
        return other;
    }

    public String getName() {
        return name;
    }
}