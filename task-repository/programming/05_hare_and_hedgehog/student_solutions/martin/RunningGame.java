public class RunningGame {

    public static void main(String[] args) {
        new RunningGame().start();
    }

    public void start() {
        Goal left = new Goal("left");
        Goal right = new Goal("right");
        left.setOther(right);
        right.setOther(left);
        Thread[] threads = new Thread[] {
                new Thread(new Hare(left)),
                new Thread(new Hedgehog(left)),
                new Thread(new Hedgehog(right))
        };
        for (Thread thread : threads) {
            thread.start();
        }
        try {
            Thread.sleep(9000);
        } catch (InterruptedException e) { }
        for (Thread thread : threads) {
            thread.interrupt();
        }
    }

}