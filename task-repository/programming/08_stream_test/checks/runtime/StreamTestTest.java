import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

@org.junit.jupiter.api.extension.ExtendWith({StudentSolutionLoader.class, TestResultExtractor.class })
public class StreamTestTest {

    @StudentClass
    public interface AquaticAnimal {
        int getFins();

        void setFins(int fins);

        int getLimbs();

        void setLimbs(int limbs);

        int getTentacles();

        void setTentacles(int tentacles);

        int getEyes();

        void setEyes(int eyes);
    }

    @StudentClass
    public interface Squid extends AquaticAnimal {
        @Override
        String toString();
    }

    @StudentClass
    public interface StreamTest {
        Stream<AquaticAnimal> createStream1();

        Stream<AquaticAnimal> createStream2();

        Stream<AquaticAnimal> createStream3();

        Stream<AquaticAnimal> filterFins(Stream<AquaticAnimal> stream);

        Stream<Squid> filterSquids(Stream<AquaticAnimal> stream);

        double averageAppendages(Stream<AquaticAnimal> stream);

        String filterLimbs(Stream<AquaticAnimal> stream);

        int countEyes(Stream<AquaticAnimal> stream);
    }

    static StudentConstructor<StreamTest> stCtor;


    @DisplayName("Testing the filterFins() function")
    @Test
    void testFilterFins() {
        final var st = stCtor.create();

        final var fins1 = st.filterFins(st.createStream1()).count();
        testFilterFunction(3, fins1, "filterFins()", "createStream1()");

        final var fins2 = st.filterFins(st.createStream2()).count();
        testFilterFunction(15, fins2, "filterFins()", "createStream2()");

        final var fins3 = st.filterFins(st.createStream3()).count();
        testFilterFunction(30, fins3, "filterFins()", "createStream3()");
    }

    @DisplayName("Testing the filterSquids() function")
    @Test
    void testFilterSquids() {
        final var st = stCtor.create();

        final var squids1 = st.filterSquids(st.createStream1()).count();
        testFilterFunction(1, squids1, "filterSquids()", "createStream1()");

        final var squids2 = st.filterSquids(st.createStream2()).count();
        testFilterFunction(0, squids2, "filterSquids()", "createStream2()");

        final var squids3 = st.filterSquids(st.createStream3()).count();
        testFilterFunction(10, squids3, "filterSquids()", "createStream3()");
    }

    @DisplayName("Testing the averageAppendages() function")
    @Test
    void testAverageAppendages() {
        final var st = stCtor.create();

        assertEquals(5.0, st.averageAppendages(st.createStream1()),
                "The averageAppendages() function calculates a wrong average for createStream1()");

        assertEquals(4.035714285714286, st.averageAppendages(st.createStream2()),
                "The averageAppendages() function calculates a wrong average for createStream2()");

        assertEquals(5.0, st.averageAppendages(st.createStream3()),
                "The averageAppendages() function calculates a wrong average for createStream3()");
    }

    @DisplayName("Testing the filterLimbs() function")
    @Test
    void testFilterLimbs() {
        final var st = stCtor.create();

        final var limbs1 = st.filterLimbs(st.createStream1());
        testFilterLimbs("*****F", limbs1, "createStream1()");

        final var limbs2 = st.filterLimbs(st.createStream2());
        testFilterLimbs("*", limbs2, "createStream2()");

        final var limbs3 = st.filterLimbs(st.createStream3());
        testFilterLimbs("**************************************************FFFFFFFFFF", limbs3,
                "createStream3()");
    }

    @DisplayName("Testing the countEyes() function")
    @Test
    void testCountEyes() {
        final var st = stCtor.create();

        final var eyes1 = st.countEyes(st.createStream1());
        testCountEyes(10, 12, eyes1, "createStream1()");

        final var eyes2 = st.countEyes(st.createStream2());
        testCountEyes(18, 20, eyes2, "createStream2()");

        final var eyes3 = st.countEyes(st.createStream3());
        testCountEyes(0, 20, eyes3, "createStream3()");
    }

    /**
     * Helper function to test filter functions
     * @param expected expected number of elements after application of filter function
     * @param actual actual number of elements after application of filter function
     * @param functionName name of filter function that is tested
     * @param streamSupplier name of stream supplier function
     */
    private static void testFilterFunction(long expected, long actual, String functionName, String streamSupplier) {
        // test if to inaccurate
        assertFalse(actual < expected,
                String.format("The %s function filters too accurate for %s", functionName, streamSupplier));

        // test if to accurate
        assertFalse(actual > expected,
                String.format("The %s function filters too inaccurate for %s", functionName, streamSupplier));
    }

    /**
     * Helper function to test the filterLimbs function
     * @param expected expected output string of filterLimbs function
     * @param actual actual output string of filterLimbs function
     * @param streamSupplier name of stream supplier function
     */
    private static void testFilterLimbs(String expected, String actual, String streamSupplier) {
        // test filtering accuracy with length
        testFilterFunction(expected.length(), actual.length(), "filterLimbs()", streamSupplier);

        // test filtering representations
        final var wantedRepresentations = Arrays.stream(actual.split(""))
                .filter(s -> s.equals("*") || s.equals("F"));
        assertEquals(expected.length(), wantedRepresentations.count(),
                "The output of filterLimbs() contains String-Representations of " +
                        "AquaticAnimals that should be filtered out of stream from " + streamSupplier);

        // test ordering
        assertEquals(expected, actual,
                "The output of filterLimbs() from " + streamSupplier +
                        " seems not to be ordered alphabetically");
    }

    /**
     * Helper function to test the countEyes function
     * @param expectedMin number of minimal expected eyes
     * @param expectedMax number of maximal expected eyes
     * @param actual actual number of eyes
     * @param streamSupplier name of stream supplier function
     */
    private static void testCountEyes(long expectedMin, long expectedMax, long actual, String streamSupplier) {
        assertTrue(actual <= expectedMax,
                "The countEyes() function counts more eyes than expected for stream from " + streamSupplier);
        assertTrue(actual >= expectedMin,
                "The countEyes() function counts less eyes than expected for stream from " + streamSupplier);
        assertEquals(0, actual % 2,
                "The countEyes() function counts an uneven number of eyes for stream from " + streamSupplier +
                        " although all AquaticAnimals have an even number of eyes");
    }

    /*

    //mocking of streams does not work with current setup, so we have to trust student to not change
    // createStream[1-3]() functions

    public static <T> List<T> createTimes(Supplier<T> creator, int times) {
        List<T> out = new ArrayList<T>(times);
        for (int i = 0; i < times; i++) {
            out.add(creator.get());
        }
        return out;
    }

    public static AquaticAnimal createFish() {
        return aquaticAnimalCtor.create(4, 0, 0, 2);
    }

    public static AquaticAnimal createStarfish() {
        return aquaticAnimalCtor.create(0, 5, 0, 0);
    }

    public static AquaticAnimal createTurtle() {
        return aquaticAnimalCtor.create(0, 4, 0, 2);
    }

    public static Squid createSquid() {
        return squidCtor.create();
    }

    protected static Stream<AquaticAnimal> createStream(int fish, int starfish, int turtles, int squids) {
        List<AquaticAnimal> list = new ArrayList<>(fish + starfish + turtles + squids);
        list.addAll(createTimes(StreamTestTest::createFish, fish));
        list.addAll(createTimes(StreamTestTest::createStarfish, starfish));
        list.addAll(createTimes(StreamTestTest::createTurtle, turtles));
        list.addAll(createTimes(StreamTestTest::createSquid, squids));
        Collections.shuffle(list);
        return list.stream();
    }

    public static Stream<AquaticAnimal> createStream1() {
        return createStream(3, 5, 2, 1);
    }

    public static Stream<AquaticAnimal> createStream2() {
        return createStream(15, 1, 12, 0);
    }

    public static Stream<AquaticAnimal> createStream3() {
        return createStream(30, 50, 20, 10);
    }

     */
}
