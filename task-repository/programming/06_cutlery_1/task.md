# Besteck 1: Vererbung und Polymorphie

Schreiben Sie einen Java-Typen für Material, der Silber, rostfreien Stahl, Holz und Plastik (engl. silver, stainless steel, wood, plastic) repräsentieren kann.
Schreiben Sie ein Interface für Besteck (engl. Cutlery), mit einer getter- und einer setter-Methode für Material.
Schreiben Sie Java-Typen für Messer, Gabel, Löffel, Eßgabel, Eßlöffel, Kuchengabel und Teelöffel (engl. knife, fork, spoon, dinner fork, table spoon, pastry fork, tea spoon).
Schreiben Sie einen Java-Typen für Artefakte (engl. Artifact). Artefakte können nicht instanziiert werden, aber haben ein Material und die entsprechenden getter- und setter-Methoden dafür.
Überschreiben Sie die toString-Methode des Artefakt-Typs, sodass immer folgender Wert zurückgegeben wird: "[einfacher Name der Klasse] made from [Material]". Dabei sollen die beiden Werte in eckigen Klammern dynamisch ersetzt werden.
Schreiben Sie drei Klassen für Steakmesser (engl. steak knife), dreizinkige Kuchengabeln (engl. three-pronged pastry fork) und Spork (eine Kombination aus Löffel/Spoon und Gabel/Fork). Verwenden Sie dabei die bisher definierten Typen. Passen Sie ggf. die Definition dieser Typen an.
Die u.a. main-Methode soll nun folgende Ausgabe erzeugen:
```
TeaSpoon made from STAINLESS_STEEL
SteakKnife made from SILVER
Spork made from PLASTIC
Spork made from WOOD
My very own ThreeProngedPastryFork made from SILVER
```
Main-Methode in CutleryTest1 Klasse:
```java
public class CutleryTest1 {
    
    public static void main(String[] args) {
        Spoon c1 = new TeaSpoon() {
            
            Material material;
			
            @Override
            public void setMaterial(Material material) { 
                this.material = material; 
            }
            
            @Override
            public Material getMaterial() { 
                return material; 
            }
        };
        c1.setMaterial(Material.STAINLESS_STEEL);
        System.out.println(c1.getClass().getInterfaces()[0].getSimpleName() + " made from " + c1.getMaterial());
        Artifact c2 = new SteakKnife(Material.SILVER);
        System.out.println(c2);
        Spoon c3 = new Spork(Material.PLASTIC);
        System.out.println(c3);
        Fork c4 = new Spork(Material.WOOD);
        System.out.println(c4);
        Cutlery c5 = new ThreeProngedPastryFork(Material.SILVER) {
            @Override
            public String toString() { 
                return "My very own ThreeProngedPastryFork made from " + getMaterial(); 
            }
        };
        System.out.println(c5); 
    }
    
    // THIS METHOD IS NEEDED FOR TESTING!
    public static Material getMat(String name) {
        return Material.valueOf(name);
    }
}

```
