/*
Besteck 1: Vererbung und Polymorphie
Schreiben Sie einen Java-Typen für Material, der Silber, rostfreien Stahl, Holz und Plastik (engl. silver, stainless stell, wood, plastic) repräsentieren kann.
Schreiben Sie ein Interface für Besteck (engl. Cutlery), mit einer getter- und einer setter-Methode für Material.
Schreiben Sie Java-Typen für Messer, Gabel, Löffel, Eßgabel, Eßlöffel, Kuchengabel und Teelöffel (engl. knife, fork, spoon, dinner fork, table spoon, pastry fork, tea spoon).
Schreiben Sie einen Java-Typen für Artefakte (engl. Artifact). Artefakte können nicht instanziiert werden, aber haben ein Material und die entsprechenden getter- und setter-Methoden dafür.
Überschreiben Sie die toString-Methode des Artefakt-Typs, so dass immer folgender Wert zurückgegeben wird: "[einfacher Name der Klasse] made from [Material]". Dabei sollen die beiden Werte in eckigen Klammern dynamisch ersetzt werden.
Schreiben Sie drei Klassen für Steakmesser (engl. steak knife), dreizinkige Kuchengabeln (engl. three-pronged pastry fork) und Spork (eine Kombination aus Löffel/Spoon und Gabel/Fork). Verwenden Sie dabei die bisher definierten Typen. Passen Sie ggf. die Definition dieser Typen an.
Die u.a. main-Methode soll nun folgende Ausgabe erzeugen:
TeaSpoon made from STAINLESS_STEEL
SteakKnife made from SILVER
Spork made from PLASTIC
Spork made from WOOD
My very own ThreeProngedPastryFork made from SILVER
 */
public class CutleryTest1 {

	public static Material getMat(String name) {
		return Material.valueOf(name);
	}
	
	public static void main(String[] args) {
		Spoon c1 = new TeaSpoon() {

			Material material;
			
			@Override
			public void setMaterial(Material material) {
				this.material = material;
			}

			@Override
			public Material getMaterial() {
				return material;
			}
		};
		c1.setMaterial(Material.STAINLESS_STEEL);
		System.out.println(c1.getClass().getInterfaces()[0].getSimpleName() + " made from " + c1.getMaterial());

		Artifact c2 = new SteakKnife(Material.SILVER);
		System.out.println(c2);
		Spoon c3 = new Spork(Material.PLASTIC);
		System.out.println(c3);
		Fork c4 = new Spork(Material.WOOD);
		System.out.println(c4);
		Cutlery c5 = new ThreeProngedPastryFork(Material.SILVER) {

			@Override
			public String toString() {
				return "My very own ThreeProngedPastryFork made from " + getMaterial();
			}
			
		};
		System.out.println(c5);
	}
	
	public interface Cutlery {
		
		void setMaterial(Material material);
		Material getMaterial();
		
	}
	
	public interface Knife extends Cutlery { }
	
	public interface Fork extends Cutlery { }
	
	public interface Spoon extends Cutlery { }
	
	public interface TableSpoon extends Spoon { }
	
	public interface TeaSpoon extends Spoon { }
	
	public interface DinnerFork extends Fork { }
	
	public interface PastryFork extends Fork { }
	
	public static abstract class Artifact {
		
		protected Material material;

		public Artifact(Material material) {
			setMaterial(material);
		}

		public Material getMaterial() {
			return material;
		}

		public void setMaterial(Material material) {
			this.material = material;
		}
		
		@Override
		public String toString() {
			return getClass().getSimpleName() + " made from " + material;
		}
		
	}
	
	public static class SteakKnife extends Artifact implements Knife {
		public SteakKnife(Material material) {
			super(material);
		}
	}
	
	public static class ThreeProngedPastryFork extends Artifact implements PastryFork {
		public ThreeProngedPastryFork(Material material) {
			super(material);
		}
	}
	
	public static class Spork extends Artifact implements Spoon, Fork {
		public Spork(Material material) {
			super(material);
		}
	}
	
	public static enum Material {
		SILVER, STAINLESS_STEEL, WOOD, PLASTIC
	}

}
