##Besteck 2: Generics
Definieren Sie ein Interface ```Food``` mit den Spezialisierungen ```SolidFood```, ```LiquidFood``` und ```SoftFood```. Food hat eine Methode ```getName()```, die einen String zurückgibt.<br>
Definieren Sie weitere Interfaces ```Cake``` (als Spezialisierung von ```SolidFood``` und ```SoftFood```), ```Soup```  (als Spezialisierung von ```LiquidFood``` und ```SoftFood```) und ```Pudding``` (als Spezialisierung von ```SoftFood```).<br>
Definieren Sie jetzt ein generisches Interface ```Cutlery```, das nur auf Food-Typen anwendbar ist. ```Cutlery``` hat eine Methode ```process(T food)```, die ein Objekt des generischen Food-Typs als Parameter bekommt.<br>
Definieren Sie weitere Interfaces ```Fork```, ```Spoon``` und ```Spork```, die ```Cutlery``` spezialisieren, aber jeweils auf ```SolidFood```, ```LiquidFood``` bzw. ```SoftFood``` beschränkt sind.<br>
Definieren Sie nun drei Klassen ```PastryFork```, ```SoupSpoon``` und ```TravelSpork```. ```PastryFork``` ist eine Gabel nur für Kuchen, ```SoupSpoon``` ist ein Löffel nur für Suppen, und ```TravelSpork``` ist ein generischer Spork für beliebiges Essen, das ```SoftFood``` spezialisiert. Die ```process```-Methoden geben jeweils den Text "Eating [Name des Essens] with [fork/spoon/spork]." auf der Konsole aus.<br>
Die u.a. main-Methode erzeugt damit folgende Ausgabe:
```
Eating Cake with fork.
Eating Soup with spoon.
Eating Pudding with spork.
Eating Cake with spork.
Eating Soup with spork.
```
```java
public class Cutlery{
	
	public static void main(String[] args) {
		PastryFork fork = new PastryFork();
		SoupSpoon spoon = new SoupSpoon();
		TravelSpork<SoftFood> spork = new TravelSpork<>();
		Cake cake = new Cake() {
			public String getName() { return "Cake"; }
		};
		Soup soup = new Soup() {
			public String getName() { return "Soup"; }
		};
		Pudding pudding = new Pudding() {
			public String getName() { return "Pudding"; }
		};
		fork.process(cake);
		spoon.process(soup);
		spork.process(pudding);
		spork.process(cake);
		spork.process(soup);
	}
}
```
