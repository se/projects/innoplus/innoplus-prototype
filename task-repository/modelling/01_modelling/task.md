# Immobilien

Ein Immobilienmakler möchte die Verwaltung seiner Immobilien digitalisieren und beauftragt sie deshalb damit, ein System für die zugehörigen Informationen zu entwickeln.

---

Der Makler verfügt über eine Auswahl von Immobilien, die er im Auftrag seiner Kunden verkaufen möchte. Um möglichen Interessenten eine vernünftige Vorauswahl bieten zu können wird zu jeder Immobilie – sei es Wohnung,  Wohnhaus oder Industriegebäude – grundlegende Informationen wie die Adresse, Nutzfläche und natürlich der Kaufpreis hinterlegt.

Für Wohnhäuser und Industriegebäude wird zudem auch das zugehörigen Grundstück vermerkt, sofern es zusammen mit der Immobilie verkauft wird. Dieses hat eine feste Größe und kann entweder unbefestigt, gepflastert oder mit Schotter ausgelegt sein (sollten mehrere Kategorien zutreffen wird diejenige gewählt, welche den größten Anteil des Grundstücks ausmacht).

Bei Industriegebäuden wird zudem häufig nach bestimmten Ausstattungen gesucht (also beispielsweise ob eine Hebebühne vorhanden ist). Deshalb bietet der Makler eine kurze Beschreibung des Gebäudes, in dem alle bestehenden Maschinen und Einrichtungen aufgeführt werden.

Für Privatpersonen kann es bei einer Wohnung wichtig sein, mit ihrem Haustier einzuziehen. Dies muss aber durch den Vermieter erlaubt sein, ansonsten kommt diese Immobilie nicht in Frage.

Diese Informationen werden auch für alle Wohnungen gespeichert, die in einem Wohnhaus
enthalten sind.

---

### Aufgabe
- Modellieren sie dieses System als UML-Klassendiagramm.
  Erstellen sie dafür ein Entwurfsmodell, dass sich direkt nach Java übersetzen lässt.
  Achten sie darauf, unnötige Duplikationen und Redundanz zu vermeiden.
