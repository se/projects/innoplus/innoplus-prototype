# GUI

In einem aktuellen Softwareprojekt soll eine graphische Benutzeroberfläche (GUI) erstellt werden. Dafür wird der folgende Prototyp erstellt:

![Komplettes Fenster Beispiel](images/ex1.png "Komplettes Fenster") ![Objektdiagramm GUI](images/dia.png "Objektdiagramm GUI")

Anschließend wurde für diesen Prototypen ein Objektdiagramm erstellt, das alle Elemente der GUI dieses Prototypen beschreibt, wobei von weiteren Layoutinformationen wie Positionsangaben abstrahiert wurde.

1. Abstrahieren Sie von konkreten Prototypen, um zu einem allgemeinen Modell für GUI-Anwendungen zu gelangen. Erstellen Sie dazu ein UML-Klassendiagramm (Entwurfsmodell (ohne Methoden und Konstruktoren), aus dem ohne Änderung compilierbarer Java-Code generiert werden kann) für GUIs, indem Sie alle wesentlichen Konzepte aus dem gegebenen Objektdiagramm erheben. Achten Sie dabei auch darauf, die aus der Vorlesung bekannten Prinzipien einzuhalten (z.B. Redundanzvermeidung). Berücksichtigen Sie außerdem folgende zusätzliche Informationen zu Multiplizitäten und Generalisierungen im Klassendiagramm, die nicht aus dem Objektdiagramm ersichtlich sind:
    * Ein Frame besitzt genau ein Panel.
    * Ein Frame ist der Rahmen einer GUI-Anwendung und kann nicht in Panels oder andere Frames geschachtelt werden.
    * Labels, Buttons, CheckBoxen und Panels sind Elemente.
    * Ein Panel enthält beliebig viele Elemente.

2. Ergänzen Sie im Klassendiagramm die im Folgenden beschriebenen Konzepte von GUIs, die nicht im Objektdiagramm dargestellt sind:
    * Eine Liste (List) ist ein Element und verfügt über keine Text-Eigenschaft.
    * Eine Liste enthält mindestens zwei Einträge (Entry), die jeweils einen Text (String) als Inhalt haben.
    * Einträge können nicht unabhängig von Listen existieren.
    * Zu jedem Zeitpunkt ist genau ein Eintrag einer Liste ausgewählt.

![Listenauswahl Beispiel](images/ex2.png "Listenauswahl Beispiel")

