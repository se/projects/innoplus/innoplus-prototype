package de.unioldenburg.se.innoplus.eclipse.java;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import de.unioldenburg.se.innoplus.eclipse.Logging;
import de.unioldenburg.se.innoplus.eclipse.PluginManager;
import de.unioldenburg.se.innoplus.eclipse.PluginUtils;
import de.unioldenburg.se.innoplus.eclipse.feedback.FeedbackView;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.feedback.JavaFeedbackRequest;
import de.unioldenburg.se.innoplus.message.feedback.JavaFeedbackResponse;

public class RequestJavaFeedbackHandler extends AbstractHandler {
	protected IJavaProject javaProject;

	public class FileEntry {
		private final String pathLocal;
		private final String pathServer;
		private final String content;
		private final boolean selectedByDefault;
		public FileEntry(String pathLocal, String pathServer, String content, boolean select) {
			super();
			this.pathLocal = pathLocal;
			this.pathServer = pathServer;
			this.content = content;
			this.selectedByDefault = select;
		}
		public String getPathLocal() {
			return pathLocal;
		}
		public String getPathServer() {
			return pathServer;
		}
		public String getContent() {
			return content;
		}
		public boolean isSelectedByDefault() {
			return selectedByDefault;
		}
		@Override
		public String toString() {
			return pathLocal + " == " + pathServer + " (selected by default: " + selectedByDefault + "):\n" + content;
		}
	}

	public RequestJavaFeedbackHandler() {
		super();
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		Shell shell = window.getShell();

		// get selected project
		ISelection sel = HandlerUtil.getActiveMenuSelection(event);
		IStructuredSelection selection = (IStructuredSelection) sel;
		List<?> elems = selection.toList();

		// collect Java files and their content
		javaProject = null;
		Map<String, FileEntry> javaFiles = new HashMap<>(); // fully-qualified file path -> ...
		for (Object selectedElement : elems) {
			IJavaProject result;
			try {
				if (selectedElement instanceof IJavaProject) {
					result = collect(javaFiles, (IJavaProject) selectedElement);
				} else if (selectedElement instanceof IPackageFragmentRoot) {
					result = collect(javaFiles, (IPackageFragmentRoot) selectedElement);
				} else if (selectedElement instanceof IPackageFragment) {
					result = collect(javaFiles, (IPackageFragment) selectedElement);
				} else if (selectedElement instanceof ICompilationUnit) {
					result = collect(javaFiles, (ICompilationUnit) selectedElement);
				} else if (selectedElement instanceof IProject) {
					result = collect(javaFiles, (IProject) selectedElement);
				} else if (selectedElement instanceof IFolder) {
					result = collect(javaFiles, selectedElement);
				} else if (selectedElement instanceof IFile) {
					result = collect(javaFiles, selectedElement);
				} else {
					result = null;
				}
			} catch (JavaModelException e) {
				Logging.error("Invalid Selection: Perhaps the build before failed?" + selectedElement, e, shell);
				return null;
			} catch (CoreException e) {
				Logging.error("Invalid Selection: Perhaps the build before failed?" + selectedElement, e, shell);
				return null;
			}
			if (validate(result) == false) {
				Logging.error("Invalid Selection: Only Java elements or projects may be selected: " + selectedElement.getClass().getName(), shell);
				return null;
			}
		}
		if (javaProject == null) {
			Logging.error("Invalid Selection: Nothing was selected.", shell);
			return null;
		}

		// detect the Java version of the Java project
		String javaVersion = "<not found>";
		List<String> possibleJavaVersions = Activator.createListOfPossibleJavaVersions();
		try {
			// define some possible Java versions
			Map<String, String> versions = new HashMap<>(); // string contained in the path => corresponding Java version
			possibleJavaVersions.stream().forEach(v -> versions.put("JavaSE-" + v, v));

			if (PluginManager.DEVELOPMENT_MODE) {
				Logging.info("class path: " + Arrays.asList(javaProject.getRawClasspath()), null);
			}
			/* class path:
			 * [/TestProject/src[CPE_SOURCE][K_SOURCE][isExported:false],
			 * org.eclipse.jdt.launching.JRE_CONTAINER/org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-1.8[CPE_CONTAINER][K_SOURCE][isExported:false]]
			 * content kind = 1, entry kind = 5
			 */
			List<String> entries = Arrays.stream(javaProject.getRawClasspath())
					.filter(cp -> cp.getEntryKind() == IClasspathEntry.CPE_LIBRARY || cp.getEntryKind() == IClasspathEntry.CPE_CONTAINER)
					.map(cp -> cp.getPath().toPortableString())
					.filter(cp -> cp.contains("jdk") || cp.contains("jre") || cp.contains("jdt"))
					.collect(toList());
			if (PluginManager.DEVELOPMENT_MODE) {
				Logging.info("class path: " + entries, null);
			}
			for (Entry<String, String> entry : versions.entrySet()) {
				if (entries.stream().filter(e -> e.contains(entry.getKey())).count() >= 1) {
					javaVersion = entry.getValue();
					break;
				}
			}
		} catch (JavaModelException e) {
			Logging.error("getting the Java files failed", e, shell);
		}
		final String identifiedJavaVersion = javaVersion;

		if (PluginManager.DEVELOPMENT_MODE) {
			Logging.info("all found files: " + javaFiles, null);
		}

		/* detect the TabWidth for Java
		 * - "org.eclipse.ui.editors", "tabWidth": is used for arbitrary files
		 * - here, check the TabWidth specific for Java!
		 * TODO: these preferences target the default TabWidth of the current Eclipse instance, if the current project has different/custom/individual values, they are NOT detected by this approach!
		 */
		int tabWidth = getInt("org.eclipse.jdt.ui", "org.eclipse.jdt.ui.editor.tab.width"); // this value is sometimes empty (why?)
		if (tabWidth < 0) {
			tabWidth = getInt("org.eclipse.jdt.core", "org.eclipse.jdt.core.formatter.tabulation.size");
		}
		// missing values: use "4" as default value
		if (tabWidth < 0) {
			tabWidth = 4;
			if (PluginManager.DEVELOPMENT_MODE) {
				Logging.info("Java tab-width (default value): " + tabWidth, null);
			}
		} else {
			if (PluginManager.DEVELOPMENT_MODE) {
				Logging.info("Java tab-width (identified value): " + tabWidth, null);
			}
		}
		if (PluginManager.DEVELOPMENT_MODE) {
			Logging.info("profile: " + getString("org.eclipse.jdt.ui", "formatter_profile"), null);
		}
		final int identifiedTabWidth = tabWidth;

		// ensure connection with the server
		PluginManager manager = de.unioldenburg.se.innoplus.eclipse.Activator.getManager();
		manager.getCommunication().ensureRunningCommunication(shell, new Runnable() {
			@Override
			public void run() {
				PluginUtils.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						/*
						 * dialog for request
						 * - select Lecture + Task!
						 * - show Java version (adjustable!)
						 * - show files to submit (not changeable)
						 * - buttons: cancel, submit
						 */
						RequestJavaFeedbackDialog dialog = new RequestJavaFeedbackDialog(shell, identifiedJavaVersion, javaFiles);
						dialog.create();
						if (dialog.open() == Window.OK) {
							String newJavaVersion = dialog.getJavaVersion();
							if (newJavaVersion == null || newJavaVersion.isBlank()) {
								return;
							}
							if (possibleJavaVersions.contains(newJavaVersion) == false) {
								Logging.error("Invalid Java version: The specified Java version " + newJavaVersion + " is not valid! Use one of " + possibleJavaVersions + " instead.", shell);
								return;
							}
							if (dialog.getTask() == null) {
								return;
							}

							/* open the feedback view automatically:
							 * - this must be done before getting the feedback!
							 * - clear shown feedback!
							 */
							FeedbackView.getInstance(true).requestedNewFeedback();;

							// send request to the server
							Map<String, String> filesToSend = dialog.getFilesToSend();
							if (PluginManager.DEVELOPMENT_MODE) {
								Logging.info("sent files: " + filesToSend, null);
							}
							JavaFeedbackRequest request = new JavaFeedbackRequest(manager.getCurrentUserHash(), dialog.getTask().getName(), newJavaVersion, identifiedTabWidth, filesToSend);
							boolean sendToServer = manager.getCommunication().sendToServer(request, new Consumer<ServerToClient>() {
								@Override
								public void accept(ServerToClient response) {
									if (response instanceof JavaFeedbackResponse == false) {
										throw new IllegalStateException();
									}

									// open the FeedbackView again, if it was closed in the meantime
									FeedbackView.getInstance(true);

									// show success message
									Logging.info("Received feedback from the server: See 'Feedback by Inno+' view for details.", shell);
								}
							});
							if (sendToServer == false) {
								Logging.error("Failed to send the request to the server.", shell);
							} else {
								// success
								if (PluginManager.DEVELOPMENT_MODE == false) {
									Logging.info("The request for feedback is sent to the server successfully! "
											+ "Since the server executes checks for the functionality and analyzes the quality of your submitted source code, it will take several seconds. "
											+ "Please be patient!", shell);
								}
							}
						}
					}
				});
			}
		});

		return null;
	}

	public static int getInt(String node, String key) {
//		return Platform.getPreferencesService().getInt(node, key, -1, null);
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(node);
		if (preferences == null) {
			return -1;
		}
		return preferences.getInt(key, -1);
	}
	public static String getString(String node, String key) {
//		return Platform.getPreferencesService().getString(node, key, "(missing)", null);
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(node);
		if (preferences == null) {
			return "(missing)";
		}
		return preferences.get(key, "(missing)");
	}

	protected IJavaProject transform(IProject project, boolean logErrors) {
		Shell shell = Logging.defaultShell();
		if (project.exists() == false) {
			if (logErrors) {
				Logging.error("Invalid Selection: You selected a project, which does not exist in the workspace.", shell);
			}
			return null;
		}
		try {
			// https://stackoverflow.com/questions/31451860/org-eclipse-jdt-core-javacores-create-method-always-returning-null
			if (project.hasNature(JavaCore.NATURE_ID) == false) {
				if (logErrors) {
					Logging.error("Invalid Selection: You selected a project, which is no Java project.", shell);
				}
				return null;
			} else {
				// this project can be treated as Java project => proceed!
			}
		} catch (CoreException e) {
			if (logErrors) {
				Logging.error("Invalid Selection: failed to analyze the project's nature", e, shell);
			}
			return null;
		}
		return JavaCore.create(project);
	}

	protected IJavaProject collect(Map<String, FileEntry> javaFiles, IProject project) throws CoreException {
		IJavaProject javaProj = transform(project, true);

		IJavaProject result = collect(javaFiles, javaProj);
		if (validate(result) == false) {
			return null;
		}
		return javaProj;
	}

	protected IJavaProject collect(Map<String, FileEntry> javaFiles, IJavaProject element) throws CoreException {
		for (IPackageFragmentRoot root : element.getPackageFragmentRoots()) {
			IJavaProject result = collect(javaFiles, root);
			if (validate(result) == false) {
				return null;
			}
		}
		collect(javaFiles, element.getNonJavaResources(), element.getProject());
		return element;
	}
	protected IJavaProject collect(Map<String, FileEntry> javaFiles, IPackageFragmentRoot root) throws CoreException {
		if (root.getElementName().contains("test")) {
			return root.getJavaProject(); // ignore "test" stuff
		}
		for (IJavaElement child : root.getChildren()) {
			if (child instanceof IPackageFragment) {
				IJavaProject result = collect(javaFiles, (IPackageFragment) child);
				if (validate(result) == false) {
					return null;
				}
			}
		}
		collect(javaFiles, root.getNonJavaResources(), root.getJavaProject().getProject());
		return root.getJavaProject();
	}
	protected IJavaProject collect(Map<String, FileEntry> javaFiles, IPackageFragment pack) throws CoreException {
		for (ICompilationUnit file : pack.getCompilationUnits()) {
			IJavaProject result = collect(javaFiles, file);
			if (validate(result) == false) {
				return null;
			}
		}
		collect(javaFiles, pack.getNonJavaResources(), pack.getJavaProject().getProject());
		return pack.getJavaProject();
	}
	protected IJavaProject collect(Map<String, FileEntry> javaFiles, ICompilationUnit file) throws JavaModelException {
		// path(s)
		String pathLocal = file.getPath().toPortableString(); // including project and src/-folder names!
		String pathServer = file.getElementName();
		if (file.getParent() instanceof IPackageFragment) {
			String packName = file.getParent().getElementName();
			if (packName != null && packName.isEmpty() == false) {
				pathServer = packName.replaceAll("\\.", "/") + "/" + pathServer;
			}
		}

		// content
		String content = file.getSource();

		if (javaFiles.containsKey(pathServer)) {
			// check for duplicate entries: this is OK, ignore this finding, use the file only once
//			Logging.error("The file " + path + " was selected multiple times. Select it only once!", null, Logging.defaultShell());
//			return null;
		} else {
			javaFiles.put(pathServer, new FileEntry(pathLocal, pathServer, content, true));
		}

		return file.getJavaProject();
	}
	protected IJavaProject collect(Map<String, FileEntry> javaFiles, Object[] nonJavaResources, IProject rootProject) throws CoreException {
		if (nonJavaResources == null || nonJavaResources.length <= 0) {
			return null;
		}
		for (int i = 0; i < nonJavaResources.length; i++) {
			Object resource = nonJavaResources[i];
			IJavaProject result = collect(javaFiles, resource);
			if (validate(result) == false) {
				return null;
			}
		}
		return transform(rootProject, false);
	}
	protected IJavaProject collect(Map<String, FileEntry> javaFiles, Object resource) throws CoreException {
		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			// path(s)
			String pathLocal = file.getFullPath().toPortableString(); // including project and src/-folder names!
			String pathServer = file.getProjectRelativePath().toPortableString();
			// content
			String content;
			try {
				content = IOUtils.toString(file.getContents(), file.getCharset());
			} catch (IOException | CoreException e) {
				Logging.error("The file " + pathServer + " was not readable and will be ignored", null, Logging.defaultShell());
				return null;
			}
			// remember this file
			if (javaFiles.containsKey(pathServer)) {
				// check for duplicate entries: this is OK, ignore this finding, use the file only once
//				Logging.error("The file " + path + " was selected multiple times. Select it only once!", null, Logging.defaultShell());
//				return null;
			} else {
				javaFiles.put(pathServer, new FileEntry(pathLocal, pathServer, content, false));
			}
			return transform(file.getProject(), false);
		} else if (resource instanceof IFolder) {
			// check all children of the folder
			return collect(javaFiles, ((IFolder) resource).members(), ((IFolder) resource).getProject());
		} else if (resource instanceof IStorage) {
			// IStorage is for entries inside a JAR: ignore it
			return null;
		} else {
			// unknown stuff: ignore it
			return null;
		}
	}

	protected boolean validate(IJavaProject proj) {
		Shell shell = Logging.defaultShell();

		/* a missing project is OK: important is, that a non-null project ...
		 * - is usable
		 * - is a Java project
		 * - contains ALL files which should be send to the server (do not collect files spread over two or more projects!)
		 *
		 * reasons for 'null' here:
		 * - strange/corrupt files were selected, but ignored
		 * - empty folder
		 * - ...
		 */
		if (proj == null) {
			return true;
		}

		// check the Java project
		if (proj.exists() == false) {
			Logging.error("Invalid Selection: The selected Java project does not exist!", shell);
			return false;
		}
		if (proj.getProject().isOpen() == false) {
			Logging.error("Invalid Selection: The selected project must be open!", shell);
			return false;
		}
		try {
			if (proj.isStructureKnown() == false) {
				Logging.error("Invalid Selection: The structure of this project is unkown, probably, there are compiler issues. Fix them before!", shell);
				return false;
			}
		} catch (JavaModelException e) {
			Logging.error("Invalid Selection: failed to analyze the project's structure", e, shell);
			return false;
		}

		// check, if different parts of the selection belong to the same Java project
		if (javaProject == null) {
			// remember the first Java Project => everything is fine
			javaProject = proj;
		} else {
			if (javaProject.getElementName().equals(proj.getElementName()) == false) {
				// there are selected parts which belong to different Java projects => cancel, is not allowed, might contradict each other, e.g. they might have different Java versions!
				Logging.error("Invalid Selection: You selected elements of different projects: " + javaProject.getElementName() + " VS " + proj.getElementName(), shell);
				return false;
			} else {
				// the different parts of the selection belong to the same Java project => everything is fine
			}
		}
		return true;
	}
}
