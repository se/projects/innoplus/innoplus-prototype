package de.unioldenburg.se.innoplus.eclipse.java;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.unioldenburg.se.innoplus.InnoplusConstants;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.data.TaskCategory;
import de.unioldenburg.se.innoplus.eclipse.Activator;
import de.unioldenburg.se.innoplus.eclipse.feedback.FeedbackView;
import de.unioldenburg.se.innoplus.eclipse.java.RequestJavaFeedbackHandler.FileEntry;

import de.unioldenburg.se.innoplus.eclipse.Logging;

public class RequestJavaFeedbackDialog extends TitleAreaDialog {
	// settings to remember
	protected static Task rememberTask;
	protected static String rememberJavaVersion;
	protected static List<String> rememberFiles = new ArrayList<>();

	// GUI elements
	private ComboViewer taskCombo;
	private ComboViewer txtJavaVersion;
	private CheckboxTableViewer filesViewer;

	// return data
	private String javaVersion;
	private Task task;
	private Map<String, String> filesToSubmit;

	// input data
	private final Map<String, FileEntry> allFiles;

	public RequestJavaFeedbackDialog(Shell parentShell, String javaVersion, Map<String, FileEntry> filesToSubmit) {
		super(parentShell);
		this.javaVersion = javaVersion;
		this.allFiles = Objects.requireNonNull(filesToSubmit);
	}

	@Override
	public void create() {
		super.create();
		setTitle("Request Feedback for Java");
		setMessage("Send Java files to the server and get feedback back.", IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// http://www.vogella.com/tutorials/EclipseDialogs/article.html#titleareadialog
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(2, false);
		container.setLayout(layout);

		// select the task to check
		Label labelTask = new Label(container, SWT.NONE);
		labelTask.setText("Select the Task:");
		taskCombo = new ComboViewer(container, SWT.READ_ONLY);
		taskCombo.setContentProvider(ArrayContentProvider.getInstance());
		taskCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Task) {
					return ((Task) element).getName();
				}
				return super.getText(element);
			}
		});

		// add all Java tasks
		List<Task> taskList;
		Optional<TaskCategory> optJavaTaskCategory = Activator.getManager().getDataServer().getTaskCategories().stream().filter(c -> InnoplusConstants.TASK_JAVA.equals(c.getName())).findAny();
		if (optJavaTaskCategory.isPresent()) {
			TaskCategory javaTaskCategory = optJavaTaskCategory.get();
			taskList = Activator.getManager().getTasks().stream().filter(t -> t.getCategory() == javaTaskCategory).collect(toList());
		} else {
			Logging.error("Received no matching tasks from the server!", null);
			taskList = Collections.emptyList();
		}
		taskCombo.setInput(taskList);


		// review the identified Java version
		Label labelJavaVersion = new Label(container, SWT.NONE);
		String tooltipJavaVersion = "The Java version for compiling, found in the configuration of the current project: You can change it now!";
		labelJavaVersion.setToolTipText(tooltipJavaVersion);
		labelJavaVersion.setText("Java Version:");

		GridData dataFilename = new GridData(); // required, to make the TextField bigger!
		dataFilename.grabExcessHorizontalSpace = true;
		dataFilename.horizontalAlignment = GridData.FILL;
		txtJavaVersion = new ComboViewer(container, SWT.READ_ONLY);
		txtJavaVersion.setContentProvider(ArrayContentProvider.getInstance());
		txtJavaVersion.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof String) {
					return (String) element;
				}
				return super.getText(element);
			}
		});
		List<String> possibleJavaVersions = de.unioldenburg.se.innoplus.eclipse.java.Activator.createListOfPossibleJavaVersions();
		if (possibleJavaVersions.contains(javaVersion)) {
			// do not allow to change the identified Java version
			txtJavaVersion.setInput(Collections.singletonList(javaVersion));
			txtJavaVersion.setSelection(new StructuredSelection(javaVersion));
		} else {
			// a valid Java version could not found => the user must select one of the supported Java versions
			txtJavaVersion.setInput(possibleJavaVersions);
			// select nothing
		}
//		txtJavaVersion.setMessage(tooltipJavaVersion);
//		txtJavaVersion.setText(javaVersion); // default value
//		txtJavaVersion.setLayoutData(dataFilename);


		// show files to submit
		Label labelFilesHint = new Label(container, SWT.NONE);
		labelFilesHint.setText("Files to submit:");
		filesViewer = CheckboxTableViewer.newCheckList(parent, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION);
		filesViewer.setContentProvider(ArrayContentProvider.getInstance());
		filesViewer.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof FileEntry) {
					return ((FileEntry) element).getPathLocal();
				}
				return super.getText(element);
			}
		});
		List<FileEntry> list = allFiles.values().stream()
				.sorted((o1, o2) -> o1.getPathLocal().compareTo(o2.getPathLocal()))
				.collect(toList());
		filesViewer.setInput(list);


		// use the remembered values as initial values
		taskCombo.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				if (selection.size() > 0) {
					Task currentTask = (Task) selection.getFirstElement();
					if (currentTask != null && rememberTask != null && Objects.equals(currentTask.getName(), rememberTask.getName()) && rememberFiles.isEmpty() == false) {
						long reusableItems = list.stream().filter(e -> rememberFiles.contains(e.getPathLocal())).count();
						if (reusableItems >= 1) {
							// for the previous task: use remembered selection of files (if there are some matching entries)
							list.forEach(e -> filesViewer.setChecked(e, rememberFiles.contains(e.getPathLocal())));
						} else {
							// if the previous selection matched NO current entry: use the default selection
							list.forEach(e -> filesViewer.setChecked(e, e.isSelectedByDefault()));
						}
					} else {
						// for another task: use the default selection
						list.forEach(e -> filesViewer.setChecked(e, e.isSelectedByDefault()));
					}
				}
			}
		});
		if (rememberTask != null && taskList.contains(rememberTask)) {
			taskCombo.setSelection(new StructuredSelection(rememberTask));
		} else {
			taskCombo.setSelection(null);
		}
		if (rememberJavaVersion != null && possibleJavaVersions.contains(rememberJavaVersion)) {
			txtJavaVersion.setSelection(new StructuredSelection(rememberJavaVersion));
		}

		return area;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		javaVersion = (String) txtJavaVersion.getStructuredSelection().getFirstElement();
		task = (Task) taskCombo.getStructuredSelection().getFirstElement();

		// retrieve the selected files to send to the server
		Object[] selection = filesViewer.getCheckedElements();
		filesToSubmit =  new HashMap<>();
		if (selection != null) {
			for (int i = 0; i < selection.length; i++) {
				FileEntry entry = (FileEntry) selection[i];
				filesToSubmit.put(entry.getPathServer(), entry.getContent());
				if (FeedbackView.mapResolveFilePaths.containsKey(entry.getPathServer())) {
					throw new IllegalStateException(entry.getPathServer() + " is already contained");
				}
				FeedbackView.mapResolveFilePaths.put(entry.getPathServer(), entry.getPathLocal());
			}
		}

		// remember some settings
		if (javaVersion != null && javaVersion.isBlank() == false) {
			rememberJavaVersion = javaVersion;
		}
		if (task != null) {
			rememberTask = task;
		}
		if (filesToSubmit.isEmpty() == false) {
			rememberFiles = filesToSubmit.keySet().stream()
					.map(serverPath -> FeedbackView.mapResolveFilePaths.get(serverPath))
					.collect(toList());
		}

		super.okPressed();
	}

	public String getJavaVersion() {
		return javaVersion;
	}

	public Task getTask() {
		return task;
	}

	public Map<String, String> getFilesToSend() {
		return filesToSubmit;
	}
}
