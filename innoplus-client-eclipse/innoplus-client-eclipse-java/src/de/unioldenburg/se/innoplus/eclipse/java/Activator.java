package de.unioldenburg.se.innoplus.eclipse.java;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "innoplus-client-eclipse-java"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static List<String> createListOfPossibleJavaVersions() {
		List<String> versions = new ArrayList<>(JavaCore.getAllVersions());

		// do not support very old versions, another reasons: there is another format for the used Java version in the Class Path!
		IntStream.rangeClosed(1, 5).forEach(i -> versions.remove("1." + i));
		versions.remove(JavaCore.VERSION_CLDC_1_1);

		return versions;
	}

}
