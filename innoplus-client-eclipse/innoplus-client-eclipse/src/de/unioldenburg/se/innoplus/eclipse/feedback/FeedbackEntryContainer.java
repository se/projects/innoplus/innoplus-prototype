package de.unioldenburg.se.innoplus.eclipse.feedback;

import java.util.Objects;

import org.eclipse.core.resources.IMarker;

import de.unioldenburg.se.innoplus.data.QueryResult;

public class FeedbackEntryContainer {
	protected final QueryResult result;
	protected final IMarker marker;
	protected final String pathLocal;

	public FeedbackEntryContainer(QueryResult result, IMarker marker, String pathLocal) {
		super();
		this.result = Objects.requireNonNull(result);
		this.marker = marker; // might be null
		this.pathLocal = Objects.requireNonNull(pathLocal);

		if (this.pathLocal.contains(":")) {
			throw new IllegalArgumentException("the local path must contain only the path, no line numbers: " + this.pathLocal);
		}
	}

	public QueryResult getResult() {
		return result;
	}

	public IMarker getMarker() {
		return marker;
	}

	public boolean isPathLocalUsable() {
		if (marker != null) {
			return true;
		}
		String location = getPathLocal(false);
		return location != null && location.isBlank() == false;
	}

	public String getPathLocal(boolean withLineNumbers) {
		String wantedLocation = pathLocal;
		// add line numbers (only, if the path is not empty!)
		if (withLineNumbers && wantedLocation.isBlank() == false) {
			String pathServer = getPathServer();
			int index = pathServer.indexOf(":");
			if (index < 0) {
				// no line numbers available in the server path
			} else {
				wantedLocation = wantedLocation + pathServer.substring(index);
			}
		}
		return wantedLocation;
	}

	public String getPathServer() {
		String location = result.getLocation();
		if (location == null) {
			return "";
		}
		return location.trim();
	}
}
