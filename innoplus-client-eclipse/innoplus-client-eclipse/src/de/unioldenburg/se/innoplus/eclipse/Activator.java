package de.unioldenburg.se.innoplus.eclipse;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.unioldenburg.se.innoplus.InitializerSharedData;
import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.DataPackage;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.eclipse.communication.Communication;
import de.unioldenburg.se.innoplus.eclipse.communication.CommunicationMQTT;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "innoplus-client-eclipse"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	private static PluginManager manager;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		DataPackage.eINSTANCE.getDataFactory();

//		Communication communication = new CommunicationDirect(); // old dummy implementation
		Communication communication = new CommunicationMQTT(); // using MQTT

		manager = new PluginManager(communication);

		// TODO: später diese Daten vom Server holen; hier werden nur ein Testdaten angelegt
		InnoplusDatabase data = InitializerSharedData.createDefaultData();
		DataFactory factory = DataFactory.eINSTANCE;
		Task task = factory.createTask();
		task.setName("Merge sorted Arrays");
		task.setDescription("TODO description");
		data.getTasks().add(task);
		task.setCategory(data.getTaskCategories().get(0));
//		task.setAuthor(lecturer);
		task.getUsedInLectures().addAll(data.getLectures());
		manager.setDataServer(data);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// disconnect from server!
		if (manager != null && manager.getCommunication() != null) {
			manager.getCommunication().closeCommunication();
		}

		plugin = null;
		manager = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public static PluginManager getManager() {
		return manager;
	}


	public static ImageDescriptor getImage(QueryResultType type) {
		if (type == null) {
			throw new IllegalArgumentException();
		}
		switch (type) {
		case INFORMATION:
			return PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK);
		case SUCCESS:
			return PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK); // TODO: schöneres Icon finden!
		case WARNING:
			return PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJS_WARN_TSK);
		case ERROR:
			return PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_OBJS_ERROR_TSK);
		default:
			throw new IllegalStateException("unknown type " + type);
		}
	}
}
