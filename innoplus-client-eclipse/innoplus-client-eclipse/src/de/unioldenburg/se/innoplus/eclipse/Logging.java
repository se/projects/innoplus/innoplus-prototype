package de.unioldenburg.se.innoplus.eclipse;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * Utilities to add errors into the "Error Log".
 * @author Johannes Meier
 */
public class Logging {
	public static void error(String message, Shell shellToShowDialog) {
		error(message, null, shellToShowDialog);
	}
	public static void error(String message, Throwable error, Shell shellToShowDialog) {
		log(message, error, IStatus.ERROR);
		if (shellToShowDialog != null) {
			messageError(shellToShowDialog, message, error);
		}
	}

	public static void info(String message, Shell shellToShowDialog) {
		log(message, null, IStatus.INFO);
		if (shellToShowDialog != null) {
			messageInformation(shellToShowDialog, message);
		}
	}

	private static void log(String message, Throwable error, int severity) {
		// http://stackoverflow.com/questions/8122787/eclipse-pde-logging
		// ILog log = Platform.getLog(Platform.getBundle(Activator.PLUGIN_ID));

		// http://stackoverflow.com/questions/28481943/proper-logging-for-eclipse-plug-in-development
		ILog log = Activator.getDefault().getLog();
		if (error != null) {
			log.log(new Status(severity, Activator.PLUGIN_ID, message, error));
		} else {
			log.log(new Status(severity, Activator.PLUGIN_ID, message));
		}
		/* if you run the plugin project within a new eclipse instance, the following works:
		 * - System.out.
		 * - System err.
		 */
	}

	public static Shell defaultShell() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}

	public static void messageInformation(String message) {
		messageInformation(defaultShell(), message);
	}
	public static void messageInformation(Shell shell, String message) {
		PluginUtils.runOnUiThread(() -> MessageDialog.openInformation(shell, "Inno+ Information", message));
	}

	public static void messageError(String message) {
		messageError(defaultShell(), message);
	}
	public static void messageError(Shell shell, String message) {
		PluginUtils.runOnUiThread(() -> MessageDialog.openError(shell, "Inno+ Error", message));
	}

	public static void messageError(String message, Throwable e) {
		messageError(defaultShell(), message, e);
	}
	public static void messageError(Shell shell, String message, Throwable e) {
		String m = message;
		if (e != null) {
			m = m + ":\n" + e.toString();
		}
		final String msg = m;
		PluginUtils.runOnUiThread(() -> MessageDialog.openError(shell, "Inno+ Error", msg));
	}
}
