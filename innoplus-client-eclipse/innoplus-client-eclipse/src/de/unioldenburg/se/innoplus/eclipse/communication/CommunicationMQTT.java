package de.unioldenburg.se.innoplus.eclipse.communication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import de.unioldenburg.se.innoplus.InnoplusUtils;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.eclipse.Activator;
import de.unioldenburg.se.innoplus.eclipse.Logging;
import de.unioldenburg.se.innoplus.eclipse.PluginManager;
import de.unioldenburg.se.innoplus.eclipse.PluginUtils;
import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.login.BeginSessionRequest;
import de.unioldenburg.se.innoplus.message.login.BeginSessionResponse;
import de.unioldenburg.se.innoplus.message.login.LoginRequest;
import de.unioldenburg.se.innoplus.message.login.LoginResponse;
import de.unioldenburg.se.innoplus.networking.Connection;
import de.unioldenburg.se.innoplus.networking.Connection.MessageListener;
import de.unioldenburg.se.innoplus.networking.MqttConnection;
import de.unioldenburg.se.innoplus.networking.MqttConnection.Configuration;

/**
 * This implementation is NOT thread-safe!
 *
 * @author Johannes Meier
 */
public class CommunicationMQTT implements Communication, MessageListener<ServerToClient> {
	private final static String MQTT_BROKER_URL_PRODUCTION = "ssl://innoplus.Informatik.Uni-Oldenburg.DE:8883";
	private final static String MQTT_BROKER_URL_DEVELOPMENT = "ssl://innoplus-dev.Informatik.Uni-Oldenburg.DE:8883";

	protected Connection<ServerToClient, ClientToServer> client; // (client != null) ==> MQTT connection is usable
	protected final List<Consumer<ServerToClient>> receiver;
	protected final Consumer<ServerToClient> handleAnswer;
	protected final AtomicBoolean session; // existing MQTT connection AND successful start of the session
	protected final AtomicBoolean login; // session AND successful login

	protected Consumer<ServerToClient> currentReaction;

	public CommunicationMQTT() throws IOException {
		super();
		receiver = new ArrayList<>();
		handleAnswer = new Consumer<ServerToClient>() {
			@Override
			public void accept(ServerToClient message) {
				handleAnswer(message);
			}
		};
		session = new AtomicBoolean(false);
		login = new AtomicBoolean(false);
	}

	private Shell getShellToUse(Shell shell) {
		return shell != null ? shell : Logging.defaultShell();
	}

	@Override
	public void ensureRunningCommunication(Shell shell, Runnable runAfterwards) {
		ensureLogin(shell, runAfterwards);
	}

	@Override
	public boolean beginCommunication(Shell shell, Runnable runAfterwards) {
		// close existing connection
		closeCommunication();

		// start new connection:
		return ensureLogin(shell, runAfterwards);
	}

	/**
	 * 
	 * This method is blocking!
	 * This method can be called from any thread.
	 * @param shell the shell to use for the dialog (if necessary)
	 * @return the user hash (or null, if the user did not choose a user name)
	 */
	protected String ensureUserHash(Shell shell) {
		String userHash = Activator.getManager().getCurrentUserHash();

		if (userHash == null || userHash.isBlank()) {
			// ask the user for the missing user name
			AtomicReference<String> selectedUserHash = new AtomicReference<String>(null);
			PluginUtils.runOnUiThreadBlocking(new Runnable() {
				@Override
				public void run() {
					LoginUsernameDialog dialog = new LoginUsernameDialog(getShellToUse(shell));
					dialog.create();
					if (dialog.open() == Window.OK) {
						String userName = dialog.getUsername();
						if (userName == null || userName.isBlank()) {
							return;
						}
						
						// remember only the user hash!
						String userHash = InnoplusUtils.md5(userName);
						selectedUserHash.set(userHash);
					} else {
						// no user name
					}
				}
			});

			// remember the user hash!
			userHash = selectedUserHash.get();
			if (userHash != null && userHash.isBlank() == false) {
				Activator.getManager().setCurrentUserHash(userHash);
			}
		}
		return userHash;
	}

	/**
	 * 
	 * This method is blocking!
	 * This method can be called from any thread.
	 * @param shell the shell to use for the connection (if necessary)
	 * @return true, if the MQTT connection was established successfully, false, if it failed at some point
	 */
	protected boolean ensureMqttConnection(Shell shell) {
		if (client != null) {
			// already running
			return true;
		}
		Shell shellToUse = getShellToUse(shell);

		// use the user hash for identifying this client
		final String userHash = ensureUserHash(shellToUse);
		if (userHash == null || userHash.isBlank()) {
			return false;
		}

		// establish the connection with the server
		try {
			var config =  new Configuration(
					false,
					PluginManager.DEVELOPMENT_MODE ? MQTT_BROKER_URL_DEVELOPMENT : MQTT_BROKER_URL_PRODUCTION,
					userHash,
					"cli3n1",
					"ztj7fmzGRTBovog8E2EeR3bwtWhGyDVc"
			);
			client = MqttConnection.createClient(config);
			client.addListener(ServerToClient.class, this);
			return true;
		} catch (IOException e) {
			Logging.error("failed to establish MQTT connection for " + userHash, e, shellToUse);
			closeCommunication();
			return false;
		}
	}

	/**
	 * 
	 * This method is non-blocking!
	 * This method can be called from any thread.
	 * @param shell the shell to use for the session (if necessary)
	 * @param runAfterSessionStart logic (might be null) which is called, after the session was established
	 * @return true, if the session was started successfully (i.e. the session begin request was sent successfully to the server),
	 * false, if it failed at some point
	 */
	protected boolean ensureSession(Shell shell, Runnable runAfterSessionStart) {
		boolean mqttRunning = ensureMqttConnection(shell); // blocking
		if (mqttRunning == false || client == null) {
			return false;
		}
		if (login.get() || session.get()) {
			if (runAfterSessionStart != null) {
				runAfterSessionStart.run();
			}
			return true;
		}

		registerFromServerListener(handleAnswer);
		return sendToServerLogic(new BeginSessionRequest(Activator.getManager().getCurrentUserHash()), new Consumer<ServerToClient>() {
			@Override
			public void accept(ServerToClient response) {
				if (response instanceof BeginSessionResponse == false) {
					throw new IllegalStateException();
				}

				// save initial data in the response of the server
				Activator.getManager().setDataServer(((BeginSessionResponse) response).getInitialClientData());

				session.set(true);
				if (runAfterSessionStart != null) {
					runAfterSessionStart.run();
				}
			}
		});
	}

	/**
	 * 
	 * This method is non-blocking!
	 * This method can be called from any thread.
	 * @param shell the shell to use for the login (if necessary)
	 * @param runAfterSuccessfulLogin logic (might be null) which is called, after the login was successful
	 * @return true, if the login was successful (i.e. the login request was sent successfully to the server),
	 * false, if it failed at some point
	 */
	protected boolean ensureLogin(Shell shell, Runnable runAfterSuccessfulLogin) {
		Shell shellToUse = getShellToUse(shell);
		if (session.get() == false) {
			// missing session => 1. start session, 2. login
			return ensureSession(shellToUse, () -> ensureLogin(shellToUse, runAfterSuccessfulLogin)); // non-blocking
		}

		if (login.get()) {
			// already logged in
			if (runAfterSuccessfulLogin != null) {
				runAfterSuccessfulLogin.run();
			}
			return true;
		}

		// try to login now ...

		// select the lecture
		AtomicReference<Lecture> selectedLecture = new AtomicReference<Lecture>(null);
		PluginUtils.runOnUiThreadBlocking(new Runnable() {
			@Override
			public void run() {
				LoginLectureDialog dialog = new LoginLectureDialog(shellToUse);
				dialog.create();
				if (dialog.open() == Window.OK) {
					Lecture selection = dialog.getLecture();
					selectedLecture.set(selection);
				} else {
					// keep the MQTT connection, but login is not successful
				}
			}
		});
		Lecture lecture = selectedLecture.get();
		if (lecture == null) {
			return false;
		}

		// login (with the selected lecture)
		return sendToServerLogic(new LoginRequest(Activator.getManager().getCurrentUserHash(), lecture.getId()), new Consumer<ServerToClient>() {
			@Override
			public void accept(ServerToClient response) {
				if (response instanceof LoginResponse == false) {
					throw new IllegalStateException("" + response);
				}
				LoginResponse loginResponse = (LoginResponse) response;
				if (loginResponse.isLoginResult()) {
					// save the updated data from the server locally
					Activator.getManager().setDataServer(loginResponse.getTaskDatabase());
					
					// remember this lecture for the current user
					EList<Lecture> lectures = Activator.getManager().getCurrentUser().getLectures();
					if (lectures.contains(lecture) == false) {
						lectures.add(lecture);
					}
					
					// now, the connection is usable!
					login.set(true);
					if (runAfterSuccessfulLogin != null) {
						runAfterSuccessfulLogin.run();
					}
				} else {
					// keep the MQTT connection, but login is not successful
					Logging.messageError(shellToUse, "You are not registered as user in the lecture '" + lecture.getName() + "'. Did you selected the correct lecture?");
				}
			}
		});
	}

	@Override
	public boolean sendToServer(ClientToServer message, Consumer<ServerToClient> reactOnNextMessage) {
		if (client == null || session.get() == false || login.get() == false) {
			return false;
		}
		return sendToServerLogic(message, reactOnNextMessage);
	}

	protected boolean sendToServerLogic(ClientToServer message, Consumer<ServerToClient> reactOnNextMessage) {
		if (Objects.equals(message.getClientID(), Activator.getManager().getCurrentUserHash()) == false) {
			throw new IllegalStateException("the current user hash must be used in the message");
		}

		// use MQTT connection to send the event
		if (reactOnNextMessage != null) {
			currentReaction = reactOnNextMessage;
		}
		try {
			client.send(message);
			return true;
		} catch (IOException e) {
			Logging.error("failed to send message to the server: " + message, e, null);
			currentReaction = null;
			return false;
		}
	}

	protected void handleAnswer(ServerToClient answer) {
		if (currentReaction == null) {
			return;
		}

		Consumer<ServerToClient> temp = currentReaction;

		// 1. de-register all stuff
		currentReaction = null;

		// 2. call the handler, because it could start another request with response!
		temp.accept(answer);
	}

	@Override
	public void registerFromServerListener(Consumer<ServerToClient> messageReceiver) {
		Objects.requireNonNull(messageReceiver);
		if (receiver.contains(messageReceiver) == false) {
			receiver.add(messageReceiver);
		}
	}

	@Override
	public void deregisterFromServerListener(Consumer<ServerToClient> messageReceiver) {
		receiver.remove(messageReceiver);
	}

	@Override
	public void handle(ServerToClient message) {
		if (PluginManager.DEVELOPMENT_MODE) {
			Logging.info("message from server: " + message, null);
		}
		new ArrayList<>(receiver).forEach(r -> r.accept(message));
	}

	@Override
	public void closeCommunication() {
		deregisterFromServerListener(handleAnswer);
		session.set(false);
		login.set(false);
		if (client != null) {
			try {
				client.removeListener(ServerToClient.class);
				client.close();
			} catch (IOException e) {
				Logging.error("failed to remove the listener", null);
			} catch (Exception e) {
				Logging.error("failed to close the communication", null);
			} finally {
				client = null;
			}
		}
	}
}
