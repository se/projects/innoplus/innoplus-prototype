package de.unioldenburg.se.innoplus.eclipse;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.InnoplusDatabase;
import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.data.Person;
import de.unioldenburg.se.innoplus.data.Student;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.eclipse.communication.Communication;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.feedback.FeedbackResponse;

public class PluginManager {
	/**
	 * Flag which eases the development by less dialogs and more initial data (true),
	 * but ensuring the production mode by 'false'.
	 */
	public static final boolean DEVELOPMENT_MODE = false;

	protected final Communication communication;
	protected InnoplusDatabase dataServer; // data from the server (generic): lectures, task, categories, ...
	protected InnoplusDatabase dataClient; // data from the client (specific for the current session): user, results, ...

	protected final AtomicInteger requestCounter;
	protected final Consumer<ServerToClient> messageListener;

	public PluginManager(Communication communication) {
		super();
		this.communication = Objects.requireNonNull(communication);

		DataFactory factory = DataFactory.eINSTANCE;
		dataClient = factory.createInnoplusDatabase();

		requestCounter = new AtomicInteger(0);
		messageListener = message -> reactOnServerMessage(message);
		this.communication.registerFromServerListener(messageListener); // TODO: deregister fehlt noch!

		// template for the own student representing the current user of the Eclipse plugin
		Student student = factory.createStudent();
		dataClient.getPersons().add(student);
	}

	public void reactOnServerMessage(ServerToClient message) {
		if (message instanceof FeedbackResponse) {
			requestCounter.incrementAndGet();
		}
	}

	/**
	 * Determines, if it makes sense to ask for the questionnaire.
	 * @return true, if the user should be informed about the questionnaire
	 */
	public boolean askForQuestionnaire() {
		return requestCounter.get() >= 1;
	}
	/**
	 * Informs about the event, that the user opened the questionnaire.
	 */
	public void openedQuestionnaireLink() {
		requestCounter.set(0);
	}

	public Communication getCommunication() {
		return communication;
	}

	public Person getCurrentUser() {
		return dataClient.getPersons().get(0);
	}

	public String getCurrentUserHash() {
		return getCurrentUser().getId();
	}

	public void setCurrentUserHash(String currentUserHash) {
		getCurrentUser().setId(currentUserHash);
	}

	public InnoplusDatabase getDataServer() {
		return dataServer;
	}

	public void setDataServer(InnoplusDatabase data) {
		this.dataServer = data;
	}

	public List<Task> getTasks() {
		/*
		 * TODO:
		 * - sub DBs
		 * - filter regarding the current lecture of the current user (he selected during log-in)
		 */
		return dataServer.getTasks();
	}

	public List<Lecture> getLectures() {
		/*
		 * TODO:
		 * - sub DBs
		 */
		return dataServer.getLectures();
	}
}
