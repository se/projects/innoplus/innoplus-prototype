package de.unioldenburg.se.innoplus.eclipse.marker;

import org.eclipse.core.resources.IMarker;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMarkerResolution2;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.eclipse.Logging;
import de.unioldenburg.se.innoplus.eclipse.PluginUtils;
import de.unioldenburg.se.innoplus.eclipse.feedback.FeedbackEntryContainer;
import de.unioldenburg.se.innoplus.eclipse.feedback.FeedbackView;

public class MarkerActionShowInformation implements IMarkerResolution2 {

	@Override
	public String getLabel() {
		return "Show (meta) information";
	}

	@Override
	public void run(IMarker marker) {
		String key = marker.getAttribute("key", null);
		if (key == null) {
			Logging.error("missing hint-key for " + marker, null);
			return;
		}
		/*
		 * Instead of using the FeedbackEntryContainer
		 * - which is not very nice
		 * - the "message to show" could be added directly into the marker as property/value
		 * - but this design is more extendable for the future
		 */
		FeedbackEntryContainer container = FeedbackView.getData(key);
		if (container == null) {
			Logging.error("missing entry for key " + key, null);
			return;
		}

		QueryResult hint = container.getResult();
		PluginUtils.showHintDetailsDialog(hint);
	}

	@Override
	public String getDescription() {
		return "This shows some more (meta) information about the current feedback.";
	}

	@Override
	public Image getImage() {
		/* this icon represents the kind of "quick fix", NOT the kind of hint!
		 *
		 * for the icon of the hint itself:
		 * https://stackoverflow.com/questions/2888207/eclipse-plugin-custom-icon-for-a-marker
		 */
		return null;
	}

}
