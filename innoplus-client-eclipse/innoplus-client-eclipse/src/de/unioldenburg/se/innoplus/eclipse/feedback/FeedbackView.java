package de.unioldenburg.se.innoplus.eclipse.feedback;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.inject.Inject;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.IDocumentProvider;

import de.unioldenburg.se.innoplus.data.DataFactory;
import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;
import de.unioldenburg.se.innoplus.data.StudentSolution;
import de.unioldenburg.se.innoplus.eclipse.Activator;
import de.unioldenburg.se.innoplus.eclipse.Logging;
import de.unioldenburg.se.innoplus.eclipse.PluginManager;
import de.unioldenburg.se.innoplus.eclipse.PluginUtils;
import de.unioldenburg.se.innoplus.eclipse.communication.Communication;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.feedback.FeedbackResponse;
import de.unioldenburg.se.innoplus.message.feedback.JavaFeedbackResponse;


/**
 * This implementation bases on a generated sample of Eclipse for new views.
 *
 * An alternative solution could be a CustomMarkersView as described here http://blog.eclipse-tips.com/2008/11/creating-custom-marker-view.html,
 * but that resource was found after this implementation and seems to be a bit too restricted.
 *
 * @author Johannes Meier
 */
public class FeedbackView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "de.unioldenburg.se.innoplus.eclipse.FeedbackView";

	public static final String MARKER = "de.unioldenburg.se.innoplus.eclipse.marker";

	public static final String QUESTIONNAIRE_URL = "https://uol.de/se?umfrage-innovationplus";

	@Inject IWorkbench workbench;

	private TableViewer viewer;
	private Action actionConnect;
	private Action actionClearFeedback;
	private Action actionQuestionnaireAsk;
	private Action actionQuestionnaireAskExplicit;
	private Action actionQuestionnaireOpen;
	private Action doubleClickAction;

	protected List<FeedbackEntryContainer> data = new ArrayList<>();
	protected Consumer<ServerToClient> handleMessages;

	/*
	 * Make the feedback entries with their marker available outside of this view
	 * - there is only ONE instance of FeedbackView!
	 */
	protected static Map<String, FeedbackEntryContainer> mapHints;
	public static FeedbackEntryContainer getData(String key) {
		if (mapHints == null) {
			return null;
		}
		return mapHints.get(key);
	}

	/**
	 * Contains information about the current request, how to map files from the server to the corresponding paths in the Eclipse project.
	 * (path for/of server --> path in local Eclipse project)
	 */
	public static final Map<String, String> mapResolveFilePaths = new HashMap<>();

	private static FeedbackView instance;
	public static FeedbackView getInstance(boolean openIfNotExistingOrBringToFront) {
		if (instance == null || openIfNotExistingOrBringToFront) {
			PluginUtils.runOnUiThreadBlocking(new Runnable() {
				@Override
				public void run() {
					// open the feedback view automatically
					try {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(FeedbackView.ID);
					} catch (PartInitException e) {
						Logging.error("Failed to open the feedback view automatically. Do it manually: -> Window -> Show View -> Other -> Help -> Feedback by Inno+", e, Logging.defaultShell());
					}
				}
			});
			if (instance == null) {
				throw new IllegalStateException();
			}
		}
		return instance;
	}

	@Override
	public void createPartControl(Composite parent) {
		instance = this;
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ArrayContentProvider() {
			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
				// source: https://stackoverflow.com/questions/12480402/adding-a-remove-button-to-a-column-in-a-table
				// This will dispose of all the control button that were created previously
				if (((TableViewer) viewer).getTable() != null && ((TableViewer) viewer).getTable().getChildren() != null) {
					for (Control item : ((TableViewer) viewer).getTable().getChildren()) {
						// at this point there are no other controls embedded in the viewer, however different instances may require more checking of the controls here
						if ((item != null) && (item.isDisposed() == false)) {
							item.dispose();
						}
					}
				}

				super.inputChanged(viewer, oldInput, newInput);
			}
		});
		FeedbackViewComparator comparator = new FeedbackViewComparator();
		viewer.setComparator(comparator);
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableViewerColumn t1 = createTableColumn(0, comparator);
		t1.getColumn().setText("Type");
		t1.getColumn().setWidth(120);
		t1.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((FeedbackEntryContainer) element).getResult().getType() + "";
			}

			@Override
			public Image getImage(Object element) {
				return Activator.getImage((((FeedbackEntryContainer) element).getResult().getType())).createImage();
			}
		});

		TableViewerColumn t2 = createTableColumn(1, comparator);
		t2.getColumn().setText("Summary");
		t2.getColumn().setWidth(400);
		t2.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((FeedbackEntryContainer) element).getResult().getSummary();
			}
		});

		TableViewerColumn t3 = createTableColumn(2, comparator);
		t3.getColumn().setText("Path");
		t3.getColumn().setWidth(450);
		t3.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((FeedbackEntryContainer) element).getPathLocal(true);
			}
		});

		// "jump to the location"-action
		/* use links/hyperlinks instead?
		 * https://stackoverflow.com/questions/62454367/how-to-add-hyperlink-to-a-java-swt-label
		 * https://gist.github.com/wduminy/6199987
		 */
		TableViewerColumn t4 = createTableColumn(3, comparator);
		t4.getColumn().setText("Actions");
		t4.getColumn().setWidth(100);
		t4.setLabelProvider(new ColumnLabelProvider() {
			/* remember created elements to improve performance
			 * - source: https://stackoverflow.com/questions/12480402/adding-a-remove-button-to-a-column-in-a-table
			 * - make sure you dispose these buttons when viewer input changes!
			 */
			Map<Object, Button> buttons = new HashMap<>();
			Map<Object, TableEditor> editors = new HashMap<>();

			@Override
			public String getText(Object element) {
				return ""; // show no text in any case (if no button is show, the text is shown otherwise)
			}

			@Override
			public void update(ViewerCell cell) {
				TableItem tableItem = (TableItem) cell.getItem();
				if (tableItem == null) {
					return;
				}
				FeedbackEntryContainer entry = (FeedbackEntryContainer) cell.getElement();
				Composite parentControl = (Composite) cell.getViewerRow().getControl();

				Button button;
				TableEditor editor;
				if (entry.isPathLocalUsable() == false) {
					// no marker and no location/file available => show empty cell
				} else {
					if (buttons.containsKey(entry) && buttons.get(entry).isDisposed() == false) {
						button = buttons.get(entry);
						button.setParent(parentControl);
						editor = editors.get(entry);
					} else {
						button = new Button(parentControl, SWT.PUSH | SWT.CENTER);
						button.setText("Show in Code");
						buttons.put(entry, button);
						button.addSelectionListener(new SelectionListener() {
							@Override
							public void widgetSelected(SelectionEvent e) {
								openLocation((FeedbackEntryContainer) ((Button) e.getSource()).getData());
							}

							@Override
							public void widgetDefaultSelected(SelectionEvent e) {
								// do nothing
							}
						});
						button.setData(entry);

						editor = new TableEditor(tableItem.getParent());
						editor.grabHorizontal = true;
						editor.grabVertical = true;
						editors.put(entry, editor);
					}
					editor.setEditor(button, tableItem, cell.getColumnIndex());
					editor.layout();
				}

				super.update(cell); // sets text and image (using this method or not seems to make no difference)
			}

			@Override
			public void dispose(ColumnViewer viewer, ViewerColumn column) {
				editors.values().stream().forEach(b -> b.dispose());
				editors.clear();
				buttons.values().stream().filter(b -> b.isDisposed() == false).forEach(b -> b.dispose());
				buttons.clear();
				super.dispose(viewer, column);
			}
		});

		// different actions depending on the current cell? https://stackoverflow.com/questions/13991075/how-to-set-double-click-to-edit-a-table-cell-in-swt-jface
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});

		viewer.setInput(data); // must be used AFTER the content provider (and all other provider ...) were set!
		if (handleMessages == null) {
			handleMessages = new Consumer<ServerToClient>() {
				@Override
				public void accept(ServerToClient message) {
					if (message instanceof FeedbackResponse) {
						handleNewFeedback((FeedbackResponse) message);
					}
				}
			};
		}
		Activator.getManager().getCommunication().registerFromServerListener(handleMessages);

		// Create the help context id for the viewer's control
		workbench.getHelpSystem().setHelp(viewer.getControl(), "innoplus-client-eclipse.viewer");
		getSite().setSelectionProvider(viewer);
		makeActions();
		contributeToActionBars();

		if (PluginManager.DEVELOPMENT_MODE) {
			// in DEV_MODE, add some data for testing!
			DataFactory factory = DataFactory.eINSTANCE;
			StudentSolution solution = factory.createStudentSolution();
			QueryResult r = factory.createQueryResult();
			r.setDate(new Date());
			r.setSummary("Hint to show (initial entry): summary");
			r.setDescription("Hint to show (initial entry): description");
			r.setType(QueryResultType.ERROR);
//			r.setLocation("path/of/file.java");
//			r.setLocation("TestProjectMerge/src/mainpack/Main.java");
			r.setLocation("TestProjectMerge/src/mainpack/Main.java:7:21-8:9"); // first line has the number 1, the most left column has the number 1!
			solution.getQueryResults().add(r);
			handleNewFeedback(new JavaFeedbackResponse("ID-missing", solution));
		}
	}

	private TableViewerColumn createTableColumn(int columnIndex, FeedbackViewComparator comparator) {
		// adapted from https://www.vogella.com/tutorials/EclipseJFaceTable/article.html#sort-content-of-table-columns
		TableViewerColumn result = new TableViewerColumn(viewer, SWT.NONE, columnIndex);
		final TableColumn column = result.getColumn();
		column.setResizable(true);
		column.setMoveable(true);
		column.addSelectionListener(new SelectionAdapter() {
			@Override
            public void widgetSelected(SelectionEvent e) {
                comparator.setColumn(columnIndex);
                int dir = comparator.getDirection();
                viewer.getTable().setSortDirection(dir);
                viewer.getTable().setSortColumn(column);
                redrawTable();
            }
		});
		return result;
	}

	@SuppressWarnings("unused")
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				FeedbackView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(actionConnect);
		manager.add(actionClearFeedback);
		manager.add(actionQuestionnaireAskExplicit);
		// Other plug-ins can contribute actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
//		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(actionConnect);
//		manager.add(new Separator());
		manager.add(actionClearFeedback);
		manager.add(actionQuestionnaireAskExplicit);
	}

	@SuppressWarnings("unused")
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(actionConnect);
		manager.add(actionClearFeedback);
		manager.add(actionQuestionnaireAskExplicit);
	}

	private void makeActions() {
		actionConnect = new Action() {
			public void run() {
				Communication communication = Activator.getManager().getCommunication();
				communication.closeCommunication();
				Activator.getManager().setCurrentUserHash(null);
				communication.beginCommunication(viewer.getControl().getShell(), null);
			}
		};
		actionConnect.setText("Login to Server");
		actionConnect.setToolTipText("Resets an existing connection and asks for credentials and tries to login (again).");
		actionConnect.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		actionClearFeedback = new Action() {
			public void run() {
				clearFeedback(true);
			}
		};
		actionClearFeedback.setText("Clear Feedback");
		actionClearFeedback.setToolTipText("Clears the whole feedback including the annotations inside the editors.");
		actionClearFeedback.setImageDescriptor(workbench.getSharedImages().
				getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		actionQuestionnaireAskExplicit = new Action() {
			public void run() {
				if (Activator.getManager().askForQuestionnaire()) {
					actionQuestionnaireAsk.run();
				} else {
					Logging.messageInformation("Please request feedback for your results before answering the questionnaire!");
				}
			}
		};
		actionQuestionnaireAskExplicit.setText("Open Questionnaire");
		actionQuestionnaireAskExplicit.setToolTipText("Opens the Questionnaire externally in your default browser.");
		actionQuestionnaireAskExplicit.setImageDescriptor(workbench.getSharedImages().
				getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));
		actionQuestionnaireAsk = new Action() {
			public void run() {
				if (Activator.getManager().askForQuestionnaire() == false) {
					return;
				}
				boolean open = MessageDialog.openQuestion(Logging.defaultShell(), "Questionnaire for Evaluation",
						"After giving you some hints, Inno+ wants to get some feedback from you: "
						+ "Please answer some questions in a prepared questionnaire to support the ongoing development of Inno+!\n\n"
						+ "This questionnaire is in German and is hosted at the University of Oldenburg. "
						+ "After clicking on 'Yes', the questionnaire will be openend in your default browser. "
						+ "This ensures, that it is impossible to interrelate your answers with your submissions.\n\n"
						+ "Thank you very much for your support!");
				if (open) {
					actionQuestionnaireOpen.run();
				}
			}
		};
		actionQuestionnaireAsk.setText("Open Questionnaire");
		actionQuestionnaireAsk.setToolTipText("Open the Questionnaire externally in your default browser.");
		actionQuestionnaireAsk.setImageDescriptor(workbench.getSharedImages().
				getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		actionQuestionnaireOpen = new Action() {
			public void run() {
				try {
				    URL url = new URL(QUESTIONNAIRE_URL);
					PlatformUI.getWorkbench().getBrowserSupport().getExternalBrowser().openURL(url);
					Activator.getManager().openedQuestionnaireLink();
				} catch (PartInitException | MalformedURLException e) {
					Logging.error("Opening the website failed", e, Logging.defaultShell());
				}
			}
		};
		actionQuestionnaireOpen.setText("Open Questionnaire");
		actionQuestionnaireOpen.setToolTipText("Open the Questionnaire externally in your default browser.");
		actionQuestionnaireOpen.setImageDescriptor(workbench.getSharedImages().
				getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));

		doubleClickAction = new Action() {
			public void run() {
				IStructuredSelection selection = viewer.getStructuredSelection();
				FeedbackEntryContainer obj = (FeedbackEntryContainer) selection.getFirstElement();
				if (obj == null) {
					return;
				}

				PluginUtils.showHintDetailsDialog(obj.getResult());
			}

		};
	}

	private void openLocation(FeedbackEntryContainer obj) {
		if (obj == null) {
			return;
		}
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		// https://stackoverflow.com/questions/19239393/eclipse-plugin-how-to-open-file-in-ide-by-code

		// open at the position of the Marker
		if (obj.getMarker() != null) {
			try {
				IDE.openEditor(page, obj.getMarker()); // open the file directly at the shown marker!
				return; // ready
			} catch (PartInitException e) {
				Logging.error("failed to open " + obj.getResult().getLocation() + " at its position", e, null);
				// try to open the location with the next option ...
			}
		}

		// open File (without marker and without line numbers)
		if (obj.isPathLocalUsable()) {
			String location = obj.getPathLocal(false);
			try {
				IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(location));
				// opens the file at the beginning of the document
				IDE.openEditor(page, file, true);
				return; // ready
			} catch (PartInitException | IllegalArgumentException e) {
				Logging.error("failed to open " + location, e, null);
				// try to open the location with the next option ...
			}
		}

		// failed to open
		Logging.messageError("failed to open " + obj.getResult().getLocation());
	}

	public void handleNewFeedback(FeedbackResponse feedback) {
		PluginUtils.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// bring the Feedback view to the foreground
				getInstance(true);

				// clear the existing feedback
				clearFeedback(false);

				// stop the animation
				getViewSite().getActionBars().getStatusLineManager().getProgressMonitor().done();

				// get new hints
				List<QueryResult> hints = feedback.getResult().getQueryResults();
				if (mapHints == null) {
					mapHints = new HashMap<>();
				}

				/*
				 * create new marker
				 * https://itsallbinary.com/simple-useful-eclipse-plugin-to-scan-java-source-good-example-for-beginners/
				 */
				for (int i = 0; i < hints.size(); i++) {
					QueryResult result = hints.get(i);
					IMarker marker = null;
					String key = "hint-" + Integer.toHexString(result.hashCode());

					// map the server path to the local path
					String pathLocal = result.getLocation();
					if (pathLocal == null) {
						pathLocal = "";
					}
					if (pathLocal.isBlank() == false) {
						// calculate the local path
						int index = pathLocal.indexOf(":");
						if (index <= 0) {
							pathLocal = "";
						} else {
							pathLocal = pathLocal.substring(0, index);
						}
						// fix path: server VS client format
						if (mapResolveFilePaths.containsKey(pathLocal)) {
							pathLocal = mapResolveFilePaths.get(pathLocal);
						}
					}

					// https://stackoverflow.com/questions/2148821/retrieve-a-file-from-within-a-folder-in-the-workspace?noredirect=1&lq=1
					IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
					IResource resource = null;
					if (pathLocal.isBlank() == false) {
						resource = root.findMember(pathLocal);
						if (resource == null) {
							Logging.error("no resource found for marker: " + pathLocal, null);
						}
					}

					if (resource != null) {
						try {
							marker = resource.createMarker(FeedbackView.MARKER);
							marker.setAttribute("key", key);
							marker.setAttribute("summary", result.getSummary()); // theoretisch müsen diese Infos gar nicht im Marker gespeichert werden, da die Infos über die statische Map und nicht über den Marker bezogen werden
							marker.setAttribute("description", result.getDescription());
//							marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING); // is not shown!
							marker.setAttribute(IMarker.MESSAGE, result.getSummary());

							// parse the range in text files: here, first row/column start with the number/index 0!
							String loc = result.getLocation();
							if (loc.contains(":")) {

								// 0th file name
								int index = loc.indexOf(":");
								loc = loc.substring(index + 1);

								index = loc.indexOf(":");
								int startLine = parseInt(loc.substring(0, index)) - 1;
								loc = loc.substring(index + 1);

								index = loc.indexOf("-");
								int startColumn = parseInt(loc.substring(0, index)) - 1;
								loc = loc.substring(index + 1);

								index = loc.indexOf(":");
								int endLine = parseInt(loc.substring(0, index)) - 1;
								loc = loc.substring(index + 1);

								int endColumn = parseInt(loc) - 1;
								loc = loc.substring(index);


								// fix range: "-1" values representing incomplete information
								endLine = Math.max(endLine, startLine);
								if (startLine >= 0) {
									startColumn = Math.max(startColumn, 0);
								}
								if (startLine == endLine) {
									endColumn = Math.max(endColumn, startColumn);
								}

								// open the resource as text document: http://eclipse.1072660.n5.nabble.com/how-to-get-the-IDocument-of-an-ITextFileBuffer-created-from-an-IFile-td59106.html
								IDocumentProvider provider = new TextFileDocumentProvider();
								IFile file = root.getFile(resource.getFullPath());
								provider.connect(file);
								IDocument doc = provider.getDocument(file);

								// https://stackoverflow.com/questions/26980073/how-to-set-the-linenumber-of-an-imarker-for-a-icompilationunit
								try {
									int charStart = doc.getLineOffset(startLine) + startColumn;
									int charEnd = doc.getLineOffset(endLine) + endColumn;
									marker.setAttribute(IMarker.CHAR_START, charStart);
									marker.setAttribute(IMarker.CHAR_END, charEnd);
								} catch (BadLocationException e) {
									Logging.error("failed to create a marker for " + result, e, null);
									marker = null;
								}

//								marker.setAttribute(IMarker.LINE_NUMBER, 8); // specify line number XOR ...
//								marker.setAttribute(IMarker.CHAR_START, 11); // ... char position: its value is globally, not locally within one line!
//								marker.setAttribute(IMarker.CHAR_END, 22); // ... char position: its value is globally, not locally within one line!
							} else {
								// no line numbers!
							}
						} catch (CoreException e) {
							Logging.error("failed to create a marker for " + result, e, null);
							marker = null;
						}
					} else {
						// do nothing
					}

					FeedbackEntryContainer entry = new FeedbackEntryContainer(result, marker, pathLocal);
					data.add(entry);
					mapHints.put(key, entry);
				}
				mapResolveFilePaths.clear();

				// update the shown feedback entries
				viewer.setInput(data);
				redrawTable();
			}
		});
	}

	private int parseInt(String value) {
		if (value == null || value.isEmpty()) {
			return -1;
		}
		try {
			return Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			return -1;
		}
	}

	/**
	 * Inform the FeedbackView about the new Request for feedback.
	 * This notification must be done manually, since the MqttClient does not support listening for ClientToServer messages!
	 */
	public void requestedNewFeedback() {
		PluginUtils.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				clearFeedback(true);

				// start animation for the request during the waiting time
				getViewSite().getActionBars().getStatusLineManager().getProgressMonitor().beginTask("Calculating individual Feedback on the Server ...", IProgressMonitor.UNKNOWN);
			}
		});
	}

	public void clearFeedback(boolean refresh) {
		PluginUtils.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// remove old marker
				data.stream().filter(e -> e.getMarker() != null).forEach(e -> {
					try {
						e.getMarker().delete();
					} catch (CoreException ex) {
						// ignore it
						Logging.error("removing a marker failed", ex, null);
					}
				});

				// remove old data
				viewer.setInput(null);
//				viewer.setInput(Collections.emptyList()); // makes no difference
				data.clear();

				if (mapHints != null) {
					mapHints.clear();
				}

				if (refresh) {
					redrawTable();
				}
			}
		});
	}

	private void redrawTable() {
		viewer.refresh(); // does not work
		viewer.getTable().getParent().layout(); // updated only one column
		viewer.getTable().getParent().redraw(); // marks the parent to be redrawn
		viewer.getTable().getParent().update(); // forces to fulfill all redraw requests
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		// show the questionnaire
		if (PluginManager.DEVELOPMENT_MODE) {
			// for development, this behaviour is annoying ...
		} else {
			actionQuestionnaireAsk.run();
		}

		// clear the existing feedback including the markers!
		clearFeedback(false);

		Activator.getManager().getCommunication().deregisterFromServerListener(handleMessages);
		super.dispose();
		instance = null;
	}
}
