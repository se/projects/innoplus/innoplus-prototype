package de.unioldenburg.se.innoplus.eclipse.communication;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.unioldenburg.se.innoplus.data.Lecture;
import de.unioldenburg.se.innoplus.eclipse.Activator;

public class LoginLectureDialog extends TitleAreaDialog {

	// GUI elements
	private ComboViewer lectureCombo;

	// data
	private Lecture lecture;

	public LoginLectureDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("Login to the Inno+ Server");
		setMessage("Login to the Inno+ Server to request feedback: Select the lecture.", IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// http://www.vogella.com/tutorials/EclipseDialogs/article.html#titleareadialog
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(2, false);
		container.setLayout(layout);

		// lecture
		Label labelTask = new Label(container, SWT.NONE);
		labelTask.setText("Select the Lecture:");
		lectureCombo = new ComboViewer(container, SWT.READ_ONLY);
		lectureCombo.setContentProvider(ArrayContentProvider.getInstance());
		lectureCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Lecture) {
					return ((Lecture) element).getName();
				}
				return super.getText(element);
			}
		});
		lectureCombo.setInput(Activator.getManager().getLectures());

		return area;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		lecture = (Lecture) lectureCombo.getStructuredSelection().getFirstElement();
		super.okPressed();
	}

	public Lecture getLecture() {
		return lecture;
	}
}
