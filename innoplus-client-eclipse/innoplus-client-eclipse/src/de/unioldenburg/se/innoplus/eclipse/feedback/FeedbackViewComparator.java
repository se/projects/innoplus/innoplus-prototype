package de.unioldenburg.se.innoplus.eclipse.feedback;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;

/* adapted from
 * https://www.vogella.com/tutorials/EclipseJFaceTable/article.html#sort-content-of-table-columns
 */
public class FeedbackViewComparator extends ViewerComparator {
	private int propertyIndex;
	private static final int DESCENDING = 1;
	private int direction = DESCENDING;

	public FeedbackViewComparator() {
		super();
		this.propertyIndex = 0;
		direction = DESCENDING;
	}

	public int getDirection() {
		return direction == 1 ? SWT.DOWN : SWT.UP;
	}

	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		} else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = DESCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		FeedbackEntryContainer p1 = (FeedbackEntryContainer) e1;
		FeedbackEntryContainer p2 = (FeedbackEntryContainer) e2;
		int rc = 0;
		switch (propertyIndex) {
		case 0:
			rc = p1.getResult().getType().toString().compareTo(p2.getResult().getType().toString());
			break;
		case 1:
			rc = p1.getResult().getSummary().compareTo(p2.getResult().getSummary());
			break;
		case 2:
			rc = p1.getPathLocal(true).compareTo(p2.getPathLocal(true));
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}
}
