package de.unioldenburg.se.innoplus.eclipse;

import java.text.SimpleDateFormat;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import de.unioldenburg.se.innoplus.data.QueryResult;
import de.unioldenburg.se.innoplus.data.QueryResultType;

public class PluginUtils {

	public static void runOnUiThread(Runnable run) {
		if (Thread.currentThread() == Display.getDefault().getThread()) {
			run.run();
		} else {
			Display.getDefault().asyncExec(run);
		}
	}

	public static void runOnUiThreadBlocking(Runnable run) {
		if (Thread.currentThread() == Display.getDefault().getThread()) {
			run.run();
		} else {
			Display.getDefault().syncExec(run);
		}
	}


	public static int getMessageType(QueryResultType type) {
		if (type == null) {
			throw new IllegalArgumentException();
		}
		switch (type) {
		case INFORMATION:
			return IMessageProvider.INFORMATION;
		case SUCCESS:
			return IMessageProvider.INFORMATION;
		case WARNING:
			return IMessageProvider.WARNING;
		case ERROR:
			return IMessageProvider.ERROR;
		default:
			throw new IllegalStateException("unknown type " + type);
		}
	}

	public static void showHintDetailsDialog(QueryResult hint) {
		// show description/details (or summary is fall-back)
		String msg = hint.getDescription();
		if (msg == null || msg.isBlank()) {
			msg = hint.getSummary();
		}
		final String text = msg;

		/* make the default dialog resizable and add scroll bars for the message (important, if the message contains a stack trace!)
		 * https://www.vogella.com/tutorials/EclipseDialogs/article.html
		 */
		Dialog dialog = new TitleAreaDialog(Logging.defaultShell()) {
			@Override
			public void create() {
				super.create();
				setTitle("Inno+ hint: " + hint.getSummary());
				setMessage("Here are the details for this hint, " +
						"detected on " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hint.getDate()) + ":",
						PluginUtils.getMessageType(hint.getType()));
			}

			@Override
			protected Control createDialogArea(Composite parent) {
				Composite parentArea = (Composite) super.createDialogArea(parent);

				/* https://stackoverflow.com/questions/32814028/scrolledcomposite-no-scrollbar-in-jface-dialog
				 * https://stackoverflow.com/questions/12660842/jface-dialog-with-scrolledcomposite?rq=1
				 */
				ScrolledComposite sc = new ScrolledComposite(parentArea, SWT.V_SCROLL | SWT.H_SCROLL); // | SWT.BORDER

				Composite content = new Composite(sc, SWT.NONE);

//		        content.setLayout(new GridLayout(1, false));
				content.setLayout(new FillLayout(SWT.VERTICAL));

		        Label label = new Label(content, SWT.NONE);
		        label.setText(text);

		        GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		        layoutData.heightHint = 250;
		        layoutData.widthHint = 450;
		        sc.setLayoutData(layoutData);

		        sc.setContent(content);
		        sc.setExpandHorizontal(true);
		        sc.setExpandVertical(true);
		        sc.setMinSize(content.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		        return parentArea;
			}

			@Override
			protected boolean isResizable() {
				return true;
			}
		};
//		MessageDialog dialog = new MessageDialog(Logging.defaultShell(), "Details",
//				null, msg, MessageDialog.INFORMATION, 0, new String[] { IDialogConstants.OK_LABEL }) {
//			@Override
//			protected boolean isResizable() {
//				return true;
//			}
//		};
//		dialog.setShellStyle(SWT.NONE);
		dialog.open();
	}

}
