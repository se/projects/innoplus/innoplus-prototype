package de.unioldenburg.se.innoplus.eclipse.marker;

import org.eclipse.core.resources.IMarker;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator2;

/**
 * Provides "Quick Fixes" for the Inno+ marker.
 *
 * https://stackoverflow.com/questions/13436440/how-can-i-add-an-eclipse-quick-fix-for-a-custom-java-marker
 *
 * @author Johannes Meier
 */
public class FeedbackMarkerResolutionProvider implements IMarkerResolutionGenerator2 {

	@Override
	public IMarkerResolution[] getResolutions(IMarker marker) {
		IMarkerResolution[] result = new IMarkerResolution[1];
		result[0] = new MarkerActionShowInformation();
		return result;
	}

	@Override
	public boolean hasResolutions(IMarker marker) {
		return true;
	}

}
