package de.unioldenburg.se.innoplus.eclipse.communication;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import org.eclipse.swt.widgets.Shell;

import de.unioldenburg.se.innoplus.eclipse.Logging;
import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.ServerToClient;

public class CommunicationDirect implements Communication {
	protected final List<Consumer<ServerToClient>> receiver;

	public CommunicationDirect() {
		super();
		receiver = new ArrayList<>();
	}

	@Override
	public void ensureRunningCommunication(Shell parentShell, Runnable runAfterwards) {
		// do nothing
		if (runAfterwards != null) {
			runAfterwards.run();
		}
	}

	@Override
	public boolean beginCommunication(Shell parentShell, Runnable runAfterwards) {
		if (runAfterwards != null) {
			runAfterwards.run();
		}
		return true;
	}

	@Override
	public boolean sendToServer(ClientToServer message, Consumer<ServerToClient> reactOnNextMessage) {
		// TODO call response handler??
		Logging.info("Send " + message + " to the server", null);
		return true;
	}

	@Override
	public void registerFromServerListener(Consumer<ServerToClient> messageReceiver) {
		Objects.requireNonNull(messageReceiver);
		if (receiver.contains(messageReceiver) == false) {
			receiver.add(messageReceiver);
		}
	}

	@Override
	public void deregisterFromServerListener(Consumer<ServerToClient> messageReceiver) {
		receiver.remove(messageReceiver);
	}

	@Override
	public void closeCommunication() {
		// do nothing
	}
}
