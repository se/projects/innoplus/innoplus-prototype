package de.unioldenburg.se.innoplus.eclipse.communication;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class LoginUsernameDialog extends TitleAreaDialog {

	// GUI elements
	private Text usernameTxt;

	// data
	private String username;

	public LoginUsernameDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("Login to the Inno+ Server");
		setMessage("Login to the Inno+ Server to request feedback. Only the MD5 hash of your Stud.IP user name is used and stored.", IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// http://www.vogella.com/tutorials/EclipseDialogs/article.html#titleareadialog
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(2, false);
		container.setLayout(layout);

		// user name
		Label labelJavaVersion = new Label(container, SWT.NONE);
		String tooltipUsername = "Stud.IP user name in the form 'abcd1234'";
		labelJavaVersion.setToolTipText(tooltipUsername);
		labelJavaVersion.setText(tooltipUsername);

		GridData dataFilename = new GridData(); // required, to make the TextField bigger!
		dataFilename.grabExcessHorizontalSpace = true;
		dataFilename.horizontalAlignment = GridData.FILL;
		usernameTxt = new Text(container, SWT.BORDER);
		usernameTxt.setMessage(tooltipUsername);
		usernameTxt.setText(""); // default value
		usernameTxt.setLayoutData(dataFilename);

		return area;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		username = usernameTxt.getText();
		super.okPressed();
	}

	public String getUsername() {
		return username;
	}
}
