package de.unioldenburg.se.innoplus.eclipse.communication;

import java.util.function.Consumer;

import org.eclipse.swt.widgets.Shell;

import de.unioldenburg.se.innoplus.message.ClientToServer;
import de.unioldenburg.se.innoplus.message.ServerToClient;

public interface Communication {

	/**
	 * This method is non-blocking!
	 * @param message the message to send
	 * @param reactOnNextMessage if not null, this handle is informed about the next message from the server
	 * @return true, if the message was sent successfully, false, if it failed
	 */
	public boolean sendToServer(ClientToServer message, Consumer<ServerToClient> reactOnNextMessage);

	public void registerFromServerListener(Consumer<ServerToClient> messageReceiver);
	public void deregisterFromServerListener(Consumer<ServerToClient> messageReceiver);

	/**
	 * This method is non-blocking!
	 * @param parentShell the shell to use for the login (if necessary)
	 * @param runAfterwards logic (might be null) which is called, after the communication is established including session and login
	 */
	public void ensureRunningCommunication(Shell parentShell, Runnable runAfterwards);

	/**
	 * This method is non-blocking!
	 * @param shell the shell to use for the login (if necessary)
	 * @param runAfterwards logic which is called, after the communication is established (might be null)
	 * @return true, if the connection to the server was established successfully (i.e. the login request was sent successfully to the server),
	 * false, if it failed at some point
	 */
	public boolean beginCommunication(Shell shell, Runnable runAfterwards);

	/**
	 * This method is blocking!
	 */
	public void closeCommunication();
}
