package de.unioldenburg.se.innoplus.eclipse.papyrus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.uml2.uml.Model;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.papyrus.emf.facet.custom.metamodel.v0_2_0.internal.treeproxy.EObjectTreeElement;

import de.unioldenburg.se.innoplus.eclipse.Logging;
import de.unioldenburg.se.innoplus.eclipse.PluginManager;
import de.unioldenburg.se.innoplus.eclipse.PluginUtils;
import de.unioldenburg.se.innoplus.eclipse.feedback.FeedbackView;
import de.unioldenburg.se.innoplus.message.ServerToClient;
import de.unioldenburg.se.innoplus.message.feedback.UMLFeedbackRequest;
import de.unioldenburg.se.innoplus.message.feedback.UMLFeedbackResponse;

public class RequestUMLFeedbackHandler extends AbstractHandler {

	private String umlFile, notationFile, umlFileContent, notationFileContent;
	private final Path workspacePath;

	public RequestUMLFeedbackHandler() {
		super();
		this.workspacePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().toFile().toPath();
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		Shell shell = window.getShell();

		// get selected project
		ISelection sel = HandlerUtil.getActiveMenuSelection(event);
		IStructuredSelection selection = (IStructuredSelection) sel;
		List<?> elems = selection.toList();

		// collect *.uml file and its content
		umlFile = null;
		notationFile = null;
		umlFileContent = null;
		notationFileContent = null;
		boolean skipped = false;

		for (Object selectedElement : elems) {
			try {
				if (selectedElement instanceof IFile) {
					readFromFile((IFile) selectedElement);
					break;
				}
				else if (selectedElement instanceof EObjectTreeElement) {
					var treeEl = (EObjectTreeElement) selectedElement;

					while (treeEl.getParent() != null)
						treeEl = (EObjectTreeElement) treeEl.getParent();

					var model = treeEl.getEObject();
					var res = model.eResource();
					if (res == null)
						continue;

					var uri = res.getURI();
					if (uri == null || !uri.isPlatform()) {
						skipped = true;
						continue;
					}

					umlFile = uri.toPlatformString(true);
					if (umlFile.charAt(0) == '/')
						umlFile = umlFile.substring(1);

					var realPath = workspacePath.resolve(umlFile);
					if (!Files.exists(realPath))
						Logging.error("Could not find UML file!", shell);

					umlFileContent = Files.readString(realPath);
					break;
				}
				// Newer Papyrus versions show a slightly different dialog
				// Test => (uml, di, notation) instead of Test.uml, Test.di, ...
				// https://www.eclipse.org/forums/index.php/t/622877/
				else if (selectedElement instanceof IAdaptable){
					var file = (IFile) ((IAdaptable) selectedElement).getAdapter(IFile.class);

					if (file != null) {
						readFromFile(file);
						break;
					}
				}

			} catch (IOException | CoreException e) {
				throw new ExecutionException("Failed to load files to submit!", e);
			}
		}
		if (umlFile == null) {
			var msg = skipped
					? "Invalid Selection: Please select parts/diagrams of your own model."
					: "Invalid Selection: Nothing was selected.";

			Logging.error(msg, shell);
			return null;
		}

		// ensure connection with the server
		PluginManager manager = de.unioldenburg.se.innoplus.eclipse.Activator.getManager();
		manager.getCommunication().ensureRunningCommunication(shell, new Runnable() {
			@Override
			public void run() {
				PluginUtils.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						/*
						 * dialog for request
						 * - select Lecture + Task!
						 * - show files to submit (not changeable)
						 * - buttons: cancel, submit
						 */
						RequestUMLFeedbackDialog dialog = new RequestUMLFeedbackDialog(shell, umlFile, notationFile);
						dialog.create();
						if (dialog.open() == Window.OK) {

							if (dialog.getTask() == null) {
								return;
							}

							/* open the feedback view automatically:
							 * - this must be done before getting the feedback!
							 * - clear shown feedback!
							 */
							FeedbackView.getInstance(true).requestedNewFeedback();;

							// send request to the server
							if (PluginManager.DEVELOPMENT_MODE) {
								Logging.info("sent files: " + umlFile + ", " + notationFile, shell);
							}
							var request = new UMLFeedbackRequest(manager.getCurrentUserHash(), dialog.getTask().getName(), umlFileContent, notationFileContent);
							boolean sendToServer = manager.getCommunication().sendToServer(request, new Consumer<ServerToClient>() {
								@Override
								public void accept(ServerToClient response) {
									if (response instanceof UMLFeedbackResponse == false) {
										throw new IllegalStateException();
									}

									// open the FeedbackView again, if it was closed in the meantime
									FeedbackView.getInstance(true);

									// show success message
									Logging.info("Received feedback from the server: See 'Feedback by Inno+' view for details.", shell);
								}
							});
							if (sendToServer == false) {
								Logging.error("Failed to send the request to the server.", shell);
							} else {
								// success
								if (PluginManager.DEVELOPMENT_MODE == false) {
									Logging.info("The request for feedback is sent to the server successfully! "
											+ "Since the server executes checks for the functionality and analyzes the quality of your submitted source code, it will take several seconds. "
											+ "Please be patient!", shell);
								}
							}
						}
					}
				});
			}
		});

		return null;
	}

	/** Reads the UML file from an IFile instance **/
	private void readFromFile(IFile file) throws IOException, CoreException {
		umlFile = file.getName();
		umlFileContent = new String(file.getContents().readAllBytes());

		notationFile = "";
		notationFileContent = "";
	}
}
