package de.unioldenburg.se.innoplus.eclipse.papyrus;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.unioldenburg.se.innoplus.InnoplusConstants;
import de.unioldenburg.se.innoplus.data.Task;
import de.unioldenburg.se.innoplus.data.TaskCategory;
import de.unioldenburg.se.innoplus.eclipse.Activator;

public class RequestUMLFeedbackDialog extends TitleAreaDialog {
	// settings to remember
	protected static Task rememberTask;

	// GUI elements
	private ComboViewer taskCombo;

	// return data
	private Task task;
	private final String umlFile;
	private final String notationFile;

	public RequestUMLFeedbackDialog(Shell parentShell, String umlFile, String notationFile) {
		super(parentShell);
		this.umlFile = umlFile;
		this.notationFile = notationFile;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Request Feedback for UML");
		setMessage("Send UML files to the server and get feedback back.", IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		// http://www.vogella.com/tutorials/EclipseDialogs/article.html#titleareadialog
		Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(2, false);
		container.setLayout(layout);

		// select the task to check
		Label labelTask = new Label(container, SWT.NONE);
		labelTask.setText("Select the Task:");
		taskCombo = new ComboViewer(container, SWT.READ_ONLY);
		taskCombo.setContentProvider(ArrayContentProvider.getInstance());
		taskCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Task) {
					return ((Task) element).getName();
				}
				return super.getText(element);
			}
		});

		// add all UML tasks
		List<Task> taskList;
		Optional<TaskCategory> optUmlTaskCategory = Activator.getManager().getDataServer().getTaskCategories().stream().filter(c -> InnoplusConstants.TASK_UML.equals(c.getName())).findAny();
		if (optUmlTaskCategory.isPresent()) {
			TaskCategory javaTaskCategory = optUmlTaskCategory.get();
			taskList = Activator.getManager().getTasks().stream().filter(t -> t.getCategory() == javaTaskCategory).collect(toList());
		} else {
			taskList = Collections.emptyList();
		}
		taskCombo.setInput(taskList);



		// show files to submit
		Label labelFilesHint = new Label(container, SWT.NONE);
		labelFilesHint.setText("Submitted files:");

		var fileList = new ListViewer(container);
		fileList.add(umlFile);
		if (notationFile != null)
			fileList.add(notationFile);

		if (rememberTask != null && taskList.contains(rememberTask)) {
			taskCombo.setSelection(new StructuredSelection(rememberTask));
		} else {
			taskCombo.setSelection(null);
		}

		return area;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		task = (Task) taskCombo.getStructuredSelection().getFirstElement();

		// remember some settings
		if (task != null) {
			rememberTask = task;
		}

		super.okPressed();
	}

	public Task getTask() {
		return task;
	}
}
