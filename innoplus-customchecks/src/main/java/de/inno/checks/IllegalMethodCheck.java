package de.inno.checks;

import com.puppycrawl.tools.checkstyle.api.AbstractCheck;
import com.puppycrawl.tools.checkstyle.api.DetailAST;
import com.puppycrawl.tools.checkstyle.api.TokenTypes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class IllegalMethodCheck extends AbstractCheck {

    public void setIllegalMethods(String[] illegalMethods) {
        Arrays.stream(illegalMethods).forEach(this.illegalMethods::add);
    }

    public void setCustomMessages(String[] customMessages){
        // Sanity check, we need tuples of method / message pairs
        if (customMessages.length == 0 || customMessages.length % 2 != 0)
            throw new IllegalStateException("customMessages should be a list of the form [methodName0, errorMessage0, " +
                    "methodName1, errorMessage1, ..");

        // TODO we kind of need to enforce the order of operations here
        for (int i = 0; i < customMessages.length; i+=2) {
            this.customMessages.put(customMessages[i], customMessages[i+1]);
        }
    }

    private final Map<String, String> customMessages = new HashMap<>();

    private final HashSet<String> illegalMethods = new HashSet();
    public IllegalMethodCheck(){
    }

    @Override
    public int[] getDefaultTokens() {
        return getRequiredTokens();
    }

    @Override
    public int[] getAcceptableTokens() {
        return getRequiredTokens();
    }

    @Override
    public int[] getRequiredTokens() {
        return new int[] {TokenTypes.METHOD_CALL };
    }


    @Override
    public void visitToken(DetailAST ast)
    {
        do{
            ast = ast.getFirstChild();
        }while (ast != null && ast.getFirstChild() != null);
        if (ast.getNextSibling() != null){
            ast = ast.getNextSibling();
            if (!illegalMethods.isEmpty() &&
                    illegalMethods.contains(ast.getText())){
                var errorText = customMessages.getOrDefault(ast.getText(),
                        "You should not use " + ast.getText() + " in this context.");
                log(ast.getLineNo(), ast.getColumnNo(), errorText);
            }
        }
    }
}
