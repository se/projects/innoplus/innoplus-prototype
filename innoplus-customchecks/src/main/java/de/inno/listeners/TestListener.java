package de.inno.listeners;

import com.puppycrawl.tools.checkstyle.api.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class TestListener extends AutomaticBean implements AuditListener {

    private PrintWriter mWriter = new PrintWriter(System.out);
    private boolean mCloseOut = false;
    private int mTotalErrors;
    private int mErrors;

    public void setFile(String aFileName) throws FileNotFoundException
    {
        final OutputStream out = new FileOutputStream(aFileName);
        mWriter = new PrintWriter(out);
        mCloseOut = true;
    }

    @Override
    public void auditStarted(AuditEvent aEvt)
    {
        mTotalErrors = 0;
        mWriter.println("Audit started.");
    }

    @Override
    public void auditFinished(AuditEvent aEvt)
    {
        mWriter.println("Audit finished. Total errors: " + mTotalErrors);
        mWriter.flush();
        if (mCloseOut) {
            mWriter.close();
        }
    }

    @Override
    public void fileStarted(AuditEvent aEvt)
    {
        mErrors = 0;
        mWriter.println(
                "Started checking file '" + aEvt.getFileName() + "'.");
    }

    @Override
    public void fileFinished(AuditEvent aEvt)
    {
        mWriter.println("Finished checking file '" + aEvt.getFileName()
                + "'. Errors: " + mErrors);
    }

    @Override
    public void addError(AuditEvent aEvt)
    {
        printEvent(aEvt);
        if (SeverityLevel.ERROR.equals(aEvt.getSeverityLevel())) {
            mErrors++;
            mTotalErrors++;
        }
    }

    @Override
    public void addException(AuditEvent aEvt, Throwable aThrowable)
    {
        printEvent(aEvt);
        aThrowable.printStackTrace(System.out);
        mErrors++;
        mTotalErrors++;
    }

    private void printEvent(AuditEvent aEvt)
    {
        mWriter.println("Logging error -"
                + " file: '" + aEvt.getFileName() + "'"
                + " line: " + aEvt.getLine()
                + " column: " + aEvt.getColumn()
                + " severity: " + aEvt.getSeverityLevel()
                + " message: " + aEvt.getMessage()
                + " source: " + aEvt.getSourceName());
    }

    @Override
    protected void finishLocalSetup() throws CheckstyleException {

    }
}
