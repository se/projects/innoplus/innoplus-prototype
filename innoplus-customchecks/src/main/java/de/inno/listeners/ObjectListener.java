package de.inno.listeners;

import com.puppycrawl.tools.checkstyle.AstTreeStringPrinter;
import com.puppycrawl.tools.checkstyle.JavaParser;
import com.puppycrawl.tools.checkstyle.api.AuditEvent;
import com.puppycrawl.tools.checkstyle.api.AuditListener;
import com.puppycrawl.tools.checkstyle.api.AutomaticBean;
import com.puppycrawl.tools.checkstyle.api.CheckstyleException;
import de.unioldenburg.se.innoplus.communication.CheckstyleError;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ObjectListener extends AutomaticBean implements AuditListener {

    /** List to store Errors */
    private List<CheckstyleError> errors = new ArrayList<>();

    /** default Filename **/
    private String filename = "feedback";

    /** We need a Setter to enable changing the filename via the Checkstyle config*/
    public void setFilename(String filename) {
        this.filename = filename;
    }


    @Override
    protected void finishLocalSetup() throws CheckstyleException {
    }

    @Override
    public void auditStarted(AuditEvent event) {
    }

    @Override
    public void auditFinished(AuditEvent event) {
        var file = Paths.get(".", filename).toFile();
        try (var outStream = new ObjectOutputStream(new FileOutputStream(file))){
            outStream.writeObject(errors);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fileStarted(AuditEvent event) {
        var file = Paths.get(event.getFileName()).toFile();
        try {
            System.out.println(AstTreeStringPrinter.printFileAst(file, JavaParser.Options.WITHOUT_COMMENTS ));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fileFinished(AuditEvent event) {
    }

    @Override
    public void addError(AuditEvent event) {
        errors.add(
                new CheckstyleError(
                        Paths.get(event.getFileName()).getFileName().toString(),
                        event.getLine(),
                        event.getColumn(),
                        event.getSeverityLevel().getName(),
                        event.getMessage())
        );
    }

    @Override
    public void addException(AuditEvent event, Throwable throwable) {

    }

}
