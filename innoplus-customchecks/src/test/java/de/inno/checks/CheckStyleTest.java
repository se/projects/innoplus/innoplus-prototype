package de.inno.checks;

import com.puppycrawl.tools.checkstyle.DefaultConfiguration;
import com.puppycrawl.tools.checkstyle.TreeWalker;
import com.puppycrawl.tools.checkstyle.api.Configuration;

import java.nio.charset.StandardCharsets;

public abstract class CheckStyleTest {

    /**
     * This methods creates a Configuration, that mirrors the xml structure. We have a base module,
     * a treewalker module inside and inside the treewalker module we have our check
     */
    protected static DefaultConfiguration wrap(Configuration config){
        var dc = new DefaultConfiguration("configuration");
        var twConfig = createModuleConfig(TreeWalker.class);

        dc.addAttribute("charset", StandardCharsets.UTF_8.name());
        dc.addChild(twConfig);
        twConfig.addChild(config);

        return dc;
    }

    protected static DefaultConfiguration createModuleConfig(Class<?> clazz) {
        return new DefaultConfiguration(clazz.getName());
    }
}
