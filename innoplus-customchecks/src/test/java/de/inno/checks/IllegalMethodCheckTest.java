package de.inno.checks;

import com.puppycrawl.tools.checkstyle.Checker;
import de.inno.listeners.ObjectListener;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

class IllegalMethodCheckTest extends CheckStyleTest{

    @Test
    public void testRun() throws Exception {

        // create the config for our Custom Check
        var config = createModuleConfig(IllegalMethodCheck.class);
        config.addAttribute("illegalMethods", "notify");

        // We need to wrap our config in a Treewalker Config.
        config = wrap(config);

        var checker = new Checker();
        checker.setModuleClassLoader(Thread.currentThread().getContextClassLoader());
        // add listeners
        //checker.addListener(new de.inno.listeners.TestListener());
        checker.addListener(new ObjectListener());
        checker.configure(config);

        // Files we want to check
        var solutionPath = Paths.get(Thread.currentThread().getContextClassLoader().getResource("student_solutions").toURI());
        var files = Files.walk(solutionPath)
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());
        files.forEach(System.out::println);

        checker.process(files);
    }

}