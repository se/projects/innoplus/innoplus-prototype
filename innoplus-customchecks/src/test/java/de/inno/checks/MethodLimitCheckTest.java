package de.inno.checks;


import com.puppycrawl.tools.checkstyle.Checker;
import de.inno.listeners.ObjectListener;
import de.unioldenburg.se.innoplus.communication.CheckstyleError;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

class MethodLimitCheckTest extends CheckStyleTest{

    @Test
    public void testRun() throws Exception {
        // create the config for our Custom Check
        var config = createModuleConfig(MethodLimitCheck.class);
        config.addAttribute("max", "1");

        // We need to wrap our config in a Treewalker Config.
        config = wrap(config);

        var checker = new Checker();
        checker.setModuleClassLoader(Thread.currentThread().getContextClassLoader());
        // add listeners
        // checker.addListener(new TestListener());
        checker.addListener(new ObjectListener());
        checker.configure(config);

        // Files we want to check
        var solutionPath = Paths.get(Thread.currentThread().getContextClassLoader().getResource("student_solutions").toURI());
        var files = Files.walk(solutionPath)
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());
        files.forEach(System.out::println);

        checker.process(files);

        checker.destroy();

        try (var inStream = new ObjectInputStream(new FileInputStream(Paths.get(".", "feedback").toFile()))){
            List<CheckstyleError> in = (List<CheckstyleError>) inStream.readObject();
            in.forEach(System.out::println);
        }
    }


}