public class Aufgabe3 {

	// Optimal valid solution
	public static class Star {
		private String name, id, type;
		private double distance, apparentMagnitude;

		public Star() {
		}

		public Star(String name, String id) {
			this.name = name;
			this.id = id;
		}

		public Star(String name, String id, String type, double distance, double apparentMagnitude) {
			this.name = name;
			this.id = id;
			this.type = type;
			this.distance = distance;
			this.apparentMagnitude = apparentMagnitude;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public double getDistance() {
			return distance;
		}

		public void setDistance(double distance) {
			this.distance = distance;
		}

		public double getApparentMagnitude() {
			return apparentMagnitude;
		}

		public void setApparentMagnitude(double apparentMagnitude) {
			this.apparentMagnitude = apparentMagnitude;
		}
	}

	public static class StarDatabase {

		private Star[] stars = new Star[0];

		/** Fügt einen neuen Stern hinzu **/
		public void add(Star star) {
			Star[] res = new Star[this.stars.length + 1];
			for (int i = 0; i < this.stars.length; i++)
				res[i] = stars[i];

			res[stars.length] = star;
			this.stars = res;
		}

		/** Entfernt den Stern am gegebenen Index **/
		public void remove(int index) {
			Star[] res = new Star[this.stars.length - 1];

			for (int i = 0, cur = 0; i < this.stars.length; i++) {
				if (i != index)
					res[cur++] = stars[i];
			}

			this.stars = res;
		}

		/** Liefert den Stern am gegebenen Index **/
		public Star get(int index) {
			return this.stars[index];
		}

		/** Liefert die Anzahl der Sterne **/
		public int size() {
			return this.stars.length;
		}

		/** Findet den Stern mit gegebener ID (wenn vorhanden, ansonsten null) **/
		public Star find(String id) {
			for (Star star : this.stars) {
				if (star.getId().equals(id))
					return star;
			}
			return null;
		}

		/** Liefert alle Sterne mit eine scheinbaren Helligkeit in [low, high] **/
		public Star[] getMagnitudeRange(double low, double high) {
			int count = 0;

			for (Star star : this.stars) {
				double mag = star.getApparentMagnitude();
				if (low <= mag && mag <= high)
					count++;
			}

			Star[] result = new Star[count];
			count = 0;

			for (Star star : this.stars) {
				double mag = star.getApparentMagnitude();
				if (low <= mag && mag <= high)
					result[count++] = star;
			}

			return result;
		}
	}

}
