ssl_session_timeout       5m;
ssl_protocols             TLSv1.2;
ssl_ciphers               HIGH:!aNULL:!MD5;
ssl_prefer_server_ciphers on;
ssl_session_cache         shared:SSL:10m;
