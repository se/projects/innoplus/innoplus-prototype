# Innovation+ - Services

Dieser Ordner definiert alle Services, die das Backend für Innovation+
ausmachen. Nähere Informationen zu den einzelnen Services befinden sich
in der Konfigurationsdatei `docker-compose.yml`.
